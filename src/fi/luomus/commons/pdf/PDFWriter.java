package fi.luomus.commons.pdf;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Writes single PDF files and also possibly a collection of the PDF files that have been generated so far.
 * So for example if you call writePdf() three times and after that createCollection(), createCollection will return
 * the three previouly generated files combined into one PDF file.
 * @author eopiirai
 *
 */
public interface PDFWriter {
	
	public static final String	SELECTED_CHECKBOX	= "Yes";
	
	/**
	 * Fills a PDF template with given data. Values of the data are put to form fields of the
	 * template that match with the keys of the values. If a key from the data is not found from 
	 * the template form, nothing is done.
	 * @param templatename name of the template 
	 * @param data 
	 * @param filename name of the file
	 * @return produced file
	 */
	public File writePdf(String templatename, Map<String, String> data, String filename);
	
	public File createCollection(String collectionFilename);
	
	public List<String> producedFiles();
	
	public List<String> noticeTexts();
	
}
