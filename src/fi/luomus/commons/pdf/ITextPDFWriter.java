package fi.luomus.commons.pdf;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfCopyFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import fi.luomus.commons.utils.FileUtils;
import fi.luomus.commons.utils.Utils;

/**
 * Implementation of _PDFWriter using iText PDF: a AGPL Java-PDF library.
 * http://itextpdf.com/
 * http://www.gnu.org/licenses/agpl.txt
 */
public class ITextPDFWriter implements PDFWriter {

	public static final Collection<String> UNICODE_FONTS = Utils.collection("ARIAL.TTF", "ARIALBD.TTF", "ARIALBI.TTF", "ARIALI.TTF", "ARIALUNI.TTF");

	private static final String	TEXTSIZE	= "textsize";
	private final List<String>	producedFiles;
	private final List<String>	noticeTexts;
	private final String		templatefolder;
	private final String		outputfolder;
	private final String		fontfolder;

	public ITextPDFWriter(String templatefolder, String outputfolder, String fontFolder) {
		producedFiles = new ArrayList<>();
		noticeTexts = new ArrayList<>();
		this.templatefolder = templatefolder;
		this.outputfolder = outputfolder;
		this.fontfolder = fontFolder;
	}

	public ITextPDFWriter(String templatefolder, String outputfolder) {
		this(templatefolder, outputfolder, null);
	}

	@Override
	public File writePdf(String templatename, Map<String, String> data, String filename) {
		filename = handleFilename(filename);
		File templateFile = new File(templatefolder + File.separator + templatename + ".pdf");
		File outputFile = new File(outputfolder + File.separator + filename);
		FileInputStream streamIn = null;
		FileOutputStream streamOut = null;

		try {
			outputFile.getParentFile().mkdirs();
			streamIn = new FileInputStream(templateFile);
			streamOut = new FileOutputStream(outputFile);
			PdfReader reader = new PdfReader(streamIn);
			PdfStamper stamp = new PdfStamper(reader, streamOut);
			AcroFields form = stamp.getAcroFields();

			if (fontfolder != null) {
				ArrayList<BaseFont> substitutionFonts = new ArrayList<>();
				for (String font : UNICODE_FONTS) {
					substitutionFonts.add(BaseFont.createFont(fontfolder + File.separator + font, BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
				}
				form.setSubstitutionFonts(substitutionFonts);
			}

			writeIndividualPdf(data, filename, form);

			stamp.setFormFlattening(true);
			stamp.close();
			producedFiles.add(filename);
		} catch (Exception e) {
			noticeTexts.add("ERROR: " + filename +": " + e);
			return null;
		} finally {
			FileUtils.close(streamIn, streamOut);
		}

		return outputFile;
	}

	private void writeIndividualPdf(Map<String, String> data, String filename, AcroFields form) {
		for (Map.Entry<String, String> entry : data.entrySet()) {
			try {
				String fieldName = entry.getKey();
				String text = entry.getValue();
				if (text == null || text.length() < 1) continue;
				int lineCount = fi.luomus.commons.utils.Utils.countNumberOf("\n", text);
				if (lineCount > 8) {
					form.setFieldProperty(fieldName, TEXTSIZE, Float.valueOf(6), null);
				}
				form.setField(fieldName, text);
			} catch (Exception e) {
				noticeTexts.add("ERROR: " + filename + ".pdf : " + entry.getKey() + " : " + entry.getValue() + " : " + e.getMessage() +  " " +e.getStackTrace()[0]);
			}
		}
	}



	@Override
	public File createCollection(String collectionFilename) {
		if (producedFiles.size() < 1) return null;

		collectionFilename = handleFilename(collectionFilename);

		File outputFile = new File(outputfolder + File.separator + collectionFilename);
		FileOutputStream streamOut = null;
		try {
			streamOut = new FileOutputStream(outputFile);
			writeCollection(collectionFilename, streamOut);
			producedFiles.add(collectionFilename);
		} catch (Exception e) {
			noticeTexts.add("ERROR: " + collectionFilename + " : " + e);
			return null;
		} finally {
			FileUtils.close(streamOut);
		}
		return outputFile;
	}

	private void writeCollection(String collectionFilename, FileOutputStream streamOut) throws DocumentException {
		PdfCopyFields collection = new PdfCopyFields(streamOut);
		for (String producedFile : producedFiles) {
			FileInputStream streamIn = null;
			try {
				streamIn = new FileInputStream(outputfolder + File.separator + producedFile);
				PdfReader reader = new PdfReader(streamIn);
				collection.addDocument(reader);
			} catch (Exception e) {
				noticeTexts.add("ERROR: could not add " + producedFile + " to " + collectionFilename + " : " + e);
			} finally {
				FileUtils.close(streamIn);
			}
		}
		collection.close();
	}

	@Override
	public List<String> noticeTexts() {
		return noticeTexts;
	}

	@Override
	public List<String> producedFiles() {
		return producedFiles;
	}

	private final static Map<Character, Character> SCANDS = initScands();
	private final static Collection<Character> CHARS = initAlphas();

	private static Collection<Character> initAlphas() {
		Collection<Character> chars = new HashSet<>();
		String alphas = "abcdefghijklmnopqrstuvwxyz0123456789_-.";
		for (char c : alphas.toCharArray()) {
			chars.add(c);
			chars.add(Character.toUpperCase(c));
		}
		return chars;
	}

	private static Map<Character, Character> initScands() {
		Map<Character, Character> map = new HashMap<>();
		map.put('Ä', 'A');
		map.put('ä', 'a');
		map.put('Ö', 'O');
		map.put('ö', 'o');
		map.put('Å', 'A');
		map.put('å', 'a');
		return map;
	}
	
	private String handleFilename(String filename) {
		if (filename == null) return "null.pdf";
		filename = filename.trim();
		StringBuilder handledName = new StringBuilder();
		for (char c : filename.toCharArray()) {
			if (isAllowed(c)) {
				handledName.append(c);
			} else if (isScand(c)) {
				handledName.append(toNonScand(c));
			}
		}
		if (!filename.toLowerCase().endsWith(".pdf")) {
			handledName.append(".pdf");
		}
		return handledName.toString();
	}

	private Character toNonScand(char c) {
		return SCANDS.get(c);
	}

	private boolean isAllowed(char c) {
		return CHARS.contains(c);
	}

	private boolean isScand(char c) {
		return SCANDS.containsKey(c);
	}

}
