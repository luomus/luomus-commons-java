package fi.luomus.commons.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import fi.luomus.commons.db.connectivity.ConnectionDescription;

public class ConfigReader implements Config {

	private final HashMap<String, String> properties;

	public ConfigReader(InputStream stream) throws FileNotFoundException {
		properties = new HashMap<>();
		try {
			Properties p = new Properties();
			p.load(stream);
			for (Map.Entry<Object, Object> e : p.entrySet()) {
				properties.put(e.getKey().toString().trim(), e.getValue().toString().trim());
			}
		} catch (Exception e) {
			throw new FileNotFoundException();
		}
		finally {
			if (stream != null) try { stream.close(); } catch (IOException e) { }
		}
	}

	public ConfigReader(String filename) throws FileNotFoundException {
		this(new FileInputStream(filename));
	}

	@Override
	public boolean defines(String property) {
		String p = properties.get(property);
		if (p == null) return false;
		return true;
	}

	@Override
	public String get(String property) throws IllegalArgumentException {
		String p = properties.get(property);
		if (p == null) throw new IllegalArgumentException("Unknown property " + property);
		return p;
	}

	@Override
	public int getInteger(String property) throws IllegalArgumentException, NumberFormatException {
		return Integer.valueOf(get(property));
	}

	@Override
	public ConnectionDescription connectionDescription() throws UnsupportedOperationException {
		String driver = tryToGet(DB_DRIVER);
		String url = tryToGet(DB_URL);
		String user = tryToGet(DB_USERNAME);
		String passwd = tryToGet(DB_PASSWORD);
		return new ConnectionDescription(driver, url, user, passwd);
	}

	@Override
	public ConnectionDescription connectionDescription(String systemName) throws UnsupportedOperationException {
		String driver = tryToGet(systemName + "_" + DB_DRIVER);
		String url = tryToGet(systemName + "_" + DB_URL);
		String user = tryToGet(systemName + "_" + DB_USERNAME);
		String passwd = tryToGet(systemName + "_" + DB_PASSWORD);
		return new ConnectionDescription(driver, url, user, passwd);
	}

	private String tryToGet(String property) {
		String s;
		try {
			s = get(property);
		} catch (Exception e) {
			throw new UnsupportedOperationException(property + " not found");
		}
		return s;
	}

	@Override
	public String baseFolder() throws UnsupportedOperationException {
		return tryToGet(BASE_FOLDER);
	}

	@Override
	public String baseURL() throws UnsupportedOperationException {
		return tryToGet(BASE_URL);
	}

	@Override
	public String commonURL() throws UnsupportedOperationException {
		return tryToGet(COMMON_URL);
	}

	@Override
	public String languagefileFolder() throws UnsupportedOperationException {
		return baseFolder() + tryToGet(LANGUAGE_FILE_FOLDER);
	}

	@Override
	public String templateFolder() throws UnsupportedOperationException {
		return baseFolder() + tryToGet(TEMPLATE_FOLDER);
	}

	@Override
	public String defaultLanguage() throws UnsupportedOperationException {
		return tryToGet(DEFAULT_LANGUAGE);
	}

	@Override
	public boolean parameterLogging() {
		if (!this.defines(PARAMETER_LOGGING)) return false;
		String value = get(PARAMETER_LOGGING);
		if (YES.equalsIgnoreCase(value)) return true;
		return false;
	}

	@Override
	public String logFolder() throws UnsupportedOperationException {
		return baseFolder() + tryToGet(LOG_FOLDER);
	}

	@Override
	public String reportFolder() throws UnsupportedOperationException {
		return baseFolder() + tryToGet(REPORT_FOLDER);
	}

	@Override
	public String pdfFolder() throws UnsupportedOperationException {
		return baseFolder() + tryToGet(PDF_FOLDER);
	}

	@Override
	public String fontFolder() throws UnsupportedOperationException {
		return baseFolder() + tryToGet(FONT_FOLDER);
	}

	@Override
	public String dataFolder() throws UnsupportedOperationException {
		return baseFolder() + tryToGet(DATA_FOLDER);
	}
	
	@Override
	public String kirjekyyhkyFolder() throws UnsupportedOperationException {
		return baseFolder() + tryToGet(KIRJEKYYHKY_FOLDER);
	}

	@Override
	public String systemId() throws UnsupportedOperationException {
		return tryToGet(SYSTEM_ID);
	}

	@Override
	public boolean developmentMode() {
		if (!this.defines(DEVELOPMENT_MODE)) return true;
		String value = get(DEVELOPMENT_MODE);
		if (NO.equalsIgnoreCase(value)) return false;
		return true;
	}

	@Override
	public boolean stagingMode() {
		if (!this.defines(STAGING_MODE)) return false;
		String value = get(STAGING_MODE);
		if (YES.equalsIgnoreCase(value)) return true;
		return false;
	}

	@Override
	public boolean productionMode() {
		return !developmentMode() && !stagingMode();
	}

	@Override
	public String characterEncoding() {
		if (this.defines(CHARACTER_ENCODING)) return this.get(CHARACTER_ENCODING);
		return "UTF-8";
	}

	@Override
	public Collection<String> supportedLanguages() throws UnsupportedOperationException {
		Collection<String> supportedLanguages = new HashSet<>(5);
		for (String lang : get(SUPPORTED_LANGUAGES).split(",")) {
			lang = lang.toUpperCase().trim();
			if (lang.length() < 1) continue;
			supportedLanguages.add(lang);
		}
		return supportedLanguages;
	}

	@Override
	public String languageFilePrefix() throws UnsupportedOperationException {
		return tryToGet(LANGUAGE_FILE_PREFIX);
	}

	@Override
	public String staticURL() throws UnsupportedOperationException {
		return tryToGet(Config.STATIC_URL);
	}

	@Override
	public List<String> languageFiles() throws UnsupportedOperationException {
		List<String> fileNames = new ArrayList<>();
		String files = tryToGet(LANGUAGE_FILES);
		for (String fileName : files.split(",")) {
			fileName = fileName.trim();
			if (fileName.length() < 1) continue;
			if (!fileName.endsWith(".")) {
				fileName += ".";
			}
			fileNames.add(fileName);
		}
		return fileNames;
	}

}
