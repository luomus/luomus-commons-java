package fi.luomus.commons.config;

import java.util.Collection;
import java.util.List;

import fi.luomus.commons.db.connectivity.ConnectionDescription;

/**
 * Contains application defined configuration
 */
public interface Config {
	
	public static final String	YES								= "YES";
	public static final String	NO								= "NO";
	
	public static final String	DEVELOPMENT_MODE				= "DevelopmentMode";
	public static final String	STAGING_MODE					= "StagingMode";
	public static final String	CHARACTER_ENCODING				= "CharacterEncoding";
	public static final String	SYSTEM_ID						= "SystemID";
	public static final String	DB_DRIVER						= "DBdriver";
	public static final String	DB_URL							= "DBurl";
	public static final String	DB_USERNAME						= "DBusername";
	public static final String	DB_PASSWORD						= "DBpassword";
	public static final String	BASE_FOLDER						= "BaseFolder";
	public static final String	TEMPLATE_FOLDER					= "TemplateFolder";
	public static final String	LANGUAGE_FILE_FOLDER			= "LanguageFileFolder";
	public static final String	LANGUAGE_FILE_PREFIX			= "LanguageFilePrefix";
	public static final String	LANGUAGE_FILES					= "LanguageFiles";
	public static final String	SUPPORTED_LANGUAGES				= "SupportedLanguages";
	public static final String	DEFAULT_LANGUAGE				= "DefaultLanguage";
	public static final String	BASE_URL						= "BaseURL";
	public static final String	COMMON_URL						= "CommonURL";
	public static final String	STATIC_URL						= "StaticURL";
	public static final String	PARAMETER_LOGGING				= "ParameterLogging";
	public static final String	PARAMETER_LOG_FILE_PREFIX		= "ParameterLogFilePrefix";
	public static final String	LOG_FOLDER						= "LogFolder";
	public static final String	REPORT_FOLDER					= "ReportFolder";
	public static final String	PDF_FOLDER						= "PDFFolder";
	public static final String	FONT_FOLDER						= "FontFolder";
	public static final String  DATA_FOLDER                     = "DataFolder";
	public static final String	KIRJEKYYHKY_FOLDER				= "KirjekyyhkyFolder";
	public static final String  KIRJEKYYHKY_API_URI				= "KirjekyyhkyAPI_URI";
	public static final String  KIRJEKYYHKY_API_USERNAME		= "KirjekyyhkyAPI_Username";
	public static final String  KIRJEKYYHKY_API_PASSWORD		= "KirjekyyhkyAPI_Password";
	public static final String  TIPU_API_URI					= "TipuAPI_URI";
	public static final String  TIPU_API_USERNAME				= "TipuAPI_Username";
	public static final String  TIPU_API_PASSWORD				= "TipuAPI_Password";
	public static final String	DATABASE_LOG_FILE_PREFIX		= "DatabaseLogFilePrefix";
	public static final String	ERROR_REPORTING_SMTP_HOST		= "ErrorReporting_SMTP_Host";
	public static final String	ERROR_REPORTING_SMTP_USERNAME	= "ErrorReporting_SMTP_Username";
	public static final String	ERROR_REPORTING_SMTP_PASSWORD	= "ErrorReporting_SMTP_Password";
	public static final String	ERROR_REPORTING_SMTP_SEND_TO	= "ErrorReporting_SMTP_SendTo";
	public static final String	ERROR_REPORTING_SMTP_SEND_FROM	= "ErrorReporting_SMTP_SendFrom";
	public static final String	ERROR_REPORTING_SMTP_SUBJECT	= "ErrorReporting_SMTP_Subject";
	public static final String	LINTUVAARA_URL					= "LintuvaaraURL";
	public static final String	LINTUVAARA_PUBLIC_RSA_KEY		= "LintuvaaraPubRSAKey";
	public static final String	MAX_RESULT_COUNT				= "MaxResultCount";
	
	/**
	 * Tells if this log defines (contains) a property with the given name
	 * @param property
	 * @return
	 */
	public boolean defines(String property);
	
	/**
	 * Returns the value of a property
	 * @param property
	 * @return
	 * @throws IllegalArgumentException if value not defined
	 */
	public String get(String property) throws IllegalArgumentException;
	
	/**
	 * Returns the value of a property as int
	 * @param property
	 * @return
	 * @throws IllegalArgumentException
	 * @throws NumberFormatException
	 */
	public int getInteger(String property) throws IllegalArgumentException, NumberFormatException;
	
	/**
	 * Is system in development mode
	 * @return true if in development mode, false if in other mode
	 */
	public boolean developmentMode();
	
	/**
	 * Is system in staging mode
	 * @return true if in staging mode, false if in other mode
	 */
	public boolean stagingMode();
	
	/**
	 * Is system in production mode
	 * @return true if in production mode, false if in other mode
	 */
	public boolean productionMode();
	
	public String characterEncoding();
	
	public String systemId() throws UnsupportedOperationException;
	
	public String baseFolder() throws UnsupportedOperationException;
	
	public String baseURL() throws UnsupportedOperationException;
	
	public String commonURL() throws UnsupportedOperationException;
	
	public String templateFolder() throws UnsupportedOperationException;
	
	public String languagefileFolder() throws UnsupportedOperationException;
	
	public String defaultLanguage() throws UnsupportedOperationException;
	
	public ConnectionDescription connectionDescription() throws UnsupportedOperationException;
	
	public ConnectionDescription connectionDescription(String systemName) throws UnsupportedOperationException;
	
	public boolean parameterLogging();
	
	public String logFolder() throws UnsupportedOperationException;
	
	public String reportFolder() throws UnsupportedOperationException;
	
	public String pdfFolder() throws UnsupportedOperationException;
	
	public String fontFolder() throws UnsupportedOperationException;
	
	public String dataFolder() throws UnsupportedOperationException;
	
	public String kirjekyyhkyFolder() throws UnsupportedOperationException;
	
	public Collection<String> supportedLanguages() throws UnsupportedOperationException;
	
	public String languageFilePrefix() throws UnsupportedOperationException;

	public List<String> languageFiles() throws UnsupportedOperationException;
	
	public String staticURL() throws UnsupportedOperationException;
	
}
