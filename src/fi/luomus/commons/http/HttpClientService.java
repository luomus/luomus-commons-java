package fi.luomus.commons.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.XMLReader;

/**
 * A wrapper class that uses org.apache.http.client.HttpClient.
 */
public class HttpClientService implements AutoCloseable {

	public static class NotFoundException extends IOException {
		private static final long serialVersionUID = -624847992184150496L;
		public NotFoundException(String message) {
			super(message);
		}
	}

	private final CloseableHttpClient httpclient;

	/**
	 * Returns a new DefaultHttpClient
	 */
	public HttpClientService() {
		HttpClientBuilder builder = getBuilderWithSSLTrustSelfSigned();
		this.httpclient = builder.build();
	}

	private HttpClientBuilder getBuilderWithSSLTrustSelfSigned() {
		try {
			HttpClientBuilder builder = HttpClientBuilder.create();
			SSLContext sslContext = SSLContexts.custom()
					.loadTrustMaterial(new TrustSelfSignedStrategy()) // Trust self-signed certs
					.build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
					sslContext, NoopHostnameVerifier.INSTANCE); // NoopHostnameVerifier replaces AllowAllHostnameVerifier
			builder.setSSLSocketFactory(sslsf);
			return builder;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns a new HttpClient for site that requires HTTPBasic Authentication
	 * @param address
	 * @param username
	 * @param password
	 * @throws URISyntaxException
	 */
	public HttpClientService(String address, String username, String password) throws URISyntaxException {
		HttpClientBuilder builder = getBuilderWithSSLTrustSelfSigned();
		setAuthenticationCredentials(address, username, password, builder);
		this.httpclient = builder.build();
	}

	private void setAuthenticationCredentials(String address, String username, String password, HttpClientBuilder builder) throws URISyntaxException {
		URI uri = new URI(address);
		UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);
		CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort()), creds);
		builder.setDefaultCredentialsProvider(credentialsProvider);

		// Enable Preemptive Authentication
		HttpHost targetHost = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
		AuthCache authCache = new BasicAuthCache();
		authCache.put(targetHost, new BasicScheme());

		HttpClientContext context = HttpClientContext.create();
		context.setAuthCache(authCache);

		builder.addInterceptorFirst((HttpRequestInterceptor) (request, context1) -> {
			context1.setAttribute(HttpClientContext.AUTH_CACHE, authCache);
		});
	}

	public HttpClientService(String authorizationHeader) {
		HttpClientBuilder builder = getBuilderWithSSLTrustSelfSigned();
		builder.addInterceptorFirst(new HttpRequestInterceptor() {
			@Override
			public void process(HttpRequest req, HttpContext context) throws HttpException, IOException {
				req.addHeader(HttpHeaders.AUTHORIZATION, authorizationHeader);

			}
		});
		this.httpclient = builder.build();
	}

	@Override
	public void close() {
		try {
			httpclient.close();
		} catch (IOException e) {
			throw new RuntimeException("Closing http client: " + e.getMessage(), e);
		}
	}

	public void setParams(HttpPost request, Param ... params) {
		setParamsPostOrPut(request, params);
	}

	public void setParams(HttpPut request, Param ... params) {
		setParamsPostOrPut(request, params);
	}

	private void setParamsPostOrPut(HttpEntityEnclosingRequestBase request, Param ... params) {
		List<NameValuePair> nameValuePairs = new ArrayList<>();
		for (Param param : params) {
			nameValuePairs.add(new BasicNameValuePair(param.getKey(), param.getValue()));
		}
		try {
			request.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * Returns response to request as a XML Document.
	 * @see Document
	 * @param request
	 * @return
	 * @throws NotFoundException if 404 response is given
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public Document contentAsDocument(HttpUriRequest request) throws NotFoundException, IOException, ClientProtocolException {
		String response = contentAsString(request);
		XMLReader reader = new XMLReader();
		Document doc = reader.parse(response);
		return doc;
	}

	/**
	 * Returns response to request as a JSON Object
	 * @param request
	 * @return
	 * @throws NotFoundException if 404 response is given
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public JSONObject contentAsJson(HttpUriRequest request) throws NotFoundException, ClientProtocolException, IOException {
		String response = contentAsString(request);
		JSONObject json = new JSONObject(response);
		return json;
	}

	/**
	 * Returns response to request as a JSON Array
	 * @param request
	 * @return
	 * @throws NotFoundException if 404 response is given
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public JSONArray contentAsJsonArray(HttpUriRequest request) throws NotFoundException, ClientProtocolException, IOException {
		String response = contentAsString(request);
		JSONArray json = new JSONArray(response);
		return json;
	}

	/**
	 * Returns response to request as a String.
	 * @param request
	 * @return
	 * @throws NotFoundException if 404 response is given
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public String contentAsString(HttpUriRequest request) throws NotFoundException, IOException, ClientProtocolException {
		ResponseHandler<String> handler = new ResponseHandler<String>() {
			@Override
			public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status != 200) {
					fail(response, status);
				}
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					return EntityUtils.toString(entity, "UTF-8");
				}
				return null;
			}
		};
		return httpclient.execute(request, handler);
	}

	/**
	 * Returns response to request as List of Strings containing lines of the response (separated by newline characters) using UTF-8 encoding
	 * @param request
	 * @return
	 * @throws NotFoundException if 404 response is given
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public List<String> contentAsList(HttpUriRequest request) throws NotFoundException, IOException, ClientProtocolException {
		return contentAsList(request, "UTF-8");
	}

	/**
	 * Returns response to request as List of Strings containing lines of the response (separated by newline characters)
	 * @param request
	 * @param encoding
	 * @return
	 * @throws NotFoundException if 404 response is given
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public List<String> contentAsList(HttpUriRequest request, String encoding) throws NotFoundException, IOException, ClientProtocolException {
		ResponseHandler<List<String>> handler = new ResponseHandler<List<String>>() {
			@Override
			public List<String> handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status != 200) {
					fail(response, status);
				}
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					BufferedReader reader = null;
					try {
						reader = new BufferedReader(new InputStreamReader(entity.getContent(), encoding));
						List<String> lines = new ArrayList<>();
						String line = null;
						while ((line = reader.readLine()) != null) {
							lines.add(line);
						}
						return lines;
					} finally {
						if (reader != null) reader.close();
					}
				}
				return Collections.emptyList();
			}
		};
		return httpclient.execute(request, handler);
	}

	/**
	 * Writes response to the stream given as parameter.
	 * @param request
	 * @param out
	 * @throws NotFoundException if 404 response is given
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public void contentToStream(HttpUriRequest request, final PrintWriter out) throws NotFoundException, ClientProtocolException, IOException {
		ResponseHandler<Void> handler = new ResponseHandler<Void>() {
			@Override
			public Void handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status != 200) {
					fail(response, status);
				}
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					IOUtils.copy(entity.getContent(), out, "UTF-8");
				}
				return null;
			}
		};
		httpclient.execute(request, handler);
	}

	/**
	 * Writes content to stream given as parameter.
	 * @param request
	 * @param out
	 * @throws NotFoundException if 404 response is given
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public void contentToStream(HttpUriRequest request, final OutputStream out) throws NotFoundException, ClientProtocolException, IOException {
		ResponseHandler<Void> handler = new ResponseHandler<Void>() {
			@Override
			public Void handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status != 200) {
					fail(response, status);
				}
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					IOUtils.copy(entity.getContent(), out);
				}
				return null;
			}
		};
		httpclient.execute(request, handler);
	}

	private void fail(HttpResponse response, int status) throws NotFoundException {
		String errorMessage = "";
		try {
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				errorMessage = EntityUtils.toString(entity, "UTF-8");
			}
		} catch (Exception e) {}
		if (status == 404) throw new NotFoundException(errorMessage);
		throw new RuntimeException("Request returned " + status + " with message " + errorMessage);
	}

	/**
	 * Executes a request and returns a HttpResponse.
	 * @ss {@link HttpResponse}
	 * @param request
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public CloseableHttpResponse execute(HttpUriRequest request) throws ClientProtocolException, IOException {
		return httpclient.execute(request);
	}

}
