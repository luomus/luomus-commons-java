package fi.luomus.commons.http;

import javax.servlet.http.HttpServletResponse;

/**
 * Sets HttpRequest status, possible redirect Location header
 */
public class HttpStatus {

	private final HttpServletResponse	res;
	private boolean statusWritten = false;

	public HttpStatus(HttpServletResponse res) {
		this.res = res;
	}

	/**
	 * Redirects the request to the uri with status 302
	 * @param uri
	 */
	public void redirectTo(String uri) {
		redirectTo(uri, 302);
	}

	public void redirectTo(String uri, int status) {
		res.setHeader("Location", uri);
		status(status);
	}

	/**
	 * Redirects the request to the uri with status 303
	 * @param uri
	 */
	public void redirectTo303(String uri) {
		res.setHeader("Location", uri);
		status(303);
	}

	/**
	 * Status 403
	 */
	public void status403() {
		status(403);
	}

	/**
	 * Status 404
	 */
	public void status404() {
		status(404);
	}

	/**
	 * Status 500
	 */
	public void status500() {
		status(500);
	}

	/**
	 * Set status
	 * @param statusCode
	 */
	public void status(int statusCode) {
		res.setStatus(statusCode);
		res.setHeader("Connection", "close");
		statusWritten = true;
	}

	public boolean isHttpStatusCodeSet() {
		return statusWritten;
	}

}
