package fi.luomus.commons.xml;

import java.io.PrintWriter;
import java.io.StringWriter;

import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document.Attribute;
import fi.luomus.commons.xml.Document.Node;

/**
 * Writes contents of Document object to XML.
 * @see Document
 */
public class XMLWriter {

	public static boolean NO_EMPTY_ELEMENTS = false;
	public static boolean INCLUDE_EMPTY_ELEMENTS = true;

	private final Node rootNode;
	private String encoding;
	private PrintWriter out;
	private int recursionLevel = -1;
	private boolean writeHeader = true;

	public XMLWriter(Document document) {
		this(document.getRootNode());
	}

	public XMLWriter(Node root) {
		this.rootNode = root;
		this.encoding = "utf-8";
	}

	public void writeHeader(boolean value) {
		this.writeHeader = value;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String generateXML(boolean includeEmptyElements) {
		StringWriter writer = new StringWriter();
		PrintWriter out = new PrintWriter(writer);
		generateXML(out, includeEmptyElements);
		return writer.toString();
	}

	public String generateXML() {
		return generateXML(INCLUDE_EMPTY_ELEMENTS);
	}

	public void generateXML(PrintWriter out, boolean includeEmptyElements) {
		this.out = out;
		if (writeHeader) {
			out.append("<?xml version='1.0' encoding='").append(encoding).append("'?>");
			newLine();
		}
		generateXML(rootNode, includeEmptyElements);
	}

	private void newLine() {
		out.append("\n");
	}

	private void generateXML(Node node, boolean includeEmptyElements) {
		recursionLevel++;
		String indentation = getIndentation();
		if (recursionLevel == 1) {
			newLine();
		}

		if (node.hasContents()) {
			addContentNode(node, indentation);
		} else if (node.hasChildNodes()){
			addChildNode(node, includeEmptyElements, indentation);
		} else if (node.hasAttributes() || includeEmptyElements || rootElement()) {
			beginNode(node, indentation);
		}
		newLine();
		recursionLevel--;
	}

	private boolean rootElement() {
		return recursionLevel == 0;
	}

	private void addChildNode(Node node, boolean includeEmptyElements, String indentation) {
		beginNode(node, indentation);
		newLine();
		for (Node n : node.getChildNodes()) {
			generateXML(n, includeEmptyElements);
		}
		out.append(indentation);
		if (rootElement()) {
			newLine();
		}
		endNode(node);
	}

	private void addContentNode(Node node, String indentation) {
		beginNode(node, indentation);
		if (node.contentIsXML()) {
			newLine(); newLine();
			out.append(node.getContents());
		} else if (node.contentIsCDATA()) {
			newLine();
			out.append("<![CDATA[" + node.getContents() + "]]>");
		} else {
			out.append(escape(node.getContents()));
		}
		if (node.contentIsXML() || node.contentIsCDATA()) {
			newLine();
			out.append(indentation);
		}
		endNode(node);
	}

	private String escape(String contents) {
		return Utils.toHTMLEntities(contents).replace("&#39;", "'"); // amp is encoded needlessly
	}

	private void endNode(Node node) {
		out.append("</");
		out.append(node.getName());
		out.append(">");
	}

	private void beginNode(Node node, String indentation) {
		out.append(indentation);
		out.append("<");
		out.append(node.getName());

		for (Attribute attribute : node.getAttributes()) {
			String name = Utils.toHTMLEntities(attribute.getName());
			String value = escape(attribute.getValue());
			out.append(" ").append(name).append("=\"").append(value).append("\"");
		}

		if (!node.hasContents() && !node.hasChildNodes()) {
			out.append(" />");
		} else {
			out.append(">");
		}
	}

	private String getIndentation() {
		String indentation = "";
		for (int i=0; i<recursionLevel; i++) {
			indentation += "\t";
		}
		return indentation;
	}


}
