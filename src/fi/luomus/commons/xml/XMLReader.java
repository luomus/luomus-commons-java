package fi.luomus.commons.xml;

import fi.luomus.commons.xml.Document.Node;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Parses a Document container out of XML
 * @see Document
 */
public class XMLReader {

	public Document parse(String xml) {
		org.w3c.dom.Node domRootNode = getRoot(xml);
		Document d = new Document(domRootNode.getNodeName());
		Node documentRootNode = d.getRootNode();
		parse(domRootNode, documentRootNode);
		return d;
	}
	
	private Element getRoot(String xml) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("Unkown javax.xml parser configuration error", e);
		}
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(xml));
		org.w3c.dom.Document doc;
		try {
			doc = db.parse(is);
		} catch (Exception e) {
			throw new IllegalArgumentException("Error parsing xml. Xml content was: " + xml, e);
		}
		Element root = doc.getDocumentElement();
		return root;
	}
	
	private void parse(org.w3c.dom.Node domNode, Node documentNode) {
		setAttributes(domNode, documentNode);
		NodeList childList = domNode.getChildNodes();
		for (int i=0; i<childList.getLength(); i++) {
			org.w3c.dom.Node domChildNode = childList.item(i);
			if (domChildNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				Node documentChildNode = documentNode.addChildNode(domChildNode.getNodeName());
				parse(domChildNode, documentChildNode);
			}
			else if (domChildNode.getNodeType() == org.w3c.dom.Node.TEXT_NODE) {
				String contents = domChildNode.getTextContent().trim();
				if (contents.length() > 0) {
					documentNode.setContents(contents);
				}
			} 
			else if (domChildNode.getNodeType() == org.w3c.dom.Node.CDATA_SECTION_NODE) {
				documentNode.setCDATA(domChildNode.getTextContent());
			}
		}
	}
	
	private void setAttributes(org.w3c.dom.Node domElement, Node node) {
		NamedNodeMap attributes = domElement.getAttributes();
		for (int i = 0; i<attributes.getLength(); i++) {
			Attr attribute = (Attr) attributes.item(i);
			node.addAttribute(attribute.getName(), attribute.getValue());
		}
	}
	
}

