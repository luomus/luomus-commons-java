package fi.luomus.commons.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailUtil  {

	private final Session		mailSession;

	public EmailUtil(String host) {
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", host);
		this.mailSession = Session.getDefaultInstance(props, null);
	}

	public static class Receivers {

		private final List<String> to = new ArrayList<>();
		private final List<String> cc = new ArrayList<>();

		public Receivers addReceiver(String emailAddress) {
			to.add(emailAddress);
			return this;
		}

		public Receivers addCC(String emailAddress) {
			cc.add(emailAddress);
			return this;
		}

		public Address[] to() throws AddressException {
			return toAddresses(to);
		}

		public Address[] cc() throws AddressException {
			return toAddresses(cc);
		}

		private void setReceiversToMessage(MimeMessage message) throws AddressException, MessagingException {
			if (!to.isEmpty()) {
				message.setRecipients(Message.RecipientType.TO, to());
			}
			if (!cc.isEmpty()) {
				message.setRecipients(Message.RecipientType.CC, cc());
			}
		}
	}

	public void send(Receivers receivers, String from, String subject, String content, File ... attachments) throws MessagingException, AddressException {
		MimeMessage message = new MimeMessage(mailSession);

		receivers.setReceiversToMessage(message);

		message.setFrom(new InternetAddress(from));
		message.setSubject(subject, "UTF-8");
		message.setSentDate(new Date());

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(bodyPart(content));

		for (File file : attachments) {
			if (file != null)
				multipart.addBodyPart(bodyPart(file));
		}

		message.setContent(multipart);

		Transport.send(message);
	}

	public void send(String to, String from, String subject, String content, File ... attachments) throws MessagingException, AddressException {
		send(new Receivers().addReceiver(to), from, subject, content, attachments);
	}

	private MimeBodyPart bodyPart(String content) throws MessagingException {
		MimeBodyPart bodyPart = new MimeBodyPart();
		bodyPart.setText(content, "UTF-8");
		return bodyPart;
	}

	private MimeBodyPart bodyPart(File file) throws MessagingException {
		MimeBodyPart bodyPart = new MimeBodyPart();
		FileDataSource fds = new FileDataSource(file);
		bodyPart.setDataHandler(new DataHandler(fds));
		bodyPart.setFileName(fds.getName());
		return bodyPart;
	}

	public static Address[] toAddresses(List<String> list) throws AddressException {
		Address[] addresses = new Address[list.size()];
		int i = 0;
		for (String email : list) {
			addresses[i++] = new InternetAddress("\"" +email +"\""); // Add double quotes to get unusual email addresses to work (for example those containing åäö ).
		}
		return addresses;
	}


}
