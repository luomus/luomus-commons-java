package fi.luomus.commons.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fi.luomus.commons.utils.AESDecryptor.AESDecryptionException;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;

public class LintuvaaraAuthenticationUtil {
	
	public static final String USER_TYPE = "user_type";
	private static final String AUTH_FOR = "auth_for";
	private static final String EXPIRES_AT = "expires_at";
	
	private final AESDecryptor decryptor;
	private final String systemID;
	private final Collection<String> allowedUserTypes;
	
	public LintuvaaraAuthenticationUtil(AESDecryptor decryptor, String systemID, Collection<String> allowedUserTypes) {
		this.decryptor = decryptor;
		this.systemID = systemID;
		this.allowedUserTypes = allowedUserTypes;
	}
	
	public interface AuthenticationResult {
		/**
		 * Was autentication successful? Use getCause() to see the cause if it was not.
		 * @return true if it was, false if something was wrong.
		 */
		public boolean success();
		/**
		 * Cause of authentication not being successful.
		 * @return empty string if was successful.
		 */
		public String getCause();
		/**
		 * User model's values as a Map.
		 * @return null if was not successful, a map with values from the user model if it was.
		 */
		public Map<String, String> getUsermodel();
	}
	
	public class ValidationResultImple implements AuthenticationResult {
		private boolean success = false;
		private String cause = "";
		private Map<String, String> usermodel = null;
		
		@Override
		public boolean success() {
			return success;
		}
		
		public void setCause(String string) {
			cause = string;
		}
		
		@Override
		public String getCause() {
			return cause;
		}
		
		public void setSuccess(boolean value) {
			success = value;
		}
		
		public void setModel(Map<String, String> usermodel) {
			this.usermodel = usermodel;
		}
		
		@Override
		public Map<String, String> getUsermodel() {
			return usermodel;
		}
	}
	
	public AuthenticationResult check(String key, String iv, String model) {
		ValidationResultImple result = new ValidationResultImple();
		
		if (isNull(key) || isNull(iv) || isNull(model)) {
			result.setCause("login parameters not given");
			return result;
		}
		
		try {
			model = decryptor.decrypt(key, iv, model);
		} catch (AESDecryptionException dex) {
			result.setCause("incorrect login parameters");
			return result;
		}
		
		Map<String, String> usermodel = null;
		try {
			usermodel = parseUserModel(model);
		} catch (Exception e) {
			result.setCause("malformed user model");
			return result;
		}
		
		if (invalidAuthFor(usermodel)) {
			result.setCause("user model was not for this system");
			return result;
		}
		
		if (hasExpired(usermodel)) {
			result.setCause("user model has expired");
			return result;
		}
		
		if (!allowedUserType(usermodel)) {
			result.setCause("not authorized for this user type");
			return result;
		}
		
		result.setSuccess(true);
		result.setModel(usermodel);
		return result;
	}

	private boolean isNull(String key) {
		return key == null || key.length() < 1;
	}
	
	/**
	 * Usermodel example:
	 * <login type="maallikko">
	 * <login_id>petrus.repo@iki.fi</login_id>
	 * <expires_at>1236252609</expires_at>
	 * <auth_for>Peto</auth_for>
	 * </login>
	 */
	private Map<String, String> parseUserModel(String xml) throws Exception {
		Map<String, String> usermodel = new HashMap<>();
		XMLReader reader = new XMLReader();
		Document document = reader.parse(xml);
		Node rootNode = document.getRootNode();
		String userType = rootNode.getAttribute("type");
		usermodel.put(USER_TYPE, userType);
		for (Node node : rootNode.getChildNodes()) {
			usermodel.put(node.getName(), node.getContents());
		}
		return usermodel;
	}
	
	private boolean invalidAuthFor(Map<String, String> usermodel) {
		return !systemID.equals(usermodel.get(AUTH_FOR));
	}
	
	private boolean hasExpired(Map<String, String> usermodel) {
		long current_time = DateUtils.getCurrentEpoch();
		long expires = new Integer(usermodel.get(EXPIRES_AT));
		return (current_time > expires);
	}
	
	private boolean allowedUserType(Map<String, String> usermodel) {
		String usertype = usermodel.get(USER_TYPE);
		return allowedUserTypes.contains(usertype);
	}
}
