package fi.luomus.commons.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Can be used to compress files into a zip file.
 * Note: must call close() after creating an instance. This should be done
 * inside finally -clause of  try-finally block.
 */
public class FileDecompresser {

	static final int BUFFER_SIZE = 2048;

	private final File source;
	private final File destinationFolder;
	private final byte[] buffer = new byte[BUFFER_SIZE];
	private final List<File> decompressed = new ArrayList<>();

	/**
	 * Creates a new FileDeCompresser
	 * @param source the compressed file
	 * @throws FileNotFoundException 
	 */
	public FileDecompresser(File source, File destinationFolder) {
		this.source = source;
		this.destinationFolder = destinationFolder;
	}

	public boolean decompress() {
		try (FileInputStream fis = new FileInputStream(source); 
				BufferedInputStream bis = new BufferedInputStream(fis, BUFFER_SIZE); 
				ZipInputStream stream = new ZipInputStream(bis)) {
			decompress(stream);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		if (decompressed.isEmpty()) {
			System.err.println(FileDecompresser.class.getName()+": No files found from " + source.getAbsolutePath());
			return false;
		}
		return true;
	}

	public Collection<File> getDecompressedFiles() {
		return decompressed;
	}

	private void decompress(ZipInputStream stream) throws IOException, FileNotFoundException {
		ZipEntry entry;
		while ((entry = stream.getNextEntry()) != null) {
			decompress(stream, entry);
		}
	}

	private void decompress(ZipInputStream stream, ZipEntry entry) throws IOException, FileNotFoundException {
		File outFile = new File(destinationFolder, entry.getName());
		if (outFile.getParentFile() != null) outFile.getParentFile().mkdirs();
		try (FileOutputStream fos = new FileOutputStream(outFile); 
				BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length)) {
			int len;
			while ((len = stream.read(buffer)) > 0) {
				bos.write(buffer, 0, len);
			}
			bos.flush();
			fos.flush();
		}
		decompressed.add(outFile);
	}

}
