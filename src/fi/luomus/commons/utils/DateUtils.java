package fi.luomus.commons.utils;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import fi.luomus.commons.containers.DateValue;

public class DateUtils {

	public static final Locale FI = new Locale("fi");
	public static final Locale EN = new Locale("en");
	public static final Locale SV = new Locale("sv");

	/**
	 * Returns current time in yyyy-MM-dd-HH-mm-ss -format, suitable for filenames
	 * @return
	 */
	public static String getFilenameDatetime() {
		return getCurrentDateTime("yyyy-MM-dd-HH-mm-ss");
	}

	/**
	 * Creates a java.util.Date from a String
	 * @param date in format "dd.MM.yyyy"
	 * @return
	 * @throws ParseException
	 */
	public static Date convertToDate(String date) throws ParseException {
		return convertToDate(date, "dd.MM.yyyy");
	}

	public static Date convertToDate(String date, String dateformat) throws ParseException {
		SimpleDateFormat f = new SimpleDateFormat(dateformat);
		f.setLenient(false);
		return f.parse(date);
	}

	/**
	 * Catenates the given three parametrs to dd.MM.yyyy -format
	 * if any of the parameters is null it is replaced by empty string
	 * @param d
	 * @param m
	 * @param y
	 * @return
	 */
	public static String catenateDateString(String d, String m, String y) {
		d = nullToEmptyString(d).trim();
		m = nullToEmptyString(m).trim();
		y = nullToEmptyString(y).trim();
		d = addFrontZeroIfNeeded(d);
		m = addFrontZeroIfNeeded(m);
		StringBuilder b = new StringBuilder();
		b.append(d).append(".").append(m).append(".").append(y);
		return b.toString();
	}

	/**
	 * Catenates the given three parametrs to yyyy-MM-dd -format
	 * if any of the parameters is null it is replaced by empty string
	 * @param d
	 * @param m
	 * @param y
	 * @return
	 */
	public static String catenateIsoDateString(String y, String m, String d) {
		d = nullToEmptyString(d).trim();
		m = nullToEmptyString(m).trim();
		y = nullToEmptyString(y).trim();
		d = addFrontZeroIfNeeded(d);
		m = addFrontZeroIfNeeded(m);
		StringBuilder b = new StringBuilder();
		b.append(y).append("-").append(m).append("-").append(d);
		return b.toString();
	}

	private static String nullToEmptyString(String s) {
		if (s == null || s.trim().length() == 0) return "";
		return s;
	}

	/**
	 * Parses a DateValue object from dd.MM.yyyy format 
	 * @param date
	 * @return
	 */
	public static DateValue convertToDateValue(String date) {
		return convertToDateValue(date, "dd.MM.yyyy");
	}

	public static DateValue convertToDateValue(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		DateValue dateValue = new DateValue(day, month, year);
		return dateValue;
	}

	/**
	 * Parses a DateValue object using the given dateFormat
	 * @param date
	 * @param dateFormat
	 * @return
	 */
	public static DateValue convertToDateValue(String date, String dateFormat) {
		if (date == null) return DateValue.getEmptyDate();
		date = Utils.removeWhitespace(date);
		if (!given(date)) return DateValue.getEmptyDate();

		String separatorChar = findSeparatorChar(dateFormat);

		String[] parts = date.split(Pattern.quote(separatorChar));
		String day = getPart("dd", dateFormat, separatorChar, parts);
		String month = getPart("MM", dateFormat, separatorChar, parts);
		String year = getPart("yyyy", dateFormat, separatorChar, parts);

		day = addFrontZeroIfNeeded(day);
		month = addFrontZeroIfNeeded(month);
		year = removeFrontZeros(year);

		return new DateValue(day, month, year);
	}

	private static boolean given(String date) {
		return date != null && date.length() > 0;
	}

	private static String getPart(String dateFormatPart, String dateFormat, String separatorChar, String[] parts) {
		String[] dateFormatParts = dateFormat.split(Pattern.quote(separatorChar));
		int i = indexOf(dateFormatPart, dateFormatParts);
		if (i == -1) return "";
		if (parts.length-1 < i) return "";
		return parts[i];
	}

	private static int indexOf(String dateFormatPart, String[] dateFormatParts) {
		int i = 0;
		for (String s : dateFormatParts) {
			if (s.equals(dateFormatPart)) {
				return i;
			}
			i++;
		}
		return -1;
	}

	private static String findSeparatorChar(String dateFormat) {
		String parsedDateFormat = dateFormat.replace("d", "").replace("M", "").replace("y", "");
		if (!given(parsedDateFormat)) throw new UnsupportedOperationException("Not implemented for date format " + dateFormat);
		char separator = parsedDateFormat.charAt(0);
		for (char c : parsedDateFormat.toCharArray()) {
			if (c != separator) throw new UnsupportedOperationException("Not implemented for date format " + dateFormat);
		}
		return String.valueOf(separator);
	}

	private static String removeFrontZeros(String value) {
		while (value.startsWith("0")) {
			value = value.substring(1);
		}
		return value;
	}

	private static String addFrontZeroIfNeeded(String value) {
		if (value.length() != 1) return value;
		return "0" + value;
	}

	/**
	 * Returns the current date in "yyyy-MM-dd" -format
	 * @return
	 */
	public static String getCurrentDate() {
		return getCurrentDateTime("yyyy-MM-dd");
	}

	/**
	 * Returns the current time in "HH:mm:ss" -format
	 * @return
	 */
	public static String getCurrentTime() {
		return getCurrentDateTime("HH:mm:ss");
	}

	/**
	 * Returns the current year
	 * @return
	 */
	public static int getCurrentYear() {
		return Integer.valueOf(getCurrentDateTime("yyyy"));
	}

	/**
	 * Returns the current time in the given format
	 * @param dateFormat
	 * @return
	 */
	public static String getCurrentDateTime(String dateFormat) {
		Calendar k = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(k.getTime());
	}

	/**
	 * Returns the current time in the given format using locale settings for the given language code (="fi", "sv", "en"...)
	 * @param dateFormat
	 * @param lang
	 * @return
	 */
	public static String getCurrentDateTime(String dateFormat, String lang) {
		Calendar k = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, new Locale(lang));
		return sdf.format(k.getTime());
	}

	/**
	 * Returns the given time in the given format
	 * @param timeInMillis
	 * @param dateFormat
	 * @return
	 */
	public static String format(long timeInMillis, String dateFormat) {
		Calendar k = Calendar.getInstance();
		k.setTimeInMillis(timeInMillis);
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(k.getTime());
	}

	/**
	 * Returns the given time in the given format
	 * @param timeInMillis
	 * @param dateFormat
	 * @return
	 */
	public static String format(Double timeInMillis, String dateFormat) {
		Double d = Double.valueOf(timeInMillis);
		Long l = Math.round(d);
		return DateUtils.format(l, dateFormat);
	}

	/**
	 * Returns the given time in the given format
	 * @param date
	 * @param dateFormat
	 * @return
	 */
	public static String format(Date date, String dateFormat) {
		return format(date.getTime(), dateFormat);
	}

	/**
	 * Returns the value of java.sql.Date in a String using format "dd.MM.yyyy"
	 * @param date
	 * @return
	 */
	public static String sqlDateToString(java.sql.Date date) {
		return sqlDateToString(date, "dd.MM.yyyy");
	}

	/**
	 * Returns the value of java.sql.Date in a String using the given format
	 * @param date
	 * @param dateFormat
	 * @return
	 */
	public static String sqlDateToString(java.sql.Date date, String dateFormat) {
		if (date == null) return "";
		return new SimpleDateFormat(dateFormat).format(date);
	}

	/**
	 * Current epoch timestamp (seconds)
	 * @return
	 */
	public static long getCurrentEpoch() {
		Calendar k = Calendar.getInstance();
		return k.getTimeInMillis() / 1000;
	}

	/**
	 * Returns epoch timestamp (seconds) for the given date
	 * @param d - day for example "13"
	 * @param m - month for example "4"
	 * @param y - year for example "2010"
	 * @return
	 */
	public static long getEpoch(int d, int m, int y) {
		Calendar k = Calendar.getInstance();
		k.clear();
		k.setLenient(false);
		k.set(Calendar.DAY_OF_MONTH, d);
		k.set(Calendar.MONTH, m-1);
		k.set(Calendar.YEAR, y);
		return k.getTimeInMillis() / 1000;
	}

	public static Date convertToDate(DateValue dateValue) {
		return new Date(getEpoch(dateValue) * 1000);
	}

	public static String format(DateValue dateValue, String dateFormat) {
		return format(getEpoch(dateValue) * 1000, dateFormat);
	}

	/**
	 * Returns epoch timestamp (seconds)
	 * @param dateValue
	 * @return
	 */
	public static long getEpoch(DateValue dateValue) {
		return getEpoch(Integer.valueOf(dateValue.getDay()), Integer.valueOf(dateValue.getMonth()), Integer.valueOf(dateValue.getYear()));
	}

	public static int getDayOfYear(DateValue dateValue) {
		Calendar k = new GregorianCalendar();
		k.setTimeInMillis(getEpoch(dateValue) * 1000);
		return k.get(Calendar.DAY_OF_YEAR);
	}

	private static class HumanizeStrings {
		public final String year;
		public final String years;
		public final String month;
		public final String months;
		public final String day;
		public final String days;
		public HumanizeStrings(String year, String years, String month, String months, String day, String days) {
			this.year = year;
			this.years = years;
			this.month = month;
			this.months = months;
			this.day = day;
			this.days = days;
		}
	}
	private static final Map<Locale, HumanizeStrings> HUMANIZE_STRINGS_BY_LOCALE = initHumanizeStringsByLocaleMap();

	private static Map<Locale, HumanizeStrings> initHumanizeStringsByLocaleMap() {
		Map<Locale, HumanizeStrings> map = new HashMap<>();
		map.put(DateUtils.FI, new HumanizeStrings("vuosi", "vuotta", "kuukausi", "kuukautta", "päivä", "päivää"));
		map.put(DateUtils.EN, new HumanizeStrings("year", "years", "month", "months", "day", "days"));
		map.put(DateUtils.SV, new HumanizeStrings("år", "år", "månad", "månader", "dagen", "dagar"));
		return map;
	}

	public static String humanize(DateValue yearsMonthsDays, Locale locale) {
		HumanizeStrings humanizeStrings = HUMANIZE_STRINGS_BY_LOCALE.get(locale);
		if (humanizeStrings == null) throw new UnsupportedOperationException("Not yet implemented for locale " + locale.toString());
		String yearString = humanizeStrings.years;
		String monthString = humanizeStrings.months;
		String dayString = humanizeStrings.days;
		String years = yearsMonthsDays.getYear();
		String months = yearsMonthsDays.getMonth();
		String days = yearsMonthsDays.getDay();
		if (years.equals("1")) yearString = humanizeStrings.year;
		if (months.equals("1")) monthString = humanizeStrings.month;
		if (days.equals("1")) dayString = humanizeStrings.day;
		StringBuilder humanized = new StringBuilder();
		if (givenAndNonZero(years)) {
			humanized.append(years).append(" ").append(yearString).append(" ");
		}
		if (givenAndNonZero(months)) {
			humanized.append(months).append(" ").append(monthString).append(" ");
		}
		if (givenAndNonZero(days)) {
			humanized.append(days).append(" ").append(dayString).append(" ");
		}
		return humanized.toString().trim();
	}



	private static boolean givenAndNonZero(String value) {
		return !value.equals("0") && value.length() > 0;
	}

	public static String humanizeDifference(DateValue begin, DateValue end, Locale locale) {
		return humanize(getYearsMonthsDays(begin, end), locale);
	}

	public static boolean validIsoDateTime(String string) {
		// http://stackoverflow.com/questions/25884332/validate-a-simple-time-date-format-yyyy-mm-ddthhmmss
		try {
			SimpleDateFormat format = new SimpleDateFormat(getDateFormat(string));
			format.setLenient(false);

			ParsePosition pos = new ParsePosition(0);
			Date date = format.parse(string, pos);

			int year = getYear(date);
			if (year > 9999) return false; // Parser accepts 1234567 for "yyyy", hence this check

			if (pos.getErrorIndex() >= 0) {
				return false;
			} else if (pos.getIndex() != string.length()) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	private static int getYear(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		return year;
	}

	private static String getDateFormat(String string) {
		int count = Utils.countNumberOf("-", string);
		if (count > 2) throw new IllegalArgumentException();

		boolean hasTime = string.contains("T");
		if (hasTime && count != 2) throw new IllegalArgumentException();

		if (count == 0) return "yyyy";
		if (count == 1) return "yyyy-MM";
		if (!hasTime) return "yyyy-MM-dd";
		return "yyyy-MM-dd'T'HH:mm:ss";
	}

	public static int getDayOfWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
		if (dayOfWeek == 0) return 7;
		return dayOfWeek;
	}

	public static DateValue getYearsMonthsDays(DateValue beginDate, DateValue endDate) {
		Calendar begin = Calendar.getInstance();
		begin.setTime(convertToDate(beginDate));
		Calendar end = Calendar.getInstance();
		end.setTime(convertToDate(endDate));

		int countOfYearChanges = 0;
		int countOfMonthChanges = 0;
		int countOfDayChanges = 0;
		int startDayOfMonth = end.get(Calendar.DAY_OF_MONTH);
		boolean monthHasChanged = false;

		while (end.after(begin)) {
			int previousMonth = end.get(Calendar.MONTH);
			end.add(Calendar.DAY_OF_MONTH, -1);
			if (end.get(Calendar.MONTH) != previousMonth) monthHasChanged = true;
			if (monthHasChanged && end.get(Calendar.DAY_OF_MONTH) <= startDayOfMonth) {
				countOfMonthChanges++;
				countOfDayChanges = 0;
				monthHasChanged = false;
				if (countOfMonthChanges >= 12) {
					countOfYearChanges++;
					countOfMonthChanges = 0;
				}
			} else {
				countOfDayChanges++;
			}
		}
		return new DateValue(countOfDayChanges, countOfMonthChanges, countOfYearChanges);
	}

}
