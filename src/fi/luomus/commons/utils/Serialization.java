package fi.luomus.commons.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serialization {

	public static void serialize(Object o, File file) throws IOException {
		File tempfile = new File(file.getParent(), file.getName() + ".temp");
		synchronized (file) {
			FileOutputStream out = null;
			ObjectOutputStream oout = null;
			try {
				out = new FileOutputStream(tempfile);
				oout = new ObjectOutputStream(out);
				oout.writeObject(o);
			} finally {
				FileUtils.close(out, oout);
			}
			if (file.exists() && file.isFile()) {
				file.delete();
			}
			org.apache.commons.io.FileUtils.moveFile(tempfile, file);
		}
	}

	public static Object load(File file) throws IOException, ClassNotFoundException {
		Object object = null;
		FileInputStream in = null;
		ObjectInputStream oin = null;
		try {
			in = new FileInputStream(file);
			oin = new ObjectInputStream(in);
			object = oin.readObject();
		}  finally {
			FileUtils.close(in);
			FileUtils.close(oin);
		}
		return object;
	}

}
