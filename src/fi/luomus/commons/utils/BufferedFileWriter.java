package fi.luomus.commons.utils;

import java.io.Closeable;
import java.io.File;

public class BufferedFileWriter implements Closeable {

	private static final int BUFFER_SIZE = 100;
	private final File file;
	private final StringBuilder buffer = new StringBuilder();
	private int bufferSize = 0;
	private boolean empty = true;
	
	public BufferedFileWriter(File file) {
		this.file = file;
	}

	public void write(String line) {
		empty = false;
		buffer.append(line + "\n");
		bufferSize++;
		if (bufferSize > BUFFER_SIZE) {
			writeBuffered();
		}
	}

	private void writeBuffered() {
		if (bufferSize == 0) return;
		try {
			FileUtils.writeToFile(file, buffer.toString(), "UTF-8", true);
		} catch (Exception e) {
			throw new RuntimeException(file.getAbsolutePath(), e);
		}
		buffer.setLength(0);
		bufferSize = 0;
	}

	@Override
	public void close() {
		writeBuffered();
	}

	public boolean isEmpty() {
		return empty;
	}

	public File getFile() {
		return file;
	}

	@Override
	public int hashCode() {
		return file.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BufferedFileWriter other = (BufferedFileWriter) obj;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		return true;
	}
		
}
