package fi.luomus.commons.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class Logger {

	private static Object sharedLock = new Object();

	private File logFile;
	private Writer writer;
	private OutputStream out;
	private final List<String> logLines = new ArrayList<>();
	private final boolean usingGivenWriter;
	
	public Logger(File file) {
		if (file.getParentFile() != null) {
			file.getParentFile().mkdirs();
		}
		usingGivenWriter = false;
		this.logFile = file;
	}
	
	public Logger(Writer writer) {
		this.writer = writer;
		usingGivenWriter = true;
	}

	private void openWriter() throws FileNotFoundException {
		out = new FileOutputStream(logFile, true);
		try {
			this.writer = new OutputStreamWriter(out, "utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new IllegalStateException(e);
		}
	}

	private void closeWriter() {
		if (out != null) try { out.close(); } catch (IOException e) {}
		if (writer != null) try { writer.close(); } catch (IOException e) {}
	}

	public Logger rollback() {
		logLines.clear();
		return this;
	}

	public Logger commit(String transactionID, String userId) throws IOException {
		if (logLines.isEmpty()) return this;
		String header = "--- START TRANSACTION " + transactionID + " @ " + DateUtils.getCurrentDateTime("yyyy-MM-dd HH:mm:ss") + " --- user-id: "+userId+" --- \n";
		synchronized(sharedLock) {
			if (!usingGivenWriter) {
				openWriter();
			}
			writer.write(header);
			for (String line : logLines) {
				writer.write(transactionID + ": ");
				writer.write(line);
				writer.write("\n");
			}
			writer.write("--- END TRANSACTION " + transactionID + " ---\n");
			writer.flush();
			if (!usingGivenWriter) {
				closeWriter();
			}
		}
		logLines.clear();
		return this;
	}

	public Logger log(String message) {
		if (message == null) message = "";
		this.logLines.add(message);
		return this;
	}

}
