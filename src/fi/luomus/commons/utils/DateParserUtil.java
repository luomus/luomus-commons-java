package fi.luomus.commons.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DateParserUtil {

	public static final boolean LENIENT_MODE_THAT_MAKES_QUESSES = true;
	public static final boolean STRICT_MODE = false;
	private static final Map<String, String> FINNISH_MONTHS = initFinnishMonths();
	private static final Collection<String> SEASON_STRINGS = Utils.list("kevät", "kevat", "talvi", "syksy", "kesällä", "kesalla", "keväällä", "kevaalla", "talvella", "syksyllä", "syksylla");

	private static Map<String, String> initFinnishMonths() {
		// Note: it is important that months are in descending lenght: "heinäkuuta", "heinakuuta" "heinäkuu", "heinakuu", "heinä", "heina"
		// so that when doing replacement we don't replace "heinäkuuta" with "07kuuta".  
		Map<String, String> map = new LinkedHashMap<>();
		List<String> shortMonths = Utils.list("tammi", "helmi", "maalis", "huhti", "touko", "kesä", "heinä", "elo", "syys", "loka", "marras", "joulu");
		int monthNumberInt = 1;
		for (String shortMonth : shortMonths) {
			String monthNumber = addFrontZero(String.valueOf(monthNumberInt));
			map.put(shortMonth+"kuuta", monthNumber);
			map.put(shortMonth.replace("ä", "a")+"kuuta", monthNumber);
			map.put(shortMonth+"kuu", monthNumber);
			map.put(shortMonth.replace("ä", "a")+"kuu", monthNumber);
			map.put(shortMonth, monthNumber);
			map.put(shortMonth.replace("ä", "a"), monthNumber);
			monthNumberInt++;
		}
		map.put("iii.", "03.");
		map.put("vii.", "07.");
		map.put("viii.", "08.");
		map.put("xii.", "12.");
		map.put("ii.", "02.");
		map.put("iv.", "04.");
		map.put("vi.", "06.");
		map.put("ix.", "09.");
		map.put("xi.", "11.");
		map.put("i.", "01.");
		map.put("v.", "05.");
		map.put("x.", "10.");
		return map;
	}

	private final boolean lenientMode;

	public DateParserUtil(boolean lenientModeThatMakesQuesses) {
		this.lenientMode = lenientModeThatMakesQuesses;
	}


	/**
	 * Will parse various date formats and return them in ISO_8601 format 
	 * @param date
	 * @throws IllegalArgumentException if given date is not understood
	 * @return date in ISO_8601 format
	 */
	public String parseDate(String date) throws IllegalArgumentException {
		if (date == null) return null;
		date = date.trim().toLowerCase();
		if (date.length() < 1) return "";
		try {
			return tryToParse(date);
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(date, e);
		}
	}

	private String tryToParse(String date) {
		if (containsOnlyLetters(date)) {
			if (lenientMode) {
				return "";
			}
			throw new IllegalArgumentException(date);
		}
		if (date.endsWith(" 00:00:00")) date = date.replace(" 00:00:00", "");
		if (alreadyISOFormated(date)) {
			if (date.contains("t")) return date.split("t")[0];
			return date;
		}
		date = date.replace("/", ".");
		while (date.contains("..")) {
			date = date.replace("..", ".");
		}
		if (date.endsWith(".")) date = date.substring(0, date.length()-1);
		if (date.length() < 1) return "";
		if (date.contains(":")) {
			date = Utils.removeNumbersAroundChar(date, ':');
		}
		if (severalSeparatedDates(date)) {
			return parseSeveralSeparatedDates(date);
		}
		if (containsLetters(date)) {
			if (lenientMode) {
				date = removeSeasonStringsIfThereAreAny(date);
				if (date.endsWith("luku")) {
					String possibleLukuString = date.replace("luku", "").replace("-", "").replace(" ", "");
					if (containsOnlyNumbers(possibleLukuString)) {
						return parseLuku(possibleLukuString);
					}
				}
				if (Utils.countOfNumberSequences(date) == 1) {
					date = moveNonNumbersInFrontOfNumbers(date);
				}
			}
			date = replaceWrittenFinnishMonthsWithNumbersIfThereAreAny(date);
		}
		if (date.trim().length() < 1) return "";
		if (containsOnlyNumbers(date) && date.length() == 4) {
			String year = date;
			return year;
		}
		if (countOfDots(date) == 2 && containsOnlyNumbersAndDots(date.replace(" ", ""))) {
			return parseFinnishDotNotationDate(date.replace(" ", ""));
		}
		if (countOfDots(date) == 2 && containsOnlyNumbersAndDotsAndDashes(date) && oneDashIsInDayPartAndNowhereElseAndDashSeparatersTwoValidDaysOfMonth(date)) {
			return parseFinnishDotNotationDateWithDateRange(date);
		}
		if (countOfDots(date) == 1) {
			return parseFinnishDotNotationDateWithOnlyMonthAndYear(date);
		}
		if (containsOnlyNumbers(date) && date.length() == 2) {
			return twoNumberFormatToFullYear(date);
		}
		if (date.startsWith("-") && containsOnlyNumbers(date.replace("-", ""))) {
			return twoNumberFormatToFullYear(date.replace("-", ""));
		}
		if (date.contains(" ") && containsOnlyNumbersAndSpaces(date)) {
			return parseDate(spaceSeparatedToDotSeparated(date));
		}
		int countOfNumberSequences = Utils.countOfNumberSequences(date);
		if (countOfNumberSequences == 1) {
			if (lenientMode) {
				if (returnOnlyNumbers(date).length() == 2) {
					return twoNumberFormatToFullYear(returnOnlyNumbers(date));
				}
				if (returnOnlyNumbers(date).length() == 4) {
					return returnOnlyNumbers(date);
				}
			}
		}
		if (countOfNumberSequences > 1) {
			String sequenced = combineDateSequencesToCommaSeparated(date);
			if (!sequenced.equals(date)) {
				return this.parseDate(sequenced);
			}
		}
		throw new IllegalArgumentException(date);
	}


	private boolean alreadyISOFormated(String date) {
		if (date.contains("/")) {
			for (String dateRangePart : date.split("/")) {
				if (!alreadyISOFormated(dateRangePart)) {
					return false;
				}
			}
			return true;
		}
		if (date.contains("t")) {
			date = date.split("t")[0];
		}
		if (containsOnlyNumbersAndDashes(date) && countOfDashes(date) == 2) {
			for (String part : date.split("-")) {
				if (part.length() == 0) return false;
			}
			validateISOFormatedDate(date);
			return true;
		}
		return false;
	}


	private void validateISOFormatedDate(String date) {
		String[] parts = date.split("-");
		checkValidYear(parts[0]);
		checkValidMonth(parts[1]);
		checkValidDay(parts[2]);
	}

	private String parseLuku(String date) {
		if (!date.endsWith("0")) {
			return date;
		}
		if (date.length() == 2) {
			date = twoNumberFormatToFullYear(date);
		}
		return date + "/" + date.substring(0, 3) + "9";
	}

	private String combineDateSequencesToCommaSeparated(String date) {
		List<String> sequences = parseDateSequences(date);
		return toCommaSeparated(sequences);
	}


	private String toCommaSeparated(List<String> sequences) {
		String commaSeparated = "";
		Iterator<String> i = sequences.iterator();
		while (i.hasNext()) {
			commaSeparated += i.next();
			if (i.hasNext()) {
				commaSeparated += ",";
			}
		}
		return commaSeparated;
	}


	private List<String> parseDateSequences(String date) {
		List<String> sequences = new ArrayList<>();
		String currentSequence = "";
		boolean inSequence = false;
		for (char c : date.toCharArray()) {
			if (Character.isDigit(c) || c == '.') {
				if (!inSequence) {
					inSequence = true;
					currentSequence = "" + c;
				} else {
					currentSequence += c;
				}
			} else {
				if (inSequence) {
					inSequence = false;
					sequences.add(currentSequence);
				}
			}
		}
		if (inSequence) {
			sequences.add(currentSequence);
		}
		return sequences;
	}


	private String spaceSeparatedToDotSeparated(String date) {
		date = date.replace(" ", ".");
		while (date.contains("..")) {
			date = date.replace("..", ".");
		}
		return date;
	}


	private String moveNonNumbersInFrontOfNumbers(String date) {
		String numbers = returnOnlyNumbers(date);
		if (numbers.length() == 2) {
			numbers = twoNumberFormatToFullYear(numbers);
		}
		date = removeNumbers(date).trim();
		date += " " + numbers;
		return date.trim();
	}

	private String removeNumbers(String date) {
		String onlyNonNumbers = "";
		for (char c : date.toCharArray()) {
			if (!Character.isDigit(c)) {
				onlyNonNumbers += c;
			}
		}
		return onlyNonNumbers;
	}


	private String returnOnlyNumbers(String date) {
		String numbers = "";
		for (char c : date.toCharArray()) {
			if (Character.isDigit(c)) {
				numbers += c;
			}
		}
		return numbers;
	}

	private String replaceWrittenFinnishMonthsWithNumbersIfThereAreAny(String date) {
		for (String monthCandidate : FINNISH_MONTHS.keySet()) {
			if (date.contains(monthCandidate)) {
				date = date.replace(monthCandidate, FINNISH_MONTHS.get(monthCandidate));
			}
		}
		return date;
	}

	private String removeSeasonStringsIfThereAreAny(String date) {
		if (date.endsWith("kesä")) {
			date = date.replace("kesä", "");
		}
		if (date.endsWith("kesa")) {
			date = date.replace("kesa", "");
		}
		for (String seasonString : SEASON_STRINGS) {
			if (date.contains(seasonString)) {
				date = date.replace(seasonString, "");
			}
		}
		return date.trim();
	}

	private boolean oneDashIsInDayPartAndNowhereElseAndDashSeparatersTwoValidDaysOfMonth(String date) {
		if (countOfDashes(date) != 1) return false;
		String dayPart = date.split("\\.")[0];
		if (countOfDashes(dayPart) != 1) return false;
		String[] separatedDayPart = dayPart.split("-");
		if (separatedDayPart.length != 2) return false;
		return isDay(separatedDayPart[0].trim()) && isDay(separatedDayPart[1].trim());  
	}

	private boolean isDay(String value) {
		if (isNumber(value)) {
			int i = Integer.valueOf(value);
			return i >= 1 && i <= 31;
		}
		return false;
	}

	private boolean isNumber(String value) {
		return containsOnlyNumbers(value);
	}

	private String twoNumberFormatToFullYear(String date) {
		int currentYear = DateUtils.getCurrentYear();
		String currentCentury = Integer.toString(currentYear).substring(0, 2);
		String fullYear = currentCentury + date;
		if (Integer.valueOf(fullYear) <= currentYear) {
			return fullYear;
		}
		String previousCentury = Integer.toString(currentYear-100).substring(0, 2);
		return previousCentury + date;
	}

	private String parseSeveralSeparatedDates(String severalDates) {
		String[] dates = severalDates.split("[,;]");
		String maxDate = "1000-01-01";
		String minDate = "9999-01-01";
		for (String date : dates) {
			if (lenientMode && containsOnlyLetters(date)) continue;
			String parsedDate = this.parseDate(date);
			try {
				maxDate = maxOf(maxDate, parsedDate);
				minDate = minOf(minDate, parsedDate);
			} catch (Exception e) { }
		}
		if (maxDate.equals("1000-01-01") || minDate.equals("9999-01-01")) {
			throw new IllegalStateException(severalDates);
		}
		if (maxDate.equals(minDate)) {
			return minDate;
		}
		return minDate + "/" + maxDate;
	}

	private boolean containsOnlyLetters(String date) {
		for (char c : date.toCharArray()) {
			if (Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}


	private String minOf(String date1, String date2) {
		date1 = minOfDateRange(date1);
		date2 = minOfDateRange(date2);
		if (toTimestamp(date1) < toTimestamp(date2)) {
			return date1;
		}
		return date2;
	}

	private long toTimestamp(String date) {
		String[] parts = date.split("-");
		int yyyy = Integer.valueOf(parts[0]);
		int mm = Integer.valueOf(tryGet(parts, 1));
		int dd = Integer.valueOf(tryGet(parts, 2));
		return DateUtils.getEpoch(dd, mm, yyyy);
	}

	private String tryGet(String[] parts, int i) {
		try {
			return parts[i];
		} catch (ArrayIndexOutOfBoundsException e) {
			return "1";
		}
	}

	private String maxOf(String date1, String date2) {
		date1 = maxOfDateRange(date1);
		date2 = maxOfDateRange(date2);
		if (toTimestamp(date1) > toTimestamp(date2)) {
			return date1;
		}
		return date2;
	}

	private String maxOfDateRange(String date) {
		if (date.contains("/")) {
			return date.split("/")[1];
		}
		return date;
	}

	private String minOfDateRange(String date) {
		if (date.contains("/")) {
			return date.split("/")[0];
		}
		return date;
	}

	private boolean severalSeparatedDates(String date) {
		return date.contains(",") || date.contains(";");
	}

	private String parseFinnishDotNotationDate(String date) {
		String[] parts = date.split("\\.");
		return toISO_8601(parts[2], parts[1], parts[0]);
	}

	private String parseFinnishDotNotationDateWithOnlyMonthAndYear(String date) {
		String[] parts = date.split("\\.");
		if (parts.length == 1) {
			return toISO_8601(parts[0], null, null);
		}
		if (parts[0].length() > parts[1].length()) {
			throw new IllegalArgumentException(date);
		}
		String year = parts[1];
		String month = parts[0];
		return toISO_8601(year, month, null);
	}

	private String parseFinnishDotNotationDateWithDateRange(String date) {
		String startDay = date.split("-")[0].trim();
		String rest = date.split("-")[1].trim();
		String endDate = parseFinnishDotNotationDate(rest);
		String yearMonth = endDate.substring(0, "yyyy-mm".length());
		String startDate = yearMonth + "-" + startDay;
		return startDate + "/" + endDate;
	}

	private String toISO_8601(String year, String month, String day) {
		year = year.trim();
		if (year.length() == 2) {
			year = twoNumberFormatToFullYear(year);
		}
		checkValidYear(year);
		if (month == null) {
			return year;
		}
		month = addFrontZero(month.trim());
		checkValidMonth(month);
		if (day == null) {
			return year + "-" + month;
		}
		day = addFrontZero(day.trim());
		checkValidDay(day);
		return year + "-" + month + "-" + day;
	}

	private void checkValidDay(String day) {
		try {
			int ival = Integer.valueOf(day);
			if (ival < 1 || ival > 31) {
				throw new IllegalArgumentException(day);
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(day);
		}
	}


	private void checkValidMonth(String month) {
		try {
			int ival = Integer.valueOf(month);
			if (ival < 1 || ival > 12) {
				throw new IllegalArgumentException(month);
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(month);
		}
	}

	private void checkValidYear(String year) {
		try {
			int ival = Integer.valueOf(year);
			if (ival < 1000 || ival > DateUtils.getCurrentYear()) {
				throw new IllegalArgumentException(year);
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(year);
		}
	}

	private static String addFrontZero(String s) {
		if (s.length() == 1) {
			return "0" + s;
		}
		return s;
	}

	private boolean containsOnlyNumbersAndDots(String date) {
		return containsOnlyNumbers(date.replace(".", ""));
	}

	private boolean containsOnlyNumbersAndDashes(String date) {
		return containsOnlyNumbers(date.replace("-", ""));
	}

	private boolean containsOnlyNumbersAndDotsAndDashes(String date) {
		return containsOnlyNumbers(date.replace(".", "").replace("-", ""));
	}

	private boolean containsOnlyNumbersAndSpaces(String date) {
		return containsOnlyNumbers(date.replace(" ", ""));
	}

	private boolean containsOnlyNumbers(String date) {
		for (char c : date.toCharArray()) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}

	private boolean containsLetters(String date) {
		for (char c : date.toCharArray()) {
			if (Character.isLetter(c)) {
				return true;
			}
		}
		return false;
	}

	private int countOfDots(String date) {
		return countOf(date, '.');
	}

	private int countOfDashes(String date) {
		return countOf(date, '-');
	}

	private int countOf(String date, char charToCount) {
		int count = 0;
		for (char c : date.toCharArray()) {
			if (c == charToCount) count++;
		}
		return count;
	}

}
