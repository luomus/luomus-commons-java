package fi.luomus.commons.utils;


import java.io.File;
import java.io.IOException;

public class LogUtils {

	private static final int	MAX_TRACE_LEVELS_TO_LOG	= 10;

	/**
	 * Writes the message to specified folder using the given prefix. End part of the filenime consists of "yyyy-MM.txt".
	 * In front of the message a timestamp is written. This call is synchronized.
	 * @param message
	 * @param logFolder
	 * @param filePrefix
	 */
	public static synchronized void write(String message, String logFolder, String filePrefix) {
		String filename = logFolder + File.separator + filePrefix + DateUtils.getCurrentDateTime("yyyy-MM") + ".txt";
		try {
			FileUtils.writeToFile(new File(filename), "[" + DateUtils.getCurrentDateTime("yyyy-MM-dd HH:mm:ss") + "] " + message + "\n", true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Writes the message to specified folder using the given prefix. End part of the filenime consists of "yyyy-MM.txt".
	 * In front of the message a timestamp is written, and also the given user ID. This call is synchronized.
	 * @param message
	 * @param userId
	 * @param logFolder
	 * @param filePrefix
	 */
	public static void write(String message, String userId, String logFolder, String filePrefix) {
		write("[" + userId + "] " + message, logFolder, filePrefix);
	}

	/**
	 * Writes the stacktrace of the Throwable condition to specified folder using the given prefix. End part of the filenime consists of "yyyy-MM.txt".
	 * In front of the stacktrace a timestamp is written. This call is synchronized.
	 * @param message
	 * @param userId
	 * @param logFolder
	 * @param filePrefix
	 */
	public static void write(Throwable condition, String logFolder, String filePrefix) {
		String stackTrace = buildStackTrace(condition, MAX_TRACE_LEVELS_TO_LOG);
		write(stackTrace.toString(), logFolder, filePrefix);
	}

	/**
	 * Writes the stacktrace of the Throwable condition to specified folder using the given prefix. End part of the filenime consists of "yyyy-MM.txt".
	 * In front of the stacktrace a timestamp is written. This call is synchronized.
	 * @param message
	 * @param userId
	 * @param logFolder
	 * @param filePrefix
	 */
	public static void write(String message, Throwable condition, String logFolder, String filePrefix) {
		String stackTrace = buildStackTrace(condition, MAX_TRACE_LEVELS_TO_LOG);
		write(message +":\n" + stackTrace.toString(), logFolder, filePrefix);
	}

	/**
	 * Utility that can be used to get the N top rows of stacktrace.
	 * @param condition
	 * @param maxTraceLevels
	 * @return
	 */
	public static String buildStackTrace(Throwable condition, int maxTraceLevels) {
		StringBuilder stackTrace = new StringBuilder();
		return buildStackTrace(stackTrace, condition, maxTraceLevels);
	}

	public static String buildStackTrace(StringBuilder stackTrace, Throwable condition, int maxTraceLevels) {
		if (condition == null) {
			stackTrace.append("Null condition given for error reporting!").append("\n");
			return stackTrace.toString();
		}
		stackTrace.append(condition.toString()).append("\n");
		int i = 0;
		if (condition.getStackTrace() != null) {
			for (StackTraceElement se : condition.getStackTrace()) {
				if (se == null) break;
				if (i++ > maxTraceLevels) break;
				stackTrace.append(se.toString()).append("\n");
			}
		}
		if (condition.getCause() != null) {
			buildStackTrace(stackTrace, condition.getCause(), maxTraceLevels);
		}
		return stackTrace.toString();
	}

	public static String buildStackTrace(Throwable condition) {
		return buildStackTrace(condition, Integer.MAX_VALUE);
	}

	public static String shorten(String message, int maxLength) {
		if (maxLength < 1) {
			throw new IllegalArgumentException("" +maxLength);
		}

		if (message == null) return null;

		if (message.length() <= maxLength) {
			return message;
		}

		StringBuilder shortenedMessage = new StringBuilder();
		shortenedMessage.append(message.substring(0, (maxLength/2)+1));
		shortenedMessage.append("\n[--- LONG MESSAGE SHORTENED ---]\n");
		shortenedMessage.append(message.substring(message.length() - (maxLength/2)-1, message.length()));
		return shortenedMessage.toString();
	}

}
