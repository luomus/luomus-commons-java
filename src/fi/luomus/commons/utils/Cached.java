package fi.luomus.commons.utils;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.cache.CacheBuilder;

/**
 *Wrapper for Caffeine Cache.
 * @param <K> Keys to use in the cache
 * @param <V> Values coming out from the cache
 */
public class Cached<K, V> {

	public static class ResourceWrapper<K, R> {
		private final K key;
		private final R resource;
		public ResourceWrapper(K key, R resource) {
			this.key = key;
			this.resource = resource;
		}
		public K getKey() {
			return key;
		}
		public R getResource() {
			return resource;
		}
		@Override
		public int hashCode() {
			if (key == null) return 0;
			return key.hashCode();
		}
		@Override
		public boolean equals(Object o) {
			if (o instanceof ResourceWrapper<?, ?>) {
				ResourceWrapper<?, ?> other = (ResourceWrapper<?, ?>) o;
				Object otherKey = other.getKey();
				if (key == null) return otherKey == null;
				if (otherKey == null) return key == null;
				return key.equals(otherKey);
			}
			throw new UnsupportedOperationException("Can only compare to another " + ResourceWrapper.class.getName());
		}
	}

	/**
	 * CacheLoader is provided which is used to actually read an object.
	 * Further calls to that object then come from the Cache, unless expiration time has passed (and
	 * then the object is read again using this CacheLoader ASYNCHRONOUSLY)
	 * @param <K> key
	 * @param <V> value
	 * @see Cached
	 */
	public interface CacheLoader<K, V> {
		V load(K key);
	}

	private final LoadingCache<K, V> cache;
	private final com.google.common.cache.LoadingCache<K, V> guavaCache;

	/**
	 * Creates a new object cache with async refresh
	 * @param loader which is used to load the object for the first time and if it has expired
	 * @param expireTime
	 * @param timeUnit of expireTime
	 * @param maxCountOfItems maximum count of items
	 */
	public Cached(final CacheLoader<K, V> loader, long expireTime, TimeUnit timeUnit, long maxCountOfItems) {
		this(loader, expireTime, timeUnit, maxCountOfItems, true);
	}

	/**
	 *
	 * @param loader which is used to load the object for the first time and if it has expired
	 * @param expireTime
	 * @param timeUnit of expireTime
	 * @param maxCountOfItems maximum count of items
	 * @param asyncRefresh true: staled returned immediately, reloaded in background; false: reloaded immediately and then returned
	 */
	public Cached(final CacheLoader<K, V> loader, long expireTime, TimeUnit timeUnit, long maxCountOfItems, boolean asyncRefresh) {
		if (asyncRefresh) {
			cache = Caffeine.newBuilder()
					.refreshAfterWrite(expireTime, timeUnit)
					.maximumSize(maxCountOfItems)
					.build(k->loader.load(k));
			guavaCache = null;
		} else {
			cache = null;
			guavaCache = CacheBuilder.newBuilder()
					.expireAfterWrite(expireTime, timeUnit)
					.maximumSize(maxCountOfItems)
					.build(new com.google.common.cache.CacheLoader<K, V>() {
						@Override
						public V load(K key) {
							return loader.load(key);
						}
					});
		}
	}

	/**
	 * Get object from cache by it's key. If object has never been loaded or expire time has expired, the object will be reloaded.
	 * @param key
	 * @return the object
	 */
	public V get(K key) {
		if (cache != null) {
			return cache.get(key);
		}
		return guavaCache.getUnchecked(key);
	}

	public V getForceReload(K key) {
		invalidate(key);
		return get(key);
	}

	public void invalidate(K key) {
		if (cache != null) {
			cache.invalidate(key);
		} else {
			guavaCache.invalidate(key);
		}
	}

	public void invalidateAll() {
		if (cache != null) {
			cache.invalidateAll();
		} else {
			guavaCache.invalidateAll();
		}
	}

	public String debugContents() {
		if (cache != null) {
			return cache.asMap().toString();
		}
		return guavaCache.asMap().toString();
	}

	public void put(K key, V object) {
		if (cache != null) {
			cache.put(key, object);
		} else {
			guavaCache.put(key, object);
		}
	}

	public Collection<V> getAll() {
		if (cache != null) {
			return cache.asMap().values();
		}
		return guavaCache.asMap().values();
	}

}
