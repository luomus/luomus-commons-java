package fi.luomus.commons.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URI;
import java.security.MessageDigest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.containers.KeyValuePair;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.http.HttpClientService;

public class Utils {

	private static final String	STATIC_SALT	= "sJS5LfkLSMNf63&42:,s3%,m;356mskks";

	public static String upperCaseFirst(String s) {
		if (s == null) return s;
		if (s.isEmpty()) return s;
		if (s.length() == 1) return s.toUpperCase();
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	public static String generateGUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString().toUpperCase();
	}

	/**
	 * Get list of values, separated by '|' -char
	 * @param value
	 * @return
	 */
	public static List<String> valuelist(String value) {
		List<String> list = new ArrayList<>(10);
		for (String v : value.split("\\|")) {
			list.add(v);
		}
		return list;
	}

	/**
	 * Returns the parameters in a collection
	 * @param contents
	 * @return
	 */
	@SafeVarargs
	public static <T> Collection<T> collection(T... contents) {
		return set(contents);
	}

	/**
	 * Returns union of the given collection and the parameters. Will not alter the given collection.
	 * @param c collection
	 * @param contents
	 * @return
	 */
	@SafeVarargs
	public static <T> Collection<T> collection(Collection<T> c, T... contents) {
		Collection<T> collection = set(contents);
		collection.addAll(c);
		return collection;
	}

	/**
	 * Returns the parameters in a set.
	 * @param <T>
	 * @param contents
	 * @return
	 */
	@SafeVarargs
	public static <T> Set<T> set(T... contents) {
		Set<T> set = new LinkedHashSet<>(contents == null ? 1 : contents.length);
		if (contents == null) return set;
		for (T t : contents) {
			set.add(t);
		}
		return set;
	}

	/**
	 * Returns union of the given set and the parameters. Will not alter the given set.
	 * @param c collection
	 * @param contents
	 * @return
	 */
	@SafeVarargs
	public static <T> Set<T> set(Set<T> c, T... contents) {
		Set<T> set = set(contents);
		set.addAll(c);
		return set;
	}

	/**
	 * Counts the number of expressions in a String
	 * @param expressions
	 * @param inString
	 * @return
	 */
	public static int countNumberOf(String expressions, String inString) {
		return inString.split("\\Q" + expressions + "\\E", -1).length - 1;
	}

	/**
	 * Splits a decimal to whole number and decimals, using either , or . as the delimiter
	 * @param value [0] = whole number, [1] = decimals
	 * @return
	 */
	public static String[] splitDecimal(String value) {
		String[] returnValues = {
				"", ""
		};
		value = replaceDotsWithCommas(value);
		int i = value.indexOf(',');
		if (i < 0)
			returnValues[0] = value;
		else {
			returnValues[0] = value.substring(0, i);
			returnValues[1] = value.substring(i + 1);
		}
		return returnValues;
	}

	/**
	 * Formats a String to a decimal. Rounds the decimal to the given precision (number of decimals). Returns the decimal as string using , as the delimiter.
	 * @param value
	 * @param precision
	 * @return
	 */
	public static String formatToDecimal(String value, int precision) {
		value = removeWhitespace(value);
		value = replaceCommasWithDots(value);
		try {
			Double dvalue = Double.valueOf(value);
			BigDecimal d = BigDecimal.valueOf(dvalue);
			d = d.setScale(precision, BigDecimal.ROUND_HALF_UP);
			value = d.toString();
		} catch (NumberFormatException e) {
		}
		return replaceDotsWithCommas(value);
	}

	/**
	 * Removes all whitespace from the string, replaces commas with dots and tries to return a double
	 * @param value
	 * @return
	 * @throws NumberFormatException if value is not a correctly formated double
	 */
	public static Double formatToDouble(String value) throws NumberFormatException {
		value = removeWhitespace(value);
		value = replaceCommasWithDots(value);
		return Double.valueOf(value);
	}

	private static String replaceDotsWithCommas(String value) {
		value = value.replace("." , ",");
		return value;
	}

	private static String replaceCommasWithDots(String value) {
		value = value.replace("," , ".");
		return value;
	}

	/**
	 * Removes all characters that match regular expression "\\s"
	 * @param value
	 * @return
	 */
	public static String removeWhitespace(String value) {
		return value.replaceAll("\\s", "");
	}

	public static String formatToNumber(String value) {
		value = Utils.removeWhitespace(value);
		value = value.replace("+", "");
		return value;
	}

	private static final int BREAK1 = Character.codePointAt("\u00A0", 0);
	private static final int BREAK2 = Character.codePointAt("\u2007", 0);
	private static final int BREAK3 = Character.codePointAt("\u202F", 0);

	private static boolean isBreaking(char c) {
		if (c == BREAK1) return true;
		if (c == BREAK2) return true;
		if (c == BREAK3) return true;
		return false;
	}

	public static String removeWhitespaceAround(String value) {
		if (value == null) return null;
		if (value.isEmpty()) return value;
		StringBuilder b = new StringBuilder(value);
		while (b.length() > 0 && (Character.isWhitespace(b.charAt(0)) || isBreaking(b.charAt(0))))
			b.deleteCharAt(0); // delete from the beginning
		while (b.length() > 0 && (Character.isWhitespace(b.charAt(b.length() - 1)) || isBreaking(b.charAt(b.length() -1))))
			b.deleteCharAt(b.length() - 1); // delete from the end
		return b.toString();
	}

	/**
	 * Replaces &, <, >, ' and " with their HTML entities
	 * @param value
	 * @return
	 */
	public static String toHTMLEntities(String value) {
		if (value == null) return null;
		return com.google.common.html.HtmlEscapers.htmlEscaper().escape(value);
	}

	/**
	 * Calls toString() object of all the objets and writes the result to System.out.
	 * @param objects
	 */
	public static void debug(Object...objects) {
		System.out.println(debugS(objects));
	}

	/**
	 * Calls toString() object of all the objets and catenates the results together.
	 * @param objects
	 */
	public static String debugS(Object...objects) {
		if (objects == null) {
			return "null";
		}
		StringBuilder debug = new StringBuilder();
		for (Object o : objects) {
			if (o == null) o = "null";
			debug.append(" : ").append(o);
		}
		if (debug.length() < 1) return "";
		return debug.substring(3);
	}

	/**
	 * Prints contents of the map to System.out
	 * @param map
	 */
	public static void debug(Map<?, ?> map) {
		if (map == null) {
			System.out.println("null");
			return;
		}
		for (Map.Entry<?, ?> e : map.entrySet()) {
			System.out.println("" + e.getKey() + ":" + e.getValue());
		}
		System.out.println();
	}

	/**
	 * Prints contentes of the collection to System.out
	 * @param collection
	 */
	public static void debug(Collection<String> collection) {
		if (collection == null) {
			System.out.println("null");
			return;
		}
		for (String s : collection) {
			System.out.println(s);
		}
		System.out.println();
	}

	/**
	 * Returns the String that has the highest Integer value. Strings that are not integers are ignored. If no integers were given returns an empty String.
	 * @param values
	 * @return
	 */
	public static String maxOf(String... values) {
		if (values == null) return "";
		int max = Integer.MIN_VALUE;
		for (String v : values) {
			int i = Integer.MIN_VALUE;
			try {
				i = Integer.valueOf(v);
			} catch (NumberFormatException e) {
				continue;
			}
			max = Math.max(max, i);
		}
		if (max == Integer.MIN_VALUE) return "";
		return Integer.toString(max);
	}

	public static double medianOf(List<Double> values) {
		if (values == null || values.size() == 0) return 0.0;
		Collections.sort(values);
		//If n is odd then the median is x[(n-1)/2].
		//If n is even than the median is ( x[n/2] + x[(n/2)-1] ) / 2.
		if (even(values.size())) {
			double v1 = values.get((values.size()) / 2);
			double v2 = values.get((values.size() - 1) / 2);
			return avg(v1, v2);
		}
		// odd
		return values.get((values.size() - 1) / 2);
	}

	public static double avg(double v1, double v2) {
		return (v1 + v2) / 2;
	}

	public static boolean even(int i) {
		return i % 2 == 0;
	}

	public static boolean odd(int i) {
		return !even(i);
	}

	/**
	 * Returns the String that has the lowest Integer value. Strings that are not integers are ignored. If no integers were given returns an empty String.
	 * @param values
	 * @return
	 */
	public static String minOf(String... values) {
		if (values == null) return "";
		int min = Integer.MAX_VALUE;
		for (String v : values) {
			int i = Integer.MAX_VALUE;
			try {
				i = Integer.valueOf(v);
			} catch (NumberFormatException e) {
				continue;
			}
			min = Math.min(min, i);
		}
		if (min == Integer.MAX_VALUE) return "";
		return Integer.toString(min);
	}

	/**
	 * Returns the lowest integer value
	 * @param values
	 * @return
	 */
	public static int minOf(int... values) {
		if (values == null) throw new IllegalArgumentException("null");
		int min = Integer.MAX_VALUE;
		for (int i : values) {
			min = Math.min(min, i);
		}
		return min;
	}

	/**
	 * Returns the highest integer value
	 * @param values
	 * @return
	 */
	public static int maxOf(int... values) {
		if (values == null) throw new IllegalArgumentException("null");
		int max = Integer.MIN_VALUE;
		for (int i : values) {
			max = Math.max(max, i);
		}
		return max;
	}

	/**
	 * Returns the lowest double value
	 * @param values
	 * @return
	 */
	public static double minOf(double... values) {
		if (values == null) throw new IllegalArgumentException("null");
		double min = Double.MAX_VALUE;
		for (double i : values) {
			min = Math.min(min, i);
		}
		return min;
	}

	/**
	 * Returns the highest integer value
	 * @param values
	 * @return
	 */
	public static double maxOf(double... values) {
		if (values == null) throw new IllegalArgumentException("null");
		double max = Double.MIN_VALUE;
		for (double i : values) {
			max = Math.max(max, i);
		}
		return max;
	}

	/**
	 * Tells if value is withing target +/- something, for example
	 * 5 is within 4 +/- 1
	 * @param value
	 * @param target
	 * @param plusminus
	 * @return
	 */
	public static boolean fitsInRange(long value, long target, int plusminus) {
		return (Math.abs(value - target) <= plusminus);
	}

	/**
	 * Writes contents of the list to System.out
	 * @param list
	 */
	public static void debug(List<String> list) {
		if (list == null) {
			System.out.println("null");
			return;
		}
		for (String s : list) {
			System.out.println(s);
		}
	}

	/**
	 * Removes the last comma (or any char) from the StringBuilder
	 * @param query
	 */
	public static void removeLastChar(StringBuilder query) {
		if (query.length() < 1) return;
		query.deleteCharAt(query.length()-1);
	}

	/**
	 * SHA encrypts the given String. Adds a static salt to the String.
	 * If an exception occurs, returns the plaintext as it is and logs the exception.
	 * @param plaintext
	 * @return
	 */
	public static String encrypt(String plaintext) {
		if (plaintext.length() < 1) return plaintext;
		try {
			plaintext += STATIC_SALT;
			MessageDigest md = MessageDigest.getInstance("SHA");
			md.update(plaintext.getBytes("UTF-8"));
			byte raw[] = md.digest();
			String hash = new Base64().encodeAsString(raw);
			return hash;
		} catch (Exception e) {
			e.printStackTrace();
			return plaintext;
		}
	}

	/**
	 * Removes last char of the StringBuilder
	 * @param builder
	 */
	public static void removeLastNewline(StringBuilder builder) {
		if (builder.length() < 1) return;
		builder.delete(builder.length() - "\n".length(), builder.length());
	}

	/**
	 * Removes the last "AND" from StringBuilder (or actually the last "AND".length() characters)
	 * @param query
	 */
	public static void removeLastAnd(StringBuilder query) {
		if (query.length() < 1) return;
		query.delete(query.length() - "AND".length(), query.length());
	}

	/**
	 * Adds a list of statements to the query, seperating them with AND
	 * @param query
	 * @param statements
	 */
	public static void toAndSeperatedStatement(StringBuilder query, Collection<String> statements) {
		for (String s : statements) {
			query.append(" ").append(s).append(" AND");
		}
		removeLastAnd(query);
	}

	/**
	 * Adds a list of statements to the query, seperating them with a comma
	 * for example A, B, C
	 * @param query
	 * @param statements
	 */
	public static void toCommaSeperatedStatement(StringBuilder query, Collection<String> statements) {
		toCommaSeperatedStatement(query, statements, false);
	}

	/**
	 * Adds a list of statements to the query, seperating them with a comma. If surroundWithApos is true, also surrounds the statements with 's.
	 * For example 'A', 'B', 'C'
	 * @param query
	 * @param statements
	 * @param surroundWithApos
	 */
	public static void toCommaSeperatedStatement(StringBuilder query, Collection<String> statements, boolean surroundWithApos) {
		for (String s : statements) {
			if (surroundWithApos) s = "'" + s + "'";
			query.append(" ").append(s).append(" ,");
		}
		removeLastChar(query);
	}

	/**
	 * CGI encodes the given text
	 * @see java.net.URLEncoder
	 * @param text
	 * @return
	 */
	public static String urlEncode(String text) {
		if (text == null) return null;
		try {
			text = java.net.URLEncoder.encode(text, "UTF-8");
		} catch (UnsupportedEncodingException e) {
		}
		return text;
	}

	/**
	 * CGI decodes the given text
	 * @see java.net.URLDecoder
	 * @param text
	 * @return
	 */
	public static String urlDecode(String text) {
		if (text == null) return null;
		try {
			text = java.net.URLDecoder.decode(text, "UTF-8");
		} catch (UnsupportedEncodingException e) {
		}
		return text;
	}

	/**
	 * Calls toString() of the objects, catenates the results (space delimeter) and returns them.
	 * @param objects
	 * @return
	 */
	public static String catenate(Object... objects) {
		return catenate(" ", objects);
	}

	/**
	 * Calls toString() of the objects, catenates the results (space delimeter) and returns them.
	 * @param collection
	 * @return
	 */
	public static String catenate(Collection<?> collection) {
		return catenate(collection, " ");
	}

	/**
	 * Calls toString() of the objects, catenates the results (using the given delimeter) and returns them.
	 * @param collection
	 * @return
	 */
	public static String catenate(Collection<?> collection, String delimeter) {
		StringBuilder builder = new StringBuilder();
		Iterator<?> i = collection.iterator();
		while (i.hasNext()) {
			builder.append(i.next().toString().trim());
			if (i.hasNext()) builder.append(delimeter);
		}
		return builder.toString();
	}

	/**
	 * Calls toString() of the objects, catenates the results (using the given delimeter) and returns them.
	 * @param collection
	 * @return
	 */
	public static String catenate(String delimeter, Object... objects) {
		List<Object> list = new ArrayList<>(objects.length+1);
		for (Object o : objects) {
			list.add(o);
		}
		return catenate(list, delimeter);
	}

	/**
	 * Distance between two points
	 * @param pointA_X Point A - x
	 * @param pointA_Y Point A - y
	 * @param pointB_X Point B - x
	 * @param pointB_Y Point B - y
	 * @return sqrt( (ax-bx)^2 + (ay-by)^2 )
	 */
	public static double pythagoras(Double pointA_X, Double pointA_Y, Double pointB_X, Double pointB_Y) {
		return Math.sqrt(Math.pow(pointA_X - pointB_X, 2) + Math.pow(pointA_Y - pointB_Y, 2));
	}

	public static void close(ResultSet rs) {
		if (rs != null) try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void close(PreparedStatement p) {
		if (p != null) try {
			p.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void close(PreparedStatement p, ResultSet rs) {
		close(p);
		close(rs);
	}

	public static void close(PreparedStatement p, ResultSet rs, TransactionConnection con) {
		close(p);
		close(rs);
		close(con);
	}

	public static void close(PreparedStatement p, TransactionConnection con) {
		close(p);
		close(con);
	}

	public static void close(TransactionConnection con) {
		if (con != null) con.release();
	}

	/**
	 * Trims the String to a certain length. If length is > string.length() the String is returned as it is.
	 * @param string
	 * @param i
	 * @return
	 * @throws IllegalArgumentException if length is < 0.
	 */
	public static String trimToLength(String string, int i) {
		if (string == null) return null;
		if (i < 0) throw new IllegalArgumentException("Length must be a positive integer,");
		if (i > string.length()) return string;
		return string.substring(0, i);
	}

	/**
	 * Creates a List of the objects
	 * @param <T>
	 * @param contents
	 * @return
	 */
	@SafeVarargs
	public static <T> List<T> list(T ... contents) {
		List<T> list = new ArrayList<>();
		if (contents == null) return list;
		for (T t : contents) {
			list.add(t);
		}
		return list;
	}

	/**
	 * Returns union of the given list and the parameters. Will not alter the given list.
	 * @param c collection
	 * @param contents
	 * @return
	 */
	@SafeVarargs
	public static <T> List<T> list(List<T> c, T... contents) {
		List<T> list = list(contents);
		list.addAll(c);
		return list;
	}

	/**
	 * Creates a list that can only contain this one element. The list is immutable.
	 * @param value
	 * @return
	 */
	public static <T> List<T> singleEntryList(T value) {
		return Collections.singletonList(value);
	}

	/**
	 * Creates an iterable Collection of the objects.
	 * @param <T>
	 * @param contents
	 * @return
	 */
	@SafeVarargs
	public static <T> Iterable<T> iterable(T ... contents) {
		return collection(contents);
	}

	private static final Random						RANDOMIZER					= new Random();
	private static final String						TOKEN_CHARACTERS			= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	/**
	 * Generate a token with specified length using a default set of characters: A-Z, a-z, 0-9.
	 * @param length
	 * @return
	 */
	public static String generateToken(int length) {
		return generateToken(length, TOKEN_CHARACTERS);
	}

	/**
	 * Generate a token with specified length using a given set of charactes.
	 * @param length
	 * @param tokenCharacters
	 * @return
	 */
	public static String generateToken(int length, String tokenCharacters) {
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = tokenCharacters.charAt(RANDOMIZER.nextInt(tokenCharacters.length()));
		}
		return new String(text);
	}

	/**
	 *
	 * Returns 1 for 12345, 10 for 123450, 100 for 1234500 and so on.
	 * Returns 1 for 0.
	 * Used for example to get accuracy of coordinates, max( accuracyOf(6600400), accuracyOf(3455000) ) == 1000
	 * @param number
	 * @return
	 */
	public static int accuracyOf(int number) {
		if (number == 0) return 1;
		String value = Integer.toString(number);
		int i = 0;
		while (value.endsWith("0")) {
			value = value.substring(0, value.length()-1);
			i++;
		}
		return (int) Math.pow(10, i);
	}

	public static String toTSV(String ... strings) {
		StringBuilder b = new StringBuilder();
		for (String s : strings) {
			if (s == null) s = "";
			toTSV(b, s);
		}
		removeLastChar(b);
		return b.toString();
	}

	public static String toTSV(Collection<? extends Object> values) {
		StringBuilder b = new StringBuilder();
		for (Object o : values) {
			String s = (o == null) ? "" : o.toString();
			toTSV(b, s);
		}
		removeLastChar(b);
		return b.toString();
	}

	private static void toTSV(StringBuilder b, String s) {
		s = s.replace("\t", " ").replace("\r", " ").replace("\n", " ");
		while (s.contains("  ")) {
			s = s.replace("  ", " ");
		}
		s = s.trim();
		b.append(s).append("\t");
	}

	public static String toCSV(String ... strings) {
		StringBuilder b = new StringBuilder();
		for (String s : strings) {
			if (s == null) s = "";
			toCSV(b, s);
		}
		removeLastChar(b);
		return b.toString();
	}

	public static String toCSV(Collection<? extends Object> values) {
		StringBuilder b = new StringBuilder();
		for (Object o : values) {
			String s = (o == null) ? "" : o.toString();
			toCSV(b, s);
		}
		removeLastChar(b);
		return b.toString();
	}

	private static void toCSV(StringBuilder b, String s) {
		s = s.replace("\"", "\'").replace("\r", " ").replace("\n", " ");
		while (s.contains("  ")) {
			s = s.replace("  ", " ");
		}
		s = s.trim();
		b.append("\"").append(s).append("\",");
	}

	public static String removeLastChar(String value) {
		if (value.length() > 0) value = value.substring(0, value.length() - 1);
		return value;
	}

	/**
	 * Rounds double to a precision. Last decimal is rounded using normal rounding rules (<.5>)
	 * @param unrounded the double
	 * @param precision the number of decimal places, for example with precision 3: 1.111111 -> 1.111
	 * @return
	 */
	public static double round(double unrounded, int precision) {
		BigDecimal bd = new BigDecimal(unrounded);
		BigDecimal rounded = bd.setScale(precision, BigDecimal.ROUND_HALF_UP);
		return rounded.doubleValue();
	}

	/**
	 * Rounds int to a precision. Rounds to the closest precision, for example 199, 10 -> 200;  201, 10 -> 200
	 * @param unrounded
	 * @param precision
	 * @return
	 */
	public static long round(long unrounded, int precision) {
		return Math.round(unrounded / (double)precision) * precision;
	}

	public static double bearing(double start_lat, double start_lon, double dest_lat, double dest_lon) {
		double lat1 = Math.toRadians(start_lat);
		double lat2 = Math.toRadians(dest_lat);
		double difLon = Math.toRadians(dest_lon-start_lon);

		double y = Math.sin(difLon) * Math.cos(lat2);
		double x = Math.cos(lat1)*Math.sin(lat2) -
				Math.sin(lat1)*Math.cos(lat2)*Math.cos(difLon);
		double brng = Math.atan2(y, x);
		return (Math.toDegrees(brng) + 360) % 360;
	}

	private static final int	EARTH_RADIUS_KM	= 6371;

	public static double distance(double start_lat, double start_lon, double dest_lat, double dest_lon) {
		double difLat = Math.toRadians((start_lat-dest_lat));
		double difLon = Math.toRadians((start_lon-dest_lon));
		double lat1 = Math.toRadians(start_lat);
		double lat2 = Math.toRadians(dest_lat);

		double a = Math.sin(difLat/2) * Math.sin(difLat/2) +
				Math.sin(difLon/2) * Math.sin(difLon/2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = EARTH_RADIUS_KM * c;
		return distance;
	}

	public static String trimMultipleSpaces(String value) {
		if (value == null) return null;
		while (value.contains("  ")) {
			value = value.replace("  ", " ");
		}
		return value;
	}

	public static int randomNumber(int low, int high) {
		return new Random().nextInt(high-low) + low;
	}

	public static Collection<String> toCollection(String[] strings) {
		return Utils.collection(strings);
	}

	public static String getRequestURIAndQueryString(HttpServletRequest req) {
		String requestURI = req.getRequestURL().toString();
		if (req.getQueryString() != null) {
			requestURI += "?" + req.getQueryString();
		}
		return requestURI;
	}

	public static Map<String, String> map(KeyValuePair ... pairs) {
		Map<String, String> map = new LinkedHashMap<>();
		for (KeyValuePair pair : pairs) {
			map.put(pair.getKey(), pair.getValue());
		}
		return map;
	}

	public static KeyValuePair pair(String key, String value) {
		return new KeyValuePair(key, value);
	}

	public static int countOfNumberSequences(String value) {
		if (value == null) return 0;
		int countOfSequences = 0;
		boolean inNumberSequence = false;
		for (char c : value.toCharArray()) {
			if (Character.isDigit(c)) {
				if (!inNumberSequence) {
					inNumberSequence = true;
					countOfSequences++;
				}
			} else {
				if (inNumberSequence) {
					inNumberSequence = false;
				}
			}
		}
		return countOfSequences;
	}

	/**
	 * Do something with a timeout
	 * @param callable to execute
	 * @param timeout
	 * @param timeUnit
	 * @return results of callable
	 * @throws TimeoutException if timeout occurs
	 * @throws RuntimeException if some other exception occurs (original exception is wrapped)
	 */
	public static <T> T executeWithTimeOut(Callable<T> callable, int timeout, TimeUnit timeUnit) throws TimeoutException, RuntimeException {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<T> future = executor.submit(callable);
		try {
			return future.get(timeout, timeUnit);
		} catch (TimeoutException timeoutEx) {
			throw timeoutEx;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			executor.shutdownNow();
		}
	}

	public static String getWithTimeOut(URI uri, long timeout, TimeUnit unit) throws TimeoutException, RuntimeException {
		ExecutorService executor = null;
		TimeoutException timeoutException = null;
		Exception unknownException = null;
		try {
			executor = Executors.newSingleThreadExecutor();
			Future<String> future = executor.submit(new CallableHTTPClientLoader(uri));
			return future.get(timeout, unit);
		} catch (TimeoutException e) {
			timeoutException = e;
		} catch (Exception e) {
			unknownException = e;
		} finally {
			if (executor != null) executor.shutdownNow();
		}
		if (timeoutException != null) throw timeoutException;
		throw new RuntimeException("Unknown exception for " + uri, unknownException);
	}

	private static class CallableHTTPClientLoader implements Callable<String> {
		private final URI uri;
		public CallableHTTPClientLoader(URI uri) {
			this.uri = uri;
		}
		@Override
		public String call() throws Exception {
			HttpClientService client = null;
			try {
				client = new HttpClientService();
				return client.contentAsString(new HttpGet(uri));
			} finally {
				if (client != null) client.close();
			}
		}
	}

	public static boolean validateNonEmptyEmail(String email) {
		if (email == null || email.length() < 0) return false;
		if (!email.contains(".") || !email.contains("@") || email.contains(" ")) {
			return false;
		}
		if (Utils.countNumberOf("@", email) != 1) {
			return false;
		}
		String localPart = email.split("@")[0];
		String domain = email.split("@")[1];
		if (localPart.length() < 1 || domain.length() < 4) {
			return false;
		}
		if (localPart.startsWith(".") || localPart.endsWith(".") || domain.startsWith(".") || domain.endsWith(".")) {
			return false;
		}
		if (!domain.contains(".")) {
			return false;
		}
		return true;
	}

	public static String removeNumbersAroundChar(String string, char aroundChar) {
		if (string == null) return null;
		String rightSideHandled = "";
		boolean rightSideOfAroundChar = false;
		for (char c : string.toCharArray()) {
			if (c == aroundChar) {
				rightSideOfAroundChar = true;
			}
			if (rightSideOfAroundChar && Character.isDigit(c)) {
				// do nothing
			} else {
				rightSideHandled += c;
				if (c != aroundChar) {
					rightSideOfAroundChar = false;
				}
			}
		}
		String leftSideHandled = "";
		boolean leftSideOfAroundChar = false;
		for (int i = rightSideHandled.length()-1; i>=0; i--) {
			char c = rightSideHandled.charAt(i);
			if (c == aroundChar) {
				leftSideOfAroundChar = true;
			}
			if (leftSideOfAroundChar && Character.isDigit(c)) {
				// do nothing
			} else {
				leftSideHandled = c + leftSideHandled;
				if (c != aroundChar) {
					leftSideOfAroundChar = false;
				}
			}
		}
		String retVal = leftSideHandled.replace(Character.toString(aroundChar), "").trim();
		while (retVal.contains("  ")) {
			retVal = retVal.replace("  ", " ");
		}
		return retVal;
	}

	public static int countOfUTF8Bytes(String s) {
		return toUTF8Bytes(s).length;
	}

	private static byte[] toUTF8Bytes(String s) {
		try {
			return s.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String trimToByteLength(String s, int size) {
		if (s == null) return null;
		byte[] utf8 = toUTF8Bytes(s);
		while (utf8.length > size) {
			s = s.substring(0, s.length() - 10);
			utf8 = toUTF8Bytes(s);
		}
		return s;
	}

	public static boolean given(String s) {
		return s != null && s.trim().length() > 0;
	}

	public static String capitalizeName(String name) {
		if (name == null) return name;
		String capitalized = "";
		boolean capitalizeNext = true;
		for (char c : name.trim().toLowerCase().toCharArray()) {
			if (capitalizeNext) {
				capitalized += Character.toUpperCase(c);
				capitalizeNext = false;
				continue;
			}
			if (c == '-' || c == ' ' || c == '.') {
				capitalizeNext = true;
			}
			capitalized += c;
		}
		if (capitalized.startsWith("Von ")) {
			capitalized = "von" + capitalized.substring(3);
		}
		if (capitalized.startsWith("van ")) {
			capitalized = "van" + capitalized.substring(3);
		}
		return capitalized;
	}

}
