package fi.luomus.commons.utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class URIBuilder {

	private final String baseUri;
	private final Map<String, List<Object>> parameters = new LinkedHashMap<>();

	public URIBuilder(String baseUri) {
		if (baseUri.contains("?")) {
			String[] parts = baseUri.split(Pattern.quote("?"));
			this.baseUri = parts[0];
			for (String param : parts[1].split(Pattern.quote("&"))) {
				if (param.contains("=")) {
					String[] paramParts = param.split(Pattern.quote("="), -1);
					addParameter(paramParts[0], paramParts[1]);
				}
			}
		} else {
			this.baseUri = baseUri;
		}
	}

	@Override
	public String toString() {
		StringBuilder uri = new StringBuilder(baseUri);
		if (!parameters.isEmpty()) {
			uri.append("?");
		}
		for (Map.Entry<String, List<Object>> e : parameters.entrySet()) {
			for (Object o : e.getValue()) {
				uri.append(Utils.urlEncode(e.getKey())).append("=").append(Utils.urlEncode(o.toString())).append("&");
			}
		}
		String s = uri.toString();
		if (s.endsWith("&")) s = s.substring(0, s.length() - 1);
		return s;		
	}

	public URIBuilder replaceParameter(String key, Object value) {
		parameters.remove(key);
		return addParameter(key, value);
	}

	public URIBuilder addParameter(String key, Object value) {
		if (notGiven(key) || notGiven(value)) return this;
		if (!parameters.containsKey(key)) {
			parameters.put(key, new ArrayList<>());
		}
		parameters.get(key).add(value);
		return this;
	}

	private boolean notGiven(Object o) {
		return o == null || o.toString().length() < 1;
	}

	public URI getURI() throws URISyntaxException {
		return new URI(this.toString());
	}
}
