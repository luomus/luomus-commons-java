package fi.luomus.commons.utils;

import java.security.Key;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.codec.binary.Base64;

/**
 * This AESDecryptor is used to decrypt 256 bit AES with CBC encrypted messages.
 * Key, iv, and data parameters are received. Of these key and iv have been
 * RSA encrypted and needs to be decrypted using a RSA public key. All parameters
 * are base64decoded.
 * This class requires that "Java Cryptography Extension (JCE) Unlimited
 * Strength Jurisdiction Policy Files 6" -files have been installed. <br>
 * See: http://java.sun.com/javase/downloads/index.jsp
 */
public final class AESDecryptor {
	
	/**
	 * An exception that occured while trying to decrypt a message
	 */
	public static class AESDecryptionException extends Exception {
		private static final long	serialVersionUID	= 1L;
		
		public AESDecryptionException(String message) {
			super(message);
		}
	}
	
	/**
	 * An exception that occured while trying to initialize the decryptor
	 */
	public static class AESDecryptorInitializationException extends Exception {
		private static final long	serialVersionUID	= 1L;
		
		public AESDecryptorInitializationException(String message) {
			super(message);
		}
	}
	
	private final RSAPublicKey	rsaPubKey;
	private final String		dataCharset;
	
	/**
	 * Creates a new AESDecryptor, which can be used repeatedly to decrypt multiple messages
	 * @param rsaKey RSA key as string
	 * @param dataCharset Original charset of the messages that are decrypted
	 * @throws AESDecryptorInitializationException
	 */
	public AESDecryptor(String rsaKey, String dataCharset) throws AESDecryptorInitializationException {
		this.dataCharset = dataCharset;
		try {
			byte[] rsaKeyBytes = new Base64().decode(rsaKey);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(rsaKeyBytes);
			this.rsaPubKey = (RSAPublicKey) keyFactory.generatePublic(pubSpec);
		} catch (Exception e) {
			throw new AESDecryptorInitializationException("Failed to initialize AESDecryptor: " + e.getMessage());
		}
	}
	
	/**
	 * Creates a new AESDecryptor, which can be used repeatedly to decrypt multiple messages.
	 * Uses "UTF-8" as the default charset for decrypted messages.
	 * @param rsaKey RSA key as string
	 * @throws AESDecryptorInitializationException
	 */
	public AESDecryptor(String rsaKey) throws AESDecryptorInitializationException {
		this(rsaKey, "UTF-8");
	}
	
	/**
	 * Decrypts a message
	 * @param key
	 * @param iv
	 * @param data
	 * @return the message
	 * @throws AESDecryptionException if decryption fails: parameters are incorrect
	 */
	public String decrypt(String key, String iv, String data) throws AESDecryptionException {
		try {
			Base64 base64 = new Base64();
			Cipher cipher = Cipher.getInstance("RSA");
			
			// Unwrap 'key' with the RSA key into a Key object
			cipher.init(Cipher.UNWRAP_MODE, rsaPubKey);
			byte[] keyBytes = base64.decode(key);
			Key aesKey = cipher.unwrap(keyBytes, "AES", Cipher.SECRET_KEY);
			
			// Decrypt 'iv' with the RSA key into a IvParameterSpec object
			byte[] ivBytes = base64.decode(iv);
			cipher.init(Cipher.DECRYPT_MODE, rsaPubKey);
			byte[] ivDecrypted = cipher.doFinal(ivBytes);
			IvParameterSpec ivSpec = new IvParameterSpec(ivDecrypted);
			
			// Decrypt the data using the AES key and the initial vector
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] dataBytes = base64.decode(data);
			cipher.init(Cipher.DECRYPT_MODE, aesKey, ivSpec);
			byte[] decryptedDataBytes = cipher.doFinal(dataBytes);
			return new String(decryptedDataBytes, dataCharset);
			
		} catch (Exception e) {
			throw new AESDecryptionException("AES Decryption failed: " + LogUtils.buildStackTrace(e));
		}
	}
	
}
