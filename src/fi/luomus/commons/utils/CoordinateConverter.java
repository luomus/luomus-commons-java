package fi.luomus.commons.utils;

import fi.nls.common.grt.projections.euref.Ykjetrs;

import java.awt.geom.Point2D;

/**
 * Converts coordinates between EUREF-FIN (ETRS-TM35FIN), Uniform (KKJ yhtenäiskoordinaatisto), 
 * natural degrees (KKJ astekoordinaatisto) and decimal coordinates (KKJ desimaalikoordinaastio).
 * 
 * Conversion is done by calling convert() method. Before that, one type of coordinates must be set.
 * If no source coordinates are set, an IllegalArgumentException is thrown.
 */
public class CoordinateConverter {

	private Double	ykjLon;
	private Double	ykjLat;
	private Double	eurLon;
	private Double	eurLat;
	private Double	degreesLon;
	private Double	degreesLat;
	private Double	decimalLon;
	private Double	decimalLat;

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		if (ykjGiven()) {
			s.append("ykj: ").append(ykjLat).append(" : ").append(ykjLon).append("\n");
		}
		if (eurGiven()) {
			s.append("euref: ").append(eurLat).append(" : ").append(eurLon).append("\n");
		}
		if (decimalGiven()) {
			s.append("decimal: ").append(decimalLat).append(" : ").append(decimalLon).append("\n");
		}
		if (degreesGiven()) {
			s.append("degrees: ").append(degreesLat).append(" : ").append(degreesLon).append("\n");
		}
		return s.toString();
	}

	/**
	 * Does the conversion.
	 * @throws CoordinateConversionException if something unexpected goes wrong 
	 * @throws IllegalStateException if no source coordinates are given
	 */
	public void convert() throws CoordinateConversionException {
		try {
			if (ykjGiven()) {
				convert_Ykj_to_Decimal();
				convert_Decimal_to_Degrees();
				convert_Ykj_to_Eur();
			} else if (eurGiven()) {
				convert_Eur_to_Ykj();
				convert_Ykj_to_Decimal();
				convert_Decimal_to_Degrees();
			} else if (degreesGiven()) {
				convert_Degrees_to_Decimal();
				convert_Decimal_to_Ykj();
				convert_Ykj_to_Eur();
			} else if (decimalGiven()) {
				convert_Decimal_to_Degrees();
				convert_Decimal_to_Ykj();
				convert_Ykj_to_Eur();
			} else {
				throw new IllegalStateException("No source coordinates given");
			}
		} catch (IllegalStateException e) {
			throw e;
		}
		catch (Exception e) {
			throw new CoordinateConversionException(this.toString(), e);
		}
	}

	/**
	 * Useful for converting coordinates that can not be converted to finnish systems.
	 * @throws CoordinateConversionException if something unexpected goes wrong 
	 * @throws IllegalStateException if coverter initialization parameters are inconsistent with the convert request
	 */
	public void convertToDecimalsOnly() throws CoordinateConversionException {
		try {
			if (ykjGiven()) {
				convert_Ykj_to_Decimal();
			} else if (eurGiven()) {
				convert_Eur_to_Ykj();
				convert_Ykj_to_Decimal();
			} else if (degreesGiven()) {
				convert_Degrees_to_Decimal();
			} else if (decimalGiven()) {
				throw new IllegalArgumentException("Trying to convert from decimal to decimal");
			} else {
				throw new IllegalArgumentException("No source coordinates given");
			}
		} catch (IllegalStateException e) {
			throw e;
		}
		catch (Exception e) {
			throw new CoordinateConversionException(this.toString(), e);
		}
	}

	/**
	 * Useful for converting coordinates that can not be converted to finnish systems.
	 * @throws CoordinateConversionException if something unexpected goes wrong 
	 * @throws IllegalStateException if coverter initialization parameters are inconsistent with the convert request
	 */
	public void convertToDegreesOnly() throws CoordinateConversionException {
		try {
			if (ykjGiven()) {
				convert_Ykj_to_Decimal();
				convert_Decimal_to_Degrees();
			} else if (eurGiven()) {
				convert_Eur_to_Ykj();
				convert_Ykj_to_Decimal();
				convert_Decimal_to_Degrees();
			} else if (degreesGiven()) {
				throw new IllegalArgumentException("Trying to convert from degrees to degrees");
			} else if (decimalGiven()) {
				convert_Decimal_to_Degrees();
			} else {
				throw new IllegalArgumentException("No source coordinates given");
			}
		} catch (IllegalStateException e) {
			throw e;
		}
		catch (Exception e) {
			throw new CoordinateConversionException(this.toString(), e);
		}
	}

	private void convert_Decimal_to_Ykj() {
		Point2D yht_point = decimal2ykj(decimalLon, decimalLat);
		ykjLon = yht_point.getX();
		ykjLat = yht_point.getY();
	}

	private void convert_Degrees_to_Decimal() {
		decimalLon = degreers2decimal(degreesLon);
		decimalLat = degreers2decimal(degreesLat);
	}

	private void convert_Decimal_to_Degrees() {
		degreesLon = decimal2degrees(decimalLon);
		degreesLat = decimal2degrees(decimalLat);
	}

	private void convert_Eur_to_Ykj() {
		Point2D yht_point = Ykjetrs.inverseTransform(new Point2D.Double(eurLon, eurLat), null);
		ykjLon = yht_point.getX();
		ykjLat = yht_point.getY();
	}

	private void convert_Ykj_to_Eur() {
		Point2D euref_point = Ykjetrs.transform(new Point2D.Double(ykjLon, ykjLat), null);
		eurLon = euref_point.getX();
		eurLat = euref_point.getY();
	}

	private void convert_Ykj_to_Decimal() {
		Point2D des_point = ykj2decimal(ykjLon, ykjLat);
		decimalLon = des_point.getX();
		decimalLat = des_point.getY();
	}

	private boolean decimalGiven() {
		return decimalLon != null && decimalLat != null;
	}

	private boolean degreesGiven() {
		return degreesLon != null && degreesLat != null;
	}

	private boolean ykjGiven() {
		return ykjLon != null && ykjLat != null;
	}

	private boolean eurGiven() {
		return eurLon != null && eurLat != null;
	}

	public void setYht_pituus(String yht_pituus) {
		this.ykjLon = Double.valueOf(yht_pituus);
	}

	public Long getYht_pituus() {
		return Math.round(ykjLon);
	}

	public void setYht_leveys(String yht_leveys) {
		this.ykjLat = Double.valueOf(yht_leveys);
	}

	public Long getYht_leveys() {
		return Math.round(ykjLat);
	}

	public void setEur_pituus(String eur_pituus) {
		this.eurLon = Double.valueOf(eur_pituus);
	}

	public Long getEur_pituus() {
		return Math.round(eurLon);
	}

	public void setEur_leveys(String eur_leveys) {
		this.eurLat = Double.valueOf(eur_leveys);
	}

	public Long getEur_leveys() {
		return Math.round(eurLat);
	}

	public void setAst_pituus(String ast_pituus) {
		this.degreesLon = Double.valueOf(ast_pituus);
	}

	public Long getAst_pituus() {
		return Math.round(degreesLon);
	}

	public void setAst_leveys(String ast_leveys) {
		this.degreesLat = Double.valueOf(ast_leveys);
	}

	public Long getAst_leveys() {
		return Math.round(degreesLat);
	}

	public void setDes_pituus(String des_pituus) {
		this.decimalLon = Double.valueOf(des_pituus);
	}

	public Double getDes_pituus() {
		return decimalLon;
	}

	public void setDes_leveys(String des_leveys) {
		this.decimalLat = Double.valueOf(des_leveys);
	}

	public Double getDes_leveys() {
		return decimalLat;
	}

	public void setDes_pituus(Double des_pituus) {
		this.decimalLon = des_pituus;
	}

	public void setDes_leveys(Double des_leveys) {
		this.decimalLat = des_leveys;
	}

	/**
	 * Converts yht -> des
	 * @param ykjLon
	 * @param ykjLat
	 * @return decimal coordinates, x = lon, y = lat
	 * @author Hali-projects
	 */
	public static Point2D ykj2decimal(double ykjLon, double ykjLat) {
		double lon = ykjLon - 3500000.0;
		double lat = ykjLat * 1.570436964E-7;
		double fd = lat + 2.5295069E-3 * Math.sin(2.0 * lat) + 3.732401E-6 * Math.sin(4.0 * lat) + 7.543E-9 * Math.sin(6.0 * lat);
		double vd = Math.sqrt(1.0 + 6.7681702E-3 * Math.pow(Math.cos(fd), 2));
		lat = lon / 6399936.6081;
		double lon2 = Math.atan(vd / Math.cos(fd) * (Math.exp(lat) - Math.exp(-lat)) / 2.0);
		double fii = Math.atan(Math.tan(fd) * Math.cos(vd * lon2));
		double des_lon = 27.0 + lon2 * 57.29577951;
		double des_lat = fii * 57.29577951;
		return new Point2D.Double(des_lon, des_lat);
	}

	/**
	 * Converts dec -> yht
	 * @param desLon
	 * @param desLat
	 * @return yht coordinates, x = lon, y = lat
	 * @author Hali-projects
	 */
	public static Point2D decimal2ykj(double desLon, double desLat) {
		double fii = desLat / 57.295779513;
		double l = (desLon - 27.0) / 57.295779513;
		double fd2 = fii;
		double tanfii = Math.tan(fii);
		double fd1 = fd2 * 0.9;

		int failSafe = 0;
		while (Math.abs(fd2 - fd1) > 1.0E-06 && failSafe < 200) {
			fd1 = fd2;
			fd2 = Math.atan(tanfii / Math.cos(Math.sqrt(1.0 + 0.67681701972E-2 * Math.pow(Math.cos(fd1), 2)) * l));
			failSafe++;
		}

		double vd = Math.sqrt(1.0 + 0.67681701972E-2 * Math.pow(Math.cos(fd2), 2));
		double xx = Math.tan(l) * Math.cos(fd2) / vd;
		double fd = 57.295779513 * fd2;
		double pituusDbl = 6399936.6081 * Math.log(xx + Math.sqrt(Math.pow(xx, 2) + 1.0)) + 500000.0;
		double leveysDbl = 111136.53666 * fd - 16107.03468 * Math.sin(2.0 * fd2) + 16.97621 * Math.sin(4.0 * fd2) - 0.02227 * Math.sin(6.0 * fd2);
		return new Point2D.Double(pituusDbl + 3000000, leveysDbl);
	}

	/**
	 * Converts des -> ast
	 * @param decimalCoordinate coordinate
	 * @return ast coordinate
	 * @author Hali-projects
	 */
	public static Double decimal2degrees(double decimalCoordinate) {
		int sign = 1;
		if (decimalCoordinate < 0) sign = -1;
		decimalCoordinate = Math.abs(decimalCoordinate);

		double degrees = Math.floor(decimalCoordinate);
		double minutes = Math.floor(decimalCoordinate * 60) % 60;
		double seconds = Math.floor(decimalCoordinate * 3600) % 60;

		String degreeString = toFrontZeroString(degrees);
		String minuteString = toFrontZeroString(minutes);
		String secondsString = toFrontZeroString(seconds);

		return sign * Double.valueOf(degreeString + minuteString + secondsString);
	}

	private static String toFrontZeroString(double value) {
		if (value == 0) return "00";
		String string = Double.toString(value);
		string = removeDecimals(string);
		if (string.length() < 2) {
			string = "0" + string;
		}
		return string;
	}

	/**
	 * Converts ast -> des
	 * @param degreeCoordinate in ddmmss format, for example 310206
	 * @return
	 * @author Hali-projects
	 * @throws IllegalArgumentException if coordinate not in format ddmmss
	 */
	public static Double degreers2decimal(double degreeCoordinate) {
		boolean wasNegative = degreeCoordinate < 0;
		String degreeString = Double.toString(degreeCoordinate).replace("-", "");
		degreeString = removeDecimals(degreeString);
		degreeString = addFrontZerosIfNeeded(degreeString);

		int deg = Integer.parseInt(degreeString.substring(0, 3));
		int min = Integer.parseInt(degreeString.substring(3, 5));
		int sec = Integer.parseInt(degreeString.substring(5, 7));

		double decimal = 0.0;
		decimal += deg;
		decimal += min / 60.0;
		decimal += sec / 3600.0;
		if (wasNegative) decimal *= -1;
		return decimal;
	}

	private static String addFrontZerosIfNeeded(String degreeString) {
		while (degreeString.length() < 7) {
			degreeString = "0" + degreeString;
		}
		return degreeString;
	}

	private static String removeDecimals(String astString) {
		return astString.substring(0, astString.indexOf("."));
	}

	public static class CoordinateConversionException extends Exception {

		private static final long serialVersionUID = 4168838077109946926L;

		public CoordinateConversionException(String message, Exception e) {
			super(message, e);
		}

	}
}
