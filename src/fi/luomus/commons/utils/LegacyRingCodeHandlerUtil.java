package fi.luomus.commons.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LegacyRingCodeHandlerUtil {

	private static final char ZERO = '0';
	private static final char SPACE = ' ';
	private static final Map<String, Integer> AlphaSectionToNumberOfNumbersMapping = initAlphaSectionToNumberOfNumbersMapping();

	private static Map<String, Integer> initAlphaSectionToNumberOfNumbersMapping() {
		Map<String, Integer> map = new HashMap<>();
		map.put("AA", 5);
		map.put("BA", 5);
		map.put("BB", 5);
		map.put("BV", 5);
		map.put("E", 5);
		map.put("EZ", 5);
		map.put("EH", 5);
		map.put("F", 5);
		map.put("FL", 5);
		map.put("HH", 5);
		map.put("KT", 5);
		map.put("L", 5);
		map.put("LL", 5);
		map.put("M", 5);
		map.put("ML", 5);
		map.put("MM", 5);
		map.put("MT", 5);
		map.put("N", 5);
		map.put("NL", 5);
		map.put("RL", 5);
		map.put("RT", 5);
		map.put("T", 5);
		map.put("TL", 5);
		map.put("U", 5);
		map.put("UL", 5);
		map.put("Y", 5);
		map.put("YL", 5);
		map.put("GA", 5);
		map.put("W", 5);
		map.put("UX", 4);
		map.put("CR", 4);
		map.put("CE", 4);
		map.put("CA", 4);
		map.put("GS", 4);
		map.put("EJ", 4);
		map.put("HA", 5);
		map.put("HB", 5);
		map.put("HC", 5);
		map.put("HD", 5);
		map.put("HE", 5);
		map.put("HF", 5);
		map.put("HJ", 5);
		map.put("HK", 5);
		map.put("HM", 5);
		map.put("HN", 5);
		map.put("HP", 5);
		map.put("HR", 5);
		map.put("HU", 5);
		map.put("HV", 5);
		map.put("HW", 5);
		map.put("HY", 5);
		map.put("FF", 5);
		map.put("CX", 5);
		map.put("DX", 5);
		map.put("PA", 5);
		map.put("PT", 5);
		map.put("BT", 5);
		map.put("DT", 5);
		map.put("AI", 4);
		map.put("AQ", 4);
		map.put("BI", 4);
		map.put("BQ", 4);
		map.put("CI", 4);
		map.put("CQ", 4);
		map.put("DI", 4);
		map.put("DQ", 4);
		map.put("EI", 1);
		return map;
	}

	public static String dbToActual(String ring) {
		try {
			if (notGiven(ring)) {
				return "invalid:null";
			}

			String alphaSection = parseAlphaSection(ring);
			if (alphaSection.trim().length() < 1) throw new IllegalArgumentException();

			String numberSection = parseNumberSection(ring, countOfNumbers(alphaSection));

			if (alphaInEnd(alphaSection)) {
				return numberSection + alphaSection.charAt(0);
			}
			return (alphaSection + SPACE + numberSection).trim();

		} catch (Exception e) {
			return "invalid:" + ring;
		}
	}

	private static boolean notGiven(String ring) {
		return ring == null || ring.trim().length() < 1;
	}

	private static boolean alphaInEnd(String alphaSection) {
		return alphaSection.length() == 2 && alphaSection.charAt(1) == 'L' && !alphaSection.equals("ML");
	}

	private static int countOfNumbers(String alphaSection) {
		int countOfNumbers = 6;
		if (AlphaSectionToNumberOfNumbersMapping.containsKey(alphaSection)) {
			countOfNumbers = AlphaSectionToNumberOfNumbersMapping.get(alphaSection);
		}
		return countOfNumbers;
	}

	private static String parseNumberSection(String ring, int countOfNumbers) {
		String numberSection = "";
		for (char c : ring.toCharArray()) {
			if (Character.isDigit(c)) {
				numberSection += c;
			}
		}
		if (numberSection.length() < 1) throw new IllegalArgumentException();
		while (numberSection.length() > countOfNumbers && numberSection.charAt(0) == ZERO) {
			numberSection = numberSection.substring(1);
		}
		while (numberSection.length() < countOfNumbers) {
			numberSection = "0" + numberSection;
		}
		return numberSection;
	}

	private static String parseAlphaSection(String ring) {
		ring = ring + "  ";
		String alphaSection = "";
		for (char c : ring.substring(0, 2).toUpperCase().toCharArray()) {
			if (isAlpha(c)) alphaSection += c;
		}
		return alphaSection;
	}

	private static boolean notAlpha(char c) {
		return Character.isDigit(c);
	}

	private static boolean isAlpha(char c) {
		return Character.isLetter(c) || c == '+';
	}

	public static List<String> actualToDb(String ring) {
		if (countOfAlphas(ring) == 0) throw new IllegalArgumentException("Invalid ring: " + ring);
		if (ring.startsWith("invalid:")) {
			throw new IllegalArgumentException("Invalid ring: " + ring.replace("invalid:", ""));
		}

		if (alphaInEndActual(ring)) {
			String alphaSection = lastChar(ring) + "L";
			return actualToDb(alphaSection + ring.substring(0, ring.length() - 1));
		}

		String alphaSection = parseAlphaSection(ring);
		while (alphaSection.length() < 2) {
			alphaSection += SPACE;
		}

		String numberSection = parseNumberSection(ring, 7);
		numberSection = removeFrontZeros(numberSection);

		List<String> possibleValues = new ArrayList<>();
		String zeros = "";
		for (int i = numberSection.length(); i <= 7; i++) {
			possibleValues.add(alphaSection + spaces(7-i-1) + zeros + numberSection);
			zeros += "0";
		}
		return possibleValues;
	}

	private static int countOfAlphas(String ring) {
		int count = 0;
		for (char c : ring.toCharArray()) {
			if (Character.isLetter(c))
				count++;
		}
		return count;
	}

	private static String spaces(int count) {
		String spaces = "";
		while (spaces.length() <= count) {
			spaces += SPACE;
		}
		return spaces;
	}

	private static String removeFrontZeros(String numberSection) {
		while (numberSection.charAt(0) == ZERO) {
			numberSection = numberSection.substring(1);
			if (numberSection.length() == 0) return numberSection;
		}
		return numberSection;
	}

	private static char lastChar(String ring) {
		return ring.charAt(ring.length() -1);
	}

	private static boolean alphaInEndActual(String ring) {
		return notAlpha(ring.charAt(0)) && isAlpha(lastChar(ring));
	}

}
