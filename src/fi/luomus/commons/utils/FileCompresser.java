package fi.luomus.commons.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Can be used to compress files into a zip file.
 * Note: must call close() after creating an instance. This should be done
 * inside finally -clause of  try-finally block.
 */
public class FileCompresser {
	
	static final int BUFFER_SIZE = 2048;
	
	private final ZipOutputStream out;
	private final FileOutputStream fout;
	private final byte[] buffer = new byte[BUFFER_SIZE];
	
	/**
	 * Creates a new FileCompresser
	 * @param destination destination file to which files are zipped
	 * @throws FileNotFoundException 
	 */
	public FileCompresser(File destination) throws FileNotFoundException {
		this.fout = new FileOutputStream(destination);
		this.out = new ZipOutputStream(new BufferedOutputStream(fout));
	}
	
	/**
	 * Adds a file to be compressed into the zip. Note: must call close() once last file added.
	 * @param file to be compressed
	 * @return true if succeeds, false if not
	 * @throws FileNotFoundException if file given as parameter is not found
	 */
	public boolean addToZip(File file) throws FileNotFoundException {
		BufferedInputStream  origin = new BufferedInputStream(new FileInputStream(file), BUFFER_SIZE);
		ZipEntry entry = new ZipEntry(file.getName());
		try {
			if (file.getParentFile() != null) {
				file.getParentFile().mkdirs();
			}
			out.putNextEntry(entry);
			int count;
			while((count = origin.read(buffer, 0, BUFFER_SIZE)) != -1) {
				out.write(buffer, 0, count);
			}
		} catch (IOException e) {
			return false;
		} finally {
			try { origin.close(); } catch (IOException e) { e.printStackTrace(); } 
		}
		return true;
	}
	
	/**
	 * Finalizes compression
	 * @return the destination file
	 * @throws IOException 
	 * @throws IOException
	 */
	public void close(){
		try { out.close(); } catch (Exception e) { }
		try { fout.close(); } catch (Exception e) { }
	}
	
}
