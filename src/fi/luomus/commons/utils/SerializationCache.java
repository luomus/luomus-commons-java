package fi.luomus.commons.utils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class SerializationCache<K, V extends Serializable> {

	private static final int MIN_TIME_BETWEEN_SERIALIZATIONS_IN_SECONDS = 10;

	public interface Loader<K, V> {
		public V load(K key);
	}
	
	private final Map<K, V> cachedValues;
	private final File serializationFile;
	private final boolean silent;
	private long lastSerializedTimestamp = 0;
	private Loader<K, V> loader;
	
	public SerializationCache(String cacheName) {
		this(cacheName, true);
	}

	@SuppressWarnings("unchecked")
	public SerializationCache(String cacheName, boolean silent) {
		serializationFile = getSerializetionFileFor(cacheName);
		this.silent = silent;
		if (!silent) {
			System.out.println("Will cache to " + serializationFile.getAbsolutePath());
		}
		if (serializationFile.exists()) {
			try {
				cachedValues = (Map<K, V>) Serialization.load(serializationFile);
			} catch (Exception e) {
				if (!silent) {
					e.printStackTrace();
				}
				throw new SerializationException(e.getMessage());
			}
		} else {
			cachedValues = new HashMap<>();
		}
	}

	/**
	 * Get the value for key from cache. If it has been cached, the cached value is returned. If cache does not contain a value for the key, the value is loaded.
	 * @param key
	 * @return
	 */
	public V get(K key) {
		if (loader == null) throw new LoaderNotSetException();
		return getFromCacheOrLoad(key);
	}
	
	/**
	 * Force cache to serialize itself. Mostly useful after you are done using the chase (there won't be any more get requests) and want to serialize the not yet serialized values. 
	 */
	public void close() {
		serializeCache();
	}
	
	private V getFromCacheOrLoad(K key) {
		if (alreadyCached(key)) return getFromCache(key);
		V value = loadAndSerializeCache(key);
		return value;
	}

	private V loadAndSerializeCache(K key) {
		V value = loader.load(key);
		cachedValues.put(key, value);
		serializeIfShould();
		return value;
	}

	private void serializeIfShould() {
		if (shouldSerialize()) {
			serializeCache();
			lastSerializedTimestamp = DateUtils.getCurrentEpoch();
		}
	}

	private boolean shouldSerialize() {
		return DateUtils.getCurrentEpoch() - lastSerializedTimestamp > MIN_TIME_BETWEEN_SERIALIZATIONS_IN_SECONDS;
	}
	
	private V getFromCache(K key) {
		return cachedValues.get(key);
	}

	private boolean alreadyCached(K key) {
		return cachedValues.containsKey(key);
	}

	private void serializeCache() {
		try {
			Serialization.serialize(cachedValues, serializationFile);
		} catch (IOException e) {
			if (!silent) {
				e.printStackTrace();
			}
			throw new SerializationException(e.getMessage());
		}
	}

	public void setLoader(Loader<K, V> loader) {
		this.loader = loader;
	}

	public static class LoaderNotSetException extends RuntimeException {
		private static final long serialVersionUID = 1069294397644746741L;
	}

	public static class SerializationException extends RuntimeException {
		private static final long serialVersionUID = 2795203108172796140L;
		public SerializationException(String message) {
			super(message);
		}
	}

	public static File getSerializetionFileFor(String cacheName) {
		return new File(SerializationCache.class.getCanonicalName() + "_" + cacheName + ".bin");
	}

}
