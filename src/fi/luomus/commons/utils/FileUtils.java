package fi.luomus.commons.utils;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

public class FileUtils {

	/**
	 * Reads lines of a file
	 * @param file the file
	 * @return list of lines
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static List<String> readLines(File file) throws FileNotFoundException, IOException {
		return readLines(file, "UTF-8");
	}

	public static List<String> readLines(File file, String encoding) throws FileNotFoundException, IOException {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
			List<String> lines = new ArrayList<>();
			String line = null; 
			while ((line = reader.readLine()) != null) {
				lines.add(line);
			}
			return lines;
		} finally {
			if (reader != null) reader.close();
		}
	}
	
	/**
	 * Returns convents of the file as a String
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String readContents(File file) throws FileNotFoundException, IOException {
		List<String> lines = readLines(file);
		StringBuilder contents = new StringBuilder();
		for (String line : lines) {
			contents.append(line).append("\n");
		}
		Utils.removeLastNewline(contents);
		return contents.toString();
	}

	/**
	 * Returns contents of a file using Base64 coding. 
	 * @see http://tools.ietf.org/html/rfc4648
	 * @see Base64
	 * @param file
	 * @return
	 * @throws IOException 
	 * @throws Exception
	 */
	public static String readContentsAsBase64(File file) throws IOException {
		InputStream stream_in = null;
		try {
			stream_in = new FileInputStream(file);
			ByteBuffer fileContentsAsBytes = ByteBuffer.allocate( (int) file.length());
			byte[] buf = new byte[1024];
			int len;
			while ((len = stream_in.read(buf)) > 0) {
				fileContentsAsBytes.put(buf, 0, len);
			}
			return new Base64().encodeAsString(fileContentsAsBytes.array());
		} finally {
			if (stream_in != null) stream_in.close();
		}
	}
	
	/**
	 * Writes contents to file using UTF-8
	 * @param file
	 * @param contents
	 * @param append
	 * @throws IOException
	 */
	public static synchronized void writeToFile(File file, String contents, boolean append) throws IOException {
		writeToFile(file, contents, "UTF-8", append);
	}
	
	/**
	 * Writes contents to file using UTF-8 and will not append
	 * @param file
	 * @param contents
	 * @throws IOException
	 */
	public static synchronized void writeToFile(File file, String contents) throws IOException {
		writeToFile(file, contents, "UTF-8", false);
	}
	
	/**
	 * Writes contents to file, will not append
	 * @param file
	 * @param contents
	 * @param characterEncoding
	 * @throws IOException
	 */
	public static synchronized void writeToFile(File file, String contents, String characterEncoding) throws IOException {
		writeToFile(file, contents, characterEncoding, false);
	}
	
	/**
	 * Writes contents to file
	 * @param file
	 * @param contents
	 * @param characterEncoding
	 * @param append
	 * @throws IOException
	 */
	public static synchronized void writeToFile(File file, String contents, String characterEncoding, boolean append) throws IOException {
		OutputStream out = null;
		OutputStreamWriter writer = null;
		try {
			if (file.getParentFile() != null) {
				file.getParentFile().mkdirs();
			}
			out = new FileOutputStream(file, append);
			writer = new OutputStreamWriter(out, characterEncoding);
			writer.write(contents);
		} finally {
			if (writer != null) writer.close();
			if (out != null) out.close();
		}
	}
	
	/**
	 * Closes all streams given as a parameter.
	 * @param streams
	 */
	public static void close(Closeable ... streams) {
		if (streams == null) return;
		for (Closeable stream : streams) {
			try {
				if (stream != null) { 
					stream.close(); 
				} 
			} catch (IOException ioe) { }	
		}
	}

	/**
	 * Returns the extension of a file, for example for "my.file.txt" this returns "txt".
	 * @param file
	 * @return
	 */
	public static String getFileExtension(File file) {
		if (!file.getName().contains(".")) return "";
		String[] parts = file.getName().split("\\.");
		if (parts.length == 2 && file.getName().startsWith(".")) return "";
		String fileExtension = parts[parts.length-1];
		return fileExtension;
	}

}
