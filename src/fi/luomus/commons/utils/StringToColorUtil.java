package fi.luomus.commons.utils;

public class StringToColorUtil {

	private static class RGBColor {
		public int r;
		public int g;
		public int b;
	}
	
	public static String toRGBColor(String string) {
		RGBColor rgb = toRGB(string);
		return  rgb.r + "," + rgb.g + "," + rgb.b;
	}
	
	public static String toHex(String string) {
		RGBColor rgb = toRGB(string);
		return toHex(rgb);
	}
	
	private static RGBColor toRGB(String string) {
		RGBColor rgb = new RGBColor();
		rgb.r = toColor(string, 22);
		rgb.g = toColor(string, 353);
		rgb.b = toColor(string, 3533);
		return rgb;
	}

	private static int toColor(String string, int i) {
		if (string == null) return 255;
		return (Math.abs(new Double((Math.abs(string.hashCode()) / i)).hashCode()) % 100) + (255-100);
	}

	private static String toHex(RGBColor color) {
		return toHex(color.r, color.g, color.b);
	}
	
	private static String toHex(int r, int g, int b) {
		return toHex(r)+toHex(g)+toHex(b);
	}
	
	private static String toHex(int n) {
		n = Math.max(0,Math.min(n,255));
		char first = "0123456789ABCDEF".charAt((n-n%16)/16);
		char second = "0123456789ABCDEF".charAt(n%16);
		return "" + first + second;
	}

}
