package fi.luomus.commons.utils;

import java.util.concurrent.TimeUnit;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

/**
 * Wrapper for Caffeine Cache.
 * @param <K> Keys to use in the cache
 * @param <V> Values coming out from the cache
 */
public class SingleObjectCache<V> {

	/**
	 * CacheLoader is provided which is used to actually read an object.
	 * Further calls to that object then come from the Cache, unless expiration time has passed (and
	 * then the object is read again using this CacheLoader ASYNCHRONOUSLY)
	 * @param <K> key
	 * @param <V> value
	 * @see SingleObjectCache
	 */
	public interface CacheLoader<V> {
		public V load();
	}

	private final LoadingCache<String, V> cache;

	/**
	 * Creates a new object cache
	 * @param loader which is used to load the object for the first time and if it has expired
	 * @param expireTime
	 * @param timeunit of expireTime
	 */
	public SingleObjectCache(final CacheLoader<V> loader, long expireTime, TimeUnit timeunit) {
		cache = Caffeine.newBuilder().refreshAfterWrite(expireTime, timeunit).build(k->loader.load());
	}

	/**
	 * Get object from cache by it's key. If object has never been loaded or expire time has expired, the object will be reloaded.
	 * @param key 
	 * @return the object
	 */
	public V get() {
		return cache.get("NULL");
	}

	public V getForceReload() {
		cache.invalidateAll();
		return get();
	}

	public void invalidate() {
		cache.invalidateAll();
	}

}
