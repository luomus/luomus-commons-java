package fi.luomus.commons.utils;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

import fi.luomus.commons.containers.rdf.Qname;

public class MunicipalityCodeUtil {

	public static void main(String[] args) {
		try {
			Info i = MunicipalityCodeUtil.get("SAARI");
			System.out.println(i);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static class Info {
		public Qname id;
		public Qname birdAssociationAreaId;
		public String name;
		@Override
		public String toString() {
			return "Info [id=" + id + ", birdAssociationAreaId=" + birdAssociationAreaId + ", name=" + name + "]";
		}
	}

	public static Info get(String municipalityCode) throws Exception {
		return getMap().get(municipalityCode);
	}

	private static  Map<String, Info> map = null;

	private static Map<String, Info> getMap() throws Exception {
		if (map == null) {
			initMap();
		}
		return map;
	}

	private static void initMap() throws Exception {
		map = new HashMap<>();
		try (InputStream is = MunicipalityCodeUtil.class.getClassLoader().getResourceAsStream("fi/luomus/commons/utils/municipality-data.txt"); Scanner scan = new Scanner(is, "UTF-8")) {
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				if (line.startsWith("#") || line.trim().isEmpty()) continue;
				String[] parts = line.split(Pattern.quote("\t"));
				String code = parts[0];
				Info i = new Info();
				i.id = new Qname(parts[1]);
				i.birdAssociationAreaId = new Qname(parts[3]);
				i.name = parts[2];
				map.put(code, i);

			}
		}
	}

}
