package fi.luomus.commons.services;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ResponseData {

	private String viewName = "index";
	private String redirectLocation = null;
	private int redirectStatus = 302;
	private String defaultLocale = null;
	private boolean outputAlreadyPrinted = false;
	private final Map<Object, Object> datamodel = new HashMap<>();
	private String contentType = "text/html";
	private String response;

	public ResponseData() {}

	public ResponseData(String response, String contentType) {
		this.response = response;
		this.contentType = contentType;
	}

	public Map<Object, Object> getDatamodel() {
		return Collections.unmodifiableMap(datamodel);
	}

	public ResponseData setData(Object key, Object value) {
		datamodel.put(key, value);
		return this;
	}

	public String getRedirectLocation() {
		return redirectLocation;
	}

	public int getRedirectStatus() {
		return redirectStatus;
	}
	
	public ResponseData setRedirectLocation(String location) {
		this.redirectLocation = location;
		return this;
	}

	public ResponseData setRedirectStatus(int status) {
		this.redirectStatus = status;
		return this;
	}
	
	public boolean hasBeenRequestedToBeRedirected() {
		return redirectLocation != null;
	}

	public boolean outputAlreadyPrinted() {
		return outputAlreadyPrinted;
	}

	public ResponseData setOutputAlreadyPrinted() {
		outputAlreadyPrinted = true;
		return this;
	}

	public String getViewName() {
		return viewName;
	}

	public ResponseData setViewName(String viewName) {
		this.viewName = viewName;
		return this;
	}

	public boolean hasDefaultLocaleSet() {
		return defaultLocale != null;
	}

	public String getDefaultLocale() {
		return defaultLocale;
	}

	public ResponseData setDefaultLocale(String locale) {
		this.defaultLocale = locale;
		return this;
	}

	public String getContentType() {
		return contentType ;
	}

	public ResponseData setContentType(String contentType) {
		this.contentType = contentType;
		return this;
	}

	public boolean hasStreamContents() {
		return response != null;
	}

	public String getResponse() {
		return response;
	}

}
