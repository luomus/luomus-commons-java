package fi.luomus.commons.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.services.FileServlet.FileRequest.CONTENT_TYPE;
import fi.luomus.commons.utils.Utils;

/**
 * This service is used to access files. A token is given with each request and the file is shown only if this service has been
 * notified beforehand that a file can be shown to the user.  
 * 
 * Applications use this service's authorizeNewFileRequest() -method.
 * @see FileRequest
 */
public class FileServlet extends HttpServlet {
	
	private static final int	BUFFER_SIZE	= 1024;
	
	private static final long						serialVersionUID			= 4613152385945433498L;
	
	public  static final String						TOKEN						= "token";
	public static final int							TOKEN_LENGTH				= 32;
	
	private static final int						EXPIRATION_TIME_IN_SECONDS	= 600;
	private static final Map<String, FileRequest>	AUTHORIZED_FILE_REQUESTS	= new HashMap<>();
	
	/**
	 * A file request. FileServlet's authorizeNewFileRequest() method takes these as parameter.
	 * Contains the path, contenty type, charset information that FileServlet needs to print the contents of the file.
	 */
	public static class FileRequest {
		
		/**
		 * Supported content types.
		 */
		public static enum CONTENT_TYPE {
			PLAINTEXT, PDF, ZIP
		}
		
		public final String			fullPath;
		public final CONTENT_TYPE	contentType;
		public final String			charset;
		private final long			expires;
		
		public FileRequest(String fullpath, CONTENT_TYPE contentType) {
			this(fullpath, contentType, "UTF-8");
		}
		
		public FileRequest(String fullpath, CONTENT_TYPE contentType, String charset) {
			this.fullPath = fullpath;
			this.contentType = contentType;
			this.charset = charset;
			this.expires = currentTimestamp() + EXPIRATION_TIME_IN_SECONDS;
		}
		
		public boolean hasExpired() {
			return expires < currentTimestamp();
		}
	}
	
	/**
	 * Authorizes a new file request
	 * @param fileRequest
	 * @return the token. When the token is given as a parameter to FileServlet it will return the file defined by the filerequest to the client.
	 */
	public static String authorizeNewFileRequest(FileRequest fileRequest) {
		String token = Utils.generateToken(TOKEN_LENGTH);
		while (AUTHORIZED_FILE_REQUESTS.containsKey(token)) {
			token = Utils.generateToken(TOKEN_LENGTH);
		}
		AUTHORIZED_FILE_REQUESTS.put(token, fileRequest);
		return token;
	}
	
	
	
	/**
	 * Called when the servlet is initialized; either at server startup or when the servlet is called for the first time,
	 * depending on server configuration.
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}
	
	/**
	 * Called at server shutdown (etc)
	 */
	@Override
	public void destroy() {
		AUTHORIZED_FILE_REQUESTS.clear();
		super.destroy();
	}
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		process(req, res);
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		process(req, res);
	}
	
	/**
	 * @param req request
	 * @param res response
	 * @throws IOException if  res.getWriter() throws an exception 
	 * @throws IOException
	 */
	public void process(HttpServletRequest req, HttpServletResponse res) throws IOException  {
		removeExpiredAuthorizations();
		
		req.setCharacterEncoding("UTF-8");
		String token = req.getParameter(TOKEN);
		if (token == null || !AUTHORIZED_FILE_REQUESTS.containsKey(token)) {
			redirectTo404(res);
			return;
		}
		
		FileRequest fileRequest = AUTHORIZED_FILE_REQUESTS.get(token);
		File file = new File(fileRequest.fullPath);
		if (!file.exists()) {
			redirectTo404(res);
			return;
		}
		
		initResContentType(res, fileRequest);
		PrintWriter out = res.getWriter();
		if (fileRequest.contentType == CONTENT_TYPE.PLAINTEXT) {
			writePlainTextToOut(res, file, out, fileRequest.charset);
		} else {
			writeBytestreamToOut(res, file, out);
		}
		out.flush();
	}
	
	private void writeBytestreamToOut(HttpServletResponse res, File file, PrintWriter out) throws IOException {
		FileInputStream stream_in = null;
		try {
			stream_in = new FileInputStream(file);
			int c;
			while ( (c =stream_in.read()) != -1) {
				out.write(c);
			}
		} catch (IOException e) {
			e.printStackTrace();
			redirectTo404(res);
		} finally {
			if (stream_in != null) stream_in.close();
		}
	}

	private void writePlainTextToOut(HttpServletResponse res, File file, PrintWriter out, String charset) throws IOException {
		InputStreamReader stream_in = null;
		try {
			stream_in = new InputStreamReader(new FileInputStream(file), charset);
			char[] buffer = new char[BUFFER_SIZE];
			int i;
			while ( (i =stream_in.read(buffer)) != -1) {
				out.write(buffer, 0, i);
			}
		} catch (IOException e) {
			e.printStackTrace();
			redirectTo404(res);
		} finally {
			if (stream_in != null) stream_in.close();
		}
	}
	
	private void initResContentType(HttpServletResponse res, FileRequest fileRequest) {
		switch (fileRequest.contentType) {
			case PLAINTEXT:
				res.setContentType("text/plain; charset=" + fileRequest.charset);
				break;
			case PDF:
				res.setContentType("application/pdf");
				break;
			case ZIP:
				res.setContentType("application/zip");
				break;
			default:
				throw new IllegalArgumentException("No such content type defined: " + fileRequest.contentType);
		}
	}
	
	private void removeExpiredAuthorizations() {
		Iterator<FileRequest> iterator = AUTHORIZED_FILE_REQUESTS.values().iterator();
		while (iterator.hasNext()) {
			FileRequest request = iterator.next();
			if (request.hasExpired()) {
				iterator.remove();
			}
		}
	}
	
	private void redirectTo404(HttpServletResponse res) {
		res.setStatus(404);
		res.setHeader("Connection", "close");
	}
	
	public static long currentTimestamp() {
		Calendar k = Calendar.getInstance();
		return k.getTimeInMillis() / 1000;
	}
	
}
