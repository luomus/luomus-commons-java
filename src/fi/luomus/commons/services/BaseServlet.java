package fi.luomus.commons.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.freemarker.FreemarkerTemplateViewer;
import fi.luomus.commons.http.HttpStatus;
import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.languagesupport.LanguageFileReader;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.reporting.ErrorReporterViaEmailAndToSystemErr;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.session.SessionHandler;
import fi.luomus.commons.session.SessionHandlerImple;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.XMLWriter;

public abstract class BaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Object LOCK = new Object();

	private fi.luomus.commons.config.Config config = null;
	private ErrorReporter errorReporter  = null;
	private FreemarkerTemplateViewer viewer  = null;
	private static LocalizedTextsContainer localizedTextsContainer  = null;
	private static boolean initialized = false;
	private static long initializedTimestamp = DateUtils.getCurrentEpoch();

	protected abstract String configFileName();
	protected abstract void applicationInit();
	protected abstract void applicationInitOnlyOnce();
	protected abstract void applicationDestroy();

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			String configFile = resolveConfigFile();
			System.out.println(this.getClass().getCanonicalName() + " being initialized using configfile " + configFile);
			this.config = new ConfigReader(configFile);
			if (this.config.developmentMode()) {
				this.errorReporter = new ErrorReportingToSystemErr();
			} else {
				this.errorReporter = new ErrorReporterViaEmailAndToSystemErr(this.config);
			}
			if (this.config.defines(Config.TEMPLATE_FOLDER)) {
				try {
					viewer = new FreemarkerTemplateViewer(this.config.templateFolder());
				} catch (Exception e) {
					errorReporter.report("Freemarker init exception", e);
				}
			}
			initOnlyOnce();
			applicationInit();
		} catch (Exception initException) {
			throw new ServletException(initException);
		}
	}

	private String resolveConfigFile() {
		String base = System.getProperty("catalina.base");
		String fullPath = base + File.separator + "app-conf" + File.separator + configFileName();
		return fullPath;
	}

	private void initOnlyOnce() throws Exception {
		synchronized (LOCK) {
			if (initialized) return;
			initLocalizedTexts();
			applicationInitOnlyOnce();
			initialized = true;
			System.out.println(this.getClass().getCanonicalName() +": static content initialized.");
		}
	}

	private void initLocalizedTexts() throws ServletException {
		try {
			localizedTextsContainer = new LanguageFileReader(this.config).readUITexts();
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	@Override
	public void destroy() {
		applicationDestroy();
		super.destroy();
	}

	protected ResponseData notAuthorizedRequest(@SuppressWarnings("unused") HttpServletRequest req, @SuppressWarnings("unused") HttpServletResponse res) {
		throw new UnsupportedOperationException();
	}

	protected boolean authorized(@SuppressWarnings("unused") HttpServletRequest req) {
		return true;
	}

	protected abstract ResponseData processGet(HttpServletRequest req, HttpServletResponse res) throws Exception;
	protected abstract ResponseData processPost(HttpServletRequest req, HttpServletResponse res) throws Exception;
	protected abstract ResponseData processPut(HttpServletRequest req, HttpServletResponse res) throws Exception;
	protected abstract ResponseData processDelete(HttpServletRequest req, HttpServletResponse res) throws Exception;

	private interface ApplicationProcess {
		ResponseData process(HttpServletRequest req, HttpServletResponse res) throws Exception;
	}

	private final ApplicationProcess get = new ApplicationProcess() {
		@Override
		public ResponseData process(HttpServletRequest req, HttpServletResponse res) throws Exception {
			return processGet(req, res);
		}
	};

	private final ApplicationProcess post = new ApplicationProcess() {
		@Override
		public ResponseData process(HttpServletRequest req, HttpServletResponse res) throws Exception {
			return processPost(req, res);
		}
	};

	private final ApplicationProcess put = new ApplicationProcess() {
		@Override
		public ResponseData process(HttpServletRequest req, HttpServletResponse res) throws Exception {
			return processPut(req, res);
		}
	};

	private final ApplicationProcess delete = new ApplicationProcess() {
		@Override
		public ResponseData process(HttpServletRequest req, HttpServletResponse res) throws Exception {
			return processDelete(req, res);
		}
	};

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		process(get, req, res);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		process(post, req, res);
	}

	@Override
	public void doPut(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		process(put, req, res);
	}

	@Override
	public void doDelete(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		process(delete, req, res);
	}

	@Override
	public void doHead(HttpServletRequest req, HttpServletResponse res) {
		res.setStatus(200);
	}

	public void process(ApplicationProcess app, HttpServletRequest req, HttpServletResponse res) throws ServletException {
		if (config.developmentMode()) {
			initLocalizedTexts();
		}
		try {
			req.setCharacterEncoding("UTF-8");
			ResponseData responseData = null;

			if (authorized(req)) {
				responseData = app.process(req, res);
			} else {
				responseData = notAuthorizedRequest(req, res);
			}
			if (responseData.outputAlreadyPrinted()) {
				return;
			}
			if (responseData.hasBeenRequestedToBeRedirected()) {
				new HttpStatus(res).redirectTo(responseData.getRedirectLocation(), responseData.getRedirectStatus());
				return;
			}
			if (responseData.hasStreamContents()) {
				res.setContentType(responseData.getContentType() + "; charset=utf-8");
				res.setHeader("Access-Control-Allow-Origin", "*");
				PrintWriter out = res.getWriter();
				out.write(responseData.getResponse());
				out.flush();
				return;
			}

			showView(req, res, responseData);
		} catch (Exception e) {
			errorReporter.report(buildLogMessage(req), e);
			handleException(e, req, res);
		}
	}

	protected void handleException(Exception e, @SuppressWarnings("unused") HttpServletRequest req, @SuppressWarnings("unused") HttpServletResponse res) throws ServletException {
		throw new ServletException(e);
	}

	public LocalizedTextsContainer getLocalizedTexts() {
		return localizedTextsContainer;
	}

	private void showView(HttpServletRequest req, HttpServletResponse res, ResponseData responseData) throws Exception, IOException {
		String locale = getLocale(req, responseData);
		responseData.setData("text", localizedTextsContainer.getAllTexts(locale));
		responseData.setData("locale", locale);
		responseData.setData("baseURL", config.baseURL());
		responseData.setData("staticURL", config.staticURL());
		responseData.setData("staticContentTimestamp", initializedTimestamp);
		responseData.setData("inStagingMode", config.stagingMode());
		responseData.setData("inDevelopmentMode", config.developmentMode());
		responseData.setData("inProductionMode", config.productionMode());
		responseData.setData("page", responseData.getViewName());
		res.setContentType(responseData.getContentType() + "; charset=utf-8");
		viewer.viewPage(responseData.getViewName(), responseData.getDatamodel(), res.getWriter());
	}

	protected String readBody(HttpServletRequest req) throws IOException {
		BufferedReader reader = req.getReader();
		StringBuilder b = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			b.append(line);
		}
		return b.toString();
	}

	protected String getId(HttpServletRequest req) {
		String path = req.getPathInfo();
		if (path == null || path.equals("/")) {
			return "";
		}
		String qname = path.substring(path.lastIndexOf("/")+1);
		return qname;
	}

	protected List<Qname> getQnames(HttpServletRequest req) {
		List<Qname> qnames = new ArrayList<>();
		String qnamePart = getId(req);
		for (String part : qnamePart.split(Pattern.quote("+"))) {
			Qname qname = new Qname(part);
			if (qname.isSet()) {
				qnames.add(qname);
			}
		}
		return qnames;
	}

	private String getLocale(HttpServletRequest req, ResponseData responseData) {
		String locale = null;
		if (responseData.hasDefaultLocaleSet()) {
			locale = responseData.getDefaultLocale();
		} else {
			locale = getLocale(req);
		}
		return locale;
	}

	protected String getLocale(HttpServletRequest req) {
		String locale = req.getParameter("locale");
		if (locale == null || locale.trim().length() < 1) return "fi";
		if (!validLocale(locale)) return "fi";
		return locale.toLowerCase();
	}

	private boolean validLocale(String locale) {
		return config.supportedLanguages().contains(locale.toUpperCase());
	}

	protected ResponseData response(String response, String contentType) {
		return new ResponseData(response, contentType);
	}

	protected ResponseData xmlResponse(Document responseObject) {
		String xml = new XMLWriter(responseObject).generateXML();
		return response(xml, "application/xml");
	}

	protected ResponseData jsonResponse(String json)  {
		return response(json, "application/json");
	}

	protected ResponseData jsonResponse(JSONObject responseObject) {
		return response(responseObject.reveal().toString(), "application/json");
	}

	protected ResponseData jsonResponse(JSONArray responseArray) {
		return response(responseArray.reveal().toString(), "application/json");
	}

	protected ResponseData redirectTo(String location) {
		return new ResponseData().setRedirectLocation(location);
	}

	protected ResponseData redirectTo(String location, int status) {
		return new ResponseData().setRedirectLocation(location).setRedirectStatus(status);
	}

	protected ResponseData status404(HttpServletResponse res) {
		return status(404, res);
	}

	protected ResponseData status500(HttpServletResponse res) {
		return status(500, res);
	}

	protected ResponseData status403(HttpServletResponse res) {
		return status(403, res);
	}

	protected ResponseData status(int status, HttpServletResponse res) {
		new HttpStatus(res).status(status);
		return new ResponseData().setOutputAlreadyPrinted();
	}

	public ErrorReporter getErrorReporter() {
		return this.errorReporter;
	}

	public Config getConfig() {
		return this.config;
	}

	protected SessionHandler getSession(HttpServletRequest req) {
		return getSession(req, true);
	}

	protected SessionHandler getSession(HttpServletRequest req, boolean create) {
		HttpSession session = req.getSession(create);
		SessionHandler sessionHandler = new SessionHandlerImple(session, getConfig().systemId());
		return sessionHandler;
	}

	/**
	 * Log userid, method, uri and parameters of the request to log folder (defined in config)
	 * @param req
	 */
	protected void log(HttpServletRequest req) {
		String logMessage = buildLogMessage(req);
		LogUtils.write(logMessage, config.logFolder(), "access-log-");
	}

	protected String buildLogMessage(HttpServletRequest req) {
		String user = req.getRemoteUser();
		if (!given(user)) {
			SessionHandler session = getSession(req, false);
			if (session.hasSession()) {
				user = session.userId();
			}
		}
		StringBuilder logMessage = new StringBuilder();
		logMessage.append("[").append(user).append("] ");
		logMessage.append(req.getMethod()).append(" ").append(req.getRequestURI());
		appendParams(req, logMessage);
		return logMessage.toString();
	}

	protected static boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	private void appendParams(HttpServletRequest req, StringBuilder logMessage) {
		logMessage.append(" {");
		Enumeration<String> params = req.getParameterNames();
		while (params.hasMoreElements()) {
			String key = params.nextElement();
			String value = "";
			for (String v : req.getParameterValues(key)) {
				if (v.length() > 0) value += v.trim() + "|";
			}
			value = Utils.removeLastChar(value);
			if (value.length() > 0) {
				if (hidableParameter(key)) {
					value = "xxxxxxxx";
				}
				logMessage.append("\"").append(key).append("\"=\"").append(value).append("\" ");
			}
		}
		logMessage.append("}");
	}

	private boolean hidableParameter(String key) {
		if (key == null) return false;
		key = key.toLowerCase();
		if (key.contains("password")) return true;
		if (key.contains("token")) return true;
		return false;
	}

}
