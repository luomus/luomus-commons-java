package fi.luomus.commons.languagesupport;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import fi.luomus.commons.utils.Utils;

public class LocalizedTextsContainerImple implements LocalizedTextsContainer {

	private final HashMap<String, HashMap<String, String>>	texts;

	public LocalizedTextsContainerImple(String... languages) throws IllegalArgumentException {
		this(Utils.toCollection(languages));
	}

	public LocalizedTextsContainerImple(Collection<String> languages) throws IllegalArgumentException {
		texts = new HashMap<>();
		for (String language : languages) {
			if (language == null || language.length() < 1) throw new IllegalArgumentException("Empty language");
			language = language.toLowerCase();
			if (texts.containsKey(language)) throw new IllegalArgumentException("Same language twice: " + language);
			texts.put(language, new HashMap<String, String>());
		}
	}

	public void setText(String text, String value, String language) throws IllegalArgumentException {
		checkLanguageExistence(language);
		texts.get(language.toLowerCase()).put(text, value);
	}

	@Override
	public String getText(String text, String language) throws IllegalArgumentException {
		checkLanguageExistence(language);
		String s = texts.get(language.toLowerCase()).get(text);
		if (s == null) throw new IllegalArgumentException("No such text set: " + text + " : " + language);
		return s;
	}

	@Override
	public Map<String, String> getAllTexts(String language) throws IllegalArgumentException {
		if (language == null || language.length() < 1) throw new IllegalArgumentException("Empty language!");
		if (!texts.containsKey(language.toLowerCase())) return Collections.emptyMap();
		return texts.get(language.toLowerCase());
	}

	private void checkLanguageExistence(String language) {
		if (language == null || language.length() < 1) throw new IllegalArgumentException("Empty language!");
		if (!texts.containsKey(language.toLowerCase())) throw new IllegalArgumentException("Language not initialized: " + language);
	}

	@Override
	public boolean hasText(String text) {
		return texts.values().stream().allMatch(c->c.containsKey(text));
	}

}
