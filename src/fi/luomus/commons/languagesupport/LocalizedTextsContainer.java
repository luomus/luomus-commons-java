package fi.luomus.commons.languagesupport;

import java.util.Map;

/**
 * Contains texts in different languages.
 */
public interface LocalizedTextsContainer {

	/**
	 * Returns a single text for the language
	 * @param text name of the text
	 * @param language language
	 * @return
	 * @throws IllegalArgumentException if the texts or language is not found
	 */
	String getText(String text, String language) throws IllegalArgumentException;

	/**
	 * Returns all the texts for the language
	 * @param language
	 * @return
	 * @throws IllegalArgumentException if the text or language is not found
	 */
	Map<String, String> getAllTexts(String language) throws IllegalArgumentException;

	boolean hasText(String text);

}
