package fi.luomus.commons.languagesupport;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.Utils;

/**
 * Generates a LocalizedTextsContainer from a normal Java properties -file.
 * @see LocalizedTextsContainer
 * @see Properties
 * @author eopiirai
 *
 */
public class LanguageFileReader {

	private final File directory;
	private final Map<String, List<File>> languageFiles;
	private final Collection<String> languages;

	public LanguageFileReader(Config config) throws FileNotFoundException, UnsupportedOperationException {
		this(config.languagefileFolder(), languageFiles(config), config.supportedLanguages());
	}

	private static Collection<String> languageFiles(Config config) {
		if (config.defines(Config.LANGUAGE_FILES)) {
			return config.languageFiles();
		}
		return Utils.collection(config.languageFilePrefix());
	}

	public LanguageFileReader(String languagefileFolder, String languageFilePrefix, String... supportedLanguages) throws FileNotFoundException {
		this(languagefileFolder, Utils.collection(languageFilePrefix), Utils.toCollection(supportedLanguages));
	}

	public LanguageFileReader(String languagefileFolder, Collection<String> languageFiles, Collection<String> supportedLanguages) throws FileNotFoundException {
		this.languages = supportedLanguages;
		validateLanguages(this.languages);

		this.directory = new File(languagefileFolder);
		validateDirectory(this.directory);

		this.languageFiles = new HashMap<>(10);
		for (String lang : languages) {
			for (String languageFilename : languageFiles) {
				setAndValidateLanguageFile(languageFilename, lang);
			}
		}
	}

	private void setAndValidateLanguageFile(String filePrefix, String langCode) throws FileNotFoundException {
		File langfile = new File(this.directory.getAbsolutePath() + File.separator + filePrefix + langCode);
		if (!langfile.exists()) throw new FileNotFoundException(this.directory.getAbsolutePath() + File.separator + filePrefix + langCode + " does not exist");
		if (!languageFiles.containsKey(langCode)) {
			languageFiles.put(langCode, new ArrayList<File>(10));
		}
		languageFiles.get(langCode).add(langfile);
	}

	private void validateDirectory(File directory) throws FileNotFoundException {
		if (!directory.isDirectory()) throw new FileNotFoundException("Incorrect language file directory " + directory.getAbsolutePath());
	}

	private void validateLanguages(Collection<String> languages) {
		for (String lang : languages) {
			if (lang == null || lang.length() < 1) throw new IllegalArgumentException("Language has to be defined");
		}
	}

	public LocalizedTextsContainer readUITexts() throws Exception {
		LocalizedTextsContainerImple uiTexts = new LocalizedTextsContainerImple(languages);

		if (languageFiles.size() > 0)
			for (String lang : languages) {
				for (File file : languageFiles.get(lang)) {
					readTextsFromLangfileToUITexts(uiTexts, file, lang);
				}
			}

		testAllLanguagesHaveTheSameTexts(uiTexts);

		return uiTexts;
	}

	private void readTextsFromLangfileToUITexts(LocalizedTextsContainerImple uiTexts, File file, String lang) throws IOException {
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		Reader instream = null;
		Exception caused = null;
		try {
			fis = new FileInputStream(file);
			bis = new BufferedInputStream(fis);
			instream = new InputStreamReader(bis, "utf-8");
			Properties p = new Properties();
			p.load(instream);
			for (Map.Entry<Object, Object> e : p.entrySet()) {
				uiTexts.setText(trim(e.getKey()), trim(e.getValue()), lang);
			}
		} catch (Exception e) {
			caused = e;
		} finally {
			try {
				if (fis != null) fis.close();
			} catch (Exception e) {
			}
			try {
				if (bis != null) bis.close();
			} catch (Exception e) {
			}
			try {
				if (instream != null) instream.close();
			} catch (Exception e) {
			}
		}
		if (caused != null) { throw new IOException(caused.getMessage()); }
	}

	private String trim(Object o) {
		return o.toString().trim();
	}

	private void testAllLanguagesHaveTheSameTexts(LocalizedTextsContainerImple uiTexts) throws NoSuchFieldException {
		if (languages.size() > 1) 
			for (String lang : languages) {
				Map<String, String> compareToThis = uiTexts.getAllTexts(lang);
				for (Map.Entry<String, String> e : compareToThis.entrySet()) {
					for (String thisLang : languages) {
						try {
							uiTexts.getText(e.getKey(), thisLang);
						} catch (IllegalArgumentException iae) {
							throw new NoSuchFieldException("Field '" + e.getKey() + "' not found from '" + thisLang + "' -file.");
						}
					}
				}
			}
	}

}
