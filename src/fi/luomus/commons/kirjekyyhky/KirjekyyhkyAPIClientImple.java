package fi.luomus.commons.kirjekyyhky;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.util.EntityUtils;

import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;
import fi.luomus.commons.xml.XMLWriter;

public class KirjekyyhkyAPIClientImple implements KirjekyyhkyAPIClient {

	private final HttpClientService httpclient;
	private final String uri;
	private final String systemID;

	public KirjekyyhkyAPIClientImple(String uri, String username, String password, String systemID) throws URISyntaxException {
		this.uri = uri;
		this.httpclient = new HttpClientService(uri, username, password);
		this.systemID = systemID.toLowerCase();
	}

	@Override
	public void send(File xml, File pdf) throws Exception {
		if (xml == null || !xml.exists()) {
			throw new IOException("XML file does not exist");
		}
		if (pdf == null || !pdf.exists()) {
			throw new IOException("PDF file does not exist");
		}

		HttpPost request = new HttpPost(uri + "/post/create/" + systemID);

		HttpEntity requestEntity = MultipartEntityBuilder.create()
				.addBinaryBody("xml", xml, ContentType.create("application/xml", "UTF-8"), xml.getName())
				.addBinaryBody("prefilled-pdf", pdf)
				.build();

		request.setEntity(requestEntity);

		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(request);
			int status = response.getStatusLine().getStatusCode(); 
			if (status != 200) {
				throwException(response, status);
			}
		} finally {
			if (response != null) response.close();
			EntityUtils.consume(requestEntity);
		}
	}

	private void throwException(HttpResponse response, int status) {
		throw new RuntimeException("Uploading prefilled form failed. Status code: " + status + " statusline: " + response.getStatusLine());
	}

	@Override
	public void close() {
		httpclient.close();
	}

	@Override
	public Integer getCount() throws Exception {
		HttpGet request = new HttpGet(uri + "/get/count/" + systemID);
		Document document = httpclient.contentAsDocument(request);
		String count = document.getRootNode().getContents();
		return Integer.valueOf(count);
	}

	@Override
	public List<FormData> getForms() throws Exception {
		List<FormData> list = new LinkedList<>();

		HttpGet request = new HttpGet(uri + "/get/list/" + systemID);
		Document document = httpclient.contentAsDocument(request);
		if (document == null) {
			throw new IllegalStateException("Api not working");
		}
		Node root = document.getRootNode();
		checkFormTypeIsForThisSystem(root);
		for (Node formNode : document.getRootNode().getChildNodes()) {
			FormData form = new FormData(formNode.getAttribute("id"), systemID);
			List<String> owners = new ArrayList<>();
			for (Node owner : formNode.getNode("owners")) {
				owners.add(owner.getAttribute("name"));
			}
			form.setOwners(owners);
			Map<String, String> data = new HashMap<>();
			for (Node node : formNode) {
				if (!node.hasChildNodes()) {
					data.put(node.getName(), node.getContents());
				}
			}
			form.setData(data);
			list.add(form);
		}
		return list;
	}

	@Override
	public FormData get(String id) throws Exception {
		String xml = getXML(id);
		// System.out.println(xml);
		return XmlToFormDataUtil.readFormData(xml, systemID);
	}

	private String getXML(String id) throws IOException, ClientProtocolException {
		HttpGet request = new HttpGet(uri + "/get/show/" + systemID + "/" + id);
		String xml = httpclient.contentAsString(request);
		return xml;
	}

	private void checkFormTypeIsForThisSystem(Node root) {
		if (!root.getAttribute("type").equals(systemID)) {
			throw new IllegalStateException("Invalid form type (" + root.getAttribute("type") + ") for system: " + systemID);
		}
	}

	@Override
	public void receive(String id) throws Exception {
		HttpPost request = new HttpPost(uri + "/post/fetch/" + systemID + "/" + id);
		@SuppressWarnings("unused")
		String response = httpclient.contentAsString(request);
		// System.out.println("> response to receiving form id " + id + ": \n " + response);
	}

	@Override
	public void sendForCorrections(String id, String message) throws Exception {
		HttpPost request = new HttpPost(uri + "/post/update/" + systemID);

		String xml = this.getXML(id);
		Document document = new XMLReader().parse(xml);
		Node root = document.getRootNode();
		if (root.hasChildNodes("message")) {
			Node messageNode = root.getNode("message");
			messageNode.setContents(messageNode.getContents() + " \n " + message);
		} else {
			root.addChildNode("message").setContents(message);
		}
		xml = new XMLWriter(document).generateXML();

		HttpEntity entity = new StringEntity(xml, ContentType.create("application/xml", "UTF-8"));
		request.setEntity(entity);

		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(request);
			int status = response.getStatusLine().getStatusCode();
			response.close();
			if (status != 200) {
				throwException(response, status);
			}
		} finally {
			if (response != null) response.close();
			EntityUtils.consume(entity);
		}

	}

	@Override
	public void sendFormStructure(File formStructure, File javascript) throws Exception {
		if (formStructure == null || !formStructure.exists()) {
			throw new IOException("Form-structure file does not exist");
		}
		if (javascript != null && !javascript.exists()) {
			throw new IOException(javascript.getAbsolutePath() + " does not exist");
		}

		HttpPost request = new HttpPost(uri + "/post/structure");

		MultipartEntityBuilder builder = MultipartEntityBuilder.create()
				.addBinaryBody("xml", formStructure, ContentType.create("application/xml", "UTF-8"), formStructure.getName());
		if (javascript != null) {
			builder.addBinaryBody("js", javascript, ContentType.create("text/javascript", "UTF-8"), javascript.getName());
		}
		HttpEntity requestEntity = builder.build();
		request.setEntity(requestEntity);

		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(request);
			int status = response.getStatusLine().getStatusCode();
			if (status != 200) {
				throwException(response, status);
			}
		} finally {
			if (response != null) response.close();
			EntityUtils.consume(requestEntity);
		}
	}

}
