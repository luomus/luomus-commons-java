package fi.luomus.commons.kirjekyyhky;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.utils.DateUtils;

/**
 * A structured container object for Kirjekyyhky <form-data></form-data> -resource. Can be used as a container to be 
 * passed on to an utility that writes XML from this object or is returned by an unitility that reads XML.
 * @see XmlToFormDataUtil 
 */
public class FormData {
	
	/**
	 * Single log entry.
	 */
	public static class LogEntry {
		private final String user;
		private final String time;
		private final String action;
		
		public LogEntry(String user, Double timeInSeconds, String action) {
			this.user = user;
			this.time = DateUtils.format(timeInSeconds  * 1000, "dd.MM.yyyy HH:mm");
			this.action = action;
		}
		public String getUser() {
			return user;
		}
		public String getTime() {
			return time;
		}
		public String getAction() {
			return action;
		}
	}
	
	private final String formId;
	private final String systemID;
	private List<String> owners = new ArrayList<>();
	private List<String> titles;
	private Map<String, String> data = new HashMap<>();
	private final List<LogEntry> logEntries = new ArrayList<>(); 
	private boolean returnedEmpty = false;
	private final List<String> messages = new ArrayList<>();
	
	public FormData(String formId, String systemID) {
		this.formId = formId;
		this.systemID = systemID;
	}
	
	public String getId() {
		return formId;
	}
	
	public FormData setOwners(List<String> owners) {
		this.owners = owners;
		return this;
	}
	public List<String> getOwners() {
		return owners;
	}
	
	public FormData setTitles(List<String> titles) {
		this.titles = titles;
		return this;
	}
	public List<String> getTitles() {
		return titles;
	}
	
	public FormData setData(Map<String, String> data) {
		this.data = data;
		return this;
	}
	public Map<String, String> getData() {
		return data;
	}
	
	public String getLastUpdated() {
		String updated_at = getData().get("updated_at");
		if (updated_at == null || updated_at.length() < 1) return null;
		try {
			return DateUtils.format(Double.valueOf(updated_at) * 1000, "dd.MM.yyyy HH:mm");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void addLogEntry(LogEntry logEntry) {
		logEntries.add(logEntry);
	}
	
	public List<FormData.LogEntry> getLogEntries() {
		return logEntries;
	}
	public boolean returnedEmpty() {
		return returnedEmpty;
	}
	public void setReturnedEmpty(boolean value) {
		this.returnedEmpty = value;
	}
	public void addMessage(String message) {
		this.messages.add(message);
	}
	public List<String> getMessages() {
		return this.messages;
	}
	public String getSystemID() {
		return systemID;
	}
}
