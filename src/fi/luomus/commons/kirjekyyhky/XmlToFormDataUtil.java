package fi.luomus.commons.kirjekyyhky;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;

/**
 * Utility to parse Kirjekyyhky <form-data> XML to a container object.
 * @see FormData 
 */
public class XmlToFormDataUtil {

	public static class MalformedFormDataException extends Exception {
		private static final long	serialVersionUID	= -4358056338337891731L;
		public MalformedFormDataException(String msg) {
			super(msg);
		}
	}

	public static FormData readFormData(String xml, String systemId) throws MalformedFormDataException {
		Document document = null;
		try {
			document = new XMLReader().parse(xml);
		} catch (Exception e) {
			throw new MalformedFormDataException("Form data was malformed: " + xml);
		}
		Node formData = document.getRootNode();
		String id = null;
		try {
			id = formData.getAttribute("id");
		} catch (IllegalArgumentException e) { 
			id = "undefined";
		}

		Map<String, String> formDataMap = new HashMap<>();
		if (formData.hasChildNodes("data")) {
			for (Node node : formData.getNode("data")) {
				if (node.hasContents()) {
					formDataMap.put(node.getName(), node.getContents());
				}
			}
		}
		FormData form = new FormData(id, systemId).setData(formDataMap); 

		if (formData.hasAttribute("returned-empty")) {
			String attrValue = formData.getAttribute("returned-empty"); 
			if (attrValue.equalsIgnoreCase("yes")) { 
				form.setReturnedEmpty(true);
			}
		}

		if (document.getRootNode().hasChildNodes("log")) {
			for (Node node : document.getRootNode().getNode("log")) {
				String user = node.getAttribute("owner");
				Double time = Double.valueOf(node.getAttribute("time"));
				String action = node.getContents();
				form.addLogEntry(new FormData.LogEntry(user, time, action));
			}
		}

		if (formData.hasChildNodes("message")) {
			for (Node node : document.getRootNode().getChildNodes("message")) {
				String contents = node.getContents().trim();
				if (contents.length() > 0) {
					form.addMessage(node.getContents());
				}
			}
		}

		List<String> owners = new ArrayList<>();
		if (formData.hasChildNodes("owners")) {
			for (Node owner : formData.getNode("owners")) {
				owners.add(owner.getContents());
			}
			form.setOwners(owners);
		}
		
		return form;
	}

}
