package fi.luomus.commons.kirjekyyhky;

import java.io.File;
import java.util.List;

/**
 * This interface describes the features that Kirjekyyhky provides for the services that are using it.  
 */
public interface KirjekyyhkyAPIClient {

	/**
	 * Send a new prefilled form to Kirjekyyhky
	 * @param xml
	 * @param pdf
	 * @throws Exception
	 */
	public void send(File xml, File pdf) throws Exception;	
	
	/**
	 * Get count of forms that are waiting in the "send" -folder.  
	 * @return
	 * @throws Exception
	 */
	public Integer getCount() throws Exception;
	
	/**
	 * Return list of forms that are waiting in the "send" -folder.
	 * @see FormData
	 * @return
	 * @throws Exception
	 */
	public List<FormData> getForms() throws Exception;
	
	/**
	 * See contents of a form. (Will NOT move to "archive").
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public FormData get(String id) throws Exception;
	
	/**
	 * Mark a form received. Form is moved to "archive".
	 * @param id
	 * @throws Exception
	 */
	public void receive(String id) throws Exception;
	
	/**
	 * Must be called after creating an instance of this class. (Inside try {} finally {} -clause)
	 */
	public void close();

	/**
	 * Return a form for corrections. Will move to "for corrections" -folder with the given message.
	 * @param id
	 * @param message
	 * @throws Exception
	 */
	public void sendForCorrections(String id, String message) throws Exception;
	
	/**
	 * Update the <form-structure> of a certain form or add a new type of form (if none with the given form-type already exists).
	 * @param formStructure
	 * @param javascript
	 * @throws Exception
	 */
	public void sendFormStructure(File formStructure, File javascript) throws Exception;
	
}
