package fi.luomus.commons.containers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;

public class Image {

	private static final Map<Qname, Integer> TYPE_SORT_ORDER;
	static {
		TYPE_SORT_ORDER = new HashMap<>();
		int i = Integer.MAX_VALUE - 1000;
		TYPE_SORT_ORDER.put(new Qname("MM.typeEnumLive"), i++);
		TYPE_SORT_ORDER.put(new Qname("MM.typeEnumSpecimen"), i++);
		TYPE_SORT_ORDER.put(new Qname("MM.typeEnumGenitalia"), i++);
		TYPE_SORT_ORDER.put(new Qname("MM.typeEnumMicroscopy"), i++);
		TYPE_SORT_ORDER.put(new Qname("MM.typeEnumSkeletal"), i++);
		TYPE_SORT_ORDER.put(new Qname("MM.typeEnumCarcass"), i++);
		TYPE_SORT_ORDER.put(new Qname("MM.typeEnumHabitat"), i++);
		TYPE_SORT_ORDER.put(new Qname("MM.typeEnumLabel"), i++);
	}

	private final Qname id;
	private Qname source;
	private String fullURL;
	private String largeURL;
	private String thumbnailURL;
	private String squareThumbnailURL;
	private List<String> authors;
	private Qname licenseId;
	private String licenseAbbreviation;
	private LocalizedText licenseFullname;
	private String copyrightOwner;
	private String caption;
	private LocalizedText taxonDescriptionCaption;
	private Taxon taxon;
	private List<Qname> primaryForTaxon;
	private List<String> keywords;
	private Qname type;
	private Qname side;
	private List<Qname> sex;
	private List<Qname> lifeStage;
	private List<Qname> plantLifeStage;
	private String uploadDateTime;
	private String captureDateTime;
	private Integer sortOrder = Integer.MAX_VALUE;

	private Image(Image src) {
		this(src.id, src.fullURL);
		this.source = src.source;
		this.largeURL = src.largeURL;
		this.thumbnailURL = src.thumbnailURL;
		this.squareThumbnailURL = src.squareThumbnailURL;
		this.authors = src.authors;
		this.licenseId = src.licenseId;
		this.licenseAbbreviation = src.licenseAbbreviation;
		this.licenseFullname = src.licenseFullname;
		this.copyrightOwner = src.copyrightOwner;
		this.caption = src.caption;
		this.taxonDescriptionCaption = src.taxonDescriptionCaption;
		this.taxon = src.taxon;
		this.keywords = src.keywords;
		this.type = src.type;
		this.side = src.side;
		this.sex = src.sex;
		this.lifeStage = src.lifeStage;
		this.plantLifeStage = src.plantLifeStage;
		this.primaryForTaxon = src.primaryForTaxon;
		this.uploadDateTime = src.uploadDateTime;
		this.captureDateTime = src.captureDateTime;
		this.sortOrder = src.sortOrder;
	}

	public Image copy() {
		return new Image(this);
	}

	public Image(Qname id) {
		this(id, null, null);
	}

	public Image(Qname id, String fullURL) {
		this(id, null, fullURL);
	}

	public Image(Qname id, Qname sourceId, String fullURL) {
		this.id = id;
		this.source = sourceId;
		this.fullURL = fullURL;
	}

	public String getLargeURL() {
		return largeURL;
	}

	public Image setLargeURL(String largeURL) {
		this.largeURL = largeURL;
		return this;
	}

	public String getThumbnailURL() {
		return thumbnailURL;
	}

	public Image setThumbnailURL(String thumbnailURL) {
		this.thumbnailURL = thumbnailURL;
		return this;
	}

	public String getSquareThumbnailURL() {
		return squareThumbnailURL;
	}

	public Image setSquareThumbnailURL(String squareThumbnailURL) {
		this.squareThumbnailURL = squareThumbnailURL;
		return this;
	}

	public Image addAuthor(String author) {
		author = fixStrings(author);
		if (author == null || author.isEmpty()) return this;
		if (authors == null) authors = new ArrayList<>();
		if (!authors.contains(author)) {
			authors.add(author);
		}
		return this;
	}

	public String getAuthor() {
		if (authors == null || authors.isEmpty()) return null;
		Iterator<String> i = authors.iterator();
		String s = "";
		while (i.hasNext()) {
			s += i.next();
			if (i.hasNext()) s += ", ";
		}
		return s;
	}

	public Image addKeyword(String keyword) {
		if (keyword == null || keyword.isEmpty()) return this;
		if (keywords == null) keywords = new ArrayList<>();
		if (!keywords.contains(keyword)) keywords.add(keyword);
		return this;
	}

	public List<String> getKeywords() {
		if (keywords == null) return Collections.emptyList();
		return Collections.unmodifiableList(keywords);
	}

	public Qname getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(Qname licenseId) {
		this.licenseId = licenseId;
	}

	public String getLicenseAbbreviation() {
		return licenseAbbreviation;
	}

	public Image setLicenseAbbreviation(String licenseAbbreviation) {
		this.licenseAbbreviation = fixStrings(licenseAbbreviation);
		return this;
	}

	public LocalizedText getLicenseFullname() {
		if (licenseFullname == null) {
			licenseFullname = new LocalizedText();
		}
		return licenseFullname;
	}

	public Image setLicenseFullname(LocalizedText licenseFullname) {
		this.licenseFullname = licenseFullname;
		return this;
	}

	public String getCopyrightOwner() {
		return copyrightOwner;
	}

	public Image setCopyrightOwner(String copyrightOwner) {
		this.copyrightOwner = fixStrings(copyrightOwner);
		return this;
	}

	public Qname getId() {
		return id;
	}

	public Qname getSource() {
		return source;
	}

	public Image setSource(Qname sourceId) {
		this.source = sourceId;
		return this;
	}

	public String getFullURL() {
		return fullURL;
	}

	public Image setFullURL(String fullURL) {
		this.fullURL = fullURL;
		return this;
	}

	public String getCaption() {
		return caption;
	}

	public Image setCaption(String caption) {
		this.caption = fixStrings(caption);
		return this;
	}

	public LocalizedText getTaxonDescriptionCaption() {
		if (taxonDescriptionCaption == null) {
			taxonDescriptionCaption = new LocalizedText();
		}
		return taxonDescriptionCaption;
	}

	public Image setTaxonDescriptionCaption(LocalizedText taxonDescriptionCaption) {
		this.taxonDescriptionCaption = taxonDescriptionCaption;
		return this;
	}

	public Taxon getTaxon() {
		return taxon;
	}

	public Image setTaxon(Taxon taxon) {
		this.taxon = taxon;
		return this;
	}

	public Qname getType() {
		return type;
	}

	public Image setType(Qname type) {
		this.type = type;
		return this;
	}

	public Qname getSide() {
		return side;
	}

	public Image setSide(Qname side) {
		this.side = side;
		return this;
	}

	public Image addSex(Qname sex) {
		if (sex == null || !sex.isSet()) return this;
		if (this.sex == null) this.sex = new ArrayList<>();
		if (!this.sex.contains(sex)) this.sex.add(sex);
		return this;
	}

	public List<Qname> getSex() {
		if (sex == null) return Collections.emptyList();
		return Collections.unmodifiableList(sex);
	}

	public Image addLifeStage(Qname lifeStage) {
		if (lifeStage == null || !lifeStage.isSet()) return this;
		if (this.lifeStage == null) this.lifeStage = new ArrayList<>();
		if (!this.lifeStage.contains(lifeStage)) this.lifeStage.add(lifeStage);
		return this;
	}

	public List<Qname> getLifeStage() {
		if (lifeStage == null) return Collections.emptyList();
		return Collections.unmodifiableList(lifeStage);
	}

	public Image addPlantLifeStage(Qname plantLifeStage) {
		if (plantLifeStage == null || !plantLifeStage.isSet()) return this;
		if (this.plantLifeStage == null) this.plantLifeStage = new ArrayList<>();
		if (!this.plantLifeStage.contains(plantLifeStage)) this.plantLifeStage.add(plantLifeStage);
		return this;
	}

	public List<Qname> getPlantLifeStage() {
		if (plantLifeStage == null) return Collections.emptyList();
		return Collections.unmodifiableList(plantLifeStage);
	}

	public Image addPrimaryForTaxon(Qname taxonId) {
		if (taxonId == null || !taxonId.isSet()) return this;
		if (this.primaryForTaxon == null) this.primaryForTaxon = new ArrayList<>();
		if (!this.primaryForTaxon.contains(taxonId)) this.primaryForTaxon.add(taxonId);
		return this;
	}

	public boolean isPrimaryForTaxon() {
		if (getTaxon() == null) return false;
		if (this.primaryForTaxon != null && this.primaryForTaxon.contains(getTaxon().getId())) return true;
		return false;
	}

	public String getUploadDateTime() {
		return uploadDateTime;
	}

	public String getCaptureDateTime() {
		return captureDateTime;
	}

	public void setUploadDateTime(String uploadDateTime) {
		this.uploadDateTime = uploadDateTime;
	}

	public void setCaptureDateTime(String captureDateTime) {
		this.captureDateTime = captureDateTime;
	}

	public Integer getSortOrder() {
		if (isPrimaryForTaxon()) return 1;
		Integer orderByType = TYPE_SORT_ORDER.get(getType());
		if (orderByType == null && sortOrder != null) return sortOrder;
		if (orderByType != null && sortOrder == null) return orderByType;
		if (orderByType != null && sortOrder != null) {
			if (orderByType < sortOrder) return orderByType;
		}
		return sortOrder;
	}

	public Image setSortOrder(Integer sortOrder) {
		if (sortOrder == null) {
			this.sortOrder = Integer.MAX_VALUE;
		} else {
			this.sortOrder = sortOrder;
		}
		return this;
	}

	private static String fixStrings(String s) {
		if (s == null) return null;
		if (s.equals("null") || s.equals("null null")) return null;
		return s.trim();
	}

	private interface LegacyConversion {
		void convert(String tag, Image image);
	}

	private static LegacyConversion LEGACY_SPECIMEN = new LegacyConversion() {
		@Override
		public void convert(String keyword, Image image) {
			if (notGiven(image.getType())) image.setType(new Qname("MM.typeEnumSpecimen"));
		}
	};

	private static LegacyConversion LEGACY_LABEL = new LegacyConversion() {
		@Override
		public void convert(String keyword, Image image) {
			if (notGiven(image.getType())) image.setType(new Qname("MM.typeEnumLabel"));
		}
	};

	private static LegacyConversion LEGACY_LIVE = new LegacyConversion() {
		@Override
		public void convert(String keyword, Image image) {
			if (notGiven(image.getType())) image.setType(new Qname("MM.typeEnumLive"));
		}
	};

	private static LegacyConversion LEGACY_HABITAT = new LegacyConversion() {
		@Override
		public void convert(String keyword, Image image) {
			if (notGiven(image.getType())) image.setType(new Qname("MM.typeEnumHabitat"));
		}
	};

	private static LegacyConversion LEGACY_MICROSCOPY = new LegacyConversion() {
		@Override
		public void convert(String keyword, Image image) {
			if (notGiven(image.getType())) image.setType(new Qname("MM.typeEnumMicroscopy"));
		}
	};

	private static LegacyConversion LEGACY_SKELETAL = new LegacyConversion() {
		@Override
		public void convert(String keyword, Image image) {
			if (notGiven(image.getType())) image.setType(new Qname("MM.typeEnumSkeletal"));
		}
	};

	private static LegacyConversion LEGACY_CARCASS = new LegacyConversion() {
		@Override
		public void convert(String keyword, Image image) {
			if (notGiven(image.getType())) image.setType(new Qname("MM.typeEnumCarcass"));
		}
	};

	private static LegacyConversion LEGACY_LIFESTAGE = new LegacyConversion() {
		@Override
		public void convert(String keyword, Image image) {
			if (keyword.equals("adult")) image.addLifeStage(new Qname("MY.lifeStageAdult"));
			if (keyword.equals("larva")) image.addLifeStage(new Qname("MY.lifeStageLarva"));
			if (keyword.equals("nymph")) image.addLifeStage(new Qname("MY.lifeStageNymph"));
			if (keyword.equals("egg")) image.addLifeStage(new Qname("MY.lifeStageEgg"));
			if (keyword.equals("pupa")) image.addLifeStage(new Qname("MY.lifeStagePupa"));
		}
	};

	private static LegacyConversion LEGACY_SEX = new LegacyConversion() {
		@Override
		public void convert(String keyword, Image image) {
			if (keyword.equals("male")) image.addSex(new Qname("MY.sexM"));
			if (keyword.equals("female")) image.addSex(new Qname("MY.sexF"));
		}
	};

	private static LegacyConversion LEGACY_PRIMARY = new LegacyConversion() {
		@Override
		public void convert(String keyword, Image image) {
			if (image.getTaxon() == null) return;
			if (image.primaryForTaxon != null && !image.primaryForTaxon.isEmpty()) return;
			image.addPrimaryForTaxon(image.getTaxon().getId());
		}
	};

	private static final Map<String, LegacyConversion> LEGACY_CONVERSIONS;
	static {
		LEGACY_CONVERSIONS = new HashMap<>();
		LEGACY_CONVERSIONS.put("primary", LEGACY_PRIMARY);
		LEGACY_CONVERSIONS.put("specimen", LEGACY_SPECIMEN);
		LEGACY_CONVERSIONS.put("label", LEGACY_LABEL);
		LEGACY_CONVERSIONS.put("live", LEGACY_LIVE);
		LEGACY_CONVERSIONS.put("habitaatti", LEGACY_HABITAT);
		LEGACY_CONVERSIONS.put("adult", LEGACY_LIFESTAGE);
		LEGACY_CONVERSIONS.put("larva", LEGACY_LIFESTAGE);
		LEGACY_CONVERSIONS.put("nymph", LEGACY_LIFESTAGE);
		LEGACY_CONVERSIONS.put("egg", LEGACY_LIFESTAGE);
		LEGACY_CONVERSIONS.put("pupa", LEGACY_LIFESTAGE);
		LEGACY_CONVERSIONS.put("morfologia", LEGACY_MICROSCOPY);
		LEGACY_CONVERSIONS.put("anatomia", LEGACY_MICROSCOPY);
		LEGACY_CONVERSIONS.put("skeletal", LEGACY_SKELETAL);
		LEGACY_CONVERSIONS.put("male", LEGACY_SEX);
		LEGACY_CONVERSIONS.put("female", LEGACY_SEX);
		LEGACY_CONVERSIONS.put("carcass", LEGACY_CARCASS);
	}

	private boolean legacyConversionsDone = false;

	public Image doLegacyConversions() {
		if (legacyConversionsDone) return this;
		for (String keyword : getKeywords()) {
			doLegacyConversions(keyword);
		}
		legacyConversionsDone = true;
		return this;
	}

	private void doLegacyConversions(String keyword) {
		if (keyword == null) return;
		keyword = keyword.toLowerCase();
		LegacyConversion conversion = LEGACY_CONVERSIONS.get(keyword);
		if (conversion != null) {
			conversion.convert(keyword, this);
		}
	}

	private static boolean notGiven(Qname qname) {
		return qname == null || !qname.isSet();
	}

	@Override
	public String toString() {
		return "Image [fullURL=" + fullURL + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Image other = (Image) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
