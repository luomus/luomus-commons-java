package fi.luomus.commons.containers;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import fi.luomus.commons.containers.rdf.Qname;

public class InformalTaxonGroup implements Comparable<InformalTaxonGroup> {

	private final Qname qname;
	private final LocalizedText name;
	private final Set<Qname> subGroups = new HashSet<>(3);
	private final Set<Qname> parents  = new HashSet<>(3);
	private final int order;
	private boolean isExplicitlyDefinedRoot = false;
	
	public InformalTaxonGroup() {
		this(null, null, Integer.MAX_VALUE);
	}

	public InformalTaxonGroup(Qname qname, LocalizedText name, int order) {
		this.qname = qname == null ? new Qname("") : qname;
		this.name = name == null ? new LocalizedText() : name;
		this.order = order;
	}

	@Override
	public String toString() {
		return "InformalTaxonGroup [qname=" + qname + ", name=" + name + "]";
	}

	public LocalizedText getName() {
		return name;
	}

	public String getName(String locale) {
		return name.forLocale(locale);
	}

	public Qname getQname() {
		return qname;
	}

	public Set<Qname> getSubGroups() {
		return Collections.unmodifiableSet(subGroups);
	}

	public boolean hasSubGroup(String groupQname) {
		return hasSubGroup(new Qname(groupQname));
	}

	public boolean hasSubGroup(Qname groupId) {
		return subGroups.contains(groupId);
	}

	public InformalTaxonGroup addSubGroup(Qname groupId) {
		if (groupId != null && groupId.isSet()) {
			subGroups.add(groupId);
		}
		return this;
	}

	public Set<Qname> getParents() {
		return Collections.unmodifiableSet(parents);
	}

	public InformalTaxonGroup addParent(Qname parentId) {
		this.parents.add(parentId);
		return this;
	}

	public boolean hasParents() {
		return !parents.isEmpty();
	}

	public boolean hasParent(Qname parentId) {
		return parents.contains(parentId);
	}

	@Override
	public int hashCode() {
		return this.qname.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InformalTaxonGroup other = (InformalTaxonGroup) obj;
		return this.qname.equals(other.qname);
	}

	@Override
	public int compareTo(InformalTaxonGroup o) {
		int c = Integer.compare(this.getOrder(), o.getOrder());
		if (c != 0) return c;
		String thisName = this.getName("fi") == null ? "Ö" : this.getName("fi");
		String otherName = o.getName("fi") == null ? "Ö" : o.getName("fi");		
		return thisName.compareTo(otherName);
	}

	public boolean isRoot() {
		if (isExplicitlyDefinedRoot()) return true;
		return parents.isEmpty();
	}

	public int getOrder() {
		return order;
	}

	public boolean isExplicitlyDefinedRoot() {
		return isExplicitlyDefinedRoot;
	}

	public InformalTaxonGroup setExplicitlyDefinedRoot(boolean isExplicitlyDefinedRoot) {
		this.isExplicitlyDefinedRoot = isExplicitlyDefinedRoot;
		return this;
	}

}
