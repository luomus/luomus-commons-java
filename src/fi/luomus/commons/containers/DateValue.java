package fi.luomus.commons.containers;

import fi.luomus.commons.utils.DateUtils;


/**
 * Utility container for holding simple dates.
 */
public class DateValue {

	private final String year;
	private final String month;
	private final String day;
	
	public DateValue(Object day, Object month, Object year) {
		this.year = handleNull(year);
		this.month = handleNull(month);
		this.day = handleNull(day);
	}
	
	public static DateValue getEmptyDate() {
		return new DateValue("", "", "");
	}
	
	@Override
	public String toString() {
		return DateUtils.catenateDateString(day, month, year);
	}
	
	public String toString(String dateFormat) {
		return DateUtils.format(this, dateFormat);
	}
	
	public String toIsoString() {
		return DateUtils.catenateIsoDateString(year, month, day);
	}
	
	private String handleNull(Object value) {
		if (value == null) return "";
		return value.toString();
	}

	public String getYear() {
		return year;
	}

	public String getMonth() {
		return month;
	}

	public String getDay() {
		return day;
	}
	
	public int getYearAsInt() {
		return Integer.valueOf(year);
	}
	
	public int getMonthAsInt() {
		return Integer.valueOf(month);
	}
	
	public int getDayAsInt() {
		return Integer.valueOf(day);
	}
	
	/**
	 * For backwards compability. Parameter can be either "yyyy", "mm" or "dd". Anything else throws IllegalArgumentException.
	 * @param value
	 * @return
	 * @deprecated
	 * @throws IllegalArgumentException if parameter is not one of "yyyy", "mm" or "dd".
	 */
	@Deprecated
	public String get(String value) {
		if (value.equals("yyyy")) {
			return year;
		} 
		if (value.equals("mm")) {
			return month;
		}
		if (value.equals("dd")) {
			return day;
		}
		throw new IllegalArgumentException("Illegal date field value: " + value);
	}
	
	/**
	 * No values are set.
	 * @return
	 */
	public boolean isEmpty() {
		return notSet(day) && notSet(month) && notSet(year);
	}
	
	/**
	 * At least one of the values is not set.
	 * @return
	 */
	public boolean hasEmptyFields() {
		return notSet(day) || notSet(month) || notSet(year);
	}
	
	private boolean notSet(String value) {
		return value.length() < 1;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof DateValue) {
			DateValue other = (DateValue) o; 
			return this.toString().equals(other.toString());
		}
		throw new UnsupportedOperationException("Can only compare to " + this.getClass().getName());
	}
	
	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}
	
}
