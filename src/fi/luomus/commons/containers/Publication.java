package fi.luomus.commons.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class Publication implements Comparable<Publication> {

	private Qname qname;
	private String citation = "";
	private String URI = "";

	public Publication(Qname qname) {
		this.qname = qname == null ? new Qname("") : qname;
	}

	public void setQname(Qname qname) {
		this.qname = qname == null ? new Qname("") : qname;
	}
	
	public Qname getQname() {
		return qname;
	}

	public String getCitation() {
		return citation;
	}

	public Publication setCitation(String citation) {
		if (citation == null) citation = "";
		this.citation = citation;
		return this;
	}

	public String getURI() {
		return URI;
	}

	public Publication setURI(String URI) {
		if (URI == null) URI = "";
		this.URI = URI;
		return this;
	}

	@Override
	public int compareTo(Publication o) {
		return this.getCitation().compareTo(o.getCitation());
	}

}
