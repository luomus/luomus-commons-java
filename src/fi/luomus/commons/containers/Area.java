package fi.luomus.commons.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class Area {

	private final Qname qname;
	private final Qname type;
	private final LocalizedText name;
	private String abbreviation;
	private Qname partOf;

	public Area(Qname qname, LocalizedText name, Qname type) {
		if (qname == null || !qname.isSet()) throw new IllegalArgumentException("Id not given"); 
		this.qname = qname;
		this.name = name;
		this.type = type;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public Area setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
		return this;
	}

	public Qname getPartOf() {
		return partOf;
	}

	public Area setPartOf(Qname partOf) {
		this.partOf = partOf;
		return this;
	}

	public Qname getQname() {
		return qname;
	}

	public Qname getType() {
		return type;
	}

	public LocalizedText getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return qname.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Area other = (Area) obj;
		return this.qname.equals(other.qname);
	}

	@Override
	public String toString() {
		return "Area [qname=" + qname + ", type=" + type + ", name=" + name + ", abbreviation=" + abbreviation + ", partOf=" + partOf + "]";
	}

}
