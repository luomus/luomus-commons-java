package fi.luomus.commons.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class TaxonSet implements Comparable<TaxonSet> {

	private final Qname qname;
	private final LocalizedText name;

	public TaxonSet() {
		this(null, null);
	}

	public TaxonSet(Qname qname, LocalizedText name) {
		this.qname = qname == null ? new Qname("") : qname;
		this.name = name == null ? new LocalizedText() : name;
	}

	@Override
	public String toString() {
		return "TaxonSet [qname=" + qname + ", name=" + name + "]";
	}

	public LocalizedText getName() {
		return name;
	}

	public String getName(String locale) {
		return name.forLocale(locale);
	}

	public Qname getQname() {
		return qname;
	}

	@Override
	public int hashCode() {
		return this.qname.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxonSet other = (TaxonSet) obj;
		return this.qname.equals(other.qname);
	}

	@Override
	public int compareTo(TaxonSet o) {
		String thisName = this.getName("fi") == null ? "Ö" : this.getName("fi");
		String otherName = o.getName("fi") == null ? "Ö" : o.getName("fi");
		return thisName.compareTo(otherName);
	}

}
