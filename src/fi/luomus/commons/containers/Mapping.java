package fi.luomus.commons.containers;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.Mapping.Pair;

public class Mapping<T> implements Iterable<Pair<T>> {
	
	public static class Pair<T> {
		private final T value1;
		private final T value2;
		public Pair(T value1, T value2) {
			this.value1 = value1;
			this.value2 = value2;
		}
		public T getValue1() {
			return value1;
		}
		public T getValue2() {
			return value2;
		}
	}
	
	private final Map<T, T> map = new LinkedHashMap<>();
	private final Map<T, T> inverseMap = new LinkedHashMap<>();
	
	public void map(T value1, T value2) throws AlreadyMappedException {
		if (map.containsKey(value1)) {
			throw new AlreadyMappedException(value1 + " is already mapped to " + map.get(value1));
		}
		if (inverseMap.containsKey(value2)) {
			throw new AlreadyMappedException(value2 + " is already mapped to " + inverseMap.get(value2));
		}
		map.put(value1, value2);
		inverseMap.put(value2, value1);
	}

	public T get(T value) {
		return map.get(value);
	}
	
	public T getInverse(T value) {
		return inverseMap.get(value);
	}
	
	public Set<T> keySet() {
		return map.keySet();
	}
	
	public static class AlreadyMappedException extends RuntimeException {
		private static final long serialVersionUID = -8669923255675562955L;
		public AlreadyMappedException(String message) {
			super(message);
		}
	}

	@Override
	public Iterator<Pair<T>> iterator() {
		return new Iterator<Mapping.Pair<T>>() {
			private final Iterator<T> iterator = map.keySet().iterator();
			
			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public Pair<T> next() {
				T value1 = iterator.next();
				return new Pair<>(value1, map.get(value1));
			}
			
			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	public Iterator<Pair<T>> inverseIterator() {
		return new Iterator<Mapping.Pair<T>>() {
			private final Iterator<T> iterator = inverseMap.keySet().iterator();
			
			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public Pair<T> next() {
				T value1 = iterator.next();
				return new Pair<>(value1, inverseMap.get(value1));
			}
			
			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	
}
