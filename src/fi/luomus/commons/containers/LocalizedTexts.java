package fi.luomus.commons.containers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LocalizedTexts {

	private final Map<String, List<String>> localeTextMap = new LinkedHashMap<>(3);

	public LocalizedTexts add(String locale, String text) {
		if (text == null) return this;
		text = text.trim();
		if (text.isEmpty()) return this;
		if (locale == null) locale = "";
		locale = locale.toLowerCase();
		if (!localeTextMap.containsKey(locale)) {
			localeTextMap.put(locale, new ArrayList<String>());
		}
		localeTextMap.get(locale).add(text);
		return this;
	}

	public List<String> forLocale(String locale) {
		if (locale == null) locale = "";
		if (hasTextForLocale(locale)) {
			return localeTextMap.get(locale.toLowerCase());
		}
		return Collections.emptyList(); 
	}

	public boolean hasTextForLocale(String locale) {
		if (locale == null) locale = "";
		return localeTextMap.containsKey(locale.toLowerCase());
	}

	public Map<String, List<String>> getAllTexts() {
		return Collections.unmodifiableMap(localeTextMap);
	}

	public Set<String> getAllValues() {
		Set<String> allValues = new HashSet<>(3);
		for (List<String> values : getAllTexts().values()) {
			allValues.addAll(values);
		}
		return allValues;
	}

	@Override
	public String toString() {
		return localeTextMap.toString();
	}

	public boolean isEmpty() {
		return localeTextMap.isEmpty();
	}

}
