package fi.luomus.commons.containers;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import fi.luomus.commons.containers.ContentGroups.ContentGroup;
import fi.luomus.commons.containers.rdf.Qname;

public class ContentGroups implements Iterable<ContentGroup> {

	private final List<ContentGroup> contentGroups;

	public ContentGroups(List<ContentGroup> contentGroups) {
		this.contentGroups = contentGroups;
	}

	public static class ContentGroup implements Iterable<ContentVariable> {
		private final Qname id;
		private final LocalizedText title;
		private final List<ContentVariable> contentVariables;
		public ContentGroup(Qname id, LocalizedText title, List<ContentVariable> contentVariables) {
			this.id = id;
			this.title = title;
			this.contentVariables = contentVariables;
		}
		public LocalizedText getTitle() {
			return title;
		}
		public Qname getId() {
			return id;
		}
		@Override
		public Iterator<ContentVariable> iterator() {
			return Collections.unmodifiableCollection(contentVariables).iterator();
		}
	}

	public static class ContentVariable {
		private final Qname field;
		private final LocalizedText title;
		public ContentVariable(Qname field, LocalizedText title) {
			this.field = field;
			this.title = title;
		}
		public LocalizedText getTitle() {
			return title;
		}
		public Qname getField() {
			return field;
		}
	}

	@Override
	public Iterator<ContentGroup> iterator() {
		return Collections.unmodifiableCollection(contentGroups).iterator();
	}

}
