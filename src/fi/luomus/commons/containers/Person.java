package fi.luomus.commons.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class Person {

	private final Qname id;
	private final String fullname;
	private final boolean hasFullname;
	
	public Person(Qname id, String fullname) {
		this.id = id == null ? new Qname("") : id;
		if (!given(fullname)) {
			this.fullname = "("+this.id+")";
			this.hasFullname = false;
		} else {
			this.fullname = fullname;
			this.hasFullname = true;
		}
	}
	
	private boolean given(String s) {
		return s != null && s.trim().length() > 0;
	}

	public Qname getId() {
		return id;
	}
	
	public String getFullname() {
		return fullname;
	}
	
	public boolean hasFullname() {
		return hasFullname;
	}
	
}
