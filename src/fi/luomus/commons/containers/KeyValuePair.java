package fi.luomus.commons.containers;

import java.util.Map.Entry;

public class KeyValuePair {
	
	private final String key;
	private final String value;
	
	public KeyValuePair(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public KeyValuePair(Entry<String, String> e) {
		this(e.getKey(), e.getValue());
	}

	public String getValue() {
		return value;
	}
	
	public String getKey() {
		return key;
	}
	
}
