package fi.luomus.commons.containers;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import fi.luomus.commons.containers.rdf.Qname;

public class Content {

	public static final Qname DEFAULT_DESCRIPTION_CONTEXT = new Qname("default");

	public static class Context {
		private final Map<Qname, TextsOfProperty> textsOfProperty = new LinkedHashMap<>(10);
		private final Qname id;
		public Context(Qname id) {
			this.id = id;
		}
		public Qname getId() {
			return id;
		}
		public void addText(Qname property, String text, String locale) {
			if (!textsOfProperty.containsKey(property)) {
				textsOfProperty.put(property, new TextsOfProperty());
			}
			textsOfProperty.get(property).addText(text, locale);
		}
		@Override
		public String toString() {
			return textsOfProperty.toString();
		}
		public String getText(Qname property, String locale) {
			if (!textsOfProperty.containsKey(property)) return "";
			return textsOfProperty.get(property).getText(locale);
		}
		public String getText(String property, String locale) {
			return getText(new Qname(property), locale);
		}
		public Set<String> getLocalesWithContent() {
			Set<String> locales = new LinkedHashSet<>(3);
			for (TextsOfProperty property : textsOfProperty.values()) {
				locales.addAll(property.getLocalesWithContent());
			}
			return locales;
		}
		public Set<Qname> getProperties() {
			return textsOfProperty.keySet();
		}
	}

	private static class TextsOfProperty {
		private final LocalizedText localizedText = new LocalizedText();
		public void addText(String text, String locale) {
			localizedText.set(locale, text);
		}
		public Collection<String> getLocalesWithContent() {
			return localizedText.getAllTexts().keySet();
		}
		public String getText(String locale) {
			return localizedText.forLocale(locale);
		}
		@Override
		public String toString() {
			return localizedText.toString();
		}
	}

	private Map<Qname, Context> contexts;

	public boolean isEmpty() {
		return contexts == null || contexts.isEmpty();
	}

	public void addText(Qname context, Qname property, String text, String locale) {
		if (context == null) context = DEFAULT_DESCRIPTION_CONTEXT;
		if (contexts == null) {
			contexts = new TreeMap<>(new Comparator<Qname>() {
				@Override
				public int compare(Qname o1, Qname o2) {
					if (o1.equals(DEFAULT_DESCRIPTION_CONTEXT) && o2.equals(DEFAULT_DESCRIPTION_CONTEXT)) {
						return 0;
					}
					if (DEFAULT_DESCRIPTION_CONTEXT.equals(o1)) return -1;
					if (DEFAULT_DESCRIPTION_CONTEXT.equals(o2)) return 1;
					return o1.compareTo(o2);
				}
			});
		}
		if (!contexts.containsKey(context)) {
			contexts.put(context, new Context(context));
		}
		contexts.get(context).addText(property, text, locale);
	}

	@Override
	public String toString() {
		if (contexts == null) return "null";
		return contexts.toString();
	}

	public Collection<Context> getContexts() {
		if (contexts == null) return Collections.emptyList();
		return Collections.unmodifiableCollection(contexts.values());
	}

	public Context getDefaultContext() {
		return getContext(DEFAULT_DESCRIPTION_CONTEXT);
	}

	public Context getContext(Qname context) {
		if (contexts == null) return null;
		return contexts.get(context);
	}

	public Context getContext(String context) {
		return getContext(new Qname(context));
	}

	public Map<Qname, Set<String>> getContextsWithContentAndLocales() {
		Map<Qname, Set<String>> contextsWithContent = new HashMap<>(3);
		for (Context context : getContexts()) {
			contextsWithContent.put(context.getId(), context.getLocalesWithContent());
		}
		return contextsWithContent;
	}

}
