package fi.luomus.commons.containers;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import fi.luomus.commons.containers.rdf.Qname;

public class RedListEvaluationGroup extends InformalTaxonGroup {

	private final Set<Qname> taxa = new HashSet<>(3);
	private final Set<Qname> informalGroups = new HashSet<>(3);

	public RedListEvaluationGroup() {
		super();
	}

	public RedListEvaluationGroup(Qname qname, LocalizedText name, int order) {
		super(qname, name, order);
	}

	public Set<Qname> getTaxa() {
		return Collections.unmodifiableSet(taxa);
	}

	public Set<Qname> getInformalGroups() {
		return Collections.unmodifiableSet(informalGroups);
	}

	public RedListEvaluationGroup addTaxon(Qname taxonId) {
		if (taxonId != null && taxonId.isSet()) {
			this.taxa.add(taxonId);
		}
		return this;
	}

	public RedListEvaluationGroup addInformalGroup(Qname groupId) {
		if (groupId != null && groupId.isSet()) {
			this.informalGroups.add(groupId);
		}
		return this;
	}

	public boolean hasTaxon(Qname taxonId) {
		return taxa.contains(taxonId);
	}

	public boolean hasInformalGroup(Qname groupId) {
		return informalGroups.contains(groupId);
	}
	
	public boolean hasTaxon(String taxonQname) {
		return taxa.contains(new Qname(taxonQname));
	}

	public boolean hasInformalGroup(String groupQname) {
		return informalGroups.contains(new Qname(groupQname));
	}
}

