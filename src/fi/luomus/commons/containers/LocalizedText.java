package fi.luomus.commons.containers;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class LocalizedText {

	private final Map<String, String> localeTextMap = new TreeMap<>();

	public LocalizedText set(String locale, String text) {
		if (text == null) return this;
		text = text.trim();
		if (text.isEmpty()) return this;
		if (locale == null) locale = "";
		localeTextMap.put(locale.toLowerCase(), text);
		return this;
	}

	public String forLocale(String locale) {
		if (locale == null) locale = "";
		return localeTextMap.get(locale.toLowerCase());
	}

	public boolean hasTextForLocale(String locale) {
		if (locale == null) locale = "";
		return localeTextMap.containsKey(locale.toLowerCase());
	}

	public Map<String, String> getAllTexts() {
		return Collections.unmodifiableMap(localeTextMap);
	}

	@Override
	public String toString() {
		return localeTextMap.toString();
	}

	public boolean isEmpty() {
		return localeTextMap.isEmpty();
	}

}
