package fi.luomus.commons.containers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fi.luomus.commons.containers.rdf.Qname;

public class CollectionMetadata {

	private final Qname qname;
	private final LocalizedText name;
	private final Qname parentQname;
	private String abbreviation;
	private String institutionCode;
	private Qname ownerOrganization;
	private Qname intellectualRights;
	private LocalizedText intellectualRightsDescription;
	private String contactEmail;
	private CollectionMetadata parent;
	private final List<CollectionMetadata> children = new ArrayList<>();
	private Qname collectionQuality;
	private Double dataQuarantinePeriod;
	private Qname secureLevel;
	private Integer size;
	private Integer typeSpecimenSize;
	private boolean statisticsAllowed = false;

	public CollectionMetadata(Qname qname, LocalizedText name, Qname parentQname) {
		this.qname = qname == null ? new Qname("") : qname;
		this.name = name;
		this.parentQname = parentQname;
	}

	public Qname getQname() {
		return qname;
	}

	public LocalizedText getName() {
		return name;
	}

	public Qname getParentQname() {
		return parentQname;
	}

	public List<CollectionMetadata> getChildren() {
		return children;
	}

	public Set<Qname> getChildIdsIncludeSelf() {
		Set<Qname> ids = new HashSet<>();
		ids.add(this.getQname());
		for (CollectionMetadata child : this.getChildren()) {
			ids.addAll(child.getChildIdsIncludeSelf());
		}
		return ids;
	}

	public boolean hasParent() {
		return parent != null;
	}

	public boolean hasChildren() {
		return !children.isEmpty();
	}

	public void setParent(CollectionMetadata parent) {
		this.parent = parent;
	}

	public Qname getIntellectualRights() {
		return intellectualRights;
	}

	public void setIntellectualRights(Qname intellectualRights) {
		this.intellectualRights = intellectualRights;
	}

	public LocalizedText getIntellectualRightsDescription() {
		return intellectualRightsDescription;
	}

	public void setIntellectualRightsDescription(LocalizedText intellectualRightsDescription) {
		this.intellectualRightsDescription = intellectualRightsDescription;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public Qname getCollectionQuality() {
		return collectionQuality;
	}

	public void setCollectionQuality(Qname collectionQuality) {
		this.collectionQuality = collectionQuality;
	}

	public Double getDataQuarantinePeriod() {
		return dataQuarantinePeriod;
	}

	public void setDataQuarantinePeriod(Double dataQuarantinePeriod) {
		this.dataQuarantinePeriod = dataQuarantinePeriod;
	}

	public Qname getSecureLevel() {
		return secureLevel;
	}

	public void setSecureLevel(Qname secureLevel) {
		this.secureLevel = secureLevel;
	}

	public Qname getOwnerOrganization() {
		return ownerOrganization;
	}

	public void setOwnerOrganization(Qname ownerOrganization) {
		this.ownerOrganization = ownerOrganization;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getTypeSpecimenSize() {
		return typeSpecimenSize;
	}

	public void setTypeSpecimenSize(Integer typeSpecimenSize) {
		this.typeSpecimenSize = typeSpecimenSize;
	}

	public boolean isStatisticsAllowed() {
		return statisticsAllowed;
	}

	public void setStatisticsAllowed(boolean statisticsAllowed) {
		this.statisticsAllowed = statisticsAllowed;
	}

	@Override
	public String toString() {
		return getQname() + " " + getName().forLocale("fi");
	}

}
