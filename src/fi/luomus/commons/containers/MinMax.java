package fi.luomus.commons.containers;

public class MinMax<K> {

	private final K min;
	private final K max;
	
	public MinMax(K min, K max) {
		this.min = min;
		this.max = max;
	}

	public K getMin() {
		return min;
	}

	public K getMax() {
		return max;
	}
	
}
