package fi.luomus.commons.containers;

import java.util.HashSet;
import java.util.Set;

/**
 * An utility container for reading sequential files (sarakesidonnainen tiedosto).
 * Note: indexing starts from 1. card.v(1) returns the first char of the line. 
 */
public class Card {

	private static final Set<Character> VALID_CHARACTERS = initValid();

	private static Set<Character> initValid() {
		Set<Character> valid = new HashSet<>();
		for (char c : "abcdefghijklmnopqrstuvwxyzåäö1234567890.,:;+/?- ()_<>&=%*!°\"'".toCharArray()) {
			valid.add(c);
			valid.add(Character.toUpperCase(c));
		}
		return valid;
	}

	private final String data;

	public Card(String data) {
		if (data == null) throw new IllegalArgumentException("Data is null");
		this.data = data;
		for (char c : data.toCharArray()) {
			if (!VALID_CHARACTERS.contains(c)) throw new IllegalStateException("Invalid character '" + c + "'");
		}
	}

	/**
	 * Char from index i. Indexing starts from 1.
	 * @param i
	 * @return
	 */
	public String v(int i) {
		return v(i, 1);
	}

	/**
	 * String between begining and end. Indexing starts from 1.
	 * @param beginning
	 * @param end
	 * @return
	 */
	public String v(int beginning, int length) {
		if (beginning < 1) throw new IllegalArgumentException("Invalid index: " + beginning);
		if (length < 1) throw new IllegalArgumentException("Invalid length: " + length);

		int startIndex = beginning - 1;
		if (startIndex > data.length() - 1) {
			return "";
		}

		int endIndex = startIndex + length;
		if (endIndex > data.length() - 1) {
			endIndex = data.length();
		}

		return data.substring(startIndex, endIndex);
	}



	@Override
	/**
	 * Returns the entire line.
	 */
	public String toString() {
		return data;
	}

	/**
	 * Returns the entire line.
	 * @return
	 */
	public String data() {
		return data;
	}

	/**
	 * Returns the length of the line.
	 * @return
	 */
	public int length() {
		return data.length();
	}
}