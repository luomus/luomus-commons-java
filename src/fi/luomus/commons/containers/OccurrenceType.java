package fi.luomus.commons.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class OccurrenceType {

	private final Qname qname;
	private final LocalizedText name;
	
	public OccurrenceType() {
		this(null, null);
	}

	public OccurrenceType(Qname qname, LocalizedText name) {
		this.qname = qname == null ? new Qname("") : qname;
		this.name = name == null ? new LocalizedText() : name;
	}

	@Override
	public String toString() {
		return this.name.toString();
	}

	public LocalizedText getName() {
		return name;
	}

	public String getName(String locale) {
		return name.forLocale(locale);
	}

	public Qname getQname() {
		return qname;
	}

}
