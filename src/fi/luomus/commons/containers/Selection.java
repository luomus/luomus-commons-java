package fi.luomus.commons.containers;

import fi.luomus.commons.containers.Selection.SelectionOption;

import java.util.Collection;

/**
 * A structure representing a HTML <select></select> -element.
 */
public interface Selection extends Iterable<SelectionOption> { 
	
	/**
	 * A structure representing a HTML <option></option> -element of a <select></select> -element.
	 */
	public static interface SelectionOption {
		
		/**
		 * Option value; <option value="">
		 * @return
		 */
		public String getValue();
		
		/**
		 * Option description text; <option>text</option>
		 * @return
		 */
		public String getText();
		
		/**
		 * Title under which this option belongs in <optgroup>. Empty string if not defined.
		 * @return
		 */
		public String getGroup();

		/**
		 * Option description text in some language
		 * @param langcode
		 * @throws UnsupportedOperationException if localized texts are not set
		 * @return
		 */
		String getText(String langcode) throws UnsupportedOperationException;

		/**
		 * Title under which this option belongs in <optgroup>. Empty string if not defined.
		 * @param langcode
		 * @return
		 */
		public String getGroup(String langcode);
		
	}
	
	/**
	 * List of options
	 * @see SelectionOption
	 * @return
	 */
	public Collection<SelectionOption> getOptions();
	
	/**
	 * Get option's description by it's value
	 * @param optionValue
	 * @return description
	 */
	public String get(String optionValue) throws IllegalArgumentException;
	
	/**
	 * Get options' decsription by it's value in some lang code
	 * @param optionValue
	 * @param langcode
	 * @return
	 * @throws UnsupportedOperationException if localized descriptions are not set
	 */
	public String get(String optionValue, String langcode) throws UnsupportedOperationException;
	
	/**
	 * Tells if this selection defines the given value
	 * @param optionValue
	 * @return true if value exists, false if not
	 */
	public boolean containsOption(String optionValue);

	/**
	 * Name of the selection.
	 * @return
	 */
	public String getName();
	
	/**
	 * Size of the selection is usually getOptions().size(), but if there are option groups, the number of titles is added to the value.
	 * @return
	 */
	public int getSize();
	
}
