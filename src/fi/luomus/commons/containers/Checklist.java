package fi.luomus.commons.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class Checklist implements Comparable<Checklist> {

	private final Qname qname;
	private final LocalizedText fullname;
	private final Qname rootTaxon;
	private Qname owner;
	private boolean isPublic = true;
	private LocalizedText notes = new LocalizedText();
	
	public Checklist() {
		this(null, null, null);
	}
	
	public Checklist(Qname qname, LocalizedText name, Qname rootTaxon) {
		this.qname = qname == null ? new Qname("") : qname;
		this.rootTaxon = rootTaxon == null ? new Qname("") : rootTaxon;
		this.fullname = name == null ? new LocalizedText() : name;
	}

	@Override
	public String toString() {
		return this.fullname.toString();
	}

	public LocalizedText getFullname() {
		return fullname;
	}

	public String getFullname(String locale) {
		return fullname.forLocale(locale);
	}

	public Qname getRootTaxon() {
		return rootTaxon;
	}

	public Qname getQname() {
		return qname;
	}

	@Override
	public int compareTo(Checklist o) {
		int c = Boolean.compare(o.isPublic(), this.isPublic());
		if (c != 0) return c;
		return this.getQname().compareTo(o.getQname());
	}

	public void setOwner(Qname owner) {
		this.owner = owner;
	}

	public Qname getOwner() {
		return owner;
	}

	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}

	public boolean isPublic() {
		return isPublic;
	}

	public LocalizedText getNotes() {
		return notes;
	}

	public void setNotes(LocalizedText notes) {
		this.notes = notes;
	}
}
