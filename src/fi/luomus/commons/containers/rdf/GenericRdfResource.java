package fi.luomus.commons.containers.rdf;


public abstract class GenericRdfResource implements RdfResource {

	private final Qname qname;

	public GenericRdfResource(Qname qname) {
		if (qname == null || !qname.isSet()) {
			throw new IllegalArgumentException("Null qname given");
		}
		this.qname = qname;
	}

	public GenericRdfResource(String qname) {
		this(new Qname(qname));
	}

	@Override
	public Qname getId() {
		return qname;
	}

	@Override
	public String getQname() {
		return qname.toString();
	}

	@Override
	public String getURI() {
		return qname.toURI();
	}

	@Override
	public String toString() {
		return qname.toString();
	}

	@Override
	public String getNamespacePrefix() {
		return qname.getNamespace();
	}

	@Override
	public String getNamespaceURI() {
		return NAMESPACES.get(qname.getNamespace());
	}

	public String getSubNamespacePrefix() {
		String qname = getQname();
		String namespace = getNamespacePrefix();
		if (!namespace.isEmpty()) {
			qname = qname.substring(namespace.length()+1, qname.length());
		}
		if (!qname.contains(".")) return qname;
		return qname.substring(0, qname.indexOf("."));
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o instanceof GenericRdfResource) {
			GenericRdfResource other = (GenericRdfResource) o;
			return this.getQname().equals(other.getQname());
		}
		throw new IllegalArgumentException("Can't compare " + o.getClass().getName() + " to " + this.getClass().getName());
	}

	@Override
	public int hashCode() {
		return this.getQname().hashCode();
	}

}
