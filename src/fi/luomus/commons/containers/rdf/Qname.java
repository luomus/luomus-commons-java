package fi.luomus.commons.containers.rdf;

import java.io.Serializable;

import fi.luomus.commons.containers.Mapping.Pair;
import fi.luomus.commons.containers.SingleValueObject;

public class Qname extends SingleValueObject implements Comparable<Qname>, Serializable {

	private static final long serialVersionUID = -3733260325812558939L;
	private static final int MAX_LONG_LENGTH = String.valueOf(Long.MAX_VALUE).length() - 1;
	private static final String DEFAULT_NS = "";
	private final String localpart;
	private final String namespace;
	private final String qnameString;
	private final String uriString;

	public Qname(String qname) {
		if (qname == null) {
			this.localpart = null;
			this.namespace = null;
			this.qnameString = null;
			this.uriString = null;
			return;
		}
		if (qname.startsWith("http:")) throw new IllegalArgumentException("Expecting QNAME, was given <"+qname+">");
		if (qname.startsWith("https:")) throw new IllegalArgumentException("Expecting QNAME, was given <"+qname+">");
		if (qname.startsWith("tun:")) qname = qname.replace("tun:", DEFAULT_NS); // default namespace prefix should not be used - this is for backwards compatibility

		if (!qname.contains(":")) {
			this.localpart = qname;
			this.namespace = DEFAULT_NS;
			this.qnameString = qname;
			this.uriString = RdfResource.DEFAULT_NAMESPACE_URI + localpart;
			return;
		}

		int index = qname.indexOf(':');
		String namespace = qname.substring(0, index);
		String namespaceURI = RdfResource.NAMESPACES.get(namespace);
		if (namespace.isEmpty() || namespaceURI == null) {
			this.localpart = qname;
			this.namespace = DEFAULT_NS;
			this.qnameString = qname;
			this.uriString = RdfResource.DEFAULT_NAMESPACE_URI + localpart;
			return;
		}

		this.localpart = qname.substring(index+1, qname.length());
		this.namespace = namespace;
		this.qnameString = namespace + ":" + localpart;
		this.uriString = namespaceURI + localpart;
	}

	public static Qname fromURI(String resourceUri) {
		if (resourceUri == null) return new Qname(null);
		if (resourceUri.startsWith(RdfResource.DEFAULT_NAMESPACE_URI)) {
			return new Qname(resourceUri.replace(RdfResource.DEFAULT_NAMESPACE_URI, DEFAULT_NS));
		}
		for (Pair<String> mapping : RdfResource.NAMESPACES) {
			String uriPrefix = mapping.getValue2();
			if (resourceUri.startsWith(uriPrefix)) {
				String namespace = mapping.getValue1();
				return new Qname(resourceUri.replace(uriPrefix, namespace+":"));
			}
		}
		throw new IllegalArgumentException("Unresolveable uri: " + resourceUri);
	}

	@Override
	public String toString() {
		return qnameString;
	}

	public String toURI() {
		return uriString;
	}

	public String getNamespace() {
		return namespace;
	}

	@Override
	protected String getValue() {
		return qnameString;
	}

	@Override
	public int compareTo(Qname o) {
		String name1Alpha = this.getAlphaValue();
		String name2Alpha = o.getAlphaValue();
		int c = name1Alpha.compareTo(name2Alpha);
		if (c != 0) return c;

		Long name1Numeric = this.getNumericValue();
		Long name2Numeric = o.getNumericValue();
		c = name1Numeric.compareTo(name2Numeric);
		if (c != 0) return c;

		return this.toString().compareTo(o.toString());
	}

	private String getAlphaValue() {
		if (qnameString == null) return "";
		return qnameString.replaceAll("[0-9]", "");
	}

	public Long getNumericValue() {
		if (qnameString == null) return Long.MAX_VALUE;
		String strippedValue = qnameString.replaceAll("[^0-9]", "");
		if (strippedValue.length() < 1) return Long.MAX_VALUE;
		if (strippedValue.length() > MAX_LONG_LENGTH) {
			return Long.MAX_VALUE;
		}
		return Long.valueOf(strippedValue);
	}

}
