package fi.luomus.commons.containers.rdf;


public class Predicate extends GenericRdfResource {

	public Predicate(String qname) {
		super(qname);
	}

	public Predicate(Qname qname) {
		super(qname);
	}
	
}
