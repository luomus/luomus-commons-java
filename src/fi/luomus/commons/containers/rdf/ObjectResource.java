package fi.luomus.commons.containers.rdf;



public class ObjectResource extends GenericRdfResource {

	public ObjectResource(String qname) {
		super(qname);
	}
	
	public ObjectResource(Qname qname) {
		super(qname);
	}
	
}
