package fi.luomus.commons.containers.rdf;


public class Context extends GenericRdfResource {

	public Context(String qname) {
		super(qname);
	}
	
	public Context(Qname qname) {
		super(qname);
	}
	
}
