package fi.luomus.commons.containers.rdf;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;

public class JenaModelToInternaModelConverter {

	public static fi.luomus.commons.containers.rdf.Model convert(String data, Lang lang) {
		Model jenaModel = JenaUtils.read(data, lang);
		return new JenaModelToInternaModelConverter(jenaModel).convert();
	}

	private final Model jenaModel;

	private JenaModelToInternaModelConverter(Model jenaModel) {
		this.jenaModel = jenaModel;
	}

	public fi.luomus.commons.containers.rdf.Model convert() {
		List<Statement> statements = new LinkedList<>();
		StmtIterator iterator = jenaModel.listStatements();
		Subject subject = null;
		while (iterator.hasNext()) {
			org.apache.jena.rdf.model.Statement stmt = iterator.next();
			if (subject == null) {
				subject = new Subject(qname(stmt.getSubject()));
			} else {
				Subject otherSubject = new Subject(qname(stmt.getSubject()));
				if (!subject.getQname().equals(otherSubject.getQname())) {
					throw new IllegalStateException("Model contains triplets from more than one subject! For example: " + subject.getQname() + " and " + otherSubject.getQname());
				}
			}
			String predicateMayBeContextHacked = qname(stmt.getPredicate()).toString();
			Predicate predicate = null;
			Context context = null;
			if (predicateMayBeContextHacked.contains("_CONTEXT_")) {
				String[] parts = predicateMayBeContextHacked.split(Pattern.quote("_CONTEXT_"));
				predicate = new Predicate(parts[0]);
				context = new Context(parts[1]);
			} else {
				predicate = new Predicate(predicateMayBeContextHacked);
			}
			Statement statement = createResourceOrLiteralStatement(predicate, stmt.getObject(), context);
			statements.add(statement);
		}
		fi.luomus.commons.containers.rdf.Model model = new fi.luomus.commons.containers.rdf.Model(subject);
		for (Statement s : statements) {
			model.addStatement(s);
		}
		return model;
	}

	private Statement createResourceOrLiteralStatement(Predicate predicate, RDFNode object, Context context) {
		if (object instanceof Resource) {
			return createResourceStatement(predicate, object, context);
		}
		return createLiteralStatement(predicate, object, context);
	}

	private Statement createLiteralStatement(Predicate predicate, RDFNode object, Context context) {
		Literal literal = object.asLiteral();
		String content = literal.getString();
		String langCode = literal.getLanguage();
		ObjectLiteral objectLiteral = null;
		if (given(langCode)) {
			objectLiteral = new ObjectLiteral(content, langCode);
		} else {
			objectLiteral = new ObjectLiteral(content);
		}
		return new Statement(predicate, objectLiteral, context);
	}

	private Statement createResourceStatement(Predicate predicate, RDFNode object, Context context) {
		ObjectResource objectResource = new ObjectResource(qname(object.asResource()));
		return new Statement(predicate, objectResource, context);
	}

	private Qname qname(Resource resource) {
		return Qname.fromURI(resource.getURI());
	}

	private static boolean given(String value) {
		return value != null && value.length() > 0;
	}

}
