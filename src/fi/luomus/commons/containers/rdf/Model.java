package fi.luomus.commons.containers.rdf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;

import fi.luomus.commons.utils.Utils;

public class Model implements Iterable<Statement> {

	private static final Predicate RDF_TYPE = new Predicate("rdf:type");
	private final List<Statement> statements = new ArrayList<>();
	private final Subject subject;

	public static Model fromRdf(String rdf) {
		return from(rdf, Lang.RDFXML);
	}
	
	public static Model from(String data, Lang lang) {
		return JenaModelToInternaModelConverter.convert(data, lang);
	}

	public Model(Qname subject) {
		this(new Subject(subject));
	}

	public Model(Subject subject) {
		this.subject = subject;
	}

	public Subject getSubject() {
		return subject;
	}

	public void addStatement(Statement statement) {
		this.statements.add(statement);
	}

	public List<Statement> getStatements() {
		return Collections.unmodifiableList(statements);
	}

	public String getRDF() {
		return JenaUtils.getRdf(new InternalModelToJenaModelConverter(this).getJenaModel());
	}

	public String getSerialized(RDFFormat format) {
		return JenaUtils.getSerialized(new InternalModelToJenaModelConverter(this).getJenaModel(), format);
	}

	@Override
	public Iterator<Statement> iterator() {
		return statements.iterator();
	}

	public void setType(String type) {
		removeAll(RDF_TYPE);
		addStatement(new Statement(RDF_TYPE, new ObjectResource(type)));
	}

	public String getType() {
		for (Statement s : this.getStatements()) {
			if (s.getPredicate().equals(RDF_TYPE)) {
				return s.getObjectResource().getQname();
			}
		}
		return null;
	}

	public void addStatementIfObjectGiven(Predicate predicate, String objectLiteral) {
		if (given(objectLiteral)) {
			addStatement(new Statement(predicate, new ObjectLiteral(objectLiteral)));
		}
	}

	public void addStatementIfObjectGiven(String predicate, Boolean objectLiteral) {
		this.addStatementIfObjectGiven(new Predicate(predicate), objectLiteral);
	}

	private void addStatementIfObjectGiven(Predicate predicate, Boolean objectLiteral) {
		if (objectLiteral == null) return;
		this.addStatement(new Statement(predicate, new ObjectLiteral(objectLiteral ? "true" : "false")));
	}

	public void addStatementIfObjectGiven(String predicate, String objectLiteral) {
		this.addStatementIfObjectGiven(new Predicate(predicate), objectLiteral);
	}

	public void addStatementIfObjectGiven(Predicate predicate, String objectLiteral, String lancode) {
		if (given(objectLiteral)) {
			addStatement(new Statement(predicate, new ObjectLiteral(objectLiteral, lancode)));
		}
	}

	public void addStatementIfObjectGiven(String predicate, String objectLiteral, String lancode) {
		this.addStatementIfObjectGiven(new Predicate(predicate), objectLiteral, lancode);
	}

	public void addStatementIfObjectGiven(Predicate predicate, Qname objectResource) {
		if (given(objectResource)) {
			addStatement(new Statement(predicate, new ObjectResource(objectResource)));
		}
	}

	public void addStatementIfObjectGiven(String predicate, Qname objectResource) {
		this.addStatementIfObjectGiven(new Predicate(predicate), objectResource);
	}

	private boolean given(Object object) {
		return object != null && object.toString().trim().length() > 0;
	}

	public void removeAll(Predicate predicate) {
		Iterator<Statement> i = statements.iterator();
		while (i.hasNext()) {
			Statement statement = i.next();
			if (statement.getPredicate().equals(predicate)) {
				i.remove();
			}
		}
	}

	/**
	 * Remove statement by id
	 * @param statementId
	 * @return true if statement was found, false if not
	 */
	public boolean removeStatement(long statementId) {
		Iterator<Statement> i = statements.iterator();
		while (i.hasNext()) {
			Statement s = i.next();
			if (s.getId() == statementId) {
				i.remove();
				return true;
			}
		}
		return false;
	}

	public boolean hasStatement(Statement s) {
		for (Statement existing : getStatements()) {
			if (existing.equals(s)) {
				return true;
			}
		}
		return false;
	}

	public boolean isEmpty() {
		return statements.isEmpty();
	}

	@Override
	public String toString() {
		return Utils.debugS(subject, statements);
	}

	public boolean hasStatements(String predicateQname) {
		return hasStatements(new Qname(predicateQname));
	}

	public boolean hasStatements(Qname predicateQname) {
		Predicate predicate = new Predicate(predicateQname);
		for (Statement existing : getStatements()) {
			if (existing.getPredicate().equals(predicate)) {
				return true;
			}
		}
		return false;
	}

	public boolean hasStatementsFromNonDefaultContext() {
		for (Statement s : this.getStatements()) {
			if (!s.isForDefaultContext()) return true;
		}
		return false;
	}

	public List<Statement> getStatements(Qname predicateQname) {
		return getStatements(predicateQname.toString());
	}

	public List<Statement> getStatements(String predicateQname) {
		List<Statement> statements = new ArrayList<>();
		for (Statement s : getStatements()) {
			if (s.getPredicate().getQname().equals(predicateQname)) {
				statements.add(s);
			}
		}
		return statements;
	}

}
