package fi.luomus.commons.containers.rdf;

import fi.luomus.commons.utils.Utils;

public class Statement {

	private long id;
	private final Predicate predicate;
	private final ObjectLiteral objectLiteral;
	private final ObjectResource objectResource;
	private final Context context;

	public Statement(Predicate predicate, ObjectLiteral objectLiteral, Context context) {
		this.predicate = predicate;
		this.objectLiteral = objectLiteral;
		this.objectResource = null;
		this.context = context;
	}

	public Statement(Predicate predicate, ObjectResource objectResource, Context context) {
		this.predicate = predicate;
		this.objectResource = objectResource;
		this.objectLiteral = null;
		this.context = context;
	}

	public Statement(Predicate predicate, boolean value, Context context) {
		this(predicate, new ObjectLiteral(value ? "true" : "false"), context);
	}

	public Statement(Predicate predicate, int i, Context context) {
		this(predicate, new ObjectLiteral(Integer.toString(i)), context);
	}

	public Statement(Predicate predicate, ObjectLiteral objectLiteral) {
		this(predicate, objectLiteral, null);
	}

	public Statement(Predicate predicate, ObjectResource objectResource) {
		this(predicate, objectResource, null);
	}

	public Statement(Predicate predicate, boolean value) {
		this(predicate, value, null);
	}

	public Statement(Predicate predicate, int i) {
		this(predicate, i, null);
	}

	public Predicate getPredicate() {
		return predicate;
	}

	public ObjectLiteral getObjectLiteral() {
		return objectLiteral;
	}

	public ObjectResource getObjectResource() {
		return objectResource;
	}

	public boolean isLiteralStatement() {
		return this.objectLiteral != null;
	}

	public boolean isResourceStatement() {
		return !isLiteralStatement();
	}

	public Context getContext() {
		return context;
	}

	public boolean isForDefaultContext() {
		return context == null;
	}

	@Override
	public String toString() {
		if (isResourceStatement()) {
			return Utils.debugS(predicate, objectResource, context);
		}
		return Utils.debugS(predicate, objectLiteral, context);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o instanceof Statement) {
			Statement other = (Statement) o;
			return this.toString().equals(other.toString());			
		}
		throw new UnsupportedOperationException("Can only compare to another " + this.getClass().getName());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

}

