package fi.luomus.commons.containers.rdf;

import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.safety.Whitelist;

import fi.luomus.commons.utils.Utils;

public class ObjectLiteral {

	public static final int MAX_BYTE_LENGTH = 4000;
	public static final String ALLOWED_TAGS = "p, a, b, strong, i, em, ul, li";
	private static final Whitelist WHITELIST;
	private static final Document.OutputSettings OUTPUT_SETTINGS = new Document.OutputSettings().prettyPrint(false).escapeMode(EscapeMode.xhtml);
	static {
		WHITELIST = Whitelist.none()
				.addAttributes("p", "style")
				.addAttributes("a", "href");
		for (String tag : ALLOWED_TAGS.split(Pattern.quote(","))) {
			tag = tag.trim();
			WHITELIST.addTags(tag);
		}
	}

	private final String content;
	private final String unsanitazedContent;
	private final String langcode;

	public static String sanitizeLiteral(String content) {
		if (!given(content)) return "";
		content = Utils.removeWhitespaceAround(content);
		if (!given(content)) return "";
		if (content.length() >= 1000) {
			content = Utils.trimToByteLength(content, MAX_BYTE_LENGTH);
		}
		content = Jsoup.clean(content, "", WHITELIST, OUTPUT_SETTINGS);
		content = content.replace("<p></p>", "").replace("&lt;", "<").replace("&gt;", ">").replace("&amp;", "&").trim();
		while (content.contains("  ")) {
			content = content.replace("  ", " ");
		}
		while (content.endsWith("\n")) {
			content = content.substring(0, content.length()-1);
		}
		if (content.length() >= 1000) {
			content = Utils.trimToByteLength(content, MAX_BYTE_LENGTH);
		}
		return content.trim();
	}

	private static boolean given(String s) {
		return Utils.given(s);
	}

	public ObjectLiteral(String content, String langcode) {
		this(content, langcode, true);
	}

	public ObjectLiteral(String content) {
		this(content, "");
	}

	private ObjectLiteral(String content, String langcode, boolean sanitaze) {
		this.unsanitazedContent = content;
		if (content != null) {
			if (sanitaze) {
				content = sanitizeLiteral(content);
			} else {
				content = content.trim();
			}
		}
		this.content = content;
		if (langcode == null) langcode = "";
		this.langcode = langcode.toLowerCase().trim();
	}

	public static ObjectLiteral unsanitazedObjectLiteral(String content, String langcode) {
		return new ObjectLiteral(content, langcode, false);
	}

	public String getContent() {
		return content;
	}

	public String getLangcode() {
		return langcode;
	}

	public String getUnsanitazedContent() {
		return unsanitazedContent;
	}

	public boolean hasLangcode() {
		return langcode.length() > 0;
	}

	@Override
	public String toString() {
		return " { content: " + content + " lang: " + langcode + " }";
	}
}
