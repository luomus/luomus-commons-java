package fi.luomus.commons.containers.rdf;

import fi.luomus.commons.containers.LocalizedText;

public class RdfProperty implements Comparable<RdfProperty> {

	private final Qname qname;
	private final RdfPropertyRange range;
	private LocalizedText labels;
	private LocalizedText comments;
	private int order = Integer.MAX_VALUE;
	private int minOccurs = 1;
	private int maxOccurs = 1;
	private Qname altParent;

	public RdfProperty(Qname qname) {
		this(qname, null);
	}

	public RdfProperty(Qname qname, Qname rangeQname) {
		this.qname = qname;
		this.range = new RdfPropertyRange(rangeQname);
		if (qname.toString().startsWith("rdf:_")) {
			order = Integer.valueOf(qname.toString().replace("rdf:_", ""));
		}
	}

	public boolean isLiteralProperty() {
		return range.isLiteralProperty();
	}

	public RdfPropertyRange getRange() {
		return range;
	}

	public Qname getQname() {
		return qname;
	}

	public boolean hasRange() {
		return range.hasRange();
	}

	public boolean hasRangeValues() {
		return range.hasRangeValues();
	}

	public RdfProperty setLabels(LocalizedText labels) {
		this.labels = labels;
		return this;
	}

	public LocalizedText getLabel() {
		return labels;
	}

	public boolean isBooleanProperty() {
		return range.isBooleanProperty();
	}

	public boolean isDateProperty() {
		return range.isDateProperty();
	}

	public boolean isIntegerProperty() {
		return range.isIntegerProperty();
	}

	public boolean isDecimalProperty() {
		return range.isDecimalProperty();
	}

	@Override
	public String toString() {
		return qname.toString();
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		if (order < 0) {
			order = Integer.MAX_VALUE;
		}
		this.order = order;
	}

	@Override
	public int compareTo(RdfProperty o) {
		int c = Integer.valueOf(this.getOrder()).compareTo(o.getOrder());
		if (c != 0) return c;
		return this.getQname().compareTo(o.getQname());
	}



	@Override
	public int hashCode() {
		return qname.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RdfProperty other = (RdfProperty) obj;
		if (qname == null) {
			if (other.qname != null)
				return false;
		} else if (!qname.equals(other.qname))
			return false;
		return true;
	}

	public int getMinOccurs() {
		return minOccurs;
	}

	public void setMinOccurs(int minOccurs) {
		this.minOccurs = minOccurs;
	}

	public int getMaxOccurs() {
		return maxOccurs;
	}

	public void setMaxOccurs(int maxOccurs) {
		this.maxOccurs = maxOccurs;
	}

	public boolean isRequired() {
		return minOccurs > 0;
	}

	public boolean isRepeated() {
		return maxOccurs > 1;
	}

	public boolean hasUnitOfMeasurement() {
		if (!hasRange()) return false;
		return getRange().hasUnitOfMeasurement();
	}

	public RdfProperty getUnitOfMeasurement() {
		return getRange().getUnitOfMeasurement();
	}

	public LocalizedText getComments() {
		return comments;
	}

	public void setComments(LocalizedText comments) {
		this.comments = comments;
	}

	public Qname getAltParent() {
		return altParent;
	}

	public RdfProperty setAltParent(Qname altParent) {
		this.altParent = altParent;
		return this;
	}

}