package fi.luomus.commons.containers.rdf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RdfProperties {

	private final Map<String, RdfProperty> properties = new HashMap<>();
	
	public RdfProperty getProperty(String name) {
		if (!properties.containsKey(name)) {
			throw new IllegalArgumentException("Property "+name+" is not defined!");
		}
		return properties.get(name);
	}

	public RdfProperty getProperty(Predicate predicate) {
		return getProperty(predicate.getQname());
	}

	public void addProperty(RdfProperty property) {
		if (properties.containsKey(property.getQname().toString())) {
			throw new IllegalStateException(property.getQname() + " already exists!");
		}
		properties.put(property.getQname().toString(), property);
	}

	public boolean hasProperty(String name) {
		return properties.containsKey(name);
	}
	
	public Collection<RdfProperty> getAllProperties () {
		List<RdfProperty> list = new ArrayList<>(properties.values());
		Collections.sort(list);
		return list;
	}
	
}
