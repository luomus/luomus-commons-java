package fi.luomus.commons.containers.rdf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import fi.luomus.commons.utils.Utils;

public class InternalModelToJenaModelConverter {

	private final List<Model> models;
	private final org.apache.jena.rdf.model.Model jenaModel = JenaUtils.newDefaultModel();

	public InternalModelToJenaModelConverter(Model model) {
		this.models = Utils.list(model);
	}

	public InternalModelToJenaModelConverter(Collection<Model> models) {
		this.models = new ArrayList<>(models);
		Collections.sort(this.models, new Comparator<Model>() {
			@Override
			public int compare(Model o1, Model o2) {
				return o1.getSubject().getId().compareTo(o2.getSubject().getId());
			}
		});
	}

	public org.apache.jena.rdf.model.Model getJenaModel() {
		for (Model model : models) {
			if (model == null) continue;
			addToJenaModel(model);
		}
		return jenaModel;
	}

	private void addToJenaModel(Model model) {
		addToNamespace(model.getSubject());
		for (Statement statement : model) {
			addNamespaces(statement);
			addToJenaModel(model.getSubject(), statement);
		}
	}

	private void addNamespaces(Statement statement) {
		addToNamespace(statement.getPredicate());
		if (statement.isResourceStatement()) {
			addToNamespace(statement.getObjectResource());
		}
	}

	private void addToNamespace(RdfResource resource) {
		try {
		jenaModel.setNsPrefix(resource.getNamespacePrefix(), resource.getNamespaceURI());
		} catch (Exception e) {
			throw new RuntimeException("Adding namespace failed for " + resource);
		}
	}

	private void addToJenaModel(Subject subjectOfModel, Statement statement) {
		Resource subject = jenaModel.createProperty(subjectOfModel.getURI());
		Property predicate = jenaModel.createProperty(contectHackedPredicate(statement).getURI());
		if (statement.isLiteralStatement()) {
			ObjectLiteral literal = statement.getObjectLiteral();
			if (literal.hasLangcode()) {
				jenaModel.add(subject, predicate, jenaModel.createLiteral(literal.getContent(), literal.getLangcode()));
			} else {
				jenaModel.add(subject, predicate, jenaModel.createLiteral(literal.getContent()));
			}
		} else {
			jenaModel.add(subject, predicate, jenaModel.createProperty(statement.getObjectResource().getURI()));
		}
	}

	private Predicate contectHackedPredicate(Statement statement) {
		Predicate p = statement.isForDefaultContext() ? statement.getPredicate() : new Predicate(statement.getPredicate().getQname() + "_CONTEXT_" + statement.getContext().getQname());
		return p;
	}

}
