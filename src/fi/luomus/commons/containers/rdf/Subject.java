package fi.luomus.commons.containers.rdf;


public class Subject extends GenericRdfResource {

	public Subject(String qname) {
		super(qname);
	}
	
	public Subject(Qname qname) {
		super(qname);
	}
	
}
