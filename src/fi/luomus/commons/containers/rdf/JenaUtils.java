package fi.luomus.commons.containers.rdf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RDFParser;

import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;
import fi.luomus.commons.xml.XMLWriter;

public class JenaUtils {

	public static Model newDefaultModel() {
		Model jenaModel = ModelFactory.createDefaultModel();
		return jenaModel;
	}

	public static Model read(String rdf) {
		return read(rdf, Lang.RDFXML);
	}

	public static Model read(String data,  Lang lang) {
		Model jenaModel = JenaUtils.newDefaultModel();
		InputStream is = new ByteArrayInputStream(data.getBytes());
		RDFParser.create().source(is).lang(lang).parse(jenaModel);
		return jenaModel;
	}

	public static void debugRdf(Model jenaModel) {
		debugRdf(jenaModel, RDFFormat.RDFXML_ABBREV);
	}

	public static void debugRdf(Model jenaModel, RDFFormat format) {
		RDFDataMgr.write(System.out, jenaModel, format);
		System.out.println();
	}

	public static String getRdf(Model jenaModel) {
		return getSerialized(jenaModel, RDFFormat.RDFXML_ABBREV);
	}

	public static String getSerialized(Model jenaModel, RDFFormat format) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		RDFDataMgr.write(os, jenaModel, format);
		String serialized;
		try {
			serialized = os.toString("UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
		int subjectCount = getSubjectCount(jenaModel);
		if (subjectCount > 1 && format.toString().startsWith("RDF/XML")) {
			return sort(serialized);
		}
		return serialized;
	}

	private static int getSubjectCount(Model jenaModel) {
		StmtIterator i = jenaModel.listStatements();
		Set<String> subjects = new HashSet<>();
		while (i.hasNext()) {
			subjects.add(i.next().getSubject().getURI());
		}
		int subjectCount = subjects.size();
		return subjectCount;
	}

	private static String sort(String rdfXml) {
		Document doc = new XMLReader().parse(rdfXml);
		List<Node> childs = doc.getRootNode().getChildNodes();
		Collections.sort(childs, new Comparator<Node>() {
			@Override
			public int compare(Node n1, Node n2) {
				Qname q1 = Qname.fromURI(n1.getAttribute("rdf:about"));
				Qname q2 = Qname.fromURI(n2.getAttribute("rdf:about"));
				return q1.compareTo(q2);
			}
		});
		String orderedXml = new XMLWriter(doc).generateXML();
		return orderedXml;
	}

	public static void debugStatements(Model jenaModel) {
		if (jenaModel == null) return;
		StringBuilder b = new StringBuilder();
		StmtIterator iterator = jenaModel.listStatements();
		while (iterator.hasNext()) {
			Statement stmt = iterator.next();
			Resource subject = stmt.getSubject();
			Property predicate = stmt.getPredicate();
			RDFNode object = stmt.getObject();

			b.append(subject.getURI()).append(" \t->\t ");
			b.append(predicate.toString()).append(" \t->\t ");
			if (object instanceof Resource) {
				b.append(((Resource) object).getURI());
			} else {
				b.append(object.asLiteral().getString()).append("\t");
				b.append("@").append(object.asLiteral().getLanguage());
			}
			b.append("\n");
		}
		System.out.println(b.toString());
	}

}
