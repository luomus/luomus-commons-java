package fi.luomus.commons.containers.rdf;

import fi.luomus.commons.containers.Mapping;

public interface RdfResource {

	public static final String DEFAULT_NAMESPACE_URI = "http://tun.fi/";
	public static final Mapping<String> NAMESPACES = NamespacesInitator.initNamespaces();

	public static class NamespacesInitator {
		private static Mapping<String> initNamespaces() {
			Mapping<String> namespaces = new Mapping<>();
			namespaces.map("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
			namespaces.map("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
			namespaces.map("xsd", "http://www.w3.org/2001/XMLSchema#");
			namespaces.map("xml", "http://www.w3.org/XML/1998/namespace#");
			namespaces.map("owl", "http://www.w3.org/2002/07/owl#");
			namespaces.map("vcard", "http://www.w3.org/2006/vcard/ns#");
			namespaces.map("luomus", "http://id.luomus.fi/");
			namespaces.map("dc", "http://purl.org/dc/terms/");
			namespaces.map("dcmitype", "http://purl.org/dc/dcmitype/");
			namespaces.map("dwc", "http://rs.tdwg.org/dwc/terms/");
			namespaces.map("dwciri", "http://rs.tdwg.org/dwc/iri/");
			namespaces.map("dwctype", "http://rs.tdwg.org/dwc/dwctype/");
			namespaces.map("abcd", "http://www.tdwg.org/schemas/abcd/2.06#");
			namespaces.map("naturforskaren", "http://naturforskaren.se/");
			namespaces.map("eol", "http://eol.org/");
			namespaces.map("finbif", "http://laji.fi/");
			namespaces.map("skos", "http://www.w3.org/2004/02/skos/core#");
			namespaces.map("dyntaxa", "http://dyntaxa.se/Taxon/Info/");
			namespaces.map("taxonid", "http://taxonid.org/");
			namespaces.map("zmuo", "http://id.zmuo.oulu.fi/");
			namespaces.map("herbo", "http://id.herb.oulu.fi/");
			namespaces.map("utu", "http://mus.utu.fi/");
			namespaces.map("tax", "http://rs.taxonid.org/");
			namespaces.map("so", "http://schema.org/");
			namespaces.map("cc", "https://creativecommons.org/");
			namespaces.map("gbif-dataset", "https://www.gbif.org/dataset/");
			namespaces.map("syke", "http://metatieto.ymparisto.fi:8080/geoportal/rest/document?id=");
			namespaces.map("rdfschema", "http://www.w3.org/TR/2014/REC-rdf-schema-20140225/");
			namespaces.map("geo", "http://www.w3.org/2003/01/geo/wgs84_pos#");
			namespaces.map("", DEFAULT_NAMESPACE_URI);
			return namespaces;
		}
	}

	public Qname getId();

	public String getQname();

	public String getURI();

	public String getNamespacePrefix();

	public String getNamespaceURI();

}
