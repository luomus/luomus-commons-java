package fi.luomus.commons.containers.rdf;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.utils.Utils;

public class RdfPropertyRange {

	private static final Set<String> INTEGER_RANGES = Utils.set("xsd:int", "xsd:integer", "xsd:negativeInteger", "xsd:nonNegativeInteger", "xsd:nonPositiveInteger");
	private static final Set<String> DECIMAL_RANGES = Utils.set("xsd:decimal", "xsd:double", "xsd:float");

	private static final Map<String, RdfProperty> BOOLEAN_RANGE_VALUES = initBooleanRangeValues();

	private static Map<String, RdfProperty> initBooleanRangeValues() {
		Map<String, RdfProperty> booleanValues = new LinkedHashMap<>();

		RdfProperty trueProperty = new RdfProperty(new Qname("true"), null).setLabels(new LocalizedText().set("en", "Yes").set("fi", "Kyllä"));
		RdfProperty falseProperty = new RdfProperty(new Qname("false"), null).setLabels(new LocalizedText().set("en", "No").set("fi", "Ei"));

		booleanValues.put("true", trueProperty);
		booleanValues.put("false", falseProperty);
		return Collections.unmodifiableMap(booleanValues);
	}

	private final Qname rangeQname;
	private Map<String, RdfProperty> rangeValues;
	private RdfProperty unitOfMeasurement;

	public RdfPropertyRange(Qname range) {
		this.rangeQname = range;
		if (isBooleanProperty()) {
			this.rangeValues = BOOLEAN_RANGE_VALUES;
		}
	}

	public boolean isLiteralProperty() {
		if (!hasRange()) return false;
		return rangeQname.toString().startsWith("xsd:");
	}

	public boolean hasRange() {
		return rangeQname != null;
	}

	public boolean hasRangeValues() {
		return rangeQname != null && rangeValues != null && !rangeValues.isEmpty();
	}

	public void setRangeValues(List<RdfProperty> rangeValues) {
		if (this.rangeValues != null) {
			throw new IllegalStateException(rangeQname + " already had range values: " + rangeValues.toString());
		}
		Collections.sort(rangeValues);
		this.rangeValues = new LinkedHashMap<>();
		for (RdfProperty p : rangeValues) {
			this.rangeValues.put(p.getQname().toString(), p);
		}
	}

	public Collection<RdfProperty> getValues() {
		checkHasRange();
		return rangeValues.values();
	}

	public boolean hasValue(String resourceQname) {
		try {
			checkHasRange();
		} catch (IllegalStateException e) {
			return false;
		}
		return rangeValues.containsKey(resourceQname);
	}

	public RdfProperty getValueFor(String resourceQname) {
		checkHasRange();
		if (!rangeValues.containsKey(resourceQname)) {
			throw new IllegalStateException("This range (" + rangeQname + ") does not define value " + resourceQname);
		}
		return rangeValues.get(resourceQname);
	}

	private void checkHasRange() {
		if (!hasRange() || rangeValues == null || rangeValues.isEmpty()) {
			throw new IllegalStateException("This range (" + rangeQname + ") does not define any range values.");
		}
	}

	public Qname getQname() {
		return rangeQname;
	}

	public boolean isBooleanProperty() {
		if (rangeQname == null) return false;
		return "xsd:boolean".equals(rangeQname.toString());
	}

	public boolean isDateProperty() {
		return "xsd:date".equals(rangeQname.toString());
	}

	public boolean isIntegerProperty() {
		if (rangeQname == null) return false;
		return INTEGER_RANGES.contains(rangeQname.toString());
	}

	public boolean isDecimalProperty() {
		if (rangeQname == null) return false;
		return DECIMAL_RANGES.contains(rangeQname.toString());
	}

	public RdfProperty getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(RdfProperty unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	}

	public boolean hasUnitOfMeasurement() {
		return unitOfMeasurement != null;
	}

}