package fi.luomus.commons.containers;

import java.net.URI;

public class LocalizedURL {

	private final URI uri;
	private final String locale;

	public LocalizedURL(URI uri, String locale) {
		this.uri = uri;
		this.locale = locale;
	}

	public URI getUri() {
		return uri;
	}

	public String getLocale() {
		return locale;
	}

	@Override
	public String toString() {
		return uri.toString();
	}

}
