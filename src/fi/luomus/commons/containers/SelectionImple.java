package fi.luomus.commons.containers;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class SelectionImple implements Selection { 
	
	private final String									name;
	private final LinkedHashMap<String, SelectionOption>	options;
	
	public SelectionImple(String name) {
		this.name = name.toLowerCase();
		options = new LinkedHashMap<>();
	}
	
	public SelectionImple(Selection selection) {
		this(selection.getName());
		for (SelectionOption o : selection.getOptions()) {
			this.addOption(o);
		}
	}
	
	@Override
	public String toString() {
		return options.toString();
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public Collection<SelectionOption> getOptions() {
		return options.values();
	}
	
	public void addOption(SelectionOption o) {
		if (containsOption(o)) {
			throw new IllegalStateException("Selection " + getName() + " already contains value " +o.getValue());
		}
		options.put(o.getValue(), o);
	}
	
	@Override
	public String get(String optionValue) throws IllegalArgumentException {
		return get(optionValue, null);
	}
	
	@Override
	public boolean containsOption(String optionValue) {
		return options.containsKey(optionValue.toUpperCase());
	}
	
	private boolean containsOption(SelectionOption o) {
		return containsOption(o.getValue());
	}
	
	@Override
	public Iterator<SelectionOption> iterator() {
		return options.values().iterator();
	}
	
	public void removeOption(String optionValue) {
		optionValue = optionValue.toUpperCase();
		if (!this.containsOption(optionValue)) {
			throw new IllegalArgumentException("No option with value " + optionValue +" defined");
		}
		options.remove(optionValue);
	}
	
	@Override
	public int getSize() {
		int difference = 0;
		String prevGroupName = "";
		for (SelectionOption o : getOptions()) {
			if (!o.getGroup().equals(prevGroupName)) {
				if (o.getGroup().length() > 0) {
					difference++;
				}
				prevGroupName = o.getGroup();
			}
		}
		return getOptions().size() + difference;
	}

	@Override
	public String get(String optionValue, String langcode) {
		if (optionValue.equals("")) return "";
		if (!containsOption(optionValue)) throw new IllegalArgumentException("Selection " + name + " does not define value '" + optionValue + "'.");
		return options.get(optionValue.toUpperCase()).getText(langcode);
	}
	
	public SelectionOption getOption(String optionValue) {
		if (!containsOption(optionValue)) throw new IllegalArgumentException("Selection " + name + " does not define value '" + optionValue + "'.");
		return options.get(optionValue.toUpperCase());
	}
}
