package fi.luomus.commons.containers;

import fi.luomus.commons.containers.rdf.Qname;

public class ContentContextDescription implements Comparable<ContentContextDescription> {

	private final Qname qname;
	private final LocalizedText name;

	public ContentContextDescription(Qname qname, LocalizedText name) {
		this.qname = qname;
		this.name = name == null ? new LocalizedText() : name;
	}

	public Qname getQname() {
		return qname;
	}

	public LocalizedText getName() {
		return name;
	}

	public String getName(String locale) {
		return name.forLocale(locale);
	}

	@Override
	public int compareTo(ContentContextDescription o) {
		if (this.qname == null) return Integer.MAX_VALUE;
		if (o.qname == null) return Integer.MIN_VALUE;
		return this.qname.compareTo(o.qname);
	}

	@Override
	public String toString() {
		return "ContentContextDescription [qname=" + qname + ", name=" + name + "]";
	}

}
