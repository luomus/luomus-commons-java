package fi.luomus.commons.containers;

public class Langcode extends SingleValueObject {

	private final String langcode;
		
	public Langcode(String langcode) {
		this.langcode = langcode;
	}
	
	public String getLangcode() {
		return langcode;
	}
	
	@Override
	public String toString() {
		if (!isSet()) {
			return "";
		}
		return getLangcode();
	}
	
	@Override
	protected String getValue() {
		return langcode;
	}
	
}
