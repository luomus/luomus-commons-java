package fi.luomus.commons.containers;

import java.util.Map.Entry;

public class Pair<K, V> {

	private final K key;
	private final V value;

	public Pair(K key, V value) {
		this.key = key;
		this.value = value;
	}

	public Pair(Entry<K, V> e) {
		this(e.getKey(), e.getValue());
	}

	public V getValue() {
		return value;
	}

	public K getKey() {
		return key;
	}

	@Override
	public String toString() {
		String kStr = key == null ? "null" : key.toString();
		String vStr = value == null ? "null" : value.toString();
		return kStr + ":" + vStr;
	}
}
