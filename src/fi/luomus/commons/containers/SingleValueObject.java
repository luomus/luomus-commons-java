package fi.luomus.commons.containers;

public abstract class SingleValueObject {

	protected abstract String getValue();

	public boolean isSet() {
		return getValue() != null && getValue().trim().length() > 0;
	}

	@Override
	public String toString() {
		return "" + getValue();
	}

	@Override
	public int hashCode() {
		if (!isSet()) return 0;
		return getValue().hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			if (this.isSet()) {
				return false;
			}
			return true;
		}
		if (o instanceof SingleValueObject) {
			SingleValueObject other = (SingleValueObject) o;
			if (!this.isSet()) {
				if (other.isSet()) {
					return false;
				}
				return true;
			}
			return this.getValue().equals(other.getValue());
		}
		throw new IllegalArgumentException("Can only compare with another " + this.getClass().getName() + " trying to compare to " + o.getClass().getName());
	}

}
