package fi.luomus.commons.containers;

public class Ring {

	private final String series;
	private final int numbers;
	private final String format;

	public Ring(String ring) {
		this.series = parseSeries(ring);
		this.numbers = numbers(ring);
		this.format = generateFormatString(ring);
	}

	private String generateFormatString(String ring) {
		String format = "";
		for (char c : ring.toCharArray()) {
			if (Character.isDigit(c)) {
				format += 'N';
			} else if (c == ' ') {
				format += "_";
			} else if (Character.isLetter(c)) {
				format += "A";
			} else {
				format += "X";
			}
		}
		while (format.contains("NN")) {
			format = format.replace("NN", "N");
		}
		return format;
	}

	private String parseSeries(String ring) {
		String series = alphas(ring);
		// Muutetaan "nnnn X" -muoto "XL nnnn" muodoksi
		if (alphasInEnd(ring)) {
			series = series + "L";
		}
		return series.toUpperCase();
	}

	private boolean alphasInEnd(String ring) {
		if (!given(ring)) return false;
		return Character.isLetter(ring.charAt(ring.length() - 1));
	}

	private boolean given(String ring) {
		return ring != null && ring.length() > 0;
	}

	private String alphas(String ring) {
		String alphas = "";
		for (char c : ring.toCharArray()) {
			if (Character.isLetter(c)) {
				alphas += c;
			}
		}
		return alphas;
	}

	private int numbers(String ring) {
		String numbers = "";
		for (char c : ring.toCharArray()) {
			if (Character.isDigit(c)) {
				numbers += c;
			}
		}
		if (numbers.length() == 0) return -1;
		try {
			return Integer.valueOf(numbers);
		} catch (NumberFormatException e) {
			return -1;
		}
	}

	public String getSeries() {
		return series;
	}

	public int getNumbers() {
		return numbers;
	}

	@Override
	public String toString() {
		if (numbers > 0) {
			return series + " " + numbers;
		}
		return series;
	}

	@Override 
	public boolean equals(Object o) {
		return this.toString().equals(o.toString());
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	public String getFormatString() {
		return format;
	}

	public boolean validFormat() {
		// valid: A[A][_]NN* and NN*[_]A
		if (format.equals("")) return true;
		if (format.length() < 2) return false;
		if (format.contains("X")) return false;
		if (!format.contains("A") || !format.contains("N")) return false;
		if (format.startsWith("A")) {
			return validFormatAlphaInStart(format);
		}
		if (format.endsWith("A")) {
			return validFormatAlphaInEnd(format);
		}
		return false;
	}

	private static boolean validFormatAlphaInEnd(String format) {
		String reverse = new StringBuilder(format).reverse().toString();
		return validFormatAlphaInStart(reverse, 1);
	}

	private static boolean validFormatAlphaInStart(String format) {
		return validFormatAlphaInStart(format, 2);
	}

	private static boolean validFormatAlphaInStart(String format, int allowedCountOfStartAlphas) {
		int startAlphaCount = startAlphaCount(format);
		if (startAlphaCount > allowedCountOfStartAlphas) return false;
		String rest = format.substring(startAlphaCount);
		if (rest.contains("A")) return false;
		if (rest.startsWith("_")) {
			rest = rest.substring(1);
		}
		return onlyNumbers(rest);
	}

	private static int startAlphaCount(String format) {
		int startAlphaCount = 0;
		for (char c : format.toCharArray()) {
			if (c == 'A') {
				startAlphaCount++;
			} else {
				break;
			}
		}
		return startAlphaCount;
	}

	private static boolean onlyNumbers(String formatPartion) {
		for (char c : formatPartion.toCharArray()) {
			if (c != 'N') return false;
		}
		return true;
	}

}
