package fi.luomus.commons.containers;

import java.util.HashMap;
import java.util.Map;

import fi.luomus.commons.containers.Selection.SelectionOption;
import fi.luomus.commons.utils.Utils;

public class SelectionOptionImple implements SelectionOption {
		
		private final String	value;
		private final String	description;
		private final String	group;
		private Map<String, String> localizedDescriptions;
		private Map<String, String> localizedGroups;
		
		public SelectionOptionImple(String value, String description) {
			this(value, description, null);
		}
		
		public SelectionOptionImple(String value, String description, String group) {
			this.value = value.toUpperCase();
			this.description = description;
			this.group = group;
		}
		
		@Override
		public String toString() {
			return Utils.debugS(value, description, group);
		}
		
		@Override
		public String getValue() {
			if (value ==  null) return "";
			return value;
		}
		
		@Override
		public String getText() {
			if (description == null) return "";
			return description;
		}
		
		@Override
		public String getText(String langcode) throws UnsupportedOperationException {
			if (descriptionNotSetInLanguage(langcode)) {
				return defaultText();
			}
			return localizedDescriptions.get(langcode.toLowerCase());
		}
		
		private String defaultText() {
			return getText();
		}
		
		private boolean descriptionNotSetInLanguage(String langcode) {
			if (langcode == null) return true;
			langcode = langcode.toLowerCase();
			return localizedDescriptions == null || 
			!localizedDescriptions.containsKey(langcode) || 
			localizedDescriptions.get(langcode) == null || 
			localizedDescriptions.get(langcode).length() < 1;
		}
		
		@Override
		public String getGroup() {
			if (group == null) return "";
			return group;
		}
		
		public SelectionOptionImple setText(String langcode, String description) {
			if (localizedDescriptions == null) localizedDescriptions = new HashMap<>();
			localizedDescriptions.put(langcode.toLowerCase(), description);
			return this;
		}

		public SelectionOption setGroup(String langcode, String groupName) {
			if (localizedGroups == null) localizedGroups = new HashMap<>();
			localizedGroups.put(langcode.toLowerCase(), groupName);
			return this;
		}

		@Override
		public String getGroup(String langcode) {
			if (langcode == null) return getGroup();
			if (groupNotSetInLanguage(langcode)) {
				return getGroup();
			}
			return localizedGroups.get(langcode.toLowerCase());
		}
		
		private boolean groupNotSetInLanguage(String langcode) {
			if (langcode == null) return true;
			langcode = langcode.toLowerCase();
			return localizedGroups == null || 
			!localizedGroups.containsKey(langcode) || 
			localizedGroups.get(langcode) == null || 
			localizedGroups.get(langcode).length() < 1;
		}
}
