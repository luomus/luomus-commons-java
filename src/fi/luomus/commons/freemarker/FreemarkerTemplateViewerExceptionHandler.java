package fi.luomus.commons.freemarker;

import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

public class FreemarkerTemplateViewerExceptionHandler implements TemplateExceptionHandler {
	public void handleTemplateException(TemplateException te, freemarker.core.Environment env, java.io.Writer out) {
		
		try {
			System.err.println(te.getMessage());
			out.write("LOMAKEVIRHE: " + te.getMessage());
		} catch (Exception e) {
		}
		// catch (Throwable t) { }
	}
}
