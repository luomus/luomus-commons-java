package fi.luomus.commons.freemarker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class FreemarkerTemplateViewer {

	private final Configuration	cfg;

	public FreemarkerTemplateViewer(String templatefolder) throws IOException {
		File folder = new File(templatefolder);
		if (!folder.isDirectory()) throw new FileNotFoundException("Template folder does not exist");

		cfg = new Configuration();
		cfg.setTemplateExceptionHandler(new FreemarkerTemplateViewerExceptionHandler());
		cfg.setDirectoryForTemplateLoading(folder);
		cfg.setDefaultEncoding("UTF-8");
		cfg.setNumberFormat("computer");
		cfg.setOutputEncoding("UTF-8");
		cfg.setURLEscapingCharset("UTF-8");
	}

	public void viewPage(String page, Object rootmap, PrintWriter out) throws Exception {
		try {
			Template template = cfg.getTemplate(page + ".ftl");
			template.setEncoding("UTF-8");
			template.process(rootmap, out);
		} catch (Exception e) {
			throw new Exception("template: " + page, e);
		}
	}

}
