package fi.luomus.commons.json;

import org.json.JSONException;

/**
 * Wrapper object for org.json.JSONObject
 * @author eopiirai
 *
 */
public class JSONObject {

	private final org.json.JSONObject data;

	public JSONObject(org.json.JSONObject jsonObject) {
		this.data = jsonObject;
	}

	public JSONObject(String data) {
		try {
			this.data = new org.json.JSONObject(data);
		} catch (JSONException e) {
			throw new RuntimeException(e.getMessage() + ". The JSON: " + data, e);
		}
	}

	public JSONObject() {
		this.data = new org.json.JSONObject();
	}

	public JSONObject copy() {
		return new JSONObject(data.toString());
	}

	/**
	 * @param key
	 * @return Returns the value as string or empty string if key does not exists.
	 */
	public String getString(String key) {
		if (data.has(key)) {
			try {
				String value = data.getString(key);
				if (value == null) return "";
				return value;
			} catch (JSONException e) {
				return "";
			}
		}
		return "";
	}

	/**
	 * @param key
	 * @return Returns the value as integer if key exists and is valid integer.
	 * @throws RuntimeException if key does not exists or if the value is not an integer.
	 */
	public int getInteger(String key) {
		if (!data.has(key)) {
			throw generateException("No key", key);
		}
		try {
			return data.getInt(key);
		} catch (JSONException e) {
			throw generateException("Get int", key, e);
		}
	}

	/**
	 * @param key
	 * @return Returns the value as double if key exists and is valid double.
	 * @throws RuntimeException if key does not exists or if the value is not a double.
	 */
	public double getDouble(String key) {
		if (!data.has(key)) {
			throw generateException("No key", key);
		}
		try {
			return data.getDouble(key);
		} catch (JSONException e) {
			throw generateException("Get double", key, e);
		}
	}

	/**
	 * @param key
	 * @return Returns the value as boolean
	 * @throws RuntimeException if key does not exists or if the value is not a boolean or the String "true" or "false".
	 */
	public boolean getBoolean(String key) {
		if (!data.has(key)) {
			throw generateException("No key", key);
		}
		try {
			return data.getBoolean(key);
		} catch (JSONException e) {
			throw generateException("Get boolean", key, e);
		}
	}

	private RuntimeException generateException(String message, String key, JSONException e) {
		return new RuntimeException(message + " for key " + key + "\n" + data.toString(), e);
	}

	private RuntimeException generateException(String message, String key) {
		return new RuntimeException(message + " " + key + "\n" + data.toString());
	}

	/**
	 * @param key
	 * @return Returns a wrapper for JSONArray. If if key does not exists, returns an empty wrapper array and sets it to this object as a value of the key.
	 */
	public JSONArray getArray(String key) {
		if (data.has(key) && !data.isNull(key)) {
			try {
				return new JSONArray(data.getJSONArray(key));
			} catch (JSONException e) {
				throw generateException("Get array", key, e);
			}
		}
		JSONArray newArray = new JSONArray();
		setArray(key, newArray);
		return newArray;
	}

	/**
	 * @param key
	 * @return Returns a wrapper for JSONObject. If if key does not exists, returns an empty JsonObject and sets it to this object as a value of the key.
	 */
	public JSONObject getObject(String key) {
		if (data.has(key) && !data.isNull(key)) {
			try {
				return new JSONObject(data.getJSONObject(key));
			} catch (JSONException e) {
				throw generateException("Get object", key, e);
			}
		}
		JSONObject newObject = new JSONObject();
		setObject(key, newObject);
		return newObject;
	}

	/**
	 * @param key
	 * @return Returns the value as Object if key exists - if it is array or object it is wrapped
	 * @throws RuntimeException if key does not exists or if something goes wrong
	 */
	public Object getRaw(String key) {
		if (!data.has(key)) {
			throw generateException("No key", key);
		}
		try {
			Object o = data.get(key);
			if (o instanceof org.json.JSONObject) {
				return new JSONObject((org.json.JSONObject)o);
			}
			if (o instanceof org.json.JSONArray) {
				return new JSONArray((org.json.JSONArray)o);
			}
			return o;
		} catch (JSONException e) {
			throw generateException("Get raw", key, e);
		}
	}

	/**
	 * Sets key to value
	 * @param key
	 * @param value
	 * @return this
	 * @throws RuntimeException if something is wrong..
	 */
	public JSONObject setObject(String key, JSONObject value) {
		try {
			data.put(key, value.reveal());
		} catch (JSONException e) {
			throw generateException("Set object", key, e);
		}
		return this;
	}

	/**
	 * Sets key to value
	 * @param key
	 * @param array
	 * @return this
	 * @throws RuntimeException if something is wrong..
	 */
	public JSONObject setArray(String key, JSONArray array) {
		try {
			data.put(key, array.reveal());
		} catch (JSONException e) {
			throw generateException("Set array", key, e);
		}
		return this;
	}

	/**
	 * Sets key to value
	 * @param key
	 * @param value
	 * @return this
	 * @throws RuntimeException if something is wrong..
	 */
	public JSONObject setBoolean(String key, boolean value) {
		try {
			data.put(key, value);
		} catch (JSONException e) {
			throw generateException("Set boolean", key, e);
		}
		return this;
	}

	/**
	 * Sets key to value
	 * @param key
	 * @param value
	 * @return this
	 * @throws RuntimeException if something is wrong..
	 */
	public JSONObject setString(String key, String value) {
		try {
			data.put(key, value);
		} catch (JSONException e) {
			throw generateException("Set string", key, e);
		}
		return this;
	}

	/**
	 * Sets key to value
	 * @param key
	 * @param value
	 * @return this
	 * @throws RuntimeException if something is wrong..
	 */
	public JSONObject setInteger(String key, int value) {
		try {
			data.put(key, value);
		} catch (JSONException e) {
			throw generateException("Set integer", key, e);
		}
		return this;
	}

	/**
	 * Sets key to value
	 * @param key
	 * @param value
	 * @return this
	 * @throws RuntimeException if something is wrong..
	 */
	public JSONObject setDouble(String key, double value) {
		try {
			data.put(key, value);
		} catch (JSONException e) {
			throw generateException("Set double", key, e);
		}
		return this;
	}

	/**
	 * Sets key to value
	 * @param key
	 * @param value
	 * @return this
	 * @throws RuntimeException if something is wrong..
	 */
	public JSONObject setRaw(String key, Object value) {
		try {
			data.put(key, value);
		} catch (JSONException e) {
			throw generateException("Set raw", key, e);
		}
		return this;
	}

	/**
	 * @return Names of keys. Empty array if there are no keys.
	 */
	public String[] getKeys() {
		String[] names = org.json.JSONObject.getNames(this.data);
		if (names == null) return new String[0];
		return names;
	}

	public boolean hasKey(String key) {
		return this.data.has(key);
	}

	public boolean isEmpty() {
		return getKeys().length == 0;
	}

	public boolean isNull(String key) {
		return data.isNull(key);
	}

	public boolean isObject(String key) {
		if (isNull(key)) return false;
		Object o = data.get(key);
		return o instanceof org.json.JSONObject;
	}

	public boolean isArray(String key) {
		if (isNull(key)) return false;
		Object o = data.get(key);
		return o instanceof org.json.JSONArray;
	}

	@Override
	public String toString() {
		return data.toString();
	}

	public org.json.JSONObject reveal() {
		return data;
	}

	public JSONObject remove(String key) {
		data.remove(key);
		return this;
	}

	/**
	 * Note: beautify() is not to be used with anything serious; use toString().
	 * This does not handle all cases right, like escaping escape char etc.
	 * @return indented and formatted version
	 */
	public String beautify() {
		String json = this.toString();
		StringBuilder b = new StringBuilder();
		int indent = 0;
		boolean stringOpen = false;
		boolean prevWasEscape = false;
		for (char c : json.toCharArray()) {
			if (c == '"' && !prevWasEscape) {
				stringOpen = !stringOpen;
			}
			if (!stringOpen) {
				if (c == '{' || c == '[') {
					indent++;
					b.append(c).append("\n").append(indent(indent));
				} else if (c == '}' || c == ']') {
					indent--;
					b.append("\n").append(indent(indent)).append(c);
				} else if (c == ':') {
					b.append(c).append(" ");
				} else if (c == ',') {
					b.append(",").append("\n").append(indent(indent));
				} else {
					b.append(c);
				}
			} else {
				b.append(c);
			}
			if (c == '\\') {
				prevWasEscape = true;
			} else {
				prevWasEscape = false;
			}
		}

		return b.toString();
	}

	private String indent(int indent) {
		String s = "";
		for (int i=0; i<indent; i++) {
			s += "  ";
		}
		return s;
	}

}
