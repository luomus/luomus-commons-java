package fi.luomus.commons.json;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;

/**
 * Wrapper object for org.json.JSONArray.
 */
public class JSONArray implements Iterable<String> {

	private final org.json.JSONArray array;

	public JSONArray(String data) {
		try {
			this.array = new org.json.JSONArray(data);
		}	catch (JSONException e) {
			throw new RuntimeException(e.getMessage() + ". The JSON: " + data, e);
		}
	}

	public JSONArray(org.json.JSONArray array) {
		this.array = array;
	}

	public JSONArray() {
		this.array = new org.json.JSONArray();
	}

	public JSONArray copy() {
		return new JSONArray(array.toString());
	}

	public boolean isEmpty() {
		return array.length() == 0;
	}

	@Override
	public Iterator<String> iterator() {
		List<String> list = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			try {
				list.add(array.getString(i));
			} catch (JSONException e) {
				throw new IllegalArgumentException(array.toString(), e);
			}
		}
		return list.iterator();
	}

	public List<String> iterateAsString() {
		List<String> list = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			try {
				list.add(array.getString(i));
			} catch (JSONException e) {
				throw new IllegalArgumentException(array.toString(), e);
			}
		}
		return list;
	}

	public List<JSONObject> iterateAsObject() {
		List<JSONObject> list = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			try {
				list.add(new JSONObject(array.getJSONObject(i)));
			} catch (JSONException e) {
				throw new IllegalArgumentException(array.toString(), e);
			}
		}
		return list;
	}

	public List<JSONArray> iterateAsArray() {
		List<JSONArray> list = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			try {
				list.add(new JSONArray(array.getJSONArray(i)));
			} catch (JSONException e) {
				throw new IllegalArgumentException(array.toString(), e);
			}
		}
		return list;
	}

	public List<Integer> iterateAsInteger() {
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			try {
				list.add(array.getInt(i));
			} catch (JSONException e) {
				throw new IllegalArgumentException(array.toString(), e);
			}
		}
		return list;
	}

	public List<Double> iterateAsDouble() {
		List<Double> list = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			try {
				list.add(array.getDouble(i));
			} catch (JSONException e) {
				throw new IllegalArgumentException(array.toString(), e);
			}
		}
		return list;
	}

	public List<Object> iterateAsRaw() {
		List<Object> list = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			try {
				Object o = array.get(i);
				if (o instanceof org.json.JSONObject) {
					list.add(new JSONObject((org.json.JSONObject)o));
				} else if (o instanceof org.json.JSONArray) {
					list.add(new JSONArray((org.json.JSONArray)o));
				} else {
					list.add(o);
				}
			} catch (JSONException e) {
				throw new IllegalArgumentException(array.toString(), e);
			}
		}
		return list;
	}

	@Override
	public String toString() {
		return array.toString();
	}

	/**
	 * Add value to array
	 * @param object
	 * @return this
	 */
	public JSONArray appendObject(JSONObject object) {
		array.put(object.reveal());
		return this;
	}

	/**
	 * Add value to array
	 * @param array
	 * @return this
	 */
	public JSONArray appendArray(JSONArray array) {
		this.array.put(array.reveal());
		return this;
	}

	/**
	 * Add value to array
	 * @param value
	 * @return this
	 */
	public JSONArray appendString(String value) {
		array.put(value);
		return this;
	}

	/**
	 * Add value to array
	 * @param value
	 * @return this
	 */
	public JSONArray appendInteger(int value) {
		array.put(value);
		return this;
	}

	/**
	 * Add value to array
	 * @param value
	 * @return this
	 */
	public JSONArray appendDouble(double value) {
		array.put(value);
		return this;
	}

	public org.json.JSONArray reveal() {
		return array;
	}

	public int size() {
		return array.length();
	}

}
