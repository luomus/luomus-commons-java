package fi.luomus.commons.db.connectivity;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SimpleTransactionConnection implements TransactionConnection {

	private final Connection con;

	public SimpleTransactionConnection(ConnectionDescription desc) throws SQLException {
		con = Util.con(desc);
	}

	public SimpleTransactionConnection(Connection con) {
		this.con = con;
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		PreparedStatement p = con.prepareStatement(sql);
		return p;
	}

	@Override
	public synchronized void release() {
		try {
			con.rollback();
		} catch (SQLException e) {
		}
		try {
			con.close();
		} catch (SQLException e) {
		}
	}

	@Override
	public void startTransaction() throws SQLException {
		try { con.rollback(); } catch (Exception e) {}
		con.setAutoCommit(false);
	}

	@Override
	public void commitTransaction() throws SQLException {
		con.commit();
	}

	@Override
	public void rollbackTransaction() throws SQLException {
		con.rollback();
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		return con.prepareCall(sql);
	}

	@Override
	public boolean isClosed() {
		try {
			return con.isClosed();
		} catch (SQLException e) {
			return true;
		}
	}

	@Override
	public void close() throws Exception {
		release();
	}
}
