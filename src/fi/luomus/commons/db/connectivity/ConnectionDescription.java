package fi.luomus.commons.db.connectivity;

import fi.luomus.commons.utils.Utils;

public class ConnectionDescription {
	
	private final String	driver;
	private final String	url;
	private final String	username;
	private final String	password;
	
	public ConnectionDescription(String driver, String url, String username, String password) throws IllegalArgumentException {
		if (invalid(driver)) throw new IllegalArgumentException("Invalid driver");
		if (invalid(url)) throw new IllegalArgumentException("Invalid url");
		if (invalid(username)) throw new IllegalArgumentException("Invalid username");
		if (invalid(password)) throw new IllegalArgumentException("Invalid password");
		
		this.driver = driver;
		this.url = url;
		this.username = username;
		this.password = password;
	}
	
	private boolean invalid(String value) {
		return value == null || value.length() < 1;
	}
	
	public String driver() {
		return driver;
	}
	
	public String url() {
		return url;
	}
	
	public String username() {
		return username;
	}
	
	public String password() {
		return password;
	}
	
	public String toString() {
		return super.toString() + " " + Utils.debugS(driver, url, username, password);
	}
}
