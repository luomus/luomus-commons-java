package fi.luomus.commons.db.connectivity;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * java.sql.Connection wrapper. Implementations should <br>
 * 1) keep track of all statements and close them before connection is released <br>
 * 2) implement transaction handling <br>
 * 3) call rollback before starting a new transaction <br>
 * 4) call rollback before releasing connection <br>
 * ... <br>
 */
public interface TransactionConnection extends AutoCloseable {

	void release();

	PreparedStatement prepareStatement(String sql) throws SQLException;

	void startTransaction() throws SQLException;

	void commitTransaction() throws SQLException;

	void rollbackTransaction() throws SQLException;

	CallableStatement prepareCall(String sql) throws SQLException;

	boolean isClosed() throws SQLException;

}
