package fi.luomus.commons.db.connectivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Util {

	public static Connection con(ConnectionDescription desc) throws SQLException {
		try {
			Class.forName(desc.driver());
		} catch (ClassNotFoundException e) {
			throw new SQLException(e);
		}
		return DriverManager.getConnection(desc.url(), desc.username(), desc.password());
	}
	
}
