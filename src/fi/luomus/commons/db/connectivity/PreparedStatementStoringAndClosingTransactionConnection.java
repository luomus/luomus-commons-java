package fi.luomus.commons.db.connectivity;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * This connection stores all prepared statements and makes sure they are closed when connection is released.
 * A ResultSet object is automatically closed when the Statement object that generated it is closed,
 * re-executed, or used to retrieve the next result from a sequence of multiple results.
 * http://java.sun.com/javase/6/docs/api/java/sql/ResultSet.html
 */
public class PreparedStatementStoringAndClosingTransactionConnection implements TransactionConnection {

	private final Connection				con;
	private final List<PreparedStatement>	preparedStatements;
	private final List<CallableStatement>	callableStatements;
	private boolean							released	= false;

	public PreparedStatementStoringAndClosingTransactionConnection(ConnectionDescription desc) throws SQLException {
		this(Util.con(desc));
	}

	public PreparedStatementStoringAndClosingTransactionConnection(Connection con) {
		this.con = con;
		this.preparedStatements = new LinkedList<>();
		this.callableStatements = new LinkedList<>();
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		throwExceptionIfReleased();
		PreparedStatement p = con.prepareStatement(sql);
		if (p != null) preparedStatements.add(p);
		return p;
	}

	private void throwExceptionIfReleased() throws SQLException {
		if (released) throw new SQLException("Connection has been released.");
	}

	@Override
	public synchronized void release() {
		if (released) return;
		released = true;
		try {
			con.rollback();
		} catch (SQLException e) {}
		for (PreparedStatement p : preparedStatements) {
			try {
				p.close();
			} catch (SQLException e) {
				logException(e);
			}
		}
		for (CallableStatement stmt : callableStatements) {
			try {
				stmt.close();
			} catch (SQLException e) {
				logException(e);
			}
		}
		try {
			con.close();
		} catch (SQLException e) {
			logException(e);
		}
	}

	@Override
	public void startTransaction() throws SQLException {
		try { con.rollback(); } catch (Exception e) {}
		con.setAutoCommit(false);
	}

	@Override
	public void commitTransaction() throws SQLException {
		con.commit();
	}

	@Override
	public void rollbackTransaction() throws SQLException {
		con.rollback();
	}

	@Override
	public boolean isClosed() {
		if (released) return true;
		try {
			return con.isClosed();
		} catch (SQLException e) {
			return true;
		}
	}

	private void logException(Exception e) {
		e.printStackTrace();
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		throwExceptionIfReleased();
		CallableStatement stmt = con.prepareCall(sql);
		if (stmt != null) callableStatements.add(stmt);
		return stmt;
	}

	@Override
	public void close() throws Exception {
		release();
	}
}
