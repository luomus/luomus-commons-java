package fi.luomus.commons.tipuapi;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;

import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.client.methods.HttpGet;

public class TipuAPIClientImple implements TipuAPIClient {
	
	private final HttpClientService httpclient;
	private final String uri;
	
	public TipuAPIClientImple(Config config) throws IllegalArgumentException, URISyntaxException {
		this(config.get(Config.TIPU_API_URI), config.get(Config.TIPU_API_USERNAME), config.get(Config.TIPU_API_PASSWORD));
	}
	
	public TipuAPIClientImple(String uri, String username, String password) throws URISyntaxException {
		this.uri = uri;
		this.httpclient = new HttpClientService(uri, username, password);
	}
	
	@Override
	public Document getAsDocument(String resourcename) throws Exception {
		HttpGet request = new HttpGet(uri + "/" + resourcename);
		Document document = httpclient.contentAsDocument(request);
		return document;
	}
	
	@Override
	public Document getAsDocument(String resourcename, String id) throws Exception {
		HttpGet request = new HttpGet(uri + "/" + resourcename + "/" + id);
		Document document = httpclient.contentAsDocument(request);
		return document;
	}
	
	@Override
	public Selection getAsSelection(String resourcename) throws Exception {
		return getAsSelection(resourcename, Integer.MAX_VALUE);
	}
	
	@Override
	public Selection getAsSelection(String resourcename, int countOfFields) throws Exception {
		Document document = getAsDocument(resourcename);
		SelectionImple selection = new SelectionImple(resourcename);
		for (Node resource : document.getRootNode()) {
			String id = resource.getNode("id").getContents();
			id = Utils.toHTMLEntities(id);
			StringBuilder desc = new StringBuilder();
			int i = 0;
			for (Node resourceChild : resource) {
				if (i++ > countOfFields) break;
				if (resourceChild.getName().equals("id")) continue;
				String contents = resourceChild.getContents();
				if (contents == null) continue;
				desc.append(contents).append(" ");
			}
			Utils.removeLastChar(desc);
			selection.addOption(new SelectionOptionImple(id, desc.toString())); 
		}
		return selection;
	}
	
	@Override
	public Selection getAsSelection(String resourcename, int ... fieldIndexes) throws Exception {
		Document document = getAsDocument(resourcename);
		SelectionImple selection = new SelectionImple(resourcename);
		for (Node resource : document.getRootNode()) {
			String id = resource.getNode("id").getContents();
			StringBuilder desc = new StringBuilder();
			List<Node> childNodes = resource.getChildNodes();
			for (int index : fieldIndexes) {
				Node resourceChild = childNodes.get(index);
				String contents = resourceChild.getContents();
				if (contents == null) continue;
				desc.append(contents).append(", ");
			}
			Utils.removeLastChar(desc);
			Utils.removeLastChar(desc);
			selection.addOption(new SelectionOptionImple(id, desc.toString().trim()));
		}
		return selection;
	}
	
	@Override
	public void close() {
		httpclient.close();
	}
	
	@Override
	public TipuApiResource get(String resourcename) throws Exception {
		TipuApiResource resource = new TipuApiResource();
		Document document = getAsDocument(resourcename);
		for (Node node : document.getRootNode().getChildNodes()) {
			resource.add(node);
		}
		return resource;
	}

	@Override
	public Document forceReload(String resourcename) throws Exception {
		HttpGet request = new HttpGet(uri + "/" + resourcename + "?forceReload=true");
		Document document = httpclient.contentAsDocument(request);
		return document;
	}

	@Override
	public Document forceReload(String resourcename, String id) throws Exception {
		HttpGet request = new HttpGet(uri + "/" + resourcename + "/" + id + "?forceReload=true");
		Document document = httpclient.contentAsDocument(request);
		return document;
	}
	
}
