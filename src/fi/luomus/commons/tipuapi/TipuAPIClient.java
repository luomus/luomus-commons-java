package fi.luomus.commons.tipuapi;

import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.xml.Document;

/**
 * Can be used to access resources in three different formats:  XML Document, TipuApiResource container, that maps all rows of
 * the resource for fast access (by their ID) or as a Selection -container.
 * 
 * Every instance that is created must be closed by calling the close() method. This should be done
 * in finally -clause of a try-finally block.
 * 
 * @see Document
 * @see TipuApiResource
 * @see Selection
 */
public interface TipuAPIClient {
	
	/**
	 * Kunnat
	 */
	public static final String	MUNICIPALITIES	= "municipalities";
	
	/**
	 * Nykyiset kunnat
	 */
	public static final String	CURRENT_MUNICIPALITIES	= "current-municipalities";
	
	/**
	 * Kaikki lintulajikoodit, mukaanlukien epätarkat joita rengastajien ei ole syytä käyttää.
	 */
	public static final String	SPECIES	= "species";
	
	/**
	 * Lintulajit joita rengastajien on sallittua käyttää
	 */
	public static final String	SELECTABLE_SPECIES	= "selectable-species";
	
	/**
	 * Henkilöt
	 */
	public static final String	RINGERS	= "ringers";
	
	/**
	 * Sanasto ja muut koodistot
	 */
	public static final String	CODES = "codes";
	
	/**
	 * Sanastojen koodien kuvaukset
	 */
	public static final String DESCRIPTIONS_OF_CODES = "descriptions-of-codes";
			
	/**
	 * ELY-keskukset (ympäristökeskukset)
	 */
	public static final String	ELY_CENTRES	= "ely-centres";
	
	/**
	 * Maakunnat 
	 */
	public static final String	PROVINCES	= "provinces";
	
	/**
	 * Vanhat läänit 
	 */
	public static final String	OLD_COUNTIES	= "old-counties";
	
	/**
	 * Henkilöiden synonyymit.
	 */
	public static final String	OBSERVER_SYNONYMS	= "observer-synonyms";
	
	/**
	 * Schemet (ulkomaiset rengastuskeskukset)
	 */
	public static final String SCHEMES = "schemes";
	
	/**
	 * Maat
	 */
	public static final String COUNTRIES = "countries";
	
	/**
	 * Maallikot
	 */
	public static final String LAYMEN = "laymen";
	
	/**
	 * Euring paikkakoodit (maa, provinssi)
	 */
	public static final String EURING_PROVINCES = "euring-provinces";
	
	/**
	 * Lintutieteelliset yhdistykset
	 */
	public static final String LYL_AREAS = "lyl-areas";
	
	/**
	 * Lintuasemat
	 */
	public static final String BIRD_STATIONS = "bird-stations";
	
	/**
	 * Ylläpitäjät
	 */
	public static final String ADMINS = "admins";
	
	/**
	 * Closes connection. Must be called for every instance that is created.
	 */
	public void close();
	
	/**
	 * Resource as a _Selection
	 * @param resourcename
	 * @return
	 * @throws Exception 
	 * @see {@link Selection}
	 */
	public Selection getAsSelection(String resourcename) throws Exception;
	
	/**
	 * Resource as a _Selection using specific amount of fields in Selection option description (without count parameter all fields are used) 
	 * @param resourcename
	 * @param countOfFields
	 * @return
	 * @throws Exception
	 */
	public Selection getAsSelection(String resourcename, int countOfFields) throws Exception;
	
	/**
	 * Resource as a _Selection using specific index of fields in Seleciton option description
	 * @param resourcename
	 * @param fieldIndexes
	 * @return
	 * @throws Exception
	 */
	public Selection getAsSelection(String resourcename, int ... fieldIndexes) throws Exception;
	
	/**
	 * Resource as a Document
	 * @param resourcename
	 * @return
	 * @throws Exception
	 * @see {@link Document}
	 */
	public Document getAsDocument(String resourcename) throws Exception;

	/**
	 * Resource as a Document  
	 * @param resourcename
	 * @param id
	 * @return
	 * @throws Exception
	 * @see {@link Document}
	 */
	public Document getAsDocument(String resourcename, String id) throws Exception;

	/**
	 * Resource as TipuApiResource
	 * @param resourcename
	 * @return
	 * @throws Exception 
	 * @see TipuApiResource
	 */
	public TipuApiResource get(String resourcename) throws Exception;

	/**
	 * Forces reload of TipuApiResource, effecting future calls to the resource
	 * @param resourcename
	 * @return Document as with getAsDocument
	 * @throws Exception
	 */
	public Document forceReload(String resourcename) throws Exception;
	
	/**
	 * Forces reload of TipuApiResource, effecting future calls to the resource
	 * @param resourcename
	 * @param Resource as Document as with getAsDocument
	 * @return
	 * @throws Exception
	 * @see {@link Document}
	 */
	public Document forceReload(String resourcename, String id) throws Exception;
	
}
