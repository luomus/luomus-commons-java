package fi.luomus.commons.tipuapi;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import fi.luomus.commons.xml.Document.Node;

/**
 * Container holding response to TipuAPIClient request. Provides fast access to units of the resource by their ID. 
 * @see Node
 */
public class TipuApiResource implements Iterable<Node> {
	
	private final Map<String, Node> units = new LinkedHashMap<>();
	
	public Collection<Node> getAll() {
		return this.units.values();
	}
	
	public Node getById(String id) {
		return this.units.get(id);
	}
	
	public TipuApiResource add(Node unit) {
		String id = unit.getNode("id").getContents();
		if (this.units.containsKey(id)) {
			throw new IllegalArgumentException("Resource already defined, id = " + id);
		}
		this.units.put(id, unit);
		return this;
	}
	
	public boolean containsUnitById(String id) {
		return this.units.containsKey(id);
	}

	public int size() {
		return this.units.size();
	}

	@Override
	public Iterator<Node> iterator() {
		return this.units.values().iterator();
	}


}
