package fi.luomus.commons.taxonomy;

import fi.luomus.commons.containers.rdf.Qname;

public class RedListStatus {

	private final int year;
	private final Qname status;

	public RedListStatus(int year, Qname status) {
		this.year = year;
		this.status = status;
	}

	public int getYear() {
		return year;
	}

	public Qname getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "RedListStatus [year=" + year + ", status=" + status + "]";
	}

}
