package fi.luomus.commons.taxonomy;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;

public class TaxonSearch {

	public static enum MatchType { EXACT, PARTIAL, LIKELY }
	public static enum ResponseNameMode { TAXONOMY, OBSERVATION }

	private final String searchword;
	private final int limit;
	private final Qname checklist;
	private Set<MatchType> matchTypes = Utils.set(MatchType.EXACT, MatchType.PARTIAL, MatchType.LIKELY);
	private final Set<Qname> informalTaxonGroups = new HashSet<>(0);
	private final Set<Qname> taxonSets = new HashSet<>(0);
	private final Set<Qname> excludedInformalTaxonGroups = new HashSet<>(0);
	private final Set<Qname> excludedNameTypes = new HashSet<>(0);
	private final Set<Qname> includedNameTypes = new HashSet<>(0);
	private final Set<String> includedLanguages = new HashSet<>(0);
	private Boolean species = null;
	private boolean onlyFinnish = false;
	private boolean onlyInvasive = false;
	private boolean includeHidden = true;
	private String comparisonString;
	private ResponseNameMode responseNameMode = ResponseNameMode.TAXONOMY;

	public TaxonSearch(String searchword) {
		this(searchword, 10, Taxon.MASTER_CHECKLIST);
	}

	public TaxonSearch(String searchword, int limit) {
		this(searchword, limit, Taxon.MASTER_CHECKLIST);
	}

	public TaxonSearch(String searchword, int limit, Qname checklist) {
		this.searchword = searchword;
		this.limit = limit;
		this.checklist = checklist;
	}

	public TaxonSearch addInformalTaxonGroup(Qname informalTaxonGroup) {
		this.informalTaxonGroups.add(informalTaxonGroup);
		return this;
	}

	public TaxonSearch addTaxonSet(Qname taxonSet) {
		this.taxonSets.add(taxonSet);
		return this;
	}

	public TaxonSearch addExcludedInformalTaxonGroup(Qname informalTaxonGroup) {
		this.excludedInformalTaxonGroups.add(informalTaxonGroup);
		return this;
	}

	public String getSearchword() {
		return searchword;
	}

	public int getLimit() {
		return limit;
	}

	public Qname getChecklist() {
		return checklist;
	}

	public Set<Qname> getInformalTaxonGroups() {
		return Collections.unmodifiableSet(informalTaxonGroups);
	}

	public Set<Qname> getTaxonSets() {
		return Collections.unmodifiableSet(taxonSets);
	}

	public Set<Qname> getExcludedInformalTaxonGroups() {
		return Collections.unmodifiableSet(excludedInformalTaxonGroups);
	}

	public TaxonSearch onlyExact() {
		this.matchTypes = Utils.set(MatchType.EXACT);
		return this;
	}

	public boolean isOnlyExact() {
		return matchTypes.size() == 1 && matchTypes.contains(MatchType.EXACT);
	}

	public TaxonSearch setMatchTypes(MatchType ...matchTypes) {
		if (matchTypes == null || matchTypes.length == 0) {
			throw new IllegalArgumentException("Must give at least one match type");
		}
		this.matchTypes = Utils.set(matchTypes);
		return this;
	}

	public Set<MatchType> getMatchTypes() {
		return Collections.unmodifiableSet(matchTypes);
	}

	public Boolean getSpecies() {
		return species;
	}

	public TaxonSearch setSpecies(Boolean species) {
		this.species = species;
		return this;
	}

	public boolean isOnlyFinnish() {
		return onlyFinnish;
	}

	public TaxonSearch setOnlyFinnish(boolean onlyFinnish) {
		this.onlyFinnish = onlyFinnish;
		return this;
	}

	public boolean isOnlyInvasive() {
		return onlyInvasive;
	}

	public TaxonSearch setOnlyInvasive(boolean onlyInvasive) {
		this.onlyInvasive = onlyInvasive;
		return this;
	}

	public boolean isIncludeHidden() {
		return includeHidden;
	}

	public TaxonSearch setIncludeHidden(boolean includeHidden) {
		this.includeHidden = includeHidden;
		return this;
	}

	public TaxonSearch addExlucedNameType(Qname exludedNameType) {
		this.excludedNameTypes.add(exludedNameType);
		return this;
	}

	public Set<Qname> getExlucedNameTypes() {
		return Collections.unmodifiableSet(excludedNameTypes);
	}

	public TaxonSearch addIncludedNameType(Qname includedNameType) {
		this.includedNameTypes.add(includedNameType);
		return this;
	}

	public Set<Qname> getIncludedNameTypes() {
		return Collections.unmodifiableSet(includedNameTypes);
	}

	public TaxonSearch addIncludedLanguage(String locale) {
		this.includedLanguages.add(locale);
		return this;
	}

	public Set<String> getIncludedLanguages() {
		return Collections.unmodifiableSet(includedLanguages);
	}

	public TaxonSearch setResponseNameMode(ResponseNameMode responseNameMode) {
		if (responseNameMode == null) responseNameMode = ResponseNameMode.TAXONOMY;
		this.responseNameMode = responseNameMode;
		return this;
	}

	public ResponseNameMode getResponseNameMode() {
		return responseNameMode;
	}

	public boolean hasChecklist() {
		return checklist != null && checklist.isSet();
	}

	public boolean hasFilters() {
		return !getExcludedInformalTaxonGroups().isEmpty() || !getInformalTaxonGroups().isEmpty() || !getTaxonSets().isEmpty() ||
				getSpecies() != null || isOnlyFinnish() || isOnlyInvasive() || !getExlucedNameTypes().isEmpty() ||
				!isIncludeHidden() || !getIncludedNameTypes().isEmpty() || !getIncludedLanguages().isEmpty();
	}

	// Remember not to include comparisonString to generated toString()
	@Override
	public String toString() {
		return "TaxonSearch [searchword=" + searchword + ", limit=" + limit + ", checklist=" + checklist + ", matchTypes=" + matchTypes +
				", responseNameMode=" + responseNameMode + ", informalTaxonGroups=" + informalTaxonGroups + ", taxonSets=" + taxonSets +
				", excludedInformalTaxonGroups=" + excludedInformalTaxonGroups + ", excludedNameTypes=" + excludedNameTypes + ", species=" + species +
				", onlyFinnish=" + onlyFinnish + ", onlyInvasive=" + onlyInvasive + ", includeHidden=" + includeHidden +
				", includedNameTypes=" + includedNameTypes + ", includedLanguages=" + includedLanguages + "]";
	}

	private String getComparisonString() {
		if (comparisonString == null) {
			comparisonString = toString();
		}
		return comparisonString;
	}

	@Override
	public int hashCode() {
		return getComparisonString().hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (!(o instanceof TaxonSearch)) throw new UnsupportedOperationException("Can only compare to " + TaxonSearch.class);
		TaxonSearch other = (TaxonSearch) o;
		return this.getComparisonString().equals(other.getComparisonString());
	}


}