package fi.luomus.commons.taxonomy;

import fi.luomus.commons.containers.rdf.Qname;

import java.util.Set;

public interface Filter {

	public Set<Qname> getFilteredTaxa(Qname filterValue);

}