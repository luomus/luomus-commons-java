package fi.luomus.commons.taxonomy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BoldRecords {

	private Integer publicRecords;
	private Integer specimens;
	private Integer barcodes;
	private List<String> bins;

	public Integer getPublicRecords() {
		return publicRecords;
	}
	public void setPublicRecords(Integer publicRecords) {
		this.publicRecords = publicRecords;
	}
	public Integer getSpecimens() {
		return specimens;
	}
	public void setSpecimens(Integer specimens) {
		this.specimens = specimens;
	}
	public Integer getBarcodes() {
		return barcodes;
	}
	public void setBarcodes(Integer barcodes) {
		this.barcodes = barcodes;
	}
	public List<String> getBins() {
		if (this.bins == null) return Collections.emptyList();
		return bins;
	}
	public void addToBins(String bin) {
		if (this.bins == null) this.bins = new ArrayList<>();
		this.bins.add(bin);
	}
	public Integer getBinCount() {
		return getBins().size();
	}
	public boolean hasRecords() {
		return (publicRecords != null && publicRecords.intValue() > 0) || (barcodes != null && barcodes.intValue() > 0);
	}
	@Override
	public String toString() {
		return "BoldRecords [publicRecords=" + publicRecords + ", specimens=" + specimens + ", barcodes=" + barcodes + ", bins=" + bins + "]";
	}

}
