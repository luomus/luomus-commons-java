package fi.luomus.commons.taxonomy;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import fi.luomus.commons.containers.Content;
import fi.luomus.commons.containers.Image;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.LocalizedTexts;
import fi.luomus.commons.containers.LocalizedURL;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.iucn.Evaluation;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class Taxon implements Comparable<Taxon> {

	public static final Qname MASTER_CHECKLIST = new Qname("MR.1");

	private static final Qname EMPTY_QNAME = new Qname("");
	private static final Qname SPECIES = new Qname("MX.species");
	private static final Qname SPECIES_AGGREGATE = new Qname("MX.speciesAggregate");
	private static final Qname KINGDOM = new Qname("MX.kingdom");

	private static final Set<Qname> CONSIDERED_SPECIES_LEVEL_TAXON_RANKS = Utils.set(
			new Qname("MX.infragenericTaxon"),
			new Qname("MX.aggregate"),
			SPECIES,
			new Qname("MX.nothospecies"),
			new Qname("MX.infraspecificTaxon"),
			new Qname("MX.subspecificAggregate"),
			new Qname("MX.subspecies"),
			new Qname("MX.nothosubspecies"),
			new Qname("MX.variety"),
			new Qname("MX.subvariety"),
			new Qname("MX.form"),
			new Qname("MX.subform"),
			new Qname("MX.hybrid"),
			new Qname("MX.anamorph"),
			new Qname("MX.populationGroup"),
			new Qname("MX.intergenericHybrid"),
			new Qname("MX.infragenericHybrid"),
			new Qname("MX.cultivar"),
			new Qname("MX.group"),
			new Qname("MX.grex"));

	private static final Set<Qname> ADDITIONAL_CURSIVE_TAXON_RANKS = Utils.set(
			new Qname("MX.genus"),
			new Qname("MX.subgenus"),
			new Qname("MX.nothogenus"),
			new Qname("MX.series"),
			new Qname("MX.subseries"),
			new Qname("MX.ecotype"),
			new Qname("MX.section"),
			new Qname("MX.subsection"),
			SPECIES_AGGREGATE);

	private static final Set<Qname> INVASIVE_SPECIES_TYPES_OF_OCCURRENCES = Utils.set(
			new Qname("MX.typeOfOccurrenceAnthropogenic"), new Qname("MX.typeOfOccurrenceImport"));

	private static final Set<Qname> INVASIVE_SPECIES_ADMIN_STATUSES = Utils.set(
			new Qname("MX.nationallySignificantInvasiveSpecies"), new Qname("MX.euInvasiveSpeciesList"),
			new Qname("MX.quarantinePlantPest"), new Qname("MX.nationalInvasiveSpeciesStrategy"),
			new Qname("MX.otherInvasiveSpeciesList"), new Qname("MX.controllingRisksOfInvasiveAlienSpecies"),
			new Qname("MX.qualityPlantPest"), new Qname("MX.otherPlantPest"), new Qname("MX.concernInvasiveSpeciesNotOnOtherLists"));

	public static boolean isSpecies(Qname taxonRank) {
		return CONSIDERED_SPECIES_LEVEL_TAXON_RANKS.contains(taxonRank);
	}

	public static boolean shouldCursive(Qname taxonRank) {
		return isSpecies(taxonRank) || ADDITIONAL_CURSIVE_TAXON_RANKS.contains(taxonRank);
	}

	private static final Qname STATUTORY = new Qname("MX.threatenedStatusStatutoryProtected");
	private static final Qname THREATENED = new Qname("MX.threatenedStatusThreatened");
	private static final Qname NEAR_THREATENED = new Qname("MX.threatenedStatusNearThreatened");

	private static final Set<Qname> ADMIN_STATUSES_STATUTORY = Utils.set(
			new Qname("MX.finlex160_1997_appendix4_specialInterest_2021"),
			new Qname("MX.finlex160_1997_largeBirdsOfPrey"),
			new Qname("MX.habitatsDirectiveAnnexII"),
			new Qname("MX.habitatsDirectiveAnnexIV"));
	private static final Qname NT = new Qname(Evaluation.NT);
	private static final Set<Qname> THREATENED_IUCN_STATUSES = Utils.set(
			new Qname(Evaluation.CR),
			new Qname(Evaluation.EN),
			new Qname(Evaluation.VU));

	public static final Comparator<? super Taxon> TAXON_ALPHA_COMPARATOR = new Comparator<Taxon>() {
		@Override
		public int compare(Taxon t1, Taxon t2) {
			String name1 = name(t1);
			String name2 = name(t2);
			return name1.compareTo(name2);
		}

		private String name(Taxon taxon) {
			if (given(taxon.getScientificName())) return taxon.getScientificName();
			if (given(taxon.getVernacularName().forLocale("en"))) return taxon.getVernacularName().forLocale("en");
			return '\uffff' + taxon.getQname().toString();
		}
	};

	public static final Comparator<? super Taxon> TAXONOMIC_ORDER_COMPARATOR = new Comparator<Taxon>() {
		@Override
		public int compare(Taxon t1, Taxon t2) {
			Long o1 = t1.getTaxonomicOrder();
			Long o2 = t2.getTaxonomicOrder();
			return o1.compareTo(o2);
		}
	};

	private final TaxonContainer taxonContainer;
	private final Qname qname;
	private long taxonomicOrder;
	private Qname parentQname;
	private Synonyms synonyms;
	private Qname checklist; //nameAccordingTo
	private Qname taxonRank;
	private String scientificName;
	private String scientificNameAuthorship;
	private Set<Qname> originalPublications;
	private Qname originalDescription;
	private final LocalizedText vernacularName = new LocalizedText();
	private final LocalizedTexts alternativeVernacularNames = new LocalizedTexts();
	private final LocalizedTexts obsoleteVernacularNames = new LocalizedTexts();
	private final LocalizedTexts colloquialVernacularNames = new LocalizedTexts();
	private final LocalizedTexts tradeNames = new LocalizedTexts();
	private Set<String> alsoKnownAs;
	private Set<String> overridingTargetNames;
	private Set<Qname> experts;
	private Set<Qname> editors;
	private Set<Qname> informalTaxonGroups;
	private Set<Qname> taxonSets;
	private Boolean hiddenTaxon = null;
	private Boolean stopOccurrenceInFinlandPublicationInheritance;
	private Boolean stopOriginalPublicationInheritance;
	private Boolean stopInformalTaxonGroupInheritance;
	private final Content descriptions = new Content();
	private boolean finnish = false;
	private Qname occurrenceInFinland;
	private Set<Qname> typesOfOccurrenceInFinland;
	private String typeOfOccurrenceInFinlandNotes;
	private Set<Qname> occurrenceInFinlandPublications;
	private Qname invasiveSpeciesCategory;
	private Qname invasiveSpeciesEstablishment;
	private boolean invasiveSpeciesEarlyWarning = false;
	private Set<Qname> invasiveSpeciesMainGroups;
	private Qname nameDecidedBy;
	private String nameDecidedDate;
	private String notes;
	private String privateNotes;
	private Integer sortOrder = Integer.MAX_VALUE;
	private int createdAtTimestamp = 0;
	private Occurrences occurrences;
	private HabitatOccurrenceCounts habitatOccurrenceCounts;
	private Qname secureLevel;
	private Qname breedingSecureLevel;
	private Qname winteringSecureLevel;
	private Qname nestSiteSecureLevel;
	private Qname naturaAreaSecureLevel;
	private URI customReportFormLink;
	private String birdlifeCode;
	private String euringCode;
	private Integer euringNumber;
	private List<Image> multimedia;
	private Set<LocalizedURL> externalLinks;
	private Set<Qname> administrativeStatuses;
	private Set<String> additionalIds;
	private Set<Qname> taxonConceptIds;
	private Map<Integer, RedListStatus> redListStatuses;
	private Map<Integer, Evaluation> redListEvaluations;
	private HabitatObject primaryHabitat;
	private List<HabitatObject> secondaryHabitats;
	private URI typeSpecimenURI;
	private URI occurrenceInFinlandSpecimenURI;
	private Integer frequencyScoringPoints;
	private Integer inheritedObservationCount;
	private Integer inheritedObservationCountFinland;
	private Integer inheritedObservationCountInvasiveFinland;
	private int explicitObservationCount = 0;
	private int explicitObservationCountFinland = 0;
	private boolean autoNonWild = false;
	private BoldRecords boldRecords;

	public Taxon(Qname qname, TaxonContainer taxonContainer) {
		if (qname == null || !qname.isSet()) throw new IllegalArgumentException("Empty taxon id given");
		this.qname = qname;
		this.taxonContainer = taxonContainer;
	}

	@PublicInformation(order=1.0)
	public Qname getQname() {
		return qname;
	}

	@PublicInformation(order=1.001)
	public Qname getId() {
		return getQname();
	}

	@PublicInformation(order=2)
	public Qname getChecklist() {
		return checklist;
	}

	@PublicInformation(order=3)
	public Qname getTaxonRank() {
		return taxonRank;
	}

	@PublicInformation(order=4.1)
	public String getScientificName() {
		return scientificName;
	}

	@PublicInformation(order=4.2)
	public String getScientificNameAuthorship() {
		return scientificNameAuthorship;
	}

	@PublicInformation(order=4.3)
	public String getScientificNameDisplayName() {
		if (given(this.getScientificName())) {
			if (!this.isSpecies()) {
				return this.getScientificName() + " sp.";
			}
			return this.getScientificName();
		}
		if (given(this.getVernacularName().forLocale("en"))) {
			return this.getVernacularName().forLocale("en"); // viruses
		}
		return this.getQname().toURI();
	}

	private Collection<Taxon> getTaxa(Collection<Qname> synonymTaxonIds) {
		List<Taxon> synonyms = new ArrayList<>();
		for (Qname synonymId : synonymTaxonIds) {
			if (this.getQname().equals(synonymId)) continue;
			if (!taxonContainer.hasTaxon(synonymId)) continue;
			synonyms.add(taxonContainer.getTaxon(synonymId));
		}
		Collections.sort(synonyms);
		return Collections.unmodifiableCollection(synonyms);
	}

	public boolean hasSynonyms() {
		return synonyms != null;
	}

	@PublicInformation(order=6.02)
	public Collection<Taxon> getBasionyms() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getBasionyms());
	}

	@PublicInformation(order=6.03)
	public Collection<Taxon> getObjectiveSynonyms() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getObjectiveSynonyms());
	}

	@PublicInformation(order=6.04)
	public Collection<Taxon> getSubjectiveSynonyms() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getSubjectiveSynonyms());
	}

	@PublicInformation(order=6.05)
	public Collection<Taxon> getHomotypicSynonyms() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getHomotypicSynonyms());
	}

	@PublicInformation(order=6.06)
	public Collection<Taxon> getHeterotypicSynonyms() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getHeterotypicSynonyms());
	}

	@PublicInformation(order=6.07)
	public Collection<Taxon> getSynonyms() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getSynonyms());
	}

	@PublicInformation(order=6.08)
	public Collection<Taxon> getMisspelledNames() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getMisspelledNames());
	}

	@PublicInformation(order=6.09)
	public Collection<Taxon> getOrthographicVariants() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getOrthographicVariants());
	}

	@PublicInformation(order=6.10)
	public Collection<Taxon> getUncertainSynonyms() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getUncertainSynonyms());
	}

	@PublicInformation(order=6.11)
	public Collection<Taxon> getMisappliedNames() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getMisappliedNames());
	}

	@PublicInformation(order=6.12)
	public Collection<Taxon> getAlternativeNames() {
		if (!this.hasSynonyms()) {
			return Collections.emptyList();
		}
		return getTaxa(synonyms.getAlternativeNames());
	}

	public Synonyms addSynonyms() {
		if (this.synonyms == null) {
			this.synonyms = new Synonyms();
		}
		return synonyms;
	}

	protected Synonyms getSynonymsContainer() {
		return synonyms;
	}

	public Collection<Taxon> getAllSynonyms() {
		if (!this.hasSynonyms()) return Collections.emptyList();
		return getTaxa(synonyms.getAll());
	}

	@PublicInformation(order=6.01)
	public String getSynonymNames() {
		Collection<Taxon> synonyms = getAllSynonyms();
		if (synonyms.isEmpty()) return null;
		StringBuilder b = new StringBuilder();
		Iterator<Taxon> i = synonyms.iterator();
		while (i.hasNext()) {
			Taxon synonym = i.next();
			b.append(given(synonym.getScientificName()) ? synonym.getScientificName() : synonym.getQname().toString());
			if (i.hasNext()) {
				b.append(", ");
			}
		}
		return b.toString();
	}

	public boolean isSynonym() {
		return this.getChecklist() == null;
	}

	public Taxon getSynonymParent() {
		Qname synonymParent = taxonContainer.getSynonymParent(this.getQname());
		if (synonymParent == null) return null;
		return taxonContainer.getTaxon(synonymParent);
	}

	@PublicInformation(order=8.1)
	public LocalizedText getVernacularName() {
		return vernacularName;
	}

	@PublicInformation(order=8.2)
	public LocalizedTexts getAlternativeVernacularNames() {
		return alternativeVernacularNames;
	}

	@PublicInformation(order=8.3)
	public LocalizedTexts getObsoleteVernacularNames() {
		return obsoleteVernacularNames;
	}

	@PublicInformation(order=8.4)
	public LocalizedTexts getColloquialVernacularNames() {
		return colloquialVernacularNames;
	}

	@PublicInformation(order=8.5)
	public LocalizedTexts getTradeNames() {
		return tradeNames;
	}

	@PublicInformation(order=20)
	public Qname getOccurrenceInFinland() {
		return occurrenceInFinland;
	}

	@PublicInformation(order=21)
	public Set<Qname> getTypesOfOccurrenceInFinland() {
		if (typesOfOccurrenceInFinland == null) return Collections.emptySet();
		return Collections.unmodifiableSet(typesOfOccurrenceInFinland);
	}

	@PublicInformation(order=22)
	public Set<Qname> getOccurrenceInFinlandPublications() {
		if (this.isSpecies() && !this.isFinnish()) return Collections.emptySet();
		if (!this.getExplicitlySetOccurrenceInFinlandPublications().isEmpty()) {
			return this.getExplicitlySetOccurrenceInFinlandPublications();
		}
		if (!this.hasParent() || this.isSetToStopOccurrenceInFinlandPublicationInheritance()) {
			return Collections.emptySet();
		}
		return this.getParent().getOccurrenceInFinlandPublications();
	}

	@PublicInformation(order=23)
	public String getTypeOfOccurrenceInFinlandNotes() {
		return typeOfOccurrenceInFinlandNotes;
	}

	public void setTypeOfOccurrenceInFinlandNotes(String notes) {
		this.typeOfOccurrenceInFinlandNotes = notes;
	}

	@PublicInformation(order=30)
	public Set<Qname> getOriginalPublications() {
		if (!this.getExplicitlySetOriginalPublications().isEmpty()) {
			return this.getExplicitlySetOriginalPublications();
		}
		if (!this.hasParent() || this.isSetToStopOriginalPublicationInheritance()) {
			return Collections.emptySet();
		}
		return this.getParent().getOriginalPublications();
	}

	public Taxon setChecklist(Qname checklist) {
		this.checklist = checklist;
		return this;
	}

	public Taxon addVernacularName(String locale, String name) {
		vernacularName.set(locale, name);
		return this;
	}

	public Taxon addAlternativeVernacularName(String locale, String name) {
		alternativeVernacularNames.add(locale, name);
		return this;
	}

	public Taxon addObsoleteVernacularName(String locale, String name) {
		obsoleteVernacularNames.add(locale, name);
		return this;
	}

	public Taxon addColloquialVernacularName(String locale, String name) {
		colloquialVernacularNames.add(locale, name);
		return this;
	}

	public Taxon addTradeName(String locale, String name) {
		tradeNames.add(locale, name);
		return this;
	}

	public String getScientificNameOfRank(String taxonRank) {
		return getScientificNameOfRank(taxonRankToQname(taxonRank));
	}

	private Qname taxonRankToQname(String taxonRank) {
		if (taxonRank == null) return null;
		if (!taxonRank.startsWith("MX.")) {
			taxonRank = "MX." + taxonRank;
		}
		Qname qname = new Qname(taxonRank);
		return qname;
	}

	public String getScientificNameOfRank(Qname taxonRank) {
		Taxon t = getParentOfRank(taxonRank);
		if (t == null) return null;
		return t.getScientificName();
	}

	public String getKingdomScientificName() {
		return getScientificNameOfRank(KINGDOM);
	}

	public String getNameFinnish() {
		return vernacularName("fi");
	}

	private String vernacularName(String locale) {
		if (vernacularName.hasTextForLocale(locale)) {
			return vernacularName.forLocale(locale);
		}
		return null;
	}

	public String getNameSwedish() {
		return vernacularName("sv");
	}

	public String getNameEnglish() {
		return vernacularName("en");
	}

	public Taxon getParentOfRank(String taxonRank) {
		return getParentOfRank(taxonRankToQname(taxonRank));
	}

	public Taxon getParentOfRank(Qname taxonRank) {
		if (taxonRank == null) return null;
		if (taxonRank.equals(this.getTaxonRank())) {
			return this;
		}
		if (this.hasParent()) {
			return this.getParent().getParentOfRank(taxonRank);
		}
		return null;
	}

	public List<Taxon> getChildren(boolean onlyFinnish, boolean alphabetic) {
		List<Taxon> children = getChildren(onlyFinnish);
		if (alphabetic) {
			Collections.sort(children, TAXON_ALPHA_COMPARATOR);
		}
		return children;
	}

	public List<Taxon> getChildren() {
		return getChildren(false);
	}

	public List<Taxon> getChildrenOnlyFinnish() {
		return getChildren(true);
	}

	public List<Taxon> getChildren(boolean onlyFinnish) {
		List<Taxon> children = new ArrayList<>();
		for (Qname child : taxonContainer.getChildren(this.getQname())) {
			if (!taxonContainer.hasTaxon(child)) continue;
			Taxon childTaxon = taxonContainer.getTaxon(child);
			if (onlyFinnish) {
				if (childTaxon.isFinnish()) {
					children.add(childTaxon);
				}
			} else {
				children.add(childTaxon);
			}
		}
		Collections.sort(children);
		return children;
	}

	private Boolean containsOrIsFinnishTaxon = null;

	@PublicInformation(order=5001)
	public boolean isFinnish() {
		if (containsOrIsFinnishTaxon == null) {
			containsOrIsFinnishTaxon = findOutIfThisContainsOrIsFinnishTaxon();
		}
		return containsOrIsFinnishTaxon;
	}

	private boolean findOutIfThisContainsOrIsFinnishTaxon() {
		if (this.isMarkedAsFinnishTaxon()) {
			return true;
		}
		for (Taxon child : this.getChildren()) {
			if (child.isFinnish()) return true;
		}
		return false;
	}

	@PublicInformation(order=1.21)
	public Qname getParentQname() {
		return parentQname;
	}

	public void setParentQname(Qname parentQname) {
		this.parentQname = parentQname;
	}

	@PublicInformation(order=1.22)
	public Qname getNonHiddenParentQname() {
		if (!this.hasParent()) return null;
		Taxon parent = getParent();
		while (parent.isHidden()) {
			if (!parent.hasParent()) return null;
			parent = parent.getParent();
		}
		return parent.getQname();
	}

	@PublicInformation(order=1.23)
	public List<Qname> getParentChain() {
		List<Qname> ids = new ArrayList<>();
		Taxon t = this;
		while (t.hasParent()) {
			t = t.getParent();
			ids.add(t.getQname());
		}
		Collections.reverse(ids);
		return ids;
	}

	@PublicInformation(order=1.24)
	public List<Qname> getNonHiddenParentChain() {
		List<Qname> ids = new ArrayList<>();
		Taxon t = this;
		while (t.getNonHiddenParentQname() != null) {
			t = taxonContainer.getTaxon(t.getNonHiddenParentQname());
			ids.add(t.getQname());
		}
		Collections.reverse(ids);
		return ids;
	}

	@PublicInformation(order=1.25)
	public int getDepth() {
		return getParentChain().size();
	}

	@PublicInformation(order=1.26)
	public int getNonHiddenDepth() {
		return getNonHiddenParentChain().size();
	}

	@PublicInformation(order=1.27)
	public List<Qname> getParentChainIncludeSelf() {
		List<Qname> ids = getParentChain();
		ids.add(this.getQname());
		return ids;
	}

	@PublicInformation(order=1.28)
	public List<Qname> getNonHiddenParentChainIncludeSelf() {
		List<Qname> ids = getNonHiddenParentChain();
		ids.add(this.getQname());
		return ids;
	}

	public Taxon getParent() {
		if (!this.hasParent()) throw new IllegalStateException("Taxon " + this.getQname() + " does not have a parent");
		return taxonContainer.getTaxon(this.getParentQname());
	}

	@PublicInformation(order=7000.1)
	public boolean hasParent() {
		if (!given(getParentQname())) return false;
		return taxonContainer.hasTaxon(getParentQname());
	}

	@PublicInformation(order=7000.2)
	public boolean hasChildren() {
		return getChildren().size() > 0;
	}

	@PublicInformation(order=7000.3)
	public boolean hasMultimedia() {
		return multimedia != null && !multimedia.isEmpty();
	}

	@PublicInformation(order=7000.4)
	public boolean hasDescriptions() {
		return !getDescriptions().isEmpty();
	}

	private static boolean given(Object o) {
		return o != null && o.toString().length() > 0;
	}

	public Taxon setScientificName(String name) {
		this.scientificName = name;
		return this;
	}

	public Taxon setScientificNameAuthorship(String author) {
		this.scientificNameAuthorship = author;
		return this;
	}

	public Taxon setTaxonRank(Qname taxonRank) {
		this.taxonRank = taxonRank;
		return this;
	}

	public Taxon addExpert(Qname personQname) {
		if (experts == null) experts = new HashSet<>(0);
		experts.add(personQname);
		return this;
	}

	public Taxon addEditor(Qname personQname) {
		if (editors == null) editors = new HashSet<>(0);
		editors.add(personQname);
		return this;
	}

	public Taxon addInformalTaxonGroup(Qname groupQname) {
		if (groupQname == null) return this;
		if (informalTaxonGroups == null) informalTaxonGroups = new HashSet<>(4);
		informalTaxonGroups.add(groupQname);
		return this;
	}

	@PublicInformation(order=10.0)
	public Set<Qname> getInformalTaxonGroups() {
		return Collections.unmodifiableSet(orderInformalTaxonGroups(getInformalTaxonGroupsNoOrder()));
	}

	public LocalizedText getInformalTaxonGroupNames() {
		Set<Qname> informalGroups = getInformalTaxonGroups();
		return taxonContainer.getInformalTaxonGroupNames(informalGroups);
	}

	public LocalizedText getAdministrativeStatusNames() {
		Set<Qname> statuses = getAdministrativeStatuses();
		return taxonContainer.getAdministrativeStatusNames(statuses);
	}

	private Set<Qname> orderInformalTaxonGroups(Set<Qname> informalTaxonGroups) {
		return taxonContainer.orderInformalTaxonGroups(informalTaxonGroups);
	}

	public Set<Qname> getInformalTaxonGroupsNoOrder() {
		Set<Qname> groups = new HashSet<>(4);
		groups.addAll(getExplicitlySetInformalTaxonGroups());
		if (!groups.isEmpty()) {
			Set<Qname> parents = new HashSet<>();
			for (Qname groupQname : groups) {
				parents.addAll(taxonContainer.getParentInformalTaxonGroups(groupQname));
			}
			groups.addAll(parents);
		}
		if (!this.hasParent() || this.isSetToStopInformalTaxonGroupInheritance()) {
			return groups;
		}
		groups.addAll(getParent().getInformalTaxonGroupsNoOrder());
		return groups;
	}

	public Set<Qname> getExplicitlySetInformalTaxonGroups() {
		if (informalTaxonGroups == null) return Collections.emptySet();
		return Collections.unmodifiableSet(informalTaxonGroups);
	}

	@PublicInformation(order=12)
	public Set<Qname> getRedListEvaluationGroups() {
		return Collections.unmodifiableSet(orderRedListEvaluationGroups(getRedListEvaluationGroupsNoOrder()));
	}

	private Set<Qname> orderRedListEvaluationGroups(Set<Qname> groups) {
		return taxonContainer.orderRedListEvaluationGroups(groups);
	}

	public Set<Qname> getRedListEvaluationGroupsNoOrder() {
		Set<Qname> groups = new HashSet<>(4);
		groups.addAll(this.getExplicitlySetRedListEvaluationGroups());
		if (this.hasParent()) {
			groups.addAll(getParent().getRedListEvaluationGroupsNoOrder());
		}
		Set<Qname> parents = new HashSet<>(4);
		for (Qname group : groups) {
			parents.addAll(taxonContainer.getParentRedListEvaluationGroups(group));
		}
		groups.addAll(parents);
		return groups;
	}

	private Set<Qname> getExplicitlySetRedListEvaluationGroups() {
		Set<Qname> groupsUsingThisTaxonAsBase = taxonContainer.getRedListEvaluationGroupsOfTaxon(this.getQname());
		Set<Qname> groupsUsingThisTaxonsInformalTaxonGroupsAsBase = getRedListEvaluationGroupsOfInformalGroups();

		if (groupsUsingThisTaxonAsBase.isEmpty() && groupsUsingThisTaxonsInformalTaxonGroupsAsBase.isEmpty()) return Collections.emptySet();

		Set<Qname> all = new HashSet<>(4);
		all.addAll(groupsUsingThisTaxonAsBase);
		all.addAll(groupsUsingThisTaxonsInformalTaxonGroupsAsBase);
		return all;
	}

	private Set<Qname> getRedListEvaluationGroupsOfInformalGroups() {
		if (this.getExplicitlySetInformalTaxonGroups().isEmpty()) return Collections.emptySet();
		Set<Qname> groups = new HashSet<>(4);
		for (Qname informalGroup : this.getExplicitlySetInformalTaxonGroups()) {
			groups.addAll(taxonContainer.getRedListEvaluationGroupsOfInformalTaxonGroup(informalGroup));
		}
		return groups;
	}

	@PublicInformation(order=10.1)
	public Qname getThreatenedStatus() {
		Set<Qname> statuses = getAdministrativeStatusesNoOrder();
		for (Qname s : statuses) {
			if (ADMIN_STATUSES_STATUTORY.contains(s)) return STATUTORY;
		}
		if (getLatestRedListStatusFinland() != null) {
			if (THREATENED_IUCN_STATUSES.contains(getLatestRedListStatusFinland().getStatus())) {
				return THREATENED;
			}
			if (getLatestRedListStatusFinland().getStatus().equals(NT)) return NEAR_THREATENED;
		}
		return null;
	}

	@PublicInformation(order=41)
	public Set<Qname> getExperts() {
		if (this.hasExplicitlySetExperts()) {
			return Collections.unmodifiableSet(getExplicitlySetExperts());
		}
		if (this.hasParent()) {
			return this.getParent().getExperts();
		}
		return Collections.emptySet();
	}

	public boolean hasExplicitlySetExperts() {
		return !getExplicitlySetExperts().isEmpty();
	}

	public boolean hasExplicitlySetEditors() {
		return !getExplicitlySetEditors().isEmpty();
	}

	public boolean hasExpert() {
		return !getExperts().isEmpty();
	}

	public boolean hasEditor() {
		return !getEditors().isEmpty();
	}

	@PublicInformation(order=42)
	public Set<Qname> getEditors() {
		if (this.hasExplicitlySetEditors()) {
			return Collections.unmodifiableSet(getExplicitlySetEditors());
		}
		if (this.hasParent()) {
			return this.getParent().getEditors();
		}
		return Collections.emptySet();
	}

	public List<Taxon> getSpeciesAndSubSpecies() {
		List<Taxon> species = new ArrayList<>();
		if (this.isSpecies()) {
			species.add(this);
		}
		for (Taxon child : getChildren()) {
			species.addAll(child.getSpeciesAndSubSpecies());
		}
		return species;
	}

	public Set<Qname> getExplicitlySetEditors() {
		if (editors == null) return Collections.emptySet();
		return Collections.unmodifiableSet(editors);
	}

	public Set<Qname> getExplicitlySetExperts() {
		if (experts == null) return Collections.emptySet();
		return Collections.unmodifiableSet(experts);
	}

	private Integer countOfSpecies = null;

	@PublicInformation(order=6001)
	public int getCountOfSpecies() {
		if (countOfSpecies == null) {
			countOfSpecies = calculateCountOfSpecies();
		}
		return countOfSpecies;
	}

	private int calculateCountOfSpecies() {
		if (this.isMarkedHidden()) {
			if (this.hasChildren()) {
				return childTaxonSpeciesCount(false);
			}
			return 0;
		}
		if (!fromSameChecklistAsParent()) return 0;
		if (SPECIES.equals(this.getTaxonRank())) {
			return 1;
		}
		if (!this.hasChildren() && this.isSpecies()) {
			return 1;
		}
		int count = 0;
		for (Taxon child : getChildren()) {
			count += child.getCountOfSpecies();
		}
		return count;
	}

	private Integer countOfChildren = null;

	public int getCountOfChildren() {
		if (countOfChildren == null) {
			countOfChildren = calculateCountOfChildren();
		}
		return countOfChildren;
	}

	private Integer calculateCountOfChildren() {
		if (!this.hasChildren()) return 0;
		int count = 0;
		for (Taxon child : getChildren()) {
			count += child.getCountOfChildren() + 1;
		}
		return count;
	}

	private boolean fromSameChecklistAsParent() {
		if (!this.hasParent()) return true;
		if (this.getChecklist() == null) return false;
		return this.getChecklist().equals(this.getParent().getChecklist());
	}

	private Integer countOfFinnishSpecies = null;

	@PublicInformation(order=6002)
	public int getCountOfFinnishSpecies() {
		if (countOfFinnishSpecies == null) {
			countOfFinnishSpecies = calculateCountOfFinnishSpecies();
		}
		return countOfFinnishSpecies;
	}

	private int calculateCountOfFinnishSpecies() {
		if (!this.isFinnish()) return 0;
		if (this.isHidden()) {
			if (this.hasChildren()) {
				return childTaxonSpeciesCount(true);
			}
			return 0;
		}
		if (!fromSameChecklistAsParent()) return 0;
		if (SPECIES.equals(this.getTaxonRank())) {
			return 1;
		}
		if (!this.hasChildren() && this.isSpecies()) {
			return 1;
		}
		return childTaxonSpeciesCount(true);
	}

	private int childTaxonSpeciesCount(boolean onlyFinnish) {
		int count = 0;
		for (Taxon child : getChildren(onlyFinnish)) {
			if (onlyFinnish) {
				count += child.getCountOfFinnishSpecies();
			} else {
				count += child.getCountOfSpecies();
			}

		}
		return count;
	}

	@PublicInformation(order=4.4)
	public boolean isCursiveName() {
		return shouldCursive(this.getTaxonRank());
	}

	@PublicInformation(order=5002)
	public boolean isSpecies() {
		return isSpecies(this.getTaxonRank());
	}

	@PublicInformation(order=5003)
	public boolean isFinnishSpecies() {
		if (!this.isSpecies()) return false;
		return isFinnish();
	}

	public boolean isMarkedAsFinnishTaxon() {
		return finnish;
	}

	public Taxon setFinnish(boolean finnish) {
		this.finnish = finnish;
		return this;
	}

	@PublicInformation(order=101)
	public Content getDescriptions() {
		return descriptions;
	}

	@Override
	public int compareTo(Taxon o) {
		int order = this.getSortOrder().compareTo(o.getSortOrder());
		if (order != 0) return order;
		return this.getQname().compareTo(o.getQname());
	}

	public Taxon setOccurrenceInFinland(Qname occurrenceInFinland) {
		this.occurrenceInFinland = occurrenceInFinland;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o instanceof Taxon) {
			Taxon other = (Taxon) o;
			Qname thisQname = this.getQname();
			Qname otherQname = other.getQname();
			if (thisQname == null) thisQname = EMPTY_QNAME;
			if (otherQname == null) otherQname = EMPTY_QNAME;
			return thisQname.equals(otherQname);
		}
		throw new UnsupportedOperationException("Can only compare to other " + this.getClass().getName() +". Tried to compare with " + o.getClass() + " " + o.toString());
	}

	@Override
	public int hashCode() {
		Qname qname = getQname();
		if (qname == null) return EMPTY_QNAME.hashCode();
		return qname.hashCode();
	}

	public Set<Qname> getExplicitlySetOriginalPublications() {
		if (originalPublications == null) return Collections.emptySet();
		return Collections.unmodifiableSet(originalPublications);
	}

	public Taxon addOriginalPublication(Qname publication) {
		if (originalPublications == null) originalPublications = new HashSet<>(1);
		originalPublications.add(publication);
		return this;
	}

	@PublicInformation(order=31)
	public Qname getNameDecidedBy() {
		return nameDecidedBy;
	}

	public Taxon setNameDecidedBy(Qname person) {
		this.nameDecidedBy = person;
		return this;
	}

	public Date getNameDecidedDateAsDate() {
		try {
			return DateUtils.convertToDate(getNameDecidedDate(), "yyyy-MM-dd");
		} catch (Exception e) {
			return null;
		}
	}

	@PublicInformation(order=32)
	public String getNameDecidedDate() {
		return nameDecidedDate;
	}

	public Taxon setNameDecidedDate(String nameDecidedDate) {
		this.nameDecidedDate = nameDecidedDate;
		return this;
	}

	@PublicInformation(order=9000)
	public String getNotes() {
		return notes;
	}

	public Taxon setNotes(String notes) {
		this.notes = notes;
		return this;
	}

	public String getPrivateNotes() {
		return privateNotes;
	}

	public Taxon setPrivateNotes(String privateNotes) {
		this.privateNotes = privateNotes;
		return this;
	}

	public Taxon addTypeOfOccurrenceInFinland(Qname typeOfOccurenceInFinland) {
		if (typesOfOccurrenceInFinland == null) typesOfOccurrenceInFinland = new HashSet<>(1);
		typesOfOccurrenceInFinland.add(typeOfOccurenceInFinland);
		return this;
	}

	private Boolean invasiveObservationSpecies = null;

	@PublicInformation(order=5004)
	public boolean isInvasiveSpecies() {
		for (Qname adminStatus : this.getAdministrativeStatusesNoOrder()) {
			if (INVASIVE_SPECIES_ADMIN_STATUSES.contains(adminStatus)) return true;
		}
		return false;
	}

	public boolean isInvasiveObservationSpecies() {
		if (invasiveObservationSpecies == null) {
			invasiveObservationSpecies = resolveInvasiveObservationSpecies();
		}
		return invasiveObservationSpecies;
	}

	private Boolean resolveInvasiveObservationSpecies() {
		if (!isSpecies()) return false;
		if (isInvasiveSpecies()) return true;
		if (parentHasInvasiveSpeciesAdminStatus()) {
			if (!isFinnish()) return true;
			if (hasInvasiveTypeOfOccurrence()) return true;
		}
		return false;
	}

	private boolean hasInvasiveTypeOfOccurrence() {
		for (Qname typeOfOccurrence : getTypesOfOccurrenceInFinland()) {
			if (INVASIVE_SPECIES_TYPES_OF_OCCURRENCES.contains(typeOfOccurrence)) return true;
		}
		return false;
	}

	private boolean parentHasInvasiveSpeciesAdminStatus() {
		Taxon t = this;
		while (t.hasParent()) {
			t = t.getParent();
			if (t.isInvasiveSpecies()) return true;
		}
		return false;
	}

	private static final Set<Qname> STABLE = Utils.set(
			new Qname("MX.typeOfOccurrenceOccurs"), new Qname("MX.typeOfOccurrenceStablePopulation"),
			new Qname("MX.typeOfOccurrenceSpontaneousOldResident"), new Qname("MX.typeOfOccurrenceSpontaneousNewResident"));

	@PublicInformation(order=5005)
	public boolean isStableInFinland() {
		for (Qname type : getTypesOfOccurrenceInFinland()) {
			if (STABLE.contains(type)) {
				return true;
			}
		}
		for (Taxon child : getChildren(true)) {
			if (child.isStableInFinland()) return true;
		}
		return false;
	}

	public int getCreatedAtTimestamp() {
		return createdAtTimestamp;
	}

	public Taxon setCreatedAtTimestamp(int createdAtTimestamp) {
		this.createdAtTimestamp = createdAtTimestamp;
		return this;
	}

	public Set<Qname> getExplicitlySetOccurrenceInFinlandPublications() {
		if (occurrenceInFinlandPublications == null) return Collections.emptySet();
		return Collections.unmodifiableSet(occurrenceInFinlandPublications);
	}

	public Taxon addOccurrenceInFinlandPublication(Qname publication) {
		if (occurrenceInFinlandPublications == null) occurrenceInFinlandPublications = new HashSet<>(1);
		occurrenceInFinlandPublications.add(publication);
		return this;
	}

	public boolean expertChangesFromParent() {
		if (!this.hasExplicitlySetExperts()) return false;
		if (!this.hasParent()) return true;
		Set<Qname> myExperts = getExperts();
		Set<Qname> parentExperts = this.getParent().getExperts();
		return !myExperts.containsAll(parentExperts) || !parentExperts.containsAll(myExperts);
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public Taxon setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
		return this;
	}

	public boolean hasOccurrences() {
		if (occurrences == null) return false;
		return occurrences.hasOccurrences();
	}

	@PublicInformation(order=200)
	public Occurrences getOccurrences() {
		if (occurrences == null) {
			occurrences = new Occurrences(this.qname);
		}
		return occurrences;
	}

	public boolean hasHabitatOccurrenceCounts() {
		if (habitatOccurrenceCounts == null) return false;
		return habitatOccurrenceCounts.hasCounts();
	}

	@PublicInformation(order=201)
	public HabitatOccurrenceCounts getHabitatOccurrenceCounts() {
		if (habitatOccurrenceCounts == null) {
			habitatOccurrenceCounts = new HabitatOccurrenceCounts();
		}
		return habitatOccurrenceCounts;
	}

	public Qname getInvasiveSpeciesCategory() {
		return invasiveSpeciesCategory;
	}

	public void setInvasiveSpeciesCategory(Qname invasiveSpeciesCategory) {
		this.invasiveSpeciesCategory = invasiveSpeciesCategory;
	}

	@PublicInformation(order=97)
	public Qname getInvasiveSpeciesEstablishment() {
		return invasiveSpeciesEstablishment;
	}

	public void setInvasiveSpeciesEstablishment(Qname invasiveSpeciesEstablishment) {
		this.invasiveSpeciesEstablishment = invasiveSpeciesEstablishment;
	}

	public void setHiddenTaxon(boolean hidden) {
		this.hiddenTaxon = hidden;
	}

	@PublicInformation(order=1.4)
	public boolean isHidden() {
		if (isMarkedHidden()) return true;
		if (SPECIES_AGGREGATE.equals(getTaxonRank())) return false;
		if (!isSpecies() && getCountOfSpecies() <= 0) return true;
		return false;
	}

	protected boolean isMarkedHidden() {
		return hiddenTaxon != null && hiddenTaxon;
	}

	@PublicInformation(order=1000)
	public Collection<LocalizedURL> getExternalLinks() {
		if (externalLinks == null) return Collections.emptySet();
		return Collections.unmodifiableCollection(externalLinks);
	}

	public Taxon addExternalLink(LocalizedURL url) {
		if (externalLinks == null) externalLinks = new HashSet<>(1);
		externalLinks.add(url);
		return this;
	}

	public Taxon addAdministrativeStatus(Qname status) {
		if (administrativeStatuses == null) administrativeStatuses = new HashSet<>(3);
		administrativeStatuses.add(status);
		return this;
	}

	@PublicInformation(order=35)
	public Set<Qname> getAdministrativeStatuses() {
		return Collections.unmodifiableSet(orderAdministrativeStatuses(getAdministrativeStatusesNoOrder()));
	}

	private Set<Qname> orderAdministrativeStatuses(Set<Qname> administrativeStatuses) {
		return taxonContainer.orderAdministrativeStatuses(administrativeStatuses);
	}

	public Set<Qname> getAdministrativeStatusesNoOrder() {
		if (administrativeStatuses == null) return Collections.emptySet();
		return administrativeStatuses;
	}

	public Set<String> getAlsoKnownAsNames() {
		if (alsoKnownAs == null) return Collections.emptySet();
		return Collections.unmodifiableSet(alsoKnownAs);
	}

	public void addAlsoKnownAsName(String name) {
		if (alsoKnownAs == null) alsoKnownAs = new HashSet<>(3);
		alsoKnownAs.add(name);
	}

	public Set<String> getOverridingTargetNames() {
		if (overridingTargetNames == null) return Collections.emptySet();
		return Collections.unmodifiableSet(overridingTargetNames);
	}

	public void addOverridingTargetName(String name) {
		if (overridingTargetNames == null) overridingTargetNames = new HashSet<>(3);
		overridingTargetNames.add(name);
	}


	@PublicInformation(order=150.0)
	public Qname getSecureLevel() {
		return secureLevel;
	}

	public void setSecureLevel(Qname secureLevel) {
		this.secureLevel = secureLevel;
	}

	@PublicInformation(order=150.1)
	public Qname getBreedingSecureLevel() {
		return breedingSecureLevel;
	}

	public void setBreedingSecureLevel(Qname breedingSecureLevel) {
		this.breedingSecureLevel = breedingSecureLevel;
	}

	@PublicInformation(order=150.2)
	public Qname getWinteringSecureLevel() {
		return winteringSecureLevel;
	}

	public void setWinteringSecureLevel(Qname winteringSecureLevel) {
		this.winteringSecureLevel = winteringSecureLevel;
	}

	@PublicInformation(order=150.3)
	public Qname getNestSiteSecureLevel() {
		return nestSiteSecureLevel;
	}

	public void setNestSiteSecureLevel(Qname nestSiteSecureLevel) {
		this.nestSiteSecureLevel = nestSiteSecureLevel;
	}

	@PublicInformation(order=150.5)
	public Qname getNaturaAreaSecureLevel() {
		return naturaAreaSecureLevel;
	}

	public void setNaturaAreaSecureLevel(Qname naturaAreaSecureLevel) {
		this.naturaAreaSecureLevel = naturaAreaSecureLevel;
	}

	@PublicInformation(order=151)
	public boolean isSensitive() {
		return hasSecureLevel();
	}

	public static final Set<Qname> VIRVA_RED_LIST_STATUSES = Utils.set(new Qname(Evaluation.CR), new Qname(Evaluation.EN), new Qname(Evaluation.VU), new Qname(Evaluation.NT));
	public static final Set<Qname> VIRVA_ADMIN_STATUSES = Utils.set(
			new Qname("MX.finlex160_1997_appendix4_2021"),
			new Qname("MX.finlex160_1997_appendix4_specialInterest_2021"),
			new Qname("MX.finlex160_1997_appendix2a"),
			new Qname("MX.finlex160_1997_appendix2b"),
			new Qname("MX.finlex160_1997_appendix3a"),
			new Qname("MX.finlex160_1997_appendix3b"),
			new Qname("MX.finlex160_1997_appendix3c"),
			new Qname("MX.finlex160_1997_largeBirdsOfPrey"),
			new Qname("MX.habitatsDirectiveAnnexII"),
			new Qname("MX.habitatsDirectiveAnnexIV"),
			new Qname("MX.birdsDirectiveStatusAppendix1"),
			new Qname("MX.birdsDirectiveStatusMigratoryBirds"));

	public static boolean isVirva(Set<Qname> redListStatuses, Set<Qname> adminStatuses) {
		if (redListStatuses == null || adminStatuses == null) return false;
		if (redListStatuses.size() != VIRVA_RED_LIST_STATUSES.size()) return false;
		if (adminStatuses.size() != VIRVA_ADMIN_STATUSES.size()) return false;
		if (!VIRVA_RED_LIST_STATUSES.containsAll(redListStatuses)) return false;
		if (!VIRVA_ADMIN_STATUSES.containsAll(adminStatuses)) return false;
		return true;
	}

	public boolean isVirva() {
		if (getLatestRedListStatusFinland() != null) {
			if (VIRVA_RED_LIST_STATUSES.contains(getLatestRedListStatusFinland().getStatus())) {
				return true;
			}
		}
		for (Qname adminStatus : getAdministrativeStatusesNoOrder()) {
			if (VIRVA_ADMIN_STATUSES.contains(adminStatus)) {
				return true;
			}
		}
		return false;
	}

	public boolean hasSecureLevel() {
		return secureLevel != null || breedingSecureLevel != null || winteringSecureLevel != null || nestSiteSecureLevel != null || naturaAreaSecureLevel != null;
	}

	@PublicInformation(order=370)
	public String getBirdlifeCode() {
		return birdlifeCode;
	}

	public void setBirdlifeCode(String birdlifeCode) {
		this.birdlifeCode = birdlifeCode;
	}

	@PublicInformation(order=371)
	public String getEuringCode() {
		return euringCode;
	}

	public void setEuringCode(String euringCode) {
		this.euringCode = euringCode;
	}

	@PublicInformation(order=372)
	public Integer getEuringNumber() {
		return euringNumber;
	}

	public void setEuringNumber(Integer euringNumber) {
		this.euringNumber = euringNumber;
	}

	@PublicInformation(order=390)
	public URI getCustomReportFormLink() {
		return customReportFormLink;
	}

	public void setCustomReportFormLink(URI customReportFormLink) {
		this.customReportFormLink = customReportFormLink;
	}

	@PublicInformation(order=8000)
	public Set<Qname> getInvasiveSpeciesMainGroups() {
		if (invasiveSpeciesMainGroups == null) return Collections.emptySet();
		return Collections.unmodifiableSet(invasiveSpeciesMainGroups);
	}

	public void addInvasiveSpeciesMainGroup(Qname invasiveSpeciesMainGroup) {
		if (invasiveSpeciesMainGroups == null) invasiveSpeciesMainGroups = new HashSet<>(3);
		invasiveSpeciesMainGroups.add(invasiveSpeciesMainGroup);
	}

	@PublicInformation(order=8001)
	public Set<Qname> getTaxonSets() {
		if (taxonSets == null) return Collections.emptySet();
		return Collections.unmodifiableSet(taxonSets);
	}

	public void addTaxonSet(Qname taxonSet) {
		if (taxonSets == null) taxonSets = new HashSet<>(3);
		taxonSets.add(taxonSet);
	}

	public boolean isSetToStopOccurrenceInFinlandPublicationInheritance() {
		return stopOccurrenceInFinlandPublicationInheritance != null && stopOccurrenceInFinlandPublicationInheritance;
	}

	public void setStopOccurrenceInFinlandPublicationInheritance(Boolean stopOccurrenceInFinlandPublicationInheritance) {
		this.stopOccurrenceInFinlandPublicationInheritance = stopOccurrenceInFinlandPublicationInheritance;
	}

	public boolean isSetToStopOriginalPublicationInheritance() {
		return stopOriginalPublicationInheritance != null && stopOriginalPublicationInheritance;
	}

	public void setStopOriginalPublicationInheritance(Boolean stopOriginalPublicationInheritance) {
		this.stopOriginalPublicationInheritance = stopOriginalPublicationInheritance;
	}

	public boolean isSetToStopInformalTaxonGroupInheritance() {
		return stopInformalTaxonGroupInheritance != null && stopInformalTaxonGroupInheritance;
	}

	public void setStopInformalTaxonGroupInheritance(Boolean stopInformalTaxonGroupInheritance) {
		this.stopInformalTaxonGroupInheritance = stopInformalTaxonGroupInheritance;
	}

	public boolean isInvasiveSpeciesEarlyWarning() {
		return invasiveSpeciesEarlyWarning;
	}

	public void setInvasiveSpeciesEarlyWarning(boolean invasiveSpeciesEarlyWarning) {
		this.invasiveSpeciesEarlyWarning = invasiveSpeciesEarlyWarning;
	}

	public void addMultimedia(Image image) {
		if (image.getTaxon() == null) throw new IllegalStateException("Image " + image.getId() + " does not have a taxon set");
		if (multimedia == null) {
			multimedia = new ArrayList<>();
		}
		if (alreadyExists(image)) return;
		image.doLegacyConversions();
		multimedia.add(image);
	}

	public void addCopyForSelf(Image image) {
		if (alreadyExists(image)) return;
		addMultimedia(image.copy().setTaxon(this));
	}

	private boolean alreadyExists(Image image) {
		for (Image existing : getMultimedia()) {
			if (existing.getId().equals(image.getId())) return true;
		}
		return false;
	}

	public void sortAndRetainTopMultimedia(int top) {
		if (multimedia == null) return;
		sort();
		if (multimedia.size() <= top) return;
		multimedia = getMultimedia().stream().limit(top).collect(Collectors.toList());
	}

	private void sort() {
		Collections.sort(multimedia, comparator());
		multimedia = new CategorizedTaxonImages(this).getGroupedFlatImages();
	}

	public void clearMultimedia() {
		if (multimedia != null) multimedia.clear();
		categorizedTaxonImages = null;
	}

	@PublicInformation(order=100)
	public List<Image> getMultimedia() {
		if (multimedia == null) return Collections.emptyList();
		return Collections.unmodifiableList(multimedia);
	}

	private CategorizedTaxonImages categorizedTaxonImages = null;
	private final Object lock = new Object();

	public CategorizedTaxonImages getCategorizedMultimedia() {
		if (categorizedTaxonImages == null) {
			synchronized (lock) {
				if (categorizedTaxonImages == null) {
					if (multimedia != null) {
						Collections.sort(multimedia, comparator());
					}
					categorizedTaxonImages = new CategorizedTaxonImages(this);
				}
			}
		}
		return categorizedTaxonImages;
	}

	private Comparator<Image> comparator() {
		return new Comparator<Image>() {
			@Override
			public int compare(Image i1, Image i2) {
				// if i1 should be before i2 -> return negative integer
				// if i1 should be after i2 -> return positive integer

				// if taxon has images where source taxon is the taxon itself, sort those primary images first
				// if for example a family has images set directly to the family, if they want to be shown (instead of images of child species), they must be marked as primary images
				boolean isSelfPrimary1 = isSelfPrimary(i1);
				boolean isSelfPrimary2 = isSelfPrimary(i2);
				if (isSelfPrimary1 && !isSelfPrimary2) return -1;
				if (!isSelfPrimary1 && isSelfPrimary2) return 1;
				if (isSelfPrimary1 && isSelfPrimary2) {
					return sort(i1, i2);
				}

				// if source taxon of i1 and i2 are the same taxon, sort primary images first
				if (sameTaxon(i1, i2)) {
					if (i1.isPrimaryForTaxon() && !i2.isPrimaryForTaxon()) return -1;
					if (!i1.isPrimaryForTaxon() && i2.isPrimaryForTaxon()) return 1;
					return sort(i1, i2);
				}

				// if source taxa are not the same, prefer species then subspecies etc
				boolean isExactlySpecies1 = isExactlySpecies(i1);
				boolean isExactlySpecies2 = isExactlySpecies(i2);
				if (isExactlySpecies1 && !isExactlySpecies2) return -1;
				if (!isExactlySpecies1 && isExactlySpecies2) return 1;

				boolean isSpecies1 = isSpecies(i1);
				boolean isSpecies2 = isSpecies(i2);
				if (isSpecies1 && !isSpecies2) return -1;
				if (!isSpecies1 && isSpecies2) return 1;

				// if sorting by user defined sort order or image type has differences, use this
				int c = sortBySortOrder(i1, i2);
				if (c != 0) return c;

				// Sort by occurrence count
				int occCount1 = getOccCount(i1);
				int occCount2 = getOccCount(i2);
				c = -1 * Integer.compare(occCount1, occCount2);
				if (c != 0) return c;

				return sort(i1, i2);
			}

			private int sortBySortOrder(Image i1, Image i2) {
				return i1.getSortOrder().compareTo(i2.getSortOrder()); // user defined sort order or order derived from image type (specimen, skeletal etc)
			}

			private int sort(Image i1, Image i2) {
				int c = sortBySortOrder(i1, i2);
				if (c != 0) return c;
				return i1.getFullURL().compareTo(i2.getFullURL());
			}

			private boolean sameTaxon(Image i1, Image i2) {
				if (i1.getTaxon() == null || i2.getTaxon() == null) return false;
				return i1.getTaxon().getQname().equals(i2.getTaxon().getQname());
			}

			private int getOccCount(Image i) {
				if (i.getTaxon() == null) return 0;
				return i.getTaxon().getObservationCountFinland();
			}

			private boolean isSelfPrimary(Image i) {
				if (!i.isPrimaryForTaxon()) return false;
				if (i.getTaxon() == null) return false;
				return i.getTaxon().getQname().equals(Taxon.this.getQname());
			}

			private boolean isSpecies(Image i) {
				if (i.getTaxon() == null) return false;
				return i.getTaxon().isFinnishSpecies();
			}

			private boolean isExactlySpecies(Image i) {
				if (!isSpecies(i)) return false;
				return SPECIES.equals(i.getTaxon().getTaxonRank());
			}
		};
	}

	@PublicInformation(order=401)
	public Set<String> getAdditionalIds() {
		if (additionalIds == null) return Collections.emptySet();
		return additionalIds;
	}

	public void addAdditionalId(String additionalId) {
		if (additionalIds == null) additionalIds = new HashSet<>(3);
		additionalIds.add(additionalId);
	}

	@PublicInformation(order=400)
	public Set<Qname> getTaxonConceptIds() {
		if (taxonConceptIds == null) return Collections.emptySet();
		return taxonConceptIds;
	}

	public void addTaxonConceptId(Qname conceptId) {
		if (taxonConceptIds == null) taxonConceptIds = new HashSet<>(3);
		taxonConceptIds.add(conceptId);
	}

	@PublicInformation(order=40.0)
	public RedListStatus getLatestRedListStatusFinland() {
		if (getRedListStatusesInFinland().isEmpty()) return null;
		return getRedListStatusesInFinland().iterator().next();
	}

	private static final Map<Qname, Qname> IUCN_GROUPS;
	static {
		IUCN_GROUPS = new HashMap<>();
		Qname group1 = new Qname("MX.iucnGroup1");
		Qname group5 = new Qname("MX.iucnGroup5");
		IUCN_GROUPS.put(new Qname(Evaluation.DD), group1);
		IUCN_GROUPS.put(new Qname(Evaluation.NA), group1);
		IUCN_GROUPS.put(new Qname(Evaluation.NE), group1);
		IUCN_GROUPS.put(new Qname(Evaluation.LC), group1);
		IUCN_GROUPS.put(new Qname(Evaluation.NT), new Qname("MX.iucnGroup2"));
		IUCN_GROUPS.put(new Qname(Evaluation.VU), new Qname("MX.iucnGroup3"));
		IUCN_GROUPS.put(new Qname(Evaluation.EN), new Qname("MX.iucnGroup4"));
		IUCN_GROUPS.put(new Qname(Evaluation.CR), group5);
		IUCN_GROUPS.put(new Qname(Evaluation.RE), group5);
		IUCN_GROUPS.put(new Qname(Evaluation.EW), group5);
		IUCN_GROUPS.put(new Qname(Evaluation.EX), group5);
	}

	public Qname getRedListStatusGroup() {
		RedListStatus s = getLatestRedListStatusFinland();
		if (s == null) return null;
		return IUCN_GROUPS.get(s.getStatus());
	}

	@PublicInformation(order=40.1)
	public Collection<RedListStatus> getRedListStatusesInFinland() {
		if (redListStatuses == null) return Collections.emptyList();
		return redListStatuses.values();
	}

	public Collection<Evaluation> getRedListEvaluations() {
		if (redListEvaluations == null) return Collections.emptyList();
		return redListEvaluations.values();
	}

	public void addRedListEvaluation(Evaluation evaluation) {
		if (redListEvaluations == null) redListEvaluations = integerTreeMap();
		redListEvaluations.put(evaluation.getEvaluationYear(), evaluation);
	}

	public Evaluation getLatestRedListEvaluation() {
		if (redListEvaluations == null) return null;
		int latestEvaluationYear = taxonContainer.getLatestLockedRedListEvaluationYear();
		for (Evaluation e : getRedListEvaluations()) {
			if (e.getEvaluationYear().intValue() == latestEvaluationYear) return e;
		}
		return null;
	}

	public Qname getRedListStatusForYear(int year) {
		if (redListStatuses == null) return null;
		RedListStatus redListStatus = redListStatuses.get(year);
		if (redListStatus == null) return null;
		return redListStatus.getStatus();
	}

	public Qname getRedListStatus2000Finland() {
		return getRedListStatusForYear(2000);
	}

	public Qname getRedListStatus2010Finland() {
		return getRedListStatusForYear(2010);
	}

	public Qname getRedListStatus2015Finland() {
		return getRedListStatusForYear(2015);
	}

	public Qname getRedListStatus2019Finland() {
		return getRedListStatusForYear(2019);
	}

	public Taxon setRedListStatus2000Finland(Qname status) {
		initRedListStatusesMap();
		redListStatuses.put(2000, new RedListStatus(2000, status));
		return this;
	}

	public Taxon setRedListStatus2010Finland(Qname status) {
		initRedListStatusesMap();
		redListStatuses.put(2010, new RedListStatus(2010, status));
		return this;
	}

	public Taxon setRedListStatus2015Finland(Qname status) {
		initRedListStatusesMap();
		redListStatuses.put(2015, new RedListStatus(2015, status));
		return this;
	}

	public Taxon setRedListStatus2019Finland(Qname status) {
		initRedListStatusesMap();
		redListStatuses.put(2019, new RedListStatus(2019, status));
		return this;
	}

	private void initRedListStatusesMap() {
		if (redListStatuses != null) return;
		redListStatuses = integerTreeMap();
	}

	private <T> TreeMap<Integer, T> integerTreeMap() {
		return new TreeMap<>(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2.compareTo(o1);
			}
		});
	}

	@PublicInformation(order=39.1)
	public HabitatObject getPrimaryHabitat() {
		return primaryHabitat;
	}

	public void setPrimaryHabitat(HabitatObject habitat) {
		this.primaryHabitat = habitat;
	}

	@PublicInformation(order=39.2)
	public List<HabitatObject> getSecondaryHabitats() {
		if (secondaryHabitats == null) return Collections.emptyList();
		return secondaryHabitats;
	}

	public void addSecondaryHabitat(HabitatObject habitat) {
		if (habitat == null) return;
		if (secondaryHabitats == null) secondaryHabitats = new ArrayList<>(3);
		if (secondaryHabitats.contains(habitat)) return;
		secondaryHabitats.add(habitat);
		Collections.sort(secondaryHabitats);
	}

	public long getTaxonomicOrder() {
		return taxonomicOrder;
	}

	public void setTaxonomicOrder(long taxonomicOrder) {
		this.taxonomicOrder = taxonomicOrder;
	}

	@PublicInformation(order=4.5)
	public URI getTypeSpecimenURI() {
		return typeSpecimenURI;
	}

	public void setTypeSpecimenURI(URI typeSpecimenURI) {
		this.typeSpecimenURI = typeSpecimenURI;
	}

	@PublicInformation(order=20.5)
	public URI getOccurrenceInFinlandSpecimenURI() {
		return occurrenceInFinlandSpecimenURI;
	}

	public void setOccurrenceInFinlandSpecimenURI(URI occurrenceInFinlandSpecimenURI) {
		this.occurrenceInFinlandSpecimenURI = occurrenceInFinlandSpecimenURI;
	}

	@PublicInformation(order=30.1)
	public Qname getOriginalDescription() {
		return originalDescription;
	}

	public void setOriginalDescription(Qname originalDescription) {
		this.originalDescription = originalDescription;
	}

	public Integer getFrequencyScoringPoints() {
		return frequencyScoringPoints;
	}

	public void setFrequencyScoringPoints(Integer frequencyScoringPoints) {
		this.frequencyScoringPoints = frequencyScoringPoints;
	}

	public Taxon setExplicitObservationCount(Integer observationCount) {
		this.explicitObservationCount = observationCount;
		return this;
	}

	public Taxon setExplicitObservationCountFinland(Integer observationCountFinland) {
		this.explicitObservationCountFinland = observationCountFinland;
		return this;
	}

	@PublicInformation(order=6003.10)
	public int getObservationCount() {
		if (inheritedObservationCount == null) {
			int count = explicitObservationCount;
			for (Taxon child : getChildren()) {
				count += child.getObservationCount();
			}
			inheritedObservationCount = count;
		}
		return inheritedObservationCount;
	}

	@PublicInformation(order=6003.11)
	public int getOccurrenceCount() {
		return getObservationCount();
	}

	@PublicInformation(order=6003.20)
	public int getObservationCountFinland() {
		if (inheritedObservationCountFinland == null) {
			int count = explicitObservationCountFinland;
			for (Taxon child : getChildren()) {
				count += child.getObservationCountFinland();
			}
			inheritedObservationCountFinland = count;
		}
		return inheritedObservationCountFinland;
	}

	@PublicInformation(order=6003.21)
	public int getOccurrenceCountFinland() {
		return getObservationCountFinland();
	}

	@PublicInformation(order=6003.30)
	public int getObservationCountInvasiveFinland() {
		if (inheritedObservationCountInvasiveFinland == null) {
			int count = 0;
			if (this.isInvasiveObservationSpecies()) {
				count = explicitObservationCountFinland;
			}
			for (Taxon child : getChildren()) {
				count += child.getObservationCountInvasiveFinland();
			}
			inheritedObservationCountInvasiveFinland = count;
		}
		return inheritedObservationCountInvasiveFinland;
	}

	@PublicInformation(order=6003.31)
	public int getOccurrenceCountInvasiveFinland() {
		return getObservationCountInvasiveFinland();
	}

	@PublicInformation(order=152)
	public boolean isAutoNonWild() {
		return autoNonWild;
	}

	public void setAutoNonWild(boolean autoNonWild) {
		this.autoNonWild = autoNonWild;
	}

	@PublicInformation(order=6200.1)
	public BoldRecords getBold() {
		return boldRecords;
	}

	public void setBold(BoldRecords boldRecords) {
		this.boldRecords = boldRecords;
	}

	@PublicInformation(order=6200.2)
	public boolean hasBold() {
		if (boldRecords != null && boldRecords.hasRecords()) return true;
		for (Taxon synonym : getSynonyms()) {
			if (synonym.hasBold()) return true;
		}
		return false;
	}

}
