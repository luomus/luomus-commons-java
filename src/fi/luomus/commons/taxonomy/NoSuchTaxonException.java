package fi.luomus.commons.taxonomy;

import fi.luomus.commons.containers.rdf.Qname;

public class NoSuchTaxonException extends RuntimeException {

	private static final long serialVersionUID = 7830596672932268678L;

	public NoSuchTaxonException() {
		super("No such taxon: null");
	}
	
	public NoSuchTaxonException(Qname taxonId) {
		super("No such taxon: " + (taxonId == null ? null : taxonId.toString()));
	}
	
}
