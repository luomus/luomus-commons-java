package fi.luomus.commons.taxonomy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.RdfResource;
import fi.luomus.commons.db.connectivity.SimpleTransactionConnection;
import fi.luomus.commons.db.connectivity.TransactionConnection;
import fi.luomus.commons.taxonomy.TaxonSearch.MatchType;
import fi.luomus.commons.taxonomy.TaxonSearch.ResponseNameMode;
import fi.luomus.commons.taxonomy.TaxonSearchResponse.Match;
import fi.luomus.commons.utils.Utils;

public class TaxonSearchDAOSQLQueryImple implements TaxonSearchDAO {

	private static final Qname NAME_TYPE_ID = new Qname("id");

	private static final Qname SCIENTIFIC_NAME = new Qname("MX.scientificName");

	private static final double JARO_WINKLER_DISTANCE = 0.88;

	private String likelyMatchSQL = "" +
			" SELECT   qname, casedname, nametype, namelanguage, utl_match.jaro_winkler(name, ?) match   " +
			" FROM     <schema>.taxon_search_materialized_v2                               " +
			" WHERE    utl_match.jaro_winkler(name, ?) > " + JARO_WINKLER_DISTANCE           +
			" AND      name != ?                                                           " +
			" AND      COALESCE(checklist, '.') = ?                                        " +
			" ORDER BY match DESC, name                                                    ";

	private String partialMatchSQL = "" +
			" SELECT   qname, casedname, nametype, namelanguage  " +
			" FROM     <schema>.taxon_search_materialized_v2 " +
			" WHERE    name LIKE ?                           " +
			" AND      name != ?                             " +
			" AND      COALESCE(checklist, '.') = ?          " +
			" ORDER BY name                                  ";

	private String exactMatchSQL = "" +
			" SELECT   qname, casedname, nametype, namelanguage  " +
			" FROM     <schema>.taxon_search_materialized_v2                  " +
			" WHERE    (name = ? AND COALESCE(checklist, '.') = ? )           " +
			" OR       qname = ?                                              ";

	private final TaxonomyDAO taxonomyDAO;
	private final HikariDataSource dataSource;

	public TaxonSearchDAOSQLQueryImple(TaxonomyDAO taxonomyDAO, HikariDataSource dataSource, String schemaName) {
		this.taxonomyDAO = taxonomyDAO;
		this.dataSource = dataSource;
		this.exactMatchSQL = exactMatchSQL.replace("<schema>", schemaName);
		this.likelyMatchSQL = likelyMatchSQL.replace("<schema>", schemaName);
		this.partialMatchSQL = partialMatchSQL.replace("<schema>", schemaName);
	}

	@Override
	public TaxonSearchResponse search(TaxonSearch taxonSearch) throws Exception {
		try {
			return trySearch(taxonSearch);
		}	catch  (Exception e) {
			if (taxonSearch == null) {
				throw new Exception("Taxon search: " + null, e);
			}
			throw new Exception("Taxon search: " + taxonSearch.toString(), e);
		}
	}

	private TaxonSearchResponse trySearch(TaxonSearch taxonSearch) throws Exception, SQLException {
		TaxonSearchResponse response = new TaxonSearchResponse(taxonSearch);
		if (taxonSearch.getSearchword() == null || taxonSearch.getSearchword().length() <= 2) {
			return response;
		}

		String searchword = taxonSearch.getSearchword();
		if (looksLikeTaxonId(searchword)) {
			return taxonIdSearchResponse(taxonSearch, response, searchword);
		}

		searchword = uppercaseAndCleanSearchWord(taxonSearch);

		String checklist = ".";
		if (taxonSearch.hasChecklist()) {
			checklist = taxonSearch.getChecklist().toString().trim().toUpperCase();
		}
		TransactionConnection con = null;
		try {
			con = new SimpleTransactionConnection(dataSource.getConnection());

			if (taxonSearch.getMatchTypes().contains(MatchType.EXACT)) {
				response.getExactMatches().addAll(exactMatches(searchword, checklist, taxonSearch, con));
			}

			if (taxonSearch.getMatchTypes().contains(MatchType.PARTIAL)) {
				response.getPartialMatches().addAll(partialMatches(searchword, checklist,taxonSearch, con));
			}

			if (searchword.length() >= 5) {
				if (taxonSearch.getMatchTypes().contains(MatchType.LIKELY)) {
					response.getLikelyMatches().addAll(likelyMatches(searchword, checklist, taxonSearch, con));
				}
			}
		} finally {
			Utils.close(con);
		}

		return response.finalizeMatches();
	}

	private String uppercaseAndCleanSearchWord(TaxonSearch taxonSearch) {
		String searchword = taxonSearch.getSearchword()
				.toUpperCase()
				.replace("×", "").replace("Æ", "AE").replace("Ë", "E")
				.replace(" SSP.", " SUBSP.").replace(" SSP", " SUBSP.").replace(" SUBSP ", " SUBSP. ");

		while (searchword.contains("  ")) {
			searchword = searchword.replace("  ", " ");
		}
		searchword = searchword.trim();

		if (taxonSearch.getResponseNameMode() == ResponseNameMode.OBSERVATION) {
			if (searchword.endsWith(" SP.") || searchword.endsWith(" SP") || searchword.endsWith(" S")) {
				searchword = searchword.substring(0, searchword.lastIndexOf(" "));
			}
		}

		return searchword;
	}

	private TaxonSearchResponse taxonIdSearchResponse(TaxonSearch taxonSearch, TaxonSearchResponse response, String searchword) throws Exception {
		Qname taxonId = toTaxonid(searchword);
		if (!taxonomyDAO.getTaxonContainer().hasTaxon(taxonId)) {
			return response;
		}

		Taxon taxon = taxonomyDAO.getTaxon(taxonId);
		Match match = null;
		if (given(taxon.getScientificName())) {
			match = toMatch(taxonSearch, MatchType.EXACT, taxonId, taxon.getScientificName(), SCIENTIFIC_NAME, null);
		} else {
			match = toMatch(taxonSearch, MatchType.EXACT, taxonId, taxonId.toURI(), NAME_TYPE_ID, null);
		}
		if (match != null) {
			response.getExactMatches().add(match);
		}
		return response;
	}

	private boolean given(String s) {
		return s != null && s.length() > 0;
	}

	private Qname toTaxonid(String searchword) {
		if (searchword.startsWith("MX.")) {
			return new Qname(searchword);
		}
		return Qname.fromURI(searchword);
	}

	private boolean looksLikeTaxonId(String searchword) {
		return searchword.startsWith(RdfResource.DEFAULT_NAMESPACE_URI+"MX.") || searchword.startsWith("MX.");
	}

	private List<Match> exactMatches(String searchword, String checklist, TaxonSearch taxonSearch, TransactionConnection con) throws Exception {
		List<Match> matches = new ArrayList<>();
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = con.prepareStatement(exactMatchSQL);
			p.setString(1, searchword);
			p.setString(2, checklist);
			p.setString(3, searchword);
			rs = p.executeQuery();
			while (rs.next()) {
				Match match = toMatch(rs, taxonSearch, MatchType.EXACT);
				if (match == null) continue;
				if (!matches.contains(match)) {
					matches.add(match);
				}
			}
		} finally {
			Utils.close(p, rs);
		}
		return matches;
	}

	private List<Match> likelyMatches(String searchword, String checklist, TaxonSearch taxonSearch, TransactionConnection con) throws Exception {
		List<Match> matches = new ArrayList<>();
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = con.prepareStatement(likelyMatchSQL);
			p.setString(1, searchword);
			p.setString(2, searchword);
			p.setString(3, searchword);
			p.setString(4, checklist);
			rs = p.executeQuery();
			while (rs.next()) {
				Match match = toMatch(rs, taxonSearch, MatchType.LIKELY);
				if (match == null) continue;
				match.setSimilarity(rs.getDouble(5));
				if (!matches.contains(match)) {
					matches.add(match);
				}
			}
		} finally {
			Utils.close(p, rs);
		}
		return matches;
	}

	private List<Match> partialMatches(String searchword, String checklist, TaxonSearch taxonSearch, TransactionConnection con) throws Exception {
		List<Match> matches = new ArrayList<>();
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = con.prepareStatement(partialMatchSQL);
			if (searchword.contains(" ")) {
				p.setString(1, searchword.replace(" ", "% ") + "%");
			} else {
				p.setString(1, "%" + searchword + "%");
			}
			p.setString(2, searchword);
			p.setString(3, checklist);
			rs = p.executeQuery();
			while (rs.next()) {
				Match match = toMatch(rs, taxonSearch, MatchType.PARTIAL);
				if (match == null) continue;
				if (!matches.contains(match)) {
					matches.add(match);
				}
			}
		} finally {
			Utils.close(p, rs);
		}
		return matches;
	}

	private Match toMatch(ResultSet rs, TaxonSearch taxonSearch, MatchType matchType) throws Exception {
		Qname taxonId = new Qname(rs.getString(1));
		String name = rs.getString(2);
		Qname nameType = new Qname(rs.getString(3));
		String nameLanguage = rs.getString(4);

		return toMatch(taxonSearch, matchType, taxonId, name, nameType, nameLanguage);
	}

	private Match toMatch(TaxonSearch taxonSearch, MatchType matchType, Qname taxonId, String name, Qname nameType, String nameLanguage) throws Exception {
		if (!taxonomyDAO.getTaxonContainer().hasTaxon(taxonId)) {
			Match match = new Match(null, name, matchType, nameType, nameLanguage, taxonSearch.getSearchword());
			if (!taxonMatchesFilters(taxonSearch, match)) {
				return null;
			}
			return match;
		}

		Taxon taxon = taxonomyDAO.getTaxon(taxonId);
		Match match = new Match(taxon, name, matchType, nameType, nameLanguage, taxonSearch.getSearchword());
		if (!taxonMatchesFilters(taxonSearch, match)) {
			return null;
		}

		for (Qname informalGroupQname : taxon.getInformalTaxonGroups()) {
			InformalTaxonGroup informalGroup = taxonomyDAO.getInformalTaxonGroups().get(informalGroupQname.toString());
			if (informalGroup == null) continue;
			match.getInformalGroups().add(informalGroup);
		}
		return match;
	}

	private boolean taxonMatchesFilters(TaxonSearch taxonSearch, Match match) {
		if (!taxonSearch.hasFilters()) return true;

		if (taxonSearch.getExlucedNameTypes().contains(match.getNameType())) return false;

		if (!taxonSearch.getIncludedNameTypes().isEmpty() && !taxonSearch.getIncludedNameTypes().contains(match.getNameType())) return false;
		if (match.getNameLanguage() != null && !taxonSearch.getIncludedLanguages().isEmpty() && !taxonSearch.getIncludedLanguages().contains(match.getNameLanguage())) return false;

		Taxon taxon = match.getTaxon();

		if (taxon == null) return false;

		if (Boolean.TRUE.equals(taxonSearch.getSpecies())) {
			if (!taxon.isSpecies()) return false;
		}
		if (Boolean.FALSE.equals(taxonSearch.getSpecies())) {
			if (taxon.isSpecies()) return false;
		}

		if (taxonSearch.isOnlyFinnish()) {
			if (!taxon.isFinnish()) return false;
		}

		if (taxonSearch.isOnlyInvasive()) {
			if (!taxon.isInvasiveSpecies()) return false;
		}

		if (!taxonSearch.isIncludeHidden()) {
			if (taxon.isHidden()) return false;
		}

		if (!taxonSearch.getInformalTaxonGroups().isEmpty()) {
			return taxon.getInformalTaxonGroupsNoOrder().stream().anyMatch(taxonSearch.getInformalTaxonGroups()::contains);
		}

		if (!taxonSearch.getTaxonSets().isEmpty()) {
			return taxon.getTaxonSets().stream().anyMatch(taxonSearch.getTaxonSets()::contains);
		}

		if (!taxonSearch.getExcludedInformalTaxonGroups().isEmpty()) {
			if (taxon.getInformalTaxonGroupsNoOrder().stream().anyMatch(taxonSearch.getExcludedInformalTaxonGroups()::contains)) return false;
		}

		return true;
	}

}
