package fi.luomus.commons.taxonomy.iucn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;

public class HabitatObject implements Comparable<HabitatObject> {

	private Qname id;
	private final Qname habitat;
	private final int order;
	private List<Qname> habitatSpecificTypes = null;

	public HabitatObject(Qname id, Qname habitat, int order) {
		this.id = id;
		this.habitat = habitat;
		this.order = order;
	}

	public HabitatObject addHabitatSpecificType(Qname type) {
		if (habitatSpecificTypes == null) habitatSpecificTypes = new ArrayList<>();
		habitatSpecificTypes.add(type);
		Collections.sort(habitatSpecificTypes);
		return this;
	}

	public List<Qname> getHabitatSpecificTypes() {
		if (habitatSpecificTypes == null) return Collections.emptyList();
		return Collections.unmodifiableList(habitatSpecificTypes);
	}

	public Qname getId() {
		return id;
	}

	public Qname getHabitat() {
		return habitat;
	}

	@Override
	public String toString() {
		return Utils.debugS(id, habitat, habitatSpecificTypes);
	}

	public void setId(Qname id) {
		this.id = id;
	}

	public boolean hasValues() {
		if (given(getHabitat())) return true;
		for (Qname habitat : getHabitatSpecificTypes()) {
			if (given(habitat)) return true;
		}
		return false;
	}

	private boolean given(Qname q) {
		return q != null && q.isSet();
	}

	public int getOrder() {
		return order;
	}

	@Override
	public int compareTo(HabitatObject o) {
		return Integer.valueOf(this.order).compareTo(o.order);
	}

	@Override
	public int hashCode() {
		return Utils.debugS(habitat, habitatSpecificTypes).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (getClass() != obj.getClass())
			return false;
		HabitatObject other = (HabitatObject) obj;
		return Utils.debugS(habitat, habitatSpecificTypes).equals(Utils.debugS(other.habitat, other.habitatSpecificTypes));
	}

	public String toHabitatSearchString() {
		StringBuilder b = new StringBuilder();
		if (this.getHabitat() != null) {
			b.append(this.getHabitat().toString());
		}
		if (!this.getHabitatSpecificTypes().isEmpty()) {
			b.append("[");
			Iterator<Qname> i = this.getHabitatSpecificTypes().iterator();
			while (i.hasNext()) {
				b.append(":").append(i.next()).append(":");
				if (i.hasNext()) b.append(",");
			}
			b.append("]");
		}
		return b.toString();
	}

	public static HabitatObject fromSearchString(String searchString) {
		if (!searchString.contains("[")) {
			return new HabitatObject(null, new Qname(searchString), 1);
		}
		String[] parts = searchString.split(Pattern.quote("[")); 
		String habitat = parts[0];
		String specificTypes = parts[1].replace("]", "").replace(":", "");
		HabitatObject h = new HabitatObject(null, new Qname(habitat), 1);
		for (String specificType : specificTypes.split(Pattern.quote(","))) {
			h.addHabitatSpecificType(new Qname(specificType));
		}
		return h;
	}

}
