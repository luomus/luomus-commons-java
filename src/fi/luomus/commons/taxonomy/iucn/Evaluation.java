package fi.luomus.commons.taxonomy.iucn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.ObjectLiteral;
import fi.luomus.commons.containers.rdf.ObjectResource;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.RdfProperties;
import fi.luomus.commons.containers.rdf.Statement;
import fi.luomus.commons.taxonomy.Occurrences.Occurrence;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class Evaluation {

	public static final String NE = "MX.iucnNE";
	public static final String NA = "MX.iucnNA";
	public static final String DD = "MX.iucnDD";
	public static final String LC = "MX.iucnLC";
	public static final String NT = "MX.iucnNT";
	public static final String VU = "MX.iucnVU";
	public static final String EN = "MX.iucnEN";
	public static final String CR = "MX.iucnCR";
	public static final String RE = "MX.iucnRE";
	public static final String EW = "MX.iucnEW";
	public static final String EX = "MX.iucnEX";
	public static final String TAXONOMIC_NOTES = "MKV.taxonomicNotes";
	public static final String POSSIBLY_RE = "MKV.possiblyRE";
	public static final String POPULATION_SIZE_PERIOD_BEGINNING = "MKV.populationSizePeriodBeginning"; 
	public static final String POPULATION_SIZE_PERIOD_END = "MKV.populationSizePeriodEnd";
	public static final String POPULATION_SIZE_PERIOD_NOTES = "MKV.populationSizePeriodNotes";
	public static final String OCCURRENCE_AREA_NOTES = "MKV.occurrenceAreaNotes";
	public static final String LEGACY_PUBLICATIONS = "MKV.legacyPublications";
	public static final String INDIVIDUAL_COUNT_NOTES = "MKV.individualCountNotes";
	public static final String GROUNDS_FOR_EVALUATION_NOTES = "MKV.groundsForEvaluationNotes";
	public static final String DISTRIBUATION_AREA_NOTES = "MKV.distributionAreaNotes";
	public static final String DECREASE_DURING_PERIOD = "MKV.decreaseDuringPeriod";
	public static final String REASON_FOR_STATUS_CHANGE = "MKV.reasonForStatusChange";
	public static final String LAST_SIGHTING_NOTES = "MKV.lastSightingNotes";
	public static final String BORDER_GAIN = "MKV.borderGain";
	public static final String FRAGMENTED_HABITATS = "MKV.fragmentedHabitats";
	public static final String POPULATION_VARIES = "MKV.populationVaries";
	public static final String EVALUATION_PERIOD_LENGTH = "MKV.evaluationPeriodLength";
	public static final String GENERATION_AGE = "MKV.generationAge";
	public static final String HABITAT_GENERAL_NOTES = "MKV.habitatGeneralNotes";
	public static final String OCCURRENCE_REGIONS_NOTES = "MKV.occurrenceRegionsNotes";
	public static final String OCCURRENCE_NOTES = "MKV.occurrenceNotes";
	public static final String OCCURRENCE_AREA_MAX = "MKV.occurrenceAreaMax";
	public static final String OCCURRENCE_AREA_MIN = "MKV.occurrenceAreaMin";
	public static final String DISTRIBUTION_AREA_MAX = "MKV.distributionAreaMax";
	public static final String DISTRIBUTION_AREA_MIN = "MKV.distributionAreaMin";
	public static final String TYPE_OF_OCCURRENCE_IN_FINLAND = "MKV.typeOfOccurrenceInFinland";
	public static final String IUCN_EVALUATION_NAMESPACE = "MKV";
	public static final String IUCN_RED_LIST_EVALUATION_YEAR_CLASS = "MKV.iucnRedListEvaluationYear";
	public static final String EVALUATION_CLASS = "MKV.iucnRedListEvaluation";
	public static final String EVALUATION_YEAR = "MKV.evaluationYear";
	public static final String EVALUATED_TAXON = "MKV.evaluatedTaxon";
	public static final String STATE = "MKV.state";
	public static final String STATE_READY = "MKV.stateReady";
	public static final String STATE_READY_FOR_COMMENTS = "MKV.stateReadyForComments";
	public static final String STATE_STARTED = "MKV.stateStarted";
	public static final String RED_LIST_STATUS = "MKV.redListStatus";
	public static final String HABITAT_OBJECT_CLASS = "MKV.habitatObject";
	public static final String PRIMARY_HABITAT = "MKV.primaryHabitat";
	public static final String SECONDARY_HABITAT = "MKV.secondaryHabitat";
	public static final String HABITAT = "MKV.habitat";
	public static final String HABITAT_SPECIFIC_TYPE = "MKV.habitatSpecificType";
	public static final String HAS_OCCURRENCE = "MKV.hasOccurrence";
	public static final String HAS_THREAT = "MKV.hasThreat";
	public static final String HAS_ENDANGERMENT_REASON = "MKV.hasEndangermentReason";
	public static final String ENDANGERMENT = "MKV.endangerment";
	public static final String PUBLICATION = "MKV.publication";
	public static final String EDIT_NOTES = "MKV.editNotes";
	public static final String LAST_MODIFIED_BY = "MKV.lastModifiedBy";
	public static final String LAST_MODIFIED = "MKV.lastModified";
	public static final String RED_LIST_INDEX_CORRECTION = "MKV.redListIndexCorrection";
	public static final String RED_LIST_STATUS_NOTES = "MKV.redListStatusNotes";
	public static final String NE_MARK_NOTES = "Merkitty NE-luokkaan pikatoiminnolla.";
	public static final String NA_MARK_NOTES = "Merkitty NA-luokkaan pikatoiminnolla.";
	public static final String LC_MARK_NOTES = "Merkitty LC-luokkaan pikatoiminnolla.";
	public static final String INDEX_CHANGE_NOTES = "Punaisen kirjan indeksiä (RLI) muutettu.";
	public static final String INDEX_REMOVE_NOTES = "Punaisen kirjan indeksi (RLI) on tyjennetty.";
	public static final String ENDANGERMENT_OBJECT_CLASS = "MKV.endangermentObject"; 
	public static final String CRITERIA_FOR_STATUS = "MKV.criteriaForStatus";
	public static final String CRITERIA_A = "MKV.criteriaA";
	public static final String CRITERIA_B = "MKV.criteriaB";
	public static final String CRITERIA_C = "MKV.criteriaC";
	public static final String CRITERIA_D = "MKV.criteriaD";
	public static final String CRITERIA_E = "MKV.criteriaE";
	public static final String STATUS_A = "MKV.statusA";
	public static final String STATUS_B = "MKV.statusB";
	public static final String STATUS_C = "MKV.statusC";
	public static final String STATUS_D = "MKV.statusD";
	public static final String STATUS_E = "MKV.statusE";
	public static final String IS_LOCKED = "MKV.locked";
	public static final String REMARKS = "MKV.remarks";
	public static final String LSA_RECOMMENDATION = "MKV.lsaRecommendation";
	public static final String RED_LIST_STATUS_MAX = "MKV.redListStatusMax";
	public static final String RED_LIST_STATUS_MIN = "MKV.redListStatusMin";
	public static final String INDIVIDUAL_COUNT_MAX = "MKV.individualCountMax";
	public static final String INDIVIDUAL_COUNT_MIN = "MKV.individualCountMin";
	public static final String EXTERNAL_IMPACT = "MKV.externalPopulationImpactOnRedListStatus";
	public static final String NOTE_DATE_SEPARATOR = "; ";
	public static final String PERCENTAGE_OF_GLOBAL_POPULATION = "MKV.percentageOfGlobalPopulation";
	public static final String DD_REASON = "MKV.ddReason";

	public static final List<String> CRITERIAS = Utils.list("A", "B", "C", "D", "E");

	public static final Map<String, Integer> RED_LIST_STATUS_TO_INDEX;

	static {
		RED_LIST_STATUS_TO_INDEX = new HashMap<>();
		RED_LIST_STATUS_TO_INDEX.put(EX, 5);
		RED_LIST_STATUS_TO_INDEX.put(EW, 5);
		RED_LIST_STATUS_TO_INDEX.put(RE, 5);
		RED_LIST_STATUS_TO_INDEX.put(CR, 4);
		RED_LIST_STATUS_TO_INDEX.put(EN, 3);
		RED_LIST_STATUS_TO_INDEX.put(VU, 2);
		RED_LIST_STATUS_TO_INDEX.put(NT, 1);
		RED_LIST_STATUS_TO_INDEX.put(LC, 0);
		RED_LIST_STATUS_TO_INDEX.put(DD, null);
		RED_LIST_STATUS_TO_INDEX.put(NA, null);
		RED_LIST_STATUS_TO_INDEX.put(NE, null); 
	}

	private static final Comparator<Statement> REMARK_SORTER = new Comparator<Statement>() {
		@Override
		public int compare(Statement o1, Statement o2) {
			String s1 = o1.getObjectLiteral().getContent();
			String s2 = o2.getObjectLiteral().getContent();
			Date d1 = date(s1);
			Date d2 = date(s2);
			return d2.compareTo(d1);
		}

		private Date date(String remark) {
			try {
				String[] parts = remark.split(Pattern.quote("\n"));
				parts = parts[0].split(Pattern.quote(" "));
				String date = parts[parts.length-1].replace(":", "");
				return DateUtils.convertToDate(date, "dd.MM.yyyy");
			} catch (Exception e) {
				return new Date();
			}
		}
	};

	private final RdfProperties evaluationProperties;
	private final Model evaluation;
	private Map<String, Occurrence> occurrences = null;
	private HabitatObject primaryHabitat = null;
	private List<HabitatObject> secondaryHabitats = null;
	private List<EndangermentObject> endangermentReasons = null;
	private List<EndangermentObject> threats = null;
	private boolean incompletelyLoaded = false;

	public Evaluation(Model evaluation, RdfProperties evaluationProperties) {
		if (evaluation == null) throw new IllegalArgumentException("Must give evaluation model");
		if (evaluationProperties == null) throw new IllegalArgumentException("Must give evaluation properties");
		this.evaluation = evaluation;
		this.evaluationProperties = evaluationProperties;
	}

	private Evaluation(Model evaluation) {
		this.evaluation = evaluation;
		this.evaluationProperties = null;
	}

	public static Evaluation createNonPredicateCheckingEvaluation(Model evaluation) {
		return new Evaluation(evaluation);
	}

	public void setPrimaryHabitat(HabitatObject habitat) {
		if (habitat == null) return;
		primaryHabitat = habitat;
	}

	public void addSecondaryHabitat(HabitatObject habitat) {
		if (habitat == null) return;
		if (secondaryHabitats == null) secondaryHabitats = new ArrayList<>(3);
		if (secondaryHabitats.contains(habitat)) return;
		secondaryHabitats.add(habitat);
		Collections.sort(secondaryHabitats);
	}

	public HabitatObject getPrimaryHabitat() {
		return primaryHabitat;
	}

	public List<HabitatObject> getSecondaryHabitats() {
		if (secondaryHabitats == null) return Collections.emptyList();
		return Collections.unmodifiableList(secondaryHabitats);
	}

	public boolean isLocked() {
		return "true".equals(getValue(IS_LOCKED));
	}

	public void addOccurrence(Occurrence occurrence) {
		if (occurrences == null) occurrences = new HashMap<>();
		if (occurrence.getYear() == null) occurrence.setYear(getEvaluationYear());
		occurrences.put(occurrence.getArea().toString(), occurrence);
	}

	public boolean hasOccurrence(String areaQname) {
		if (occurrences == null) return false;
		return occurrences.containsKey(areaQname);
	}

	public Occurrence getOccurrence(String areaQname) {
		if (occurrences == null) return null;
		return occurrences.get(areaQname);
	}

	public Collection<Occurrence> getOccurrences() {
		if (occurrences == null) return Collections.emptyList();
		return Collections.unmodifiableCollection(occurrences.values());
	}

	public String getId() {
		return evaluation.getSubject().getQname();
	}

	public boolean hasValue(String predicateQname) {
		validate(predicateQname);
		return evaluation.hasStatements(predicateQname);
	}

	public String getValue(String predicateQname) {
		validate(predicateQname);
		if (!evaluation.hasStatements(predicateQname)) return null;
		Statement s = evaluation.getStatements(predicateQname).get(0); 
		if (s.isLiteralStatement()) return s.getObjectLiteral().getContent();
		return s.getObjectResource().getQname();
	}

	private void validate(String predicateQname) {
		if (evaluationProperties == null) return;
		if (!evaluationProperties.hasProperty(predicateQname)) throw new IllegalArgumentException("No such property: " + predicateQname);
	}

	public List<String> getValues(String predicateQname) {
		validate(predicateQname);
		if (!evaluation.hasStatements(predicateQname)) return Collections.emptyList();
		List<String> values = new ArrayList<>();
		for (Statement s : evaluation.getStatements(predicateQname)) {
			if (s.isLiteralStatement()) {
				values.add(s.getObjectLiteral().getContent()); 
			} else {
				values.add(s.getObjectResource().getQname());
			}
		}
		return values;
	}

	public Integer getEvaluationYear() {
		if (hasValue(EVALUATION_YEAR)) {
			return Integer.valueOf(getValue(EVALUATION_YEAR));
		}
		return null;
	}

	public boolean isReady() {
		return STATE_READY.equals(getState());
	}

	public boolean isStarted() {
		return STATE_STARTED.equals(getState());
	}

	public boolean isReadyForComments() {
		return STATE_READY_FOR_COMMENTS.equals(getState());
	}

	public String getState() {
		return getValue(STATE);
	}

	public Date getLastModified() throws Exception {
		if (evaluation.hasStatements(LAST_MODIFIED)) {
			return DateUtils.convertToDate(getValue(LAST_MODIFIED), "yyyy-MM-dd");
		}
		return null;
	}

	public String getLastModifiedBy() {
		return getValue(LAST_MODIFIED_BY);
	}

	public String getIucnStatus() {
		return getValue(RED_LIST_STATUS);
	}

	public boolean hasIucnStatus() {
		return evaluation.hasStatements(RED_LIST_STATUS);
	}

	public boolean hasCorrectedStatusForRedListIndex() {
		return evaluation.hasStatements(RED_LIST_INDEX_CORRECTION);
	}

	public String getCorrectedStatusForRedListIndex() {
		if (hasCorrectedStatusForRedListIndex()) {
			return getValue(RED_LIST_INDEX_CORRECTION);
		}
		return null;
	}

	private Integer getCalcuatedIndex(String status) {
		if (status == null) return null;
		if (!RED_LIST_STATUS_TO_INDEX.containsKey(status)) throw new UnsupportedOperationException("Unknown redListStatus " + status);
		return RED_LIST_STATUS_TO_INDEX.get(status);
	}

	public Integer getCalculatedRedListIndex() {
		return getCalcuatedIndex(getIucnStatus());
	}

	public Integer getCalculatedCorrectedRedListIndex() {
		return getCalcuatedIndex(getCorrectedStatusForRedListIndex());
	}

	public String getSpeciesQname() {
		return getValue(EVALUATED_TAXON);
	}

	public Model getModel() {
		return evaluation;
	}

	public boolean isVulnerable() {
		Integer index = getCalculatedRedListIndex();
		if (index == null) return false;
		return index >= 2;
	}

	public boolean isDd() {
		return DD.equals(getIucnStatus());
	}

	public List<EndangermentObject> getEndangermentReasons() {
		if (endangermentReasons == null) return Collections.emptyList();
		return endangermentReasons;
	}

	public void addEndangermentReason(EndangermentObject endangermentReason) {
		if (endangermentReasons == null) endangermentReasons = new ArrayList<>(3);
		if (endangermentReasons.contains(endangermentReason)) return;
		endangermentReasons.add(endangermentReason);
		Collections.sort(endangermentReasons);
	}

	public List<EndangermentObject> getThreats() {
		if (threats == null) return Collections.emptyList();
		return threats;
	}

	public void addThreat(EndangermentObject threat) {
		if (threats == null) threats = new ArrayList<>(3);
		if (threats.contains(threat)) return;
		threats.add(threat);
		Collections.sort(threats);
	}

	public void copySpecifiedFieldsTo(Evaluation copyTarget) {
		copy(TYPE_OF_OCCURRENCE_IN_FINLAND, copyTarget);
		copy(DISTRIBUTION_AREA_MIN, copyTarget);
		copy(DISTRIBUTION_AREA_MAX, copyTarget);
		copy(OCCURRENCE_AREA_MIN, copyTarget);
		copy(OCCURRENCE_AREA_MAX, copyTarget);
		copy(OCCURRENCE_NOTES, copyTarget);

		for (Occurrence occurrence : this.getOccurrences()) {
			copyTarget.addOccurrence(copy(occurrence));
		}

		copyTarget.setPrimaryHabitat(copy(this.getPrimaryHabitat()));
		for (HabitatObject habitatObject : this.getSecondaryHabitats()) {
			copyTarget.addSecondaryHabitat(copy(habitatObject));
		}
		copy(HABITAT_GENERAL_NOTES, copyTarget);

		copy(GENERATION_AGE, copyTarget);
		copy(EVALUATION_PERIOD_LENGTH, copyTarget);
		copy(POPULATION_VARIES, copyTarget);
		copy(FRAGMENTED_HABITATS, copyTarget);
		copy(BORDER_GAIN, copyTarget);

		for (EndangermentObject endangermentObject : this.getEndangermentReasons()) {
			copyTarget.addEndangermentReason(copy(endangermentObject));
		}
		for (EndangermentObject endangermentObject : this.getThreats()) {
			copyTarget.addThreat(copy(endangermentObject));
		}

		copy(LAST_SIGHTING_NOTES, copyTarget);
	}

	private EndangermentObject copy(EndangermentObject endangermentObject) {
		return new EndangermentObject(null, endangermentObject.getEndangerment(), endangermentObject.getOrder());
	}

	private HabitatObject copy(HabitatObject habitatObject) {
		if (habitatObject == null) return null;
		HabitatObject copy = new HabitatObject(null, habitatObject.getHabitat(), habitatObject.getOrder());
		for (Qname habitatSpecificType : habitatObject.getHabitatSpecificTypes()) {
			copy.addHabitatSpecificType(habitatSpecificType);
		}
		return copy;
	}

	private Occurrence copy(Occurrence occurrence) {
		Occurrence copy = new Occurrence(null, occurrence.getArea(), occurrence.getStatus());
		copy.setThreatened(occurrence.getThreatened());
		return copy;
	}

	private void copy(String predicateQname, Evaluation copyTarget) {
		List<Statement> statements = this.getModel().getStatements(predicateQname);
		for (Statement statement : statements) {
			copyTarget.getModel().addStatement(copy(statement));
		}
	}

	private Statement copy(Statement statement) {
		if (statement.isLiteralStatement()) {
			ObjectLiteral literal = statement.getObjectLiteral();
			String content = literal.getContent();
			if (literal.hasLangcode()) {
				return new Statement(statement.getPredicate(), new ObjectLiteral(content, literal.getLangcode()));
			}
			return new Statement(statement.getPredicate(), new ObjectLiteral(content));
		}
		return new Statement(statement.getPredicate(), new ObjectResource(statement.getObjectResource().getQname()));
	}

	public boolean hasRemarks() {
		return getValue(REMARKS) != null;
	}

	public String getRemarks() {
		StringBuilder b = new StringBuilder();
		for (Statement s : getRemarkSatements()) {
			b.append(s.getObjectLiteral().getContent()).append("\n\n");
		}
		return b.toString();
	}

	public List<Statement> getRemarkSatements() {
		List<Statement> remarks = evaluation.getStatements(REMARKS);
		Collections.sort(remarks, REMARK_SORTER);
		return remarks;
	}

	public boolean isIncompletelyLoaded() {
		return incompletelyLoaded;
	}

	public void setIncompletelyLoaded(boolean incompletelyLoaded) {
		this.incompletelyLoaded = incompletelyLoaded;
	}

	private static final Map<String, String> EXTERNAL_QNAME_TO_VALUE;
	static {
		EXTERNAL_QNAME_TO_VALUE = new HashMap<>();
		EXTERNAL_QNAME_TO_VALUE.put("MKV.externalPopulationImpactOnRedListStatusEnumMinus1", "-1");
		EXTERNAL_QNAME_TO_VALUE.put("MKV.externalPopulationImpactOnRedListStatusEnumMinus2", "-2");
		EXTERNAL_QNAME_TO_VALUE.put("MKV.externalPopulationImpactOnRedListStatusEnumPlus1", "+1");
		EXTERNAL_QNAME_TO_VALUE.put("MKV.externalPopulationImpactOnRedListStatusEnumPlus2", "+2");
	}

	public String getExternalImpact() {
		if (!hasValue(EXTERNAL_IMPACT)) return "";
		String qname = getValue(EXTERNAL_IMPACT);
		return EXTERNAL_QNAME_TO_VALUE.get(qname);
	}

	public boolean hasRegionalThreatenedData() {
		if (hasValue("MKV.regionallyThreatenedNotes") || hasValue("MKV.regionallyThreatenedPrivateNotes")) return true;
		for (Occurrence o : getOccurrences()) {
			if (Boolean.TRUE.equals(o.getThreatened())) return true;
		}
		return false;
	}

	public boolean isCriticalDataEvaluation() {
		if (!hasIucnStatus()) return false;
		return isCriticalIUCNEvaluation(this.getIucnStatus());
	}

	private static final Set<String> NON_CRITICAL_IUCN_EVALUATION_STATUSES = Utils.set(NE, NA);

	public static boolean isCriticalIUCNEvaluation(String iucnStatus) {
		if (notGiven(iucnStatus)) return false;
		return !NON_CRITICAL_IUCN_EVALUATION_STATUSES.contains(iucnStatus);
	}

	private static boolean notGiven(String iucnStatus) {
		return iucnStatus == null || iucnStatus.isEmpty();
	}

}
