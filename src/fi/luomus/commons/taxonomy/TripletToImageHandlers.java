package fi.luomus.commons.taxonomy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.Image;
import fi.luomus.commons.containers.rdf.Qname;

public class TripletToImageHandlers {

	private final TripletToImageHandler NOP_HANDLER = new TripletToImageHandler() {
		@Override
		public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
			// do nothing
		}
	};

	private final Map<String, TripletToImageHandler> HANDLERS = initHandlers();

	public TripletToImageHandler getHandler(Qname predicatename) {
		if (!HANDLERS.containsKey(predicatename.toString())) return NOP_HANDLER;
		return HANDLERS.get(predicatename.toString());
	}

	private Map<String, TripletToImageHandler> initHandlers() {
		Map<String, TripletToImageHandler> handlers = new HashMap<>();

		handlers.put("MM.sourceSystem", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setSource(objectname);
			}
		});

		handlers.put("MM.capturerVerbatim", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.addAuthor(resourceliteral);
			}
		});

		handlers.put("MM.keyword", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.addKeyword(resourceliteral);
			}
		});

		handlers.put("MM.taxonURI", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				if (taxonIdsToAddTheImage != null) {
					taxonIdsToAddTheImage.add(objectname);
				}
			}
		});

		handlers.put("MM.fullURL", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setFullURL(resourceliteral);
			}
		});

		handlers.put("MM.largeURL", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setLargeURL(resourceliteral);
			}
		});

		handlers.put("MM.squareThumbnailURL", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setSquareThumbnailURL(resourceliteral);
			}
		});

		handlers.put("MM.thumbnailURL", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setThumbnailURL(resourceliteral);
			}
		});

		handlers.put("MZ.intellectualOwner", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setCopyrightOwner(resourceliteral);
			}
		});

		handlers.put("MZ.intellectualRights", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setLicenseId(objectname);
				image.setLicenseAbbreviation(licenseAbbreviation(objectname));
			}
		});

		handlers.put("MM.taxonDescriptionCaption", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.getTaxonDescriptionCaption().set(locale, resourceliteral);
			}
		});

		handlers.put("MM.caption", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setCaption(resourceliteral);
			}
		});

		handlers.put("MM.type", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setType(objectname);
			}
		});

		handlers.put("MY.lifeStage", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.addLifeStage(objectname);
			}
		});

		handlers.put("MY.plantLifeStage", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.addPlantLifeStage(objectname);
			}
		});

		handlers.put("MY.sex", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.addSex(objectname);
			}
		});

		handlers.put("MM.side", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setSide(objectname);
			}
		});

		handlers.put("MM.primaryForTaxon", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.addPrimaryForTaxon(objectname);
			}
		});

		handlers.put("MM..uploadDateTime", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setUploadDateTime(resourceliteral);
			}
		});

		handlers.put("MM.captureDateTime", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setCaptureDateTime(resourceliteral);
			}
		});

		handlers.put("sortOrder", new TripletToImageHandler() {
			@Override
			public void setToImage(Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage) {
				image.setSortOrder(intVal(resourceliteral));
			}
		});

		return handlers;
	}

	private Integer intVal(String resourceliteral) {
		try {
			return Integer.valueOf(resourceliteral);
		} catch (Exception e) {
			return null;
		}
	}

	private static String licenseAbbreviation(Qname license) {
		if (license == null) return "";
		String abbrv = license.toString().replace("MZ.intellectualRights", "");
		if (abbrv.equals("ARR")) return "&copy;";
		return abbrv;
	}

}
