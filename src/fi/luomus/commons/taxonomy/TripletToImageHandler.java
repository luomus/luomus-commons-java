package fi.luomus.commons.taxonomy;

import java.util.List;

import fi.luomus.commons.containers.Image;
import fi.luomus.commons.containers.rdf.Qname;

public interface TripletToImageHandler {

	void setToImage( Qname objectname, String resourceliteral, String locale, Image image, List<Qname> taxonIdsToAddTheImage);

}
