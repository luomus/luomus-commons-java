package fi.luomus.commons.taxonomy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.TaxonSearch.MatchType;
import fi.luomus.commons.taxonomy.TaxonSearch.ResponseNameMode;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;

public class TaxonSearchResponse {

	public static final Qname NAME_TYPE_ID = new Qname("id");

	private static final Qname SPECIES_TAXON_RANK = new Qname("MX.species");
	private static final Qname GENUS_TAXON_RANK = new Qname("MX.genus");

	private static final Set<Qname> NON_SCIENTIFIC_NAME_TYPES = Utils.set(
			NAME_TYPE_ID, new Qname("MX.birdlifeCode"), new Qname("MX.euringCode"),
			new Qname("MX.vernacularName"), new Qname("MX.alternativeVernacularName"), new Qname("MX.obsoleteVernacularName"),
			new Qname("MX.colloquialVernacularName"), new Qname("MX.tradeName"));

	private static final String SP = " sp.";

	private static final Map<Qname, Integer> NAME_TYPE_ORDERS;
	static {
		NAME_TYPE_ORDERS = new HashMap<>();
		NAME_TYPE_ORDERS.put(NAME_TYPE_ID, 0);
		NAME_TYPE_ORDERS.put(new Qname("MX.scientificName"), 0);
		NAME_TYPE_ORDERS.put(new Qname("MX.vernacularName"), 0);

		NAME_TYPE_ORDERS.put(new Qname("MX.birdlifeCode"), 1);
		NAME_TYPE_ORDERS.put(new Qname("MX.euringCode"), 1);

		NAME_TYPE_ORDERS.put(new Qname("MX.hasSynonym"), 2);
		NAME_TYPE_ORDERS.put(new Qname("MX.hasBasionym"), 2);
		NAME_TYPE_ORDERS.put(new Qname("MX.tradeName"), 2);
		NAME_TYPE_ORDERS.put(new Qname("MX.alternativeVernacularName"), 2);

		NAME_TYPE_ORDERS.put(new Qname("MX.hasHeterotypicSynonym"), 3);
		NAME_TYPE_ORDERS.put(new Qname("MX.hasHomotypicSynonym"), 3);
		NAME_TYPE_ORDERS.put(new Qname("MX.hasObjectiveSynonym"), 3);
		NAME_TYPE_ORDERS.put(new Qname("MX.hasSubjectiveSynonym"), 3);

		NAME_TYPE_ORDERS.put(new Qname("MX.colloquialVernacularName"), 4);
		NAME_TYPE_ORDERS.put(new Qname("MX.obsoleteVernacularName"), 4);
		NAME_TYPE_ORDERS.put(new Qname("MX.hasMisappliedName"), 4);
		NAME_TYPE_ORDERS.put(new Qname("MX.hasMisspelledName"), 4);
		NAME_TYPE_ORDERS.put(new Qname("MX.hasOrthographicVariant"), 4);
		NAME_TYPE_ORDERS.put(new Qname("MX.hasUncertainSynonym"), 4);
		NAME_TYPE_ORDERS.put(new Qname("MX.hasAlternativeName"), 4);
	}

	public static Comparator<Match> MATCH_COMPARATOR = new Comparator<Match>() {

		@Override
		public int compare(Match m1, Match m2) {
			int i = isFinnish(m2).compareTo(isFinnish(m1));
			if (i != 0) return i;

			i = isSpeciesTaxonRank(m2).compareTo(isSpeciesTaxonRank(m1));
			if (i != 0) return i;

			i = isSpeciesOrBellowRank(m2).compareTo(isSpeciesOrBellowRank(m1));
			if (i != 0) return i;

			if (!isLikely(m1) && isLikely(m2)) return -1;
			if (isLikely(m1) && !isLikely(m2)) return 1;

			int nameTypeOrder1 = nameTypeOrder(m1);
			int nameTypeOrder2 = nameTypeOrder(m2);
			if (nameTypeOrder1 < nameTypeOrder2) return -1;
			if (nameTypeOrder1 > nameTypeOrder2) return 1;

			if (m1.getSimilarity() != null && m2.getSimilarity() != null) {
				i = m2.getSimilarity().compareTo(m1.getSimilarity());
				if (i != 0) return i;
			}

			String searchWord = m1.getOriginalSearchName().toLowerCase();
			String matchName1 = m1.getMatchingName().toLowerCase();
			String matchName2 = m2.getMatchingName().toLowerCase();

			boolean matchName1Equals = searchWord.equals(matchName1);
			boolean matchName2Equals = searchWord.equals(matchName2);
			if (matchName1Equals && !matchName2Equals) return -1;
			if (!matchName1Equals && matchName2Equals) return 1;

			int wordMatchCount1 = wordMatchCount(matchName1, searchWord);
			int wordMatchCount2 = wordMatchCount(matchName2, searchWord);
			if (wordMatchCount1 > wordMatchCount2) return -1;
			if (wordMatchCount1 < wordMatchCount2) return 1;

			boolean matchName1Starts = matchName1.startsWith(searchWord);
			boolean matchName2Starts = matchName2.startsWith(searchWord);
			if (matchName1Starts && !matchName2Starts) return -1;
			if (!matchName1Starts && matchName2Starts) return 1;

			boolean match1WordStarts = wordStartsButIsNot(matchName1, searchWord);
			boolean match2WordStarts = wordStartsButIsNot(matchName2, searchWord);
			if (match1WordStarts && !match2WordStarts) return -1;
			if (!match1WordStarts && match2WordStarts) return 1;

			i = isGenusTaxonRank(m2).compareTo(isGenusTaxonRank(m1));
			if (i != 0) return i;

			Integer obsCount1 = getObsCount(m1);
			Integer obsCount2 = getObsCount(m2);
			i = obsCount2.compareTo(obsCount1);
			if (i != 0) return i;

			boolean match1WordEnds = wordEndsButIsNot(matchName1, searchWord);
			boolean match2WordEnds = wordEndsButIsNot(matchName2, searchWord);
			if (match1WordEnds && !match2WordEnds) return -1;
			if (!match1WordEnds && match2WordEnds) return 1;

			boolean matchName1End = matchName1.endsWith(searchWord);
			boolean matchName2End = matchName2.endsWith(searchWord);
			if (matchName1End && !matchName2End) return -1;
			if (!matchName1End && matchName2End) return 1;

			if ("fi".equals(m1.getNameLanguage()) && !"fi".equals(m2.getNameLanguage())) return -1;
			if (!"fi".equals(m1.getNameLanguage()) && "fi".equals(m2.getNameLanguage())) return 1;

			i = matchName1.compareTo(matchName2);
			if (i != 0) return i;

			return m1.getNameType().compareTo(m2.getNameType());
		}

		private Integer getObsCount(Match m) {
			if (m.getTaxon() == null) return 0;
			int count = m.getTaxon().getObservationCountFinland();
			return getRoundedObsCount(count);
		}

		private int nameTypeOrder(Match m) {
			Integer order = NAME_TYPE_ORDERS.get(m.getNameType());
			if (order == null) return Integer.MAX_VALUE;
			return order;
		}

		private int wordMatchCount(String name, String searchWord) {
			if (!searchWord.contains(" ")) return wordMatchPartCount(name, searchWord);

			String[] searchWordParts = searchWord.split(Pattern.quote(" "));
			int i = 0;
			for (String searchWordPart : searchWordParts) {
				i += wordMatchPartCount(name, searchWordPart);
			}
			return i;
		}

		private int wordMatchPartCount(String name, String searchWordPart) {
			if (!name.contains(" ")) {
				if (searchWordPart.equals(name)) return 1;
				return 0;
			}
			int i = 0;
			String[] parts = name.split(Pattern.quote(" "));
			for (String part : parts) {
				if (part.equals(searchWordPart)) i++;
			}
			return i;
		}

		private boolean wordEndsButIsNot(String name, String searchWord) {
			if (!name.contains(" ")) return name.endsWith(searchWord);
			String[] parts = name.split(Pattern.quote(" "));
			for (String part : parts) {
				if (part.equals(searchWord)) continue;
				if (part.endsWith(searchWord)) return true;
			}
			return false;
		}

		private boolean wordStartsButIsNot(String name, String searchWord) {
			if (!name.contains(" ")) return name.startsWith(searchWord);
			String[] parts = name.split(Pattern.quote(" "));
			for (String part : parts) {
				if (part.equals(searchWord)) continue;
				if (part.startsWith(searchWord)) return true;
			}
			return false;
		}

		private boolean isLikely(Match m) {
			return m.getMatchType() == MatchType.LIKELY;
		}

		private Boolean isSpeciesOrBellowRank(Match m) {
			return m.getTaxon() != null && m.getTaxon().isSpecies();
		}

		private Boolean isSpeciesTaxonRank(Match m) {
			return m.getTaxon() != null && SPECIES_TAXON_RANK.equals(m.getTaxon().getTaxonRank());
		}

		private Boolean isGenusTaxonRank(Match m) {
			return m.getTaxon() != null && GENUS_TAXON_RANK.equals(m.getTaxon().getTaxonRank());
		}
		private Boolean isFinnish(Match m) {
			return m.getTaxon() != null && m.getTaxon().isFinnish();
		}

	};

	public static class Match {

		private static final Qname SCIENTIFIC_NAME = new Qname("MX.scientificName");
		private final Taxon taxon;
		private String matchingName;
		private final MatchType matchType;
		private final Qname nameType;
		private final String nameLanguage;
		private final String originalSearchName;
		private Double similarity;
		private final List<InformalTaxonGroup> informalGroups = new ArrayList<>();

		public Match(Taxon taxon, String matchingName, MatchType matchType, Qname nameType, String nameLanguage, String originalSearchName) {
			this.taxon = taxon;
			this.matchingName = clean(matchingName);
			this.matchType = matchType;
			this.nameType = nameType;
			this.nameLanguage = nameLanguage;
			this.originalSearchName = clean(originalSearchName);
		}

		private static String clean(String s) {
			while (s.contains("  ")) {
				s = s.replace("  ", " ");
			}
			return s.trim();
		}

		public Double getSimilarity() {
			return similarity;
		}

		public Match setSimilarity(Double similarity) {
			this.similarity = similarity;
			return this;
		}

		public List<InformalTaxonGroup> getInformalGroups() {
			return informalGroups;
		}

		public String getMatchingName() {
			return matchingName;
		}

		public Taxon getTaxon() {
			return taxon;
		}

		public MatchType getMatchType() {
			return matchType;
		}

		public Qname getNameType() {
			return nameType;
		}

		public String getOriginalSearchName() {
			return originalSearchName;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((matchingName == null) ? 0 : matchingName.hashCode());
			result = prime * result + ((taxon == null) ? 0 : taxon.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			Match other = (Match) obj;
			if (matchingName == null) {
				if (other.matchingName != null) return false;
			} else if (!matchingName.equals(other.matchingName)) return false;
			if (taxon == null) {
				if (other.taxon != null) return false;
			} else if (!taxon.equals(other.taxon)) return false;
			return true;
		}

		public boolean isPrimaryScientificNameMatch() {
			return SCIENTIFIC_NAME.equals(nameType);
		}

		public boolean isSynonynNameMatch() {
			return nameType.toString().startsWith("MX.has");
		}

		public void setMatchingName(String matchingName) {
			this.matchingName = matchingName;
		}

		public String getNameLanguage() {
			return nameLanguage;
		}

		@Override
		public String toString() {
			return "Match [taxon=" + taxon.getQname() + ", matchingName=" + matchingName + ", matchType=" + matchType +
					", nameType=" + nameType + ", nameLanguage=" + nameLanguage + ", similarity=" + similarity + "]";
		}

	}

	private final TaxonSearch taxonSearch;
	private final List<Match> exactMatches = new ArrayList<>();
	private final List<Match> likelyMatches = new ArrayList<>();
	private final List<Match> partialMatches = new ArrayList<>();
	private String error;

	public TaxonSearchResponse(TaxonSearch taxonSearch) {
		this.taxonSearch = taxonSearch;
	}

	public boolean hasMatches() {
		return !getExactMatches().isEmpty() ||
				!getLikelyMatches().isEmpty() ||
				!getPartialMatches().isEmpty();
	}

	public List<Match> getExactMatches() {
		return exactMatches;
	}

	public List<Match> getLikelyMatches() {
		return likelyMatches;
	}

	public List<Match> getPartialMatches() {
		return partialMatches;
	}

	private boolean finalized = false;

	public TaxonSearchResponse finalizeMatches() {
		if (finalized) return this;

		Set<Qname> primaryScientificNameMatches = getPrimaryScientificNameMatches();
		removeSynonymMatches(partialMatches, primaryScientificNameMatches);
		removeSynonymMatches(likelyMatches, primaryScientificNameMatches);

		Set<Qname> matchesSoFar = new HashSet<>();

		finalizeMatches(exactMatches, matchesSoFar);
		finalizeMatches(partialMatches, matchesSoFar);
		finalizeMatches(likelyMatches, matchesSoFar);

		if (taxonSearch.getResponseNameMode() == ResponseNameMode.OBSERVATION) {
			changeObservationModeScientificNames();
		}

		finalized = true;
		return this;
	}

	private void finalizeMatches(List<Match> matches, Set<Qname> matchesSoFar) {
		Collections.sort(matches, MATCH_COMPARATOR);
		removeDuplicates(matches, matchesSoFar);
		trimSize(matches);
	}

	private void removeDuplicates(List<Match> matches, Set<Qname> matchesSoFar) {
		if (matches == null || matches.isEmpty()) return;
		Iterator<Match> i = matches.iterator();
		while (i.hasNext()) {
			Taxon t = i.next().getTaxon();
			if (t == null) continue;
			if (matchesSoFar.contains(t.getQname())) {
				i.remove();
			} else {
				matchesSoFar.add(t.getQname());
			}
		}
	}

	private void changeObservationModeScientificNames() {
		changeObservationModeScientificNames(exactMatches);
		changeObservationModeScientificNames(partialMatches);
		changeObservationModeScientificNames(likelyMatches);
	}

	private void changeObservationModeScientificNames(List<Match> matches) {
		for (Match m : matches) {
			if (m.getTaxon() == null) continue;
			if (m.getTaxon().isSpecies()) continue;
			if (!given(m.getMatchingName())) continue;
			if (isScientificName(m.getNameType())) {
				m.setMatchingName(m.getMatchingName()+SP);
			}
		}
	}

	private static boolean isScientificName(Qname nameType) {
		return !NON_SCIENTIFIC_NAME_TYPES.contains(nameType);
	}

	private void removeSynonymMatches(List<Match> matches, Set<Qname> primaryScientificNameMatches) {
		Iterator<Match> i = matches.iterator();
		while (i.hasNext()) {
			Match m = i.next();
			if (m.getTaxon() == null) continue;
			if (primaryScientificNameMatches.contains(m.getTaxon().getQname()) && m.isSynonynNameMatch()) {
				i.remove();
			}
		}
	}

	private Set<Qname> getPrimaryScientificNameMatches() {
		Set<Qname> primaryMatches = new HashSet<>(3);
		addPrimaryScientificNameMatches(exactMatches, primaryMatches);
		addPrimaryScientificNameMatches(partialMatches, primaryMatches);
		addPrimaryScientificNameMatches(likelyMatches, primaryMatches);
		return primaryMatches;
	}

	private void addPrimaryScientificNameMatches(List<Match> matches, Set<Qname> primaryMatches) {
		for (Match m : matches) {
			if (m.getTaxon() == null) continue;
			if (m.isPrimaryScientificNameMatch()) {
				primaryMatches.add(m.getTaxon().getQname());
			}
		}
	}

	private void trimSize(List<Match> list) {
		int limit = taxonSearch.getLimit();
		if (list.size() > limit) {
			list.subList(limit, list.size()).clear();
		}
	}

	public Document getResultsAsDocument() {
		Document results = initResults();
		if (this.hasError()) {
			results.getRootNode().addAttribute("error", this.getError());
			return results;
		}

		if (!exactMatches.isEmpty()) {
			Node exactMatch = new Node("exactMatch");
			results.getRootNode().addChildNode(exactMatch);
			for (Match match : exactMatches) {
				if (match.getTaxon() != null) exactMatch.addChildNode(toNode(match));
			}
		}
		if (!partialMatches.isEmpty()) {
			Node partialMatchesNode = new Node("partialMatches");
			results.getRootNode().addChildNode(partialMatchesNode);
			for (Match match : partialMatches) {
				if (match.getTaxon() != null) partialMatchesNode.addChildNode(toNode(match));
			}
		}
		if (!likelyMatches.isEmpty()) {
			Node likelyMatchesNode = new Node("likelyMatches");
			results.getRootNode().addChildNode(likelyMatchesNode);
			for (Match match : likelyMatches) {
				if (match.getTaxon() != null) likelyMatchesNode.addChildNode(toNode(match));
			}
		}
		return results;
	}

	private Node toNode(Match match) {
		Taxon t = match.getTaxon();
		Node matchNode = new Node(t.getQname().toString());
		matchNode.addAttribute("matchingName", match.getMatchingName());
		if (given(t.getScientificName())) {
			matchNode.addAttribute("scientificName", t.getScientificName());
		}
		if (given(t.getScientificNameAuthorship())) {
			matchNode.addAttribute("scientificNameAuthorship", t.getScientificNameAuthorship());
		}
		if (given(t.getTaxonRank())) {
			matchNode.addAttribute("taxonRank", t.getTaxonRank().toString());
		}
		if (given(match.getSimilarity())) {
			Double similarity = Utils.round(match.getSimilarity(), 3);
			matchNode.addAttribute("similarity", similarity.toString());
		}
		Node informalGroupsNode = matchNode.addChildNode("informalGroups");
		for (InformalTaxonGroup informalGroup : match.getInformalGroups()) {
			Node informalGroupNode = informalGroupsNode.addChildNode(informalGroup.getQname().toString());
			for (Map.Entry<String, String> e : informalGroup.getName().getAllTexts().entrySet()) {
				informalGroupNode.addAttribute(e.getKey(), e.getValue());
			}
		}
		return matchNode;
	}

	private boolean given(Object o) {
		return o != null && o.toString().length() > 0;
	}

	private static Document initResults() {
		Document results = new Document("results");
		return results;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public boolean hasError() {
		return error != null;
	}

	public static int getRoundedObsCount(int count) {
		if (count == 0) return 0;
		if (count <= 10) return 10;
		if (count <= 100) return 100;
		if (count < 1000) {
			return ((int) Math.ceil(count/100))*100;
		}
		if (count < 100000) {
			return ((int) Math.ceil(count/1000))*1000;
		}
		return ((int) Math.ceil(count/10000))*10000;
	}

}
