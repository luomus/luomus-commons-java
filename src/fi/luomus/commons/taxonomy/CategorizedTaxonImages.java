package fi.luomus.commons.taxonomy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.Image;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;

public class CategorizedTaxonImages {

	private static final Qname TYPE_SPECIMEN = new Qname("MM.typeEnumSpecimen");
	private static final Qname TYPE_LIVE = new Qname("MM.typeEnumLive");
	private static final Qname TYPE_LABEL = new Qname("MM.typeEnumLabel");
	private static final Qname TYPE_HABITAT = new Qname("MM.typeEnumHabitat");
	private static final Qname TYPE_SKELETAL = new Qname("MM.typeEnumSkeletal");
	private static final Qname TYPE_CARCASS = new Qname("MM.typeEnumCarcass");
	private static final Qname TYPE_MICROSCOPY = new Qname("MM.typeEnumMicroscopy");
	private static final Qname TYPE_GENITALIA = new Qname("MM.typeEnumGenitalia");

	private static final Collection<Qname> DEFAULT_TYPE_NOT = Utils.list(
			TYPE_GENITALIA,
			TYPE_CARCASS,
			TYPE_SKELETAL,
			TYPE_HABITAT,
			TYPE_LABEL);

	private static final Collection<Qname> TYPE_NEVER = Utils.list(
			TYPE_CARCASS,
			TYPE_LABEL);

	private static final SingleCategoryDef PRIMARY = new SingleCategoryDef("primary", title("Pääkuva", "Primary", "Primär"))
			.primary().typeNot(TYPE_NEVER);

	private static final SingleCategoryDef FEMALE = new SingleCategoryDef("female", title("Naaras", "Female", "Hona"))
			.sex(new Qname("MY.sexF")).typeNot(DEFAULT_TYPE_NOT);

	private static final SingleCategoryDef MALE = new SingleCategoryDef("male", title("Koiras", "Male", "Hane"))
			.sex(new Qname("MY.sexM")).typeNot(DEFAULT_TYPE_NOT);

	private static final SingleCategoryDef UPSIDE = new SingleCategoryDef("upside", title("Päältä", "Upside", "Fördel")).side(new Qname("MM.sideUpside"));
	private static final SingleCategoryDef DOWNSIDE = new SingleCategoryDef("downside", title("Alta", "Downside", "Nackdel")).side(new Qname("MM.sideDownside"));

	private static final SingleCategoryDef FEMALE_SIDES = new SingleCategoryDef("female", title("Naaras", "Female", "Hona"),
			new SubCategoriesDef(Utils.list(UPSIDE, DOWNSIDE)))
			.sex(new Qname("MY.sexF")).typeNot(DEFAULT_TYPE_NOT);

	private static final SingleCategoryDef MALE_SIDES = new SingleCategoryDef("male", title("Koiras", "Male", "Hane"),
			new SubCategoriesDef(Utils.list(UPSIDE, DOWNSIDE)))
			.sex(new Qname("MY.sexM")).typeNot(DEFAULT_TYPE_NOT);

	private static final SingleCategoryDef GENITALIA_FEMALE = new SingleCategoryDef("female", title("Naaras", "Female", "Hona"))
			.sex(new Qname("MY.sexF")).type(TYPE_GENITALIA);

	private static final SingleCategoryDef GENITALIA_MALE = new SingleCategoryDef("male", title("Koiras", "Male", "Hane"))
			.sex(new Qname("MY.sexM")).type(TYPE_GENITALIA);

	private static final SingleCategoryDef GENITALIA_F_M = new SingleCategoryDef("genitalia", title("Genitaali", "Genitalia", "Genitalier"),
			new SubCategoriesDef(Utils.list(GENITALIA_FEMALE, GENITALIA_MALE)));

	private static final SingleCategoryDef ADULT = new SingleCategoryDef("adult", title("Aikuinen", "Adult", "Vuxen"))
			.lifeStage(new Qname("MY.lifeStageAdult")).typeNot(TYPE_NEVER);

	private static final SingleCategoryDef LARVA = new SingleCategoryDef("lava", title("Toukka", "Larva", "Larv"))
			.lifeStage(new Qname("MY.lifeStageLarva")).typeNot(TYPE_NEVER);

	private static final SingleCategoryDef PUPA = new SingleCategoryDef("pupa", title("Kotelo", "Pupa", "Puppa"))
			.lifeStage(new Qname("MY.lifeStagePupa")).typeNot(TYPE_NEVER);

	private static final SingleCategoryDef EGG = new SingleCategoryDef("egg", title("Muna", "Egg", "Ägg"))
			.lifeStage(new Qname("MY.lifeStageEgg")).typeNot(TYPE_NEVER);

	private static final SingleCategoryDef INSECT_PRE_ADULT = new SingleCategoryDef("pre_adult", title("Elinvaiheet", "Life stages", "Livsstadier"),
			new SubCategoriesDef(Utils.list(EGG, LARVA, PUPA)));

	private static final SingleCategoryDef MARKS = new SingleCategoryDef("marks", title("Syömäjäljet", "Feeding marks", "Spår"))
			.lifeStage(new Qname("MY.lifeStageMarks")).typeNot(TYPE_NEVER).typeNot(TYPE_GENITALIA).typeNot(TYPE_SKELETAL).typeNot(TYPE_HABITAT);

	private static final SingleCategoryDef HABITAT_ONLY = new SingleCategoryDef("habitat", title("Habitaatti", "Habitat", "Habitat"))
			.type(TYPE_HABITAT);

	private static final SingleCategoryDef HABITAT_AND_MARKS = new SingleCategoryDef("habitat_marks", title("Ympäristössä", "Environment", "Miljön"),
			new SubCategoriesDef(Utils.list(HABITAT_ONLY, MARKS)));

	private static final SingleCategoryDef MICROSCOPY = new SingleCategoryDef("microscopy", title("Mikroskooppi", "Microscopy", "Mikroskopi"))
			.type(TYPE_MICROSCOPY);

	private static final SingleCategoryDef GENERAL = new SingleCategoryDef("general", title("Kuvia", "Images", "Bilder"))
			.type(TYPE_LIVE).type(TYPE_SPECIMEN);

	private static final SingleCategoryDef SKELETAL = new SingleCategoryDef("skeletal", title("Luut", "Skeletal", "Skelett")).type(TYPE_SKELETAL);

	public static class ImageCategory {

		private final SingleCategoryDef def;
		private final List<ImageCategory> subcategories;
		private final List<Image> images;

		private ImageCategory(SingleCategoryDef def) {
			this.def = def;
			this.subcategories = def.subcategories();
			this.images = new ArrayList<>();
		}

		private boolean addIfMatches(Image i) {
			if (!def.matches(i)) return false;
			if (subcategories.isEmpty()) {
				images.add(i);
				return true;
			}
			boolean added = false;
			for (ImageCategory subc : subcategories) {
				if (subc.addIfMatches(i)) added = true;
			}
			return added;
		}

		public LocalizedText getTitle() {
			return def.title;
		}

		public List<ImageCategory> getSubcategories() {
			return Collections.unmodifiableList(subcategories);
		}

		public List<Image> getImages() {
			return Collections.unmodifiableList(images);
		}

		@Override
		public String toString() {
			return "ImageCategory [def=" + def + ", subcategories=" + subcategories + ", images=" + images + "]";
		}


	}

	private static class SubCategoriesDef {
		private final List<SingleCategoryDef> defs;
		private SubCategoriesDef(List<SingleCategoryDef> defs) {
			this.defs = defs;
		}
		private SubCategoriesDef(String id, List<SingleCategoryDef> defs) {
			this.defs = subCategories(id, defs);
		}
		private List<SingleCategoryDef> subCategories(String id, List<SingleCategoryDef> defs) {
			if (defs == null) return null;
			List<SingleCategoryDef> copies = new ArrayList<>(defs.size());
			for (SingleCategoryDef def : defs) {
				copies.add(def.copyFor(id));
			}
			return copies;
		}
		private List<ImageCategory> categories() {
			List<ImageCategory> c = new ArrayList<>();
			for (SingleCategoryDef def : defs) {
				c.add(new ImageCategory(def));
			}
			return c;
		}
		private List<SingleCategoryDef> getDefs() {
			return defs;
		}
		@Override
		public String toString() {
			return "SubCategoriesDef [defs=" + defs + "]";
		}
	}

	public static class SingleCategoryDef {

		private final String id;
		private final LocalizedText title;
		private final SubCategoriesDef subcategories;
		private Boolean primary;
		private List<Qname> types;
		private List<Qname> typeNot;
		private Qname sex;
		private List<Qname> lifeStages;
		private Qname side;

		private SingleCategoryDef(String id, LocalizedText title) {
			this(id, title, null);
		}

		public SingleCategoryDef copyFor(String parentId) {
			SingleCategoryDef copy = new SingleCategoryDef(parentId+"_"+id, title);
			copy.primary = this.primary;
			copy.types = this.types;
			copy.typeNot = this.typeNot;
			copy.sex = this.sex;
			copy.lifeStages = this.lifeStages;
			copy.side = this.side;
			return copy;
		}

		private SingleCategoryDef(String id, LocalizedText title, SubCategoriesDef subcategories) {
			this.id = id;
			this.title = title;
			this.subcategories = subCategories(subcategories);
		}

		private SubCategoriesDef subCategories(SubCategoriesDef subcategories) {
			if (subcategories == null) return null;
			return new SubCategoriesDef(id, subcategories.getDefs());
		}

		public String getId() {
			return id;
		}

		public LocalizedText getTitle() {
			return title;
		}

		public List<SingleCategoryDef> getDefs() {
			if (subcategories == null) return Collections.emptyList();
			return subcategories.getDefs();
		}

		private List<ImageCategory> subcategories() {
			if (subcategories == null) return Collections.emptyList();
			return subcategories.categories();
		}

		private boolean matches(Image i) {
			if (Boolean.TRUE.equals(primary)) {
				return i.isPrimaryForTaxon();
			}
			if (i.isPrimaryForTaxon()) return false;
			if (types != null) {
				if (!types.contains(i.getType())) return false;
			}
			if (typeNot != null) {
				if (typeNot.contains(i.getType())) return false;
			}
			if (sex != null) {
				if (!i.getSex().contains(sex)) return false;
			}
			if (lifeStages != null) {
				if (i.getLifeStage().stream().noneMatch(lifeStages::contains)) return false;
			}
			if (side != null) {
				Qname imageSide = i.getSide() == null ? new Qname("MM.sideUpside") : i.getSide();
				if (!side.equals(imageSide)) return false;
			}
			return true;
		}

		private SingleCategoryDef primary() {
			this.primary = true;
			return this;
		}

		private SingleCategoryDef side(Qname side) {
			this.side = side;
			return this;
		}

		private SingleCategoryDef type(Qname type) {
			if (this.types == null) this.types = new ArrayList<>();
			this.types.add(type);
			return this;
		}

		private SingleCategoryDef typeNot(Qname type) {
			if (this.typeNot == null) this.typeNot = new ArrayList<>();
			this.typeNot.add(type);
			return this;
		}

		private SingleCategoryDef typeNot(Collection<Qname> types) {
			if (this.typeNot == null) this.typeNot = new ArrayList<>();
			this.typeNot.addAll(types);
			return this;
		}

		private SingleCategoryDef sex(Qname sex) {
			this.sex = sex;
			return this;
		}

		private SingleCategoryDef lifeStage(Qname lifeStage) {
			if (this.lifeStages == null) this.lifeStages = new ArrayList<>();
			this.lifeStages.add(lifeStage);
			return this;
		}

		@Override
		public String toString() {
			return "SingleCategoryDef [id=" + id + ", title=" + title + ", subcategories=" + subcategories + ", primary=" + primary + ", types=" + types + ", typeNot=" + typeNot + ", sex=" + sex
					+ ", lifeStages=" + lifeStages + ", side=" + side + "]";
		}

	}

	private final List<ImageCategory> categories;
	private final List<Image> uncategorizedImages = new ArrayList<>();

	public CategorizedTaxonImages(Taxon taxon) {
		SubCategoriesDef def = definition(taxon.getInformalTaxonGroups());
		this.categories = def.categories();
		for (Image i : taxon.getMultimedia()) {
			boolean added = false;
			for (ImageCategory c : this.categories) {
				if (c.addIfMatches(i)) added = true;
			}
			if (!added) {
				if (!TYPE_NEVER.contains(i.getType())) {
					uncategorizedImages.add(i);
				}
			}
		}
	}

	private SubCategoriesDef definition(Set<Qname> informalTaxonGroups) {
		List<Qname> informalGroups = new ArrayList<>(informalTaxonGroups);
		Collections.reverse(informalGroups);
		for (Qname groupId : informalGroups) {
			SubCategoriesDef def = CATEGORY_DEFS.get(groupId);
			if (def != null) return def;
		}
		return DEFAULT_DEF;
	}

	public List<ImageCategory> getCategories() {
		return Collections.unmodifiableList(categories);
	}

	public List<Image> getUncategorizedImages() {
		return Collections.unmodifiableList(uncategorizedImages);
	}

	private static final int IMAGE_PER_CATEGORY = 5;

	private List<Image> flatted = null;
	private final Object lock = new Object();

	public List<Image> getGroupedFlatImages() {
		if (flatted == null) {
			synchronized (lock) {
				if (flatted == null) {
					int c = IMAGE_PER_CATEGORY;
					List<Image> flatted = new ArrayList<>();
					List<Image> pushLater = new ArrayList<>();
					for (ImageCategory cat : getCategories()) {
						cat.getImages().stream().limit(c).forEach(flatted::add);
						cat.getImages().stream().skip(c).forEach(pushLater::add);
						for (ImageCategory sub : cat.getSubcategories()) {
							sub.getImages().stream().limit(c).forEach(flatted::add);
							sub.getImages().stream().skip(c).forEach(pushLater::add);
						}
					}
					getUncategorizedImages().stream().limit(c).forEach(flatted::add);
					getUncategorizedImages().stream().skip(c).forEach(pushLater::add);
					flatted.addAll(pushLater);
					this.flatted = flatted;
				}
			}
		}
		return flatted;
	}

	private static final SubCategoriesDef DEFAULT_DEF = defaultCat();

	private static Map<Qname, SubCategoriesDef> CATEGORY_DEFS = initDefs();

	private static Map<Qname, SubCategoriesDef> initDefs() {
		Map<Qname, SubCategoriesDef> map = new HashMap<>();

		map.put(new Qname("MVL.232"), insecta());

		map.put(new Qname("MVL.31"), butterflies());
		map.put(new Qname("MVL.38"), spiders());
		map.put(new Qname("MVL.36"), dragonflies());

		map.put(new Qname("MVL.1"), birds());

		map.put(new Qname("MVL.161"), sammakot());

		map.put(new Qname("MVL.2"), mammals());

		map.put(new Qname("MVL.343"), plants());

		map.put(new Qname("MVL.233"), fungi());
		map.put(new Qname("MVL.23"), fungi());

		map.put(new Qname("MVL.40"), nilviaiset());
		//		levät
		//		madot
		//		muut:
		//			alkuhyönteiset?
		//					limasienet
		//					bakteerit
		//					sienieläimetjne
		//					virukset
		//		tuhatjalkaiset
		//		äyriäiset:
		//			makro
		//			mikro
		return map;
	}

	private static SubCategoriesDef defaultCat() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				GENERAL,
				HABITAT_ONLY
				));
	}



	private static LocalizedText title(String fi, String en, String sv) {
		return new LocalizedText().set("fi", fi).set("en", en).set("sv", sv);
	}

	private static SubCategoriesDef insecta() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				FEMALE,
				MALE,
				GENITALIA_F_M,
				INSECT_PRE_ADULT,
				HABITAT_AND_MARKS));
	}

	private static SubCategoriesDef butterflies() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				FEMALE_SIDES,
				MALE_SIDES,
				GENITALIA_F_M,
				INSECT_PRE_ADULT,
				HABITAT_AND_MARKS));
	}

	private static SubCategoriesDef dragonflies() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				FEMALE,
				MALE,
				INSECT_PRE_ADULT,
				HABITAT_ONLY));
	}

	private static SubCategoriesDef spiders() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				GENERAL,
				GENITALIA_F_M,
				new SingleCategoryDef("net", title("Verkko", "Net", "Nät")).lifeStage(new Qname("MY.lifeStageMarks")).typeNot(TYPE_NEVER),
				HABITAT_ONLY
				));
	}

	private static SubCategoriesDef birds() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				FEMALE,
				MALE,
				new SingleCategoryDef("juvenile", title("Nuori", "Juvenile", "Juvenil")).lifeStage(new Qname("MY.lifeStageJuvenile")).lifeStage(new Qname("MY.lifeStagePullus")).typeNot(TYPE_NEVER),
				EGG,
				new SingleCategoryDef("nest", title("Pesä/Jäljet/Ulosteet", "Nest/Footprint/Feces", "Bo/Fotspår/Avföring")).lifeStage(new Qname("MY.lifeStageMarks")).typeNot(TYPE_NEVER),
				SKELETAL,
				HABITAT_ONLY));
	}

	private static SubCategoriesDef mammals() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				GENERAL,
				new SingleCategoryDef("indirect", title("Pesä/Jäljet/Ulosteet", "Nest/Footprint/Feces", "Bo/Fotspår/Avföring")).lifeStage(new Qname("MY.lifeStageMarks")).typeNot(TYPE_NEVER),
				SKELETAL,
				HABITAT_ONLY));
	}

	private static SubCategoriesDef sammakot() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				ADULT,
				EGG,
				new SingleCategoryDef("tadpole", title("Nuijapää", "Tadpole", "Grodyngel")).lifeStage(new Qname("MY.lifeStageTadpole")).typeNot(TYPE_NEVER),
				SKELETAL,
				HABITAT_ONLY));
	}

	private static SubCategoriesDef plants() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				GENERAL,
				// TODO kukka, lehti, varsi, juuri, sipuli, talventörröttäjä, ...
				MICROSCOPY,
				HABITAT_ONLY));
	}

	private static SubCategoriesDef fungi() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				GENERAL,
				MICROSCOPY,
				HABITAT_ONLY));
	}

	private static SubCategoriesDef nilviaiset() {
		return new SubCategoriesDef(Utils.list(
				PRIMARY,
				ADULT,
				EGG,
				LARVA,
				new SingleCategoryDef("juvenile", title("Nuoruusvaihe", "Juvenile", "Ungdomsstadium")).lifeStage(new Qname("MY.lifeStageJuvenile")).typeNot(TYPE_NEVER),
				MICROSCOPY,
				HABITAT_ONLY));
	}

	/**
	 * Returns category definition for the lowest level group that has a definition.
	 * Ids of groups should be given in an order: lowest levels (leafs) first and highest later (root last).
	 * @param informalTaxonGroups ids of the groups in order
	 * @return
	 */
	public static List<SingleCategoryDef> getDefs(List<Qname> informalTaxonGroups) {
		if (informalTaxonGroups == null) throw new IllegalArgumentException("null groups");
		for (Qname id : informalTaxonGroups) {
			SubCategoriesDef def = CATEGORY_DEFS.get(id);
			if (def != null) return Collections.unmodifiableList(def.getDefs());
		}
		return Collections.unmodifiableList(DEFAULT_DEF.getDefs());
	}

}
