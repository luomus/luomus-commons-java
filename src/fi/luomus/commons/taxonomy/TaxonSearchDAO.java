package fi.luomus.commons.taxonomy;

public interface TaxonSearchDAO {

	public TaxonSearchResponse search(TaxonSearch search) throws Exception;
	
}
