package fi.luomus.commons.taxonomy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.AdministrativeStatus;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;

public class AdministrativeStatusContainer {

	private final Map<String, AdministrativeStatus> administrativeStatuses;
	private final Map<Qname, Integer> treeOrderMap;

	public AdministrativeStatusContainer(Map<String, AdministrativeStatus> administrativeStatuses) {
		this.administrativeStatuses = administrativeStatuses;
		this.treeOrderMap = generateOrderMap();
	}

	public Set<Qname> orderAdministrativeStatuses(Set<Qname> administrativeStatusIds) {
		if (administrativeStatusIds.isEmpty()) return Collections.emptySet();
		return getIds(getSortedStatuses(administrativeStatusIds));
	}

	private List<AdministrativeStatus> getSortedStatuses(Set<Qname> statusIds) {
		return sortStatuses(getStatuses(statusIds));
	}

	private Set<Qname> getIds(List<AdministrativeStatus> statuses) {
		Set<Qname> orderedIds = new LinkedHashSet<>();
		for (AdministrativeStatus a : statuses) {
			orderedIds.add(a.getQname());
		}
		return orderedIds;
	}

	private List<AdministrativeStatus> sortStatuses(List<AdministrativeStatus> statuses) {
		Collections.sort(statuses, new Comparator<AdministrativeStatus>() {
			@Override
			public int compare(AdministrativeStatus o1, AdministrativeStatus o2) {
				if (o1 == null || o2 == null) throw new IllegalArgumentException("Null statuses");
				Integer order1 = treeOrderMap.get(o1.getQname());
				Integer order2 = treeOrderMap.get(o2.getQname());
				if (order1 == null) order1 = Integer.MAX_VALUE;
				if (order2 == null) order2 = Integer.MAX_VALUE;
				return Integer.compare(order1, order2);
			}
		});
		return statuses;
	}

	private Map<Qname, Integer> generateOrderMap() {
		Map<Qname, Integer> map = new HashMap<>(administrativeStatuses.size());
		int i = 0;
		for (AdministrativeStatus a : administrativeStatuses.values()) {
			map.put(a.getQname(), i++);
		}
		return map;
	}

	private List<AdministrativeStatus> getStatuses(Set<Qname> statusIds) {
		List<AdministrativeStatus> statuses = new ArrayList<>();
		for (Qname id : statusIds) {
			AdministrativeStatus a = this.administrativeStatuses.get(id.toString());
			if (a != null) {
				statuses.add(a);
			}
		}
		return statuses;
	}

	public LocalizedText getIAdministrativeStatusNames(Set<Qname> administrativeStatusIds) {
		if (administrativeStatusIds.isEmpty()) return new LocalizedText();
		List<AdministrativeStatus> statuses = getSortedStatuses(administrativeStatusIds);
		LocalizedText names = new LocalizedText();
		for (AdministrativeStatus a : statuses) {
			for (Map.Entry<String, String> e : a.getName().getAllTexts().entrySet()) {
				String locale = e.getKey();
				String name = e.getValue();
				if (!given(locale, name)) continue;
				String existigName = names.forLocale(locale);
				if (given(existigName)) {
					names.set(locale, existigName + "; " + name);
				} else {
					names.set(locale, name);
				}
			}
		}
		return names;
	}

	private boolean given(String ...strings) {
		for (String s : strings) {
			if (s == null || s.isEmpty()) return false;
		}
		return true;
	}

}
