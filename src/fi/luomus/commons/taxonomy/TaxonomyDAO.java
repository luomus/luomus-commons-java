package fi.luomus.commons.taxonomy;

import java.util.Collection;
import java.util.Map;

import fi.luomus.commons.containers.AdministrativeStatus;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.Checklist;
import fi.luomus.commons.containers.ContentContextDescription;
import fi.luomus.commons.containers.ContentGroups;
import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.OccurrenceType;
import fi.luomus.commons.containers.Person;
import fi.luomus.commons.containers.Publication;
import fi.luomus.commons.containers.RedListEvaluationGroup;
import fi.luomus.commons.containers.TaxonSet;
import fi.luomus.commons.containers.rdf.Qname;

public interface TaxonomyDAO {

	Taxon getTaxon(Qname qname) throws Exception;

	Map<String, InformalTaxonGroup> getInformalTaxonGroups() throws Exception;

	Map<String, InformalTaxonGroup> getInformalTaxonGroupsForceReload() throws Exception;

	Map<String, TaxonSet> getTaxonSets() throws Exception;

	Map<String, TaxonSet> getTaxonSetsForceReload() throws Exception;

	Map<String, RedListEvaluationGroup> getRedListEvaluationGroups() throws Exception;

	Map<String, RedListEvaluationGroup> getRedListEvaluationGroupsForceReload() throws Exception;

	Map<String, OccurrenceType> getOccurrenceTypes() throws Exception;

	Map<String, OccurrenceType> getOccurrenceTypesForceReload() throws Exception;

	Map<String, AdministrativeStatus> getAdministrativeStatuses() throws Exception;

	Map<String, AdministrativeStatus> getAdministrativeStatusesForceReload() throws Exception;

	Map<String, Checklist> getChecklists() throws Exception;

	Map<String, Checklist> getChecklistsForceReload() throws Exception;

	Map<String, Publication> getPublications() throws Exception;

	Map<String, Publication> getPublicationsForceReload() throws Exception;

	Map<String, Person> getPersons() throws Exception;

	TaxonSearchResponse search(TaxonSearch taxonSearch) throws Exception;

	Collection<Qname> getTaxonRanks() throws Exception;

	Map<String, LocalizedText> getTaxonRankLabels();

	Map<String, ContentContextDescription> getContentContextDescriptions() throws Exception;

	ContentGroups getContentGroups() throws Exception;

	Map<String, Area> getAreas() throws Exception;

	LocalizedText getLabels(Qname variable) throws Exception;

	Map<String, LocalizedText> getAlt(Qname range) throws Exception;

	Map<String, LocalizedText> getLicenseFullnames() throws Exception;

	void clearCaches();

	TaxonContainer getTaxonContainer() throws Exception;


}
