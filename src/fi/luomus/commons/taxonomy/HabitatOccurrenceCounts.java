package fi.luomus.commons.taxonomy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.taxonomy.HabitatOccurrenceCounts.HabitatOccurrenceCount;
import fi.luomus.commons.utils.Utils;

public class HabitatOccurrenceCounts implements Iterable<HabitatOccurrenceCount> {

	public static class HabitatOccurrenceCount {

		private final String id;
		private final LocalizedText habitat;
		private int occurrenceCount = 0;

		public HabitatOccurrenceCount(String id, LocalizedText habitat) {
			if (id == null) throw new IllegalArgumentException();
			if (habitat == null) throw new IllegalArgumentException();
			this.id = Utils.removeWhitespace(id).toLowerCase();
			this.habitat = habitat;
		}

		public String getId() {
			return id;
		}

		public int getOccurrenceCount() {
			return occurrenceCount;
		}

		public HabitatOccurrenceCount setOccurrenceCount(int occurrenceCount) {
			this.occurrenceCount = occurrenceCount;
			return this;
		}

		public LocalizedText getHabitat() {
			return habitat;
		}

		@Override
		public String toString() {
			return "HabitatOccurrenceCounts [id=" + id + ", habitat=" + habitat + ", occurrenceCount=" + occurrenceCount + "]";
		}

		@Override
		public int hashCode() {
			return id.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			HabitatOccurrenceCount other = (HabitatOccurrenceCount) obj;
			return this.id.equals(other.id);
		}

		public HabitatOccurrenceCount copy() {
			return new HabitatOccurrenceCount(this.id, this.habitat).setOccurrenceCount(this.occurrenceCount);
		}

	}

	private final Map<String, HabitatOccurrenceCount> counts = new HashMap<>();

	public HabitatOccurrenceCounts setCount(HabitatOccurrenceCount count) {
		if (count == null) return this;
		counts.put(count.id, count);
		return this;
	}

	public boolean hasCount(String id) {
		return counts.containsKey(id);
	}

	public HabitatOccurrenceCount getCount(String id) {
		return counts.get(id);
	}

	@Override
	public String toString() {
		return getCounts().toString();
	}

	public List<HabitatOccurrenceCount> getCounts() {
		List<HabitatOccurrenceCount> sorted = new ArrayList<>(counts.values());
		Collections.sort(sorted, new Comparator<HabitatOccurrenceCount>() {
			@Override
			public int compare(HabitatOccurrenceCount o1, HabitatOccurrenceCount o2) {
				int c = Integer.compare(o2.getOccurrenceCount(), o1.getOccurrenceCount());
				if (c != 0) return c;
				return o1.id.compareTo(o2.id);
			}
		});
		return sorted;
	}

	public boolean hasCounts() {
		return !counts.isEmpty();
	}

	@Override
	public Iterator<HabitatOccurrenceCount> iterator() {
		return getCounts().iterator();
	}

	public HabitatOccurrenceCounts retainTop(int top) {
		if (counts.size() <= top) return this;
		List<HabitatOccurrenceCount> sorted = getCounts();
		counts.clear();
		for (HabitatOccurrenceCount c : sorted) {
			setCount(c);
			if (counts.size() >= top) break;
		}
		return this;
	}

}
