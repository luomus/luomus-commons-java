package fi.luomus.commons.taxonomy;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import fi.luomus.commons.containers.rdf.Qname;

public class Synonyms {

	private final Set<Qname> basionyms = new HashSet<>(0);
	private final Set<Qname> objectiveSynonyms = new HashSet<>(0);
	private final Set<Qname> subjectiveSynonyms = new HashSet<>(0);
	private final Set<Qname> homotypicSynonyms = new HashSet<>(0);
	private final Set<Qname> heterotypicSynonyms = new HashSet<>(0);
	private final Set<Qname> synonyms = new HashSet<>(0);
	private final Set<Qname> misspelledNames = new HashSet<>(0);
	private final Set<Qname> orthographicVariants = new HashSet<>(0);
	private final Set<Qname> uncertainSynonyms = new HashSet<>(0);
	private final Set<Qname> misappliedNames = new HashSet<>(0);
	private final Set<Qname> alternativeNames = new HashSet<>(0);
	
	public Set<Qname> getBasionyms() {
		return basionyms;
	}
	public Set<Qname> getObjectiveSynonyms() {
		return objectiveSynonyms;
	}
	public Set<Qname> getSubjectiveSynonyms() {
		return subjectiveSynonyms;
	}
	public Set<Qname> getHomotypicSynonyms() {
		return homotypicSynonyms;
	}
	public Set<Qname> getHeterotypicSynonyms() {
		return heterotypicSynonyms;
	}
	public Set<Qname> getSynonyms() {
		return synonyms;
	}
	public Set<Qname> getMisspelledNames() {
		return misspelledNames;
	}
	public Set<Qname> getOrthographicVariants() {
		return orthographicVariants;
	}
	public Set<Qname> getUncertainSynonyms() {
		return uncertainSynonyms;
	}
	public Set<Qname> getMisappliedNames() {
		return misappliedNames;
	}
	public Set<Qname> getAlternativeNames() {
		return alternativeNames;
	}
	public Synonyms addBasionym(Qname taxonId) {
		basionyms.add(taxonId);
		return this;
	}
	public Synonyms addObjectiveSynonym(Qname taxonId) {
		objectiveSynonyms.add(taxonId);
		return this;
	}
	public Synonyms addSubjectiveSynonym(Qname taxonId) {
		subjectiveSynonyms.add(taxonId);
		return this;
	}
	public Synonyms addHomotypicSynonym(Qname taxonId) {
		homotypicSynonyms.add(taxonId);
		return this;
	}
	public Synonyms addHeterotypicSynonym(Qname taxonId) {
		heterotypicSynonyms.add(taxonId);
		return this;
	}
	public Synonyms addSynonym(Qname taxonId) {
		synonyms.add(taxonId);
		return this;
	}
	public Synonyms addMisspelledName(Qname taxonId) {
		misspelledNames.add(taxonId);
		return this;
	}
	public Synonyms addOrthographicVariant(Qname taxonId) {
		orthographicVariants.add(taxonId);
		return this;
	}
	public Synonyms addUncertainSynonym(Qname taxonId) {
		uncertainSynonyms.add(taxonId);
		return this;
	}
	public Synonyms addMisappliedName(Qname taxonId) {
		misappliedNames.add(taxonId);
		return this;
	}
	public Synonyms addAlternativeName(Qname taxonId) {
		alternativeNames.add(taxonId);
		return this;
	}
	public Set<Qname> getAll() {
		Set<Qname> all = new LinkedHashSet<>(3);
		all.addAll(basionyms);
		all.addAll(objectiveSynonyms);
		all.addAll(subjectiveSynonyms);
		all.addAll(homotypicSynonyms);
		all.addAll(heterotypicSynonyms);
		all.addAll(synonyms);
		all.addAll(misspelledNames);
		all.addAll(orthographicVariants);
		all.addAll(uncertainSynonyms);
		all.addAll(misappliedNames);
		all.addAll(alternativeNames);
		return all;
	}

}
