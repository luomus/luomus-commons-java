package fi.luomus.commons.taxonomy;

import java.net.URI;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Occurrences.Occurrence;
import fi.luomus.commons.utils.Utils;

public class Occurrences implements Iterable<Occurrence> {

	public static class Occurrence {

		private Qname id;
		private Qname area;
		private Qname status;
		private Integer year;
		private String notes;
		private Boolean threatened;
		private URI specimenURI;
		private Integer occurrenceCount;

		public Occurrence(Qname id, Qname area, Qname status) {
			this.id = id;
			this.area = area;
			this.status = status;
		}

		public Qname getId() {
			return id;
		}
		public void setId(Qname occurrence) {
			this.id = occurrence;
		}
		public Qname getStatus() {
			return status;
		}
		public void setStatus(Qname status) {
			this.status = status;
		}
		public Qname getArea() {
			return area;
		}
		public void setArea(Qname area) {
			this.area = area;
		}
		public Integer getYear() {
			return year;
		}
		public void setYear(Integer year) {
			this.year = year;
		}
		public String getNotes() {
			return notes;
		}
		public void setNotes(String notes) {
			this.notes = notes;
		}
		public Boolean getThreatened() {
			return threatened;
		}
		public void setThreatened(Boolean threatened) {
			this.threatened = threatened;
		}
		public URI getSpecimenURI() {
			return specimenURI;
		}
		public void setSpecimenURI(URI specimenURI) {
			this.specimenURI = specimenURI;
		}
		public Integer getOccurrenceCount() {
			return occurrenceCount;
		}
		public void setOccurrenceCount(Integer count) {
			this.occurrenceCount = count;
		}

		@Override
		public boolean equals(Object o) {
			if (o == null) return false;
			if (o instanceof Occurrence) {
				Occurrence other = (Occurrence) o;
				return this.compareString().equals(other.compareString());
			}
			throw new UnsupportedOperationException("Can only compate to other " + Occurrence.class.getName());
		}

		@Override
		public String toString() {
			return Utils.debugS(id, area, status, notes, year, threatened, specimenURI, occurrenceCount);
		}

		private String compareString() {
			return Utils.debugS(area, status, notes, year, threatened, specimenURI, occurrenceCount);
		}

		@Override
		public int hashCode() {
			return this.compareString().hashCode();
		}

		public Occurrence copy() {
			Occurrence copy = new Occurrence(null, this.getArea(), this.getStatus());
			copy.setOccurrenceCount(this.getOccurrenceCount());
			copy.setSpecimenURI(this.getSpecimenURI());
			copy.setYear(this.getYear());
			return copy;
		}

	}

	private final Qname taxonQname;
	private final Map<Qname, Occurrence> occurrences = new TreeMap<>();

	public Occurrences(Qname taxonQname) {
		this.taxonQname = taxonQname;
	}

	public Occurrences setOccurrence(Occurrence occurrence) {
		if (occurrence == null) return this;
		occurrences.put(occurrence.getArea(), occurrence);
		return this;
	}

	public Occurrences setOccurrence(Qname id, Qname area, Qname status) {
		occurrences.put(area, new Occurrence(id, area, status));
		return this;
	}

	public boolean hasStatus(Qname area, Qname status) {
		if (status == null) return false;
		if (!occurrences.containsKey(area)) {
			return false;
		}
		return status.equals(occurrences.get(area).getStatus());
	}

	public Qname getStatus(Qname area) {
		if (occurrences.containsKey(area)) {
			return occurrences.get(area).getStatus();
		}
		return null;
	}

	public Occurrence getOccurrence(Qname area) {
		return occurrences.get(area);
	}

	public Qname getTaxonQname() {
		return taxonQname;
	}

	@Override
	public String toString() {
		return occurrences.toString();
	}

	public Collection<Occurrence> getOccurrences() {
		return occurrences.values();
	}

	public boolean hasOccurrences() {
		return !occurrences.isEmpty();
	}

	@Override
	public Iterator<Occurrence> iterator() {
		return getOccurrences().iterator();
	}
}
