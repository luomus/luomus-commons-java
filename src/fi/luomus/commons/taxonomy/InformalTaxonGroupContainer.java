package fi.luomus.commons.taxonomy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;

public class InformalTaxonGroupContainer {

	private final Map<String, ? extends InformalTaxonGroup> informalTaxonGroups;
	private final Map<Qname, Integer> treeOrderMap;

	public InformalTaxonGroupContainer(Map<String, ? extends InformalTaxonGroup> informalTaxonGroups) {
		this.informalTaxonGroups = informalTaxonGroups;
		this.treeOrderMap = generateTreeOrderMap();
	}

	public Set<Qname> orderInformalTaxonGroups(Set<Qname> informalTaxonGroupIds) {
		if (informalTaxonGroupIds.isEmpty()) return Collections.emptySet();
		return getIds(getSortedGroups(informalTaxonGroupIds));
	}

	private List<InformalTaxonGroup> getSortedGroups(Set<Qname> informalTaxonGroupIds) {
		return sortGroups(getGroups(informalTaxonGroupIds));
	}

	private Set<Qname> getIds(List<InformalTaxonGroup> groups) {
		Set<Qname> orderedIds = new LinkedHashSet<>();
		for (InformalTaxonGroup g : groups) {
			orderedIds.add(g.getQname());
		}
		return orderedIds;
	}

	private List<InformalTaxonGroup> sortGroups(List<InformalTaxonGroup> groups) {
		Collections.sort(groups, new Comparator<InformalTaxonGroup>() {
			@Override
			public int compare(InformalTaxonGroup o1, InformalTaxonGroup o2) {
				if (o1 == null || o2 == null) throw new IllegalArgumentException("Null groups");
				Integer order1 = treeOrderMap.get(o1.getQname());
				Integer order2 = treeOrderMap.get(o2.getQname());
				if (order1 == null) order1 = Integer.MAX_VALUE;
				if (order2 == null) order2 = Integer.MAX_VALUE;
				return Integer.compare(order1, order2);
			}
		});
		return groups;
	}

	private Map<Qname, Integer> generateTreeOrderMap() {
		Set<Qname> rootIds = getRoots();
		Map<Qname, Integer> map = new HashMap<>(informalTaxonGroups.size());
		addOrder(rootIds, 0, map);
		return map;
	}

	private Set<Qname> getRoots() {
		Set<Qname> rootIds = new HashSet<>();
		for (InformalTaxonGroup g : informalTaxonGroups.values()) {
			if (g.getParents().isEmpty()) {
				rootIds.add(g.getQname());
			}
		}
		return rootIds;
	}

	private int addOrder(Set<Qname> rootIds, int i, Map<Qname, Integer> map) {
		if (rootIds.isEmpty()) return i;
		List<InformalTaxonGroup> alphaOrdered = new ArrayList<>();
		for (Qname id : rootIds) {
			InformalTaxonGroup g = informalTaxonGroups.get(id.toString());
			if (g !=  null) {
				alphaOrdered.add(g);
			}
		}
		Collections.sort(alphaOrdered);
		for (InformalTaxonGroup g : alphaOrdered) {
			map.put(g.getQname(), i++);
			i = addOrder(g.getSubGroups(), i, map);
		}
		return i;
	}

	private List<InformalTaxonGroup> getGroups(Set<Qname> informalTaxonGroupIds) {
		List<InformalTaxonGroup> groups = new ArrayList<>();
		for (Qname id : informalTaxonGroupIds) {
			InformalTaxonGroup g = this.informalTaxonGroups.get(id.toString());
			if (g != null) {
				groups.add(g);
			}
		}
		return groups;
	}

	public Set<Qname> getParents(Qname groupId) {
		InformalTaxonGroup g = this.informalTaxonGroups.get(groupId.toString());
		if (g == null || g.getParents().isEmpty()) return Collections.emptySet();
		Set<Qname> parents = new HashSet<>();
		for (Qname parentId : g.getParents()) {
			InformalTaxonGroup parent = this.informalTaxonGroups.get(parentId.toString());
			if (parent != null) {
				parents.add(parentId);
				parents.addAll(getParents(parentId));
			}
		}
		return parents;
	}

	public LocalizedText getInformalTaxonGroupNames(Set<Qname> informalGroupIds) {
		if (informalGroupIds.isEmpty()) return new LocalizedText();
		List<InformalTaxonGroup> groups = getSortedGroups(informalGroupIds);
		LocalizedText names = new LocalizedText();
		for (InformalTaxonGroup g : groups) {
			for (Map.Entry<String, String> e : g.getName().getAllTexts().entrySet()) {
				String locale = e.getKey();
				String name = e.getValue();
				if (!given(locale, name)) continue;
				String existigName = names.forLocale(locale);
				if (given(existigName)) {
					names.set(locale, existigName + "; " + name);
				} else {
					names.set(locale, name);
				}
			}
		}
		return names;
	}

	private boolean given(String ...strings) {
		for (String s : strings) {
			if (s == null || s.isEmpty()) return false;
		}
		return true;
	}

}
