package fi.luomus.commons.taxonomy;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.Utils;

public interface TaxonContainer {

	int ESTIMATED_NUMBER_OF_TAXA = 300000;
	Qname IS_PART_OF = new Qname("MX.isPartOf");
	Set<Qname> SYNONYM_PREDICATES = Utils.set(
			new Qname("MX.hasBasionym"), new Qname("MX.hasSynonym"), new Qname("MX.hasObjectiveSynonym"), new Qname("MX.hasSubjectiveSynonym"),
			new Qname("MX.hasHomotypicSynonym"), new Qname("MX.hasHeterotypicSynonym"), new Qname("MX.hasMisappliedName"),
			new Qname("MX.hasMisspelledName"), new Qname("MX.hasOrthographicVariant"), new Qname("MX.hasUncertainSynonym"), new Qname("MX.hasAlternativeName"));

	/**
	 * Get all taxa
	 * @return
	 */
	Collection<Taxon> getAll();

	/**
	 * Get taxon
	 * @param taxonId
	 * @return
	 * @throws NoSuchTaxonException
	 */
	Taxon getTaxon(Qname taxonId) throws NoSuchTaxonException;

	/**
	 * Examine if taxon with the given id exists
	 * @param taxonId
	 * @return
	 */
	boolean hasTaxon(Qname taxonId);

	/**
	 * Get ids of taxon's children
	 * @param parentId
	 * @return
	 */
	Set<Qname> getChildren(Qname parentId);

	/**
	 * Get id of checklist taxa that use the given taxon as a synonym
	 * @param synonymId
	 * @return
	 */
	Qname getSynonymParent(Qname synonymId);

	/**
	 * Get filter that returns ids of taxa that are part of the given informal group
	 * @return
	 */
	Filter getInformalGroupFilter();

	/**
	 * Get filter that returns ids of taxa that are part of the given IUCN red list taxon group
	 * @return
	 */
	Filter getRedListEvaluationGroupFilter();

	/**
	 * Get filter that returns ids of taxa that have the given administrative status
	 * @return
	 */
	Filter getAdminStatusFilter();

	/**
	 * Get filter that returns ids of taxa that have the given red list status
	 * @return
	 */
	Filter getRedListStatusFilter();

	/**
	 * Get filter that returns ids of taxa that have the given type of occurrence
	 * @return
	 */
	Filter getTypesOfOccurrenceFilter();

	/**
	 * Get ids of taxa that are invasive species
	 * @return
	 */
	Set<Qname> getInvasiveSpeciesFilter();

	/**
	 * Get ids of taxa that require early warning
	 * @return
	 */
	Set<Qname> getInvasiveSpeciesEarlyWarningFilter();

	/**
	 * Get ids of taxa that have descriptions
	 * @return
	 */
	Set<Qname> getHasDescriptionsFilter();

	/**
	 * Get ids of taxa that have media
	 * @return
	 * @throws UnsupportedOperationException if not implemented
	 */
	Set<Qname> getHasMediaFilter() throws UnsupportedOperationException;

	/**
	 * Number of taxa contained. Optional method.
	 * @return
	 * @throws UnsupportedOperationException if not implemented
	 */
	int getNumberOfTaxa() throws UnsupportedOperationException;

	/**
	 * Get IUCN group ids using the given informal taxon group id as base
	 * @param informalTaxonGroupId
	 * @return
	 */
	Set<Qname> getRedListEvaluationGroupsOfInformalTaxonGroup(Qname informalTaxonGroupId);

	/**
	 * Get IUCN group ids using the given taxon as base
	 * @param taxonId
	 * @return
	 */
	Set<Qname> getRedListEvaluationGroupsOfTaxon(Qname taxonId);

	/**
	 * Return a sorted set (@See {@link LinkedHashSet}) so that top group is first, children after
	 * @param informalTaxonGroups
	 * @return
	 */
	Set<Qname> orderInformalTaxonGroups(Set<Qname> informalTaxonGroups);

	/**
	 * Return a sorted set (@See {@link LinkedHashSet}) so that top group is first, children after
	 * @param groups
	 * @return
	 */
	Set<Qname> orderRedListEvaluationGroups(Set<Qname> groups);

	/**
	 * Return a sorted set (@See {@link LinkedHashSet}) so that groups are in the order they are defined in MX.adminStatusEnum
	 * @param administrativeStatuses
	 * @return
	 */
	Set<Qname> orderAdministrativeStatuses(Set<Qname> administrativeStatuses);

	/**
	 * Return ids of parents of the given group
	 * @param groupId
	 * @return
	 */
	Set<Qname> getParentInformalTaxonGroups(Qname groupId);

	/**
	 * Return ids of parents of the given group
	 * @param groupId
	 * @return
	 */
	Set<Qname> getParentRedListEvaluationGroups(Qname groupId);

	/**
	 * Get latest IUCN Red List Evaluation year that is released (="locked")
	 * @return
	 * @throws UnsupportedOperationException if not implemented
	 */
	int getLatestLockedRedListEvaluationYear() throws UnsupportedOperationException;

	/**
	 * Get names of sorted informal taxon groups in different locales
	 * @param informalGroups
	 * @return
	 */
	LocalizedText getInformalTaxonGroupNames(Set<Qname> informalGroups);

	/**
	 * Get names of sorted administrative statuses in different locales
	 * @param informalGroups
	 * @return
	 */
	LocalizedText getAdministrativeStatusNames(Set<Qname> administrativeStatuses);

}
