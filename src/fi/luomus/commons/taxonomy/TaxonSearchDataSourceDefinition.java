package fi.luomus.commons.taxonomy;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import fi.luomus.commons.db.connectivity.ConnectionDescription;

public class TaxonSearchDataSourceDefinition {

	public static HikariDataSource initDataSource(ConnectionDescription desc) {
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(desc.url());
		config.setUsername(desc.username());
		config.setPassword(desc.password());
		config.setDriverClassName(desc.driver());

		config.setAutoCommit(true); // non-transaction mode

		config.setConnectionTimeout(30000); // 30 seconds
		config.setMaximumPoolSize(40);
		config.setIdleTimeout(60000); // 1 minute
		config.setMaxLifetime(60000); // 1 minutes -- the longest possible query

		return new HikariDataSource(config);
	}

}

