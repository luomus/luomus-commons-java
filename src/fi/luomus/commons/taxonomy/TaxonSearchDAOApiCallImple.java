package fi.luomus.commons.taxonomy;

import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.URIBuilder;

import org.apache.http.client.methods.HttpGet;

public class TaxonSearchDAOApiCallImple implements TaxonSearchDAO {

	private final String apiURL;
	
	public TaxonSearchDAOApiCallImple(String apiURL) {
		this.apiURL = apiURL;
	}
	
	@Override
	@SuppressWarnings("unused")
	public TaxonSearchResponse search(TaxonSearch search) throws Exception {
		HttpClientService client = null;
		try {
			client = new HttpClientService();
			URIBuilder uri = new URIBuilder(apiURL);
			uri.addParameter("q", search.getSearchword());
			if (search.hasChecklist()) {
				uri.addParameter("checklist", search.getChecklist().toString());
			}
			uri.addParameter("limit", search.getLimit());
			if (search.isOnlyExact()) {
				uri.addParameter("onlyExact", "true");
			}
			
			JSONObject json = client.contentAsJson(new HttpGet(uri.getURI()));
			TaxonSearchResponse response = new TaxonSearchResponse(search);
			throw new UnsupportedOperationException("Not yet done"); // TODO
		} finally {
			if (client != null) client.close();
		}
	}
	
}
