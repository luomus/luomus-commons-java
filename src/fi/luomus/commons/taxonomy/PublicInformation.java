package fi.luomus.commons.taxonomy;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface PublicInformation {

	public double order() default Double.MAX_VALUE;
	
}
