package fi.luomus.commons.taxonomy;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.methods.HttpGet;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.containers.AdministrativeStatus;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.Checklist;
import fi.luomus.commons.containers.ContentContextDescription;
import fi.luomus.commons.containers.ContentGroups;
import fi.luomus.commons.containers.ContentGroups.ContentGroup;
import fi.luomus.commons.containers.ContentGroups.ContentVariable;
import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.OccurrenceType;
import fi.luomus.commons.containers.Person;
import fi.luomus.commons.containers.Publication;
import fi.luomus.commons.containers.RedListEvaluationGroup;
import fi.luomus.commons.containers.TaxonSet;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.reporting.ErrorReporter;
import fi.luomus.commons.reporting.ErrorReporterViaEmailAndToSystemErr;
import fi.luomus.commons.reporting.ErrorReportingToSystemErr;
import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.SingleObjectCache;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;
import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;

public abstract class TaxonomyDAOBaseImple implements TaxonomyDAO {

	private static final int DEFAULT_LONG_CACHE_TIME_IN_SECONDS = 60 * 60 * 24;
	private static final int DEFAULT_SHORT_CACHE_TIME_IN_SECONDS = 60 * 60;

	private static String triplestoreURL;
	private static String triplestoreUsername;
	private static String triplestorePassword;
	private final SingleObjectCache<JSONObject> cachedAlts;
	private final SingleObjectCache<Map<String, Publication>> cachedPublications;
	private final SingleObjectCache<Map<String, Checklist>> cachedChecklists;
	private final SingleObjectCache<Map<String, InformalTaxonGroup>> cachedInformalTaxonGroups;
	private final SingleObjectCache<Map<String, TaxonSet>> cachedTaxonSets;
	private final SingleObjectCache<Map<String, RedListEvaluationGroup>> cachedIucnGroups;
	private final SingleObjectCache<Map<String, AdministrativeStatus>> cachedAdministrativeStatuses;
	private final SingleObjectCache<Map<String, OccurrenceType>> cachedOccurrenceTypes;
	private final SingleObjectCache<Map<String, Person>> cachedPersons;
	private final SingleObjectCache<Map<String, ContentContextDescription>> cachedContentContextDescriptions;
	private final SingleObjectCache<Map<String, LocalizedText>> cachedTaxonRankLabels;
	private final SingleObjectCache<Collection<Qname>> cachedTaxonRanks;
	private final SingleObjectCache<Map<String, Area>> cachedAreas;
	private final Cached<String, LocalizedText> cachedLabels;
	private final Cached<Qname, Map<String, LocalizedText>> cachedAltLabels;
	private final SingleObjectCache<ContentGroups> cachedContentGroups;
	private final SingleObjectCache<Map<String, LocalizedText>> cachedLicenseFullnames;
	private final ErrorReporter errorReporter;

	/**
	 * @param config
	 * @param longCacheTime in seconds
	 * @param shortCacheTime in seconds
	 */
	public TaxonomyDAOBaseImple(Config config, int longCacheTime, int shortCacheTime) {
		TaxonomyDAOBaseImple.triplestoreURL = config.get("TriplestoreURL");
		TaxonomyDAOBaseImple.triplestoreUsername = config.get("TriplestoreUsername");
		TaxonomyDAOBaseImple.triplestorePassword = config.get("TriplestorePassword");
		errorReporter = initErrorReporter(config);
		this.cachedAlts = new SingleObjectCache<>(
				new AltLoaderFromTriplestoreAPI(),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedPublications = new SingleObjectCache<>(
				new PublicationLoaderFromTriplestoreAPI(),
				shortCacheTime, TimeUnit.SECONDS);
		this.cachedChecklists = new SingleObjectCache<>(
				new ChecklistLoaderFromTriplestoreAPI(),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedInformalTaxonGroups = new SingleObjectCache<>(
				new InformalTaxonGroupLoaderFromTriplestoreAPI(),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedTaxonSets = new SingleObjectCache<>(
				new TaxonSetLoaderFromTriplestoreAPI(),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedIucnGroups = new SingleObjectCache<>(
				new IucnGroupLoaderFromTriplestoreAPI(this),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedAdministrativeStatuses = new SingleObjectCache<>(
				new AdminStatusLoaderFromTriplestoreAPI(cachedAlts),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedOccurrenceTypes = new SingleObjectCache<>(
				new OccurrenceTypeLoaderFromTriplestoreAPI(cachedAlts),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedPersons = new SingleObjectCache<>(
				new PersonLoaderFromTriplestoreAPI(),
				shortCacheTime, TimeUnit.SECONDS);
		this.cachedContentContextDescriptions = new SingleObjectCache<>(
				new ContentContextDescriptionsLoaderFromTriplestoreAPI(),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedTaxonRanks = new SingleObjectCache<>(
				new TaxonRankLoaderFromTriplestoreAPI(cachedAlts),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedAreas = new SingleObjectCache<>(
				new AreaLoaderFromTriplestoreAPI(),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedLabels = new Cached<>(
				new LabelLoaderFromTriplestoreApi(),
				longCacheTime, TimeUnit.SECONDS, 1000);
		this.cachedContentGroups = new SingleObjectCache<>(
				new ContentGroupsLoaderFromTriplestoreApi(),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedTaxonRankLabels = new SingleObjectCache<>(
				new TaxonRankLabelLoader(cachedAlts),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedLicenseFullnames = new SingleObjectCache<>(
				new LicenseFullnameLoader(),
				longCacheTime, TimeUnit.SECONDS);
		this.cachedAltLabels = new Cached<>(
				new AltLabelLoader(cachedAlts),
				longCacheTime, TimeUnit.SECONDS, 1000);
	}

	private ErrorReporter initErrorReporter(Config config) {
		if (!config.developmentMode() && config.defines(Config.ERROR_REPORTING_SMTP_HOST)) {
			try {
				return new ErrorReporterViaEmailAndToSystemErr(config);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return new ErrorReportingToSystemErr();
	}

	public TaxonomyDAOBaseImple(Config config) {
		this(config, DEFAULT_LONG_CACHE_TIME_IN_SECONDS, DEFAULT_SHORT_CACHE_TIME_IN_SECONDS);
	}

	protected static HttpClientService getClient() throws URISyntaxException {
		return new HttpClientService(TaxonomyDAOBaseImple.triplestoreURL, TaxonomyDAOBaseImple.triplestoreUsername, TaxonomyDAOBaseImple.triplestorePassword);
	}

	@Override
	public void clearCaches() {
		cachedAdministrativeStatuses.invalidate();
		cachedAlts.invalidate();
		cachedAreas.invalidate();
		cachedChecklists.invalidate();
		cachedContentGroups.invalidate();
		cachedContentContextDescriptions.invalidate();
		cachedInformalTaxonGroups.invalidate();
		cachedLabels.invalidateAll();
		cachedOccurrenceTypes.invalidate();
		cachedPersons.invalidate();
		cachedPublications.invalidate();
		cachedTaxonRanks.invalidate();
		cachedTaxonSets.invalidate();
		cachedIucnGroups.invalidate();
	}

	protected String getTriplestoreBaseURL() {
		return triplestoreURL;
	}

	private class AltLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<JSONObject> {
		@Override
		public JSONObject load() {
			HttpClientService client = null;
			try {
				client = getClient();
				return client.contentAsJson(new HttpGet(triplestoreURL+"/schema/alt"));
			} catch (Exception e) {
				errorReporter.report("Alt loader", e);
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
	}

	private class ChecklistLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Map<String, Checklist>> {
		@Override
		public Map<String, Checklist> load() {
			HttpClientService client = null;
			try {
				client = getClient();
				List<Checklist> checklists = loadUsingClient(client);
				Collections.sort(checklists);
				Map<String, Checklist> map = new LinkedHashMap<>();
				for (Checklist checklist : checklists) {
					map.put(checklist.getQname().toString(), checklist);
				}
				return map;
			} catch (Exception e) {
				errorReporter.report("Checklist loader", e);
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
		private List<Checklist> loadUsingClient(HttpClientService client) throws Exception {
			List<Checklist> checklists = new ArrayList<>();
			URIBuilder uri = new URIBuilder(triplestoreURL + "/search")
					.addParameter("predicate", "rdf:type")
					.addParameter("objectresource", "MR.checklist");

			Document doc = client.contentAsDocument(new HttpGet(uri.getURI()));
			for (Node node : doc.getRootNode().getChildNodes()) {
				Qname qname = getObjectResourceQname(node);
				LocalizedText name = getLocalizedText(node, "dc:bibliographicCitation");
				LocalizedText notes = getLocalizedText(node, "rdfs:comment");
				Qname rootTaxon = getRooTaxonOrNull(node);
				Qname owner = getOwnerOrNull(node);
				boolean isPublic = getPublicityDefaultToTrue(node);
				Checklist checklist = new Checklist(qname, name, rootTaxon);
				checklist.setNotes(notes);
				checklist.setPublic(isPublic);
				if (owner != null) {
					checklist.setOwner(owner);
				}
				checklists.add(checklist);
			}
			return checklists;
		}
		private boolean getPublicityDefaultToTrue(Node node) {
			if (!node.hasChildNodes("MR.isPublic")) {
				return true;
			}
			String value = node.getNode("MR.isPublic").getContents();
			if (value.equals("false")) return false;
			return true;
		}
		private Qname getOwnerOrNull(Node node) {
			if (!node.hasChildNodes("MR.owner")) {
				return null;
			}
			return getObjectResourceQname(node.getNode("MR.owner"));
		}
		private Qname getRooTaxonOrNull(Node node) {
			if (node.hasChildNodes("MR.rootTaxon")) {
				return Qname.fromURI((node.getNode("MR.rootTaxon").getAttribute("rdf:resource")));
			}
			return null;
		}
	}

	private void setParents(Map<String, ? extends InformalTaxonGroup> map) {
		for (InformalTaxonGroup group : map.values())  {
			Iterator<Qname> childIterator = group.getSubGroups().iterator();
			while (childIterator.hasNext()) {
				Qname childId = childIterator.next();
				InformalTaxonGroup child = map.get(childId.toString());
				if (child == null) {
					// We will not let malformed data to prevent all loading
					childIterator.remove();
					reportMissingChild(group, childId);
				} else {
					child.addParent(group.getQname());
				}
			}
		}
	}

	private static <T extends InformalTaxonGroup> Map<String, T> sort(Map<String, T> map) {
		LinkedHashMap<String, T> sorted = new LinkedHashMap<>(map.size());
		map.values().stream()
		.filter(g -> g.isRoot())
		.sorted()
		.forEach(g -> { add(sorted, map, g); });
		return sorted;
	}

	private static <T extends InformalTaxonGroup> void add(LinkedHashMap<String, T> sorted, Map<String, T> all, T group) {
		if (!sorted.containsKey(group.getQname().toString())) {
			sorted.put(group.getQname().toString(), group); // If group exists in multiple places in the tree, prefer order of first occurrence
		}
		group.getSubGroups().stream()
		.map(id -> all.get(id.toString()))
		.sorted()
		.forEach(g -> { add(sorted, all, g); });
	}

	private void reportMissingChild(InformalTaxonGroup group, Qname childId) {
		String error = "Group " + group.getQname() + " defines child " + childId + " that does not exist!";
		try {
			errorReporter.report(error);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(error);
		}
	}

	private class InformalTaxonGroupLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Map<String, InformalTaxonGroup>> {
		@Override
		public Map<String, InformalTaxonGroup> load() {
			HttpClientService client = null;
			try {
				client = getClient();
				Map<String, InformalTaxonGroup> map = loadUsingClient(client);
				setParents(map);
				return sort(map);
			} catch (Exception e) {
				errorReporter.report("Informal taxon group loader", e);
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
		private Map<String, InformalTaxonGroup> loadUsingClient(HttpClientService client) throws Exception {
			Map<String, InformalTaxonGroup> groups = new LinkedHashMap<>();
			URIBuilder uri = new URIBuilder(triplestoreURL + "/search")
					.addParameter("type", "MVL.informalTaxonGroup")
					.addParameter("format", "rdfxml");
			Document doc = client.contentAsDocument(new HttpGet(uri.getURI()));
			for (Node node : doc.getRootNode().getChildNodes()) {
				Qname qname = getObjectResourceQname(node);
				LocalizedText name = getLocalizedText(node, "MVL.name");
				int order = getSortOrder(node);
				InformalTaxonGroup group = new InformalTaxonGroup(qname, name, order);
				for (Node subGroupNode : node.getChildNodes("MVL.hasSubGroup")) {
					Qname subGroupQname = getObjectResourceQname(subGroupNode);
					group.addSubGroup(subGroupQname);
				}
				if (node.hasChildNodes("MVL.explicitlyDefinedRoot")) {
					group.setExplicitlyDefinedRoot("true".equals(node.getNode("MVL.explicitlyDefinedRoot").getContents()));
				}
				groups.put(group.getQname().toString(), group);
			}
			return groups;
		}
	}

	private class TaxonSetLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Map<String, TaxonSet>> {
		@Override
		public Map<String, TaxonSet> load() {
			HttpClientService client = null;
			try {
				client = getClient();
				return loadUsingClient(client);
			} catch (Exception e) {
				errorReporter.report("Informal taxon group loader", e);
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
		private Map<String, TaxonSet> loadUsingClient(HttpClientService client) throws Exception {
			Map<String, TaxonSet> sets = new LinkedHashMap<>();
			URIBuilder uri = new URIBuilder(triplestoreURL + "/search")
					.addParameter("type", "MX.taxonSet")
					.addParameter("format", "rdfxml");
			Document doc = client.contentAsDocument(new HttpGet(uri.getURI()));
			for (Node node : doc.getRootNode().getChildNodes()) {
				Qname qname = getObjectResourceQname(node);
				LocalizedText name = getLocalizedText(node, "rdfs:label");
				TaxonSet set = new TaxonSet(qname, name);
				sets.put(set.getQname().toString(), set);
			}
			return sets;
		}
	}

	private class IucnGroupLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Map<String, RedListEvaluationGroup>> {
		private final TaxonomyDAOBaseImple dao;
		public IucnGroupLoaderFromTriplestoreAPI(TaxonomyDAOBaseImple dao) {
			this.dao = dao;
		}
		@Override
		public Map<String, RedListEvaluationGroup> load() {
			HttpClientService client = null;
			try {
				client = getClient();
				Map<String, RedListEvaluationGroup> map = loadUsingClient(client);
				setParents(map);
				return sort(map);
			} catch (Exception e) {
				errorReporter.report("Iucn group loader", e);
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
		private Map<String, RedListEvaluationGroup> loadUsingClient(HttpClientService client) throws Exception {
			Map<String, RedListEvaluationGroup> groups = new LinkedHashMap<>();
			URIBuilder uri = new URIBuilder(triplestoreURL + "/search")
					.addParameter("type", "MVL.iucnRedListTaxonGroup")
					.addParameter("format", "rdfxml");

			Document doc = client.contentAsDocument(new HttpGet(uri.getURI()));
			for (Node node : doc.getRootNode().getChildNodes()) {
				Qname qname = getObjectResourceQname(node);
				LocalizedText name = getLocalizedText(node, "MVL.name");
				int order = getSortOrder(node);
				RedListEvaluationGroup group = new RedListEvaluationGroup(qname, name, order);

				for (Node subGroupNode : node.getChildNodes("MVL.hasIucnSubGroup")) {
					Qname subGroupQname = getObjectResourceQname(subGroupNode);
					group.addSubGroup(subGroupQname);
				}

				for (Node includesTaxonNode : node.getChildNodes("MVL.includesTaxon")) {
					Qname taxonId = getObjectResourceQname(includesTaxonNode);
					group.addTaxon(taxonId);
				}

				for (Node includesInformalTaxonGroup : node.getChildNodes("MVL.includesInformalTaxonGroup")) {
					Qname groupId = getObjectResourceQname(includesInformalTaxonGroup);
					group.addInformalGroup(groupId);
				}

				if (name.isEmpty()) {
					if (group.getTaxa().size() == 1 && group.getInformalGroups().isEmpty()) {
						Qname taxonId = group.getTaxa().iterator().next();
						Taxon taxon = dao.getTaxon(taxonId);
						name.set("fi", getName(taxon, "fi"));
						name.set("sv", getName(taxon, "sv"));
						name.set("en", getName(taxon, "en"));
					}
					if (group.getInformalGroups().size() == 1 && group.getTaxa().isEmpty()) {
						Qname groupId = group.getInformalGroups().iterator().next();
						InformalTaxonGroup informalTaxonGroup = dao.getInformalTaxonGroups().get(groupId.toString());
						for (String lang : informalTaxonGroup.getName().getAllTexts().keySet()) {
							name.set(lang, informalTaxonGroup.getName(lang));
						}
					}
				}
				groups.put(group.getQname().toString(), group);
			}
			return groups;
		}
		private String getName(Taxon taxon, String locale) {
			String name = "";
			if (taxon.getVernacularName().hasTextForLocale(locale)) {
				name = Utils.upperCaseFirst(taxon.getVernacularName().forLocale(locale));
			}
			if (taxon.getScientificName() != null) {
				if (!name.isEmpty()) name += ", ";
				name += taxon.getScientificName();
			}
			if (name.isEmpty()) name = taxon.getQname().toString();
			return name;
		}
	}

	private static class AdminStatusLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Map<String, AdministrativeStatus>> {
		private final SingleObjectCache<JSONObject> cachedAlts;
		public AdminStatusLoaderFromTriplestoreAPI(SingleObjectCache<JSONObject> cachedAlts) {
			this.cachedAlts = cachedAlts;
		}
		@Override
		public Map<String, AdministrativeStatus> load() {
			Map<String, AdministrativeStatus> map = new LinkedHashMap<>();
			for (JSONObject json : cachedAlts.get().getArray("MX.adminStatusEnum").iterateAsObject()) {
				AdministrativeStatus status = new AdministrativeStatus(new Qname(json.getString("id")), getLocalizedText(json));
				map.put(status.getQname().toString(), status);
			}
			return map;
		}
	}

	private static class OccurrenceTypeLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Map<String, OccurrenceType>> {
		private final SingleObjectCache<JSONObject> cachedAlts;
		public OccurrenceTypeLoaderFromTriplestoreAPI(SingleObjectCache<JSONObject> cachedAlts) {
			this.cachedAlts = cachedAlts;
		}
		@Override
		public Map<String, OccurrenceType> load() {
			Map<String, OccurrenceType> map = new LinkedHashMap<>();
			for (JSONObject json : cachedAlts.get().getArray("MX.typeOfOccurrenceEnum").iterateAsObject()) {
				OccurrenceType type = new OccurrenceType(new Qname(json.getString("id")), getLocalizedText(json));
				map.put(type.getQname().toString(), type);
			}
			return map;
		}
	}

	private static Qname getObjectResourceQname(Node node) {
		String attribute = "rdf:resource";
		if (node.hasAttribute("rdf:about")) {
			attribute = "rdf:about";
		}
		String resourceUri = node.getAttribute(attribute);
		return Qname.fromURI(resourceUri);

	}

	private static LocalizedText getLocalizedText(JSONObject json) {
		LocalizedText text = new LocalizedText();
		JSONObject values = json.getObject("value");
		for (String locale : values.getKeys()) {
			text.set(locale, values.getString(locale));
		}
		return text;
	}

	private static LocalizedText getLocalizedText(Node node, String property) {
		LocalizedText text = new LocalizedText();
		for (Node nameNode : node.getChildNodes(property)) {
			if (nameNode.hasAttribute("xml:lang")) {
				String lang = nameNode.getAttribute("xml:lang");
				text.set(lang, nameNode.getContents());
			} else {
				text.set("", nameNode.getContents());
			}
		}
		return text;
	}

	private static int getSortOrder(Node node) {
		for (Node n : node.getChildNodes("sortOrder")) {
			try {
				return Integer.valueOf(n.getContents());
			} catch (Exception e) {}
		}
		return Integer.MAX_VALUE;
	}

	private static LocalizedText getLocalizedText(Node node) {
		return getLocalizedText(node, "rdfs:label");
	}

	private class PublicationLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Map<String, Publication>> {
		@Override
		public Map<String, Publication> load() {
			HttpClientService client = null;
			try {
				client = getClient();
				List<Publication> publications = loadUsingClient(client);
				Collections.sort(publications);
				Map<String, Publication> publicationMap = new LinkedHashMap<>();
				for (Publication publication : publications) {
					publicationMap.put(publication.getQname().toString(), publication);
				}
				return publicationMap;
			} catch (Exception e) {
				errorReporter.report("Publication loader", e);
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
		private List<Publication> loadUsingClient(HttpClientService client) throws Exception {
			List<Publication> publications = new ArrayList<>();
			URIBuilder uri = new URIBuilder(triplestoreURL + "/search")
					.addParameter("predicate", "rdf:type")
					.addParameter("objectresource", "MP.publication")
					.addParameter("limit", 50000);

			Document doc = client.contentAsDocument(new HttpGet(uri.getURI()));
			for (Node node : doc.getRootNode().getChildNodes()) {
				Qname qname = getObjectResourceQname(node);
				String citation = node.hasChildNodes("dc:bibliographicCitation") ? node.getNode("dc:bibliographicCitation").getContents() : null;
				String link = node.hasChildNodes("dc:URI") ? node.getNode("dc:URI").getContents() : null;
				Publication publication = new Publication(qname);
				publication.setCitation(citation);
				publication.setURI(link);
				publications.add(publication);
			}
			return publications;
		}
	}

	private class LabelLoaderFromTriplestoreApi implements Cached.CacheLoader<String, LocalizedText> {
		@Override
		public LocalizedText load(String variable) {
			HttpClientService client = null;
			try {
				client = getClient();
				return loadUsingClient(client, variable);
			} catch (Exception e) {
				errorReporter.report("Label loader: " + variable, e);
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
		private LocalizedText loadUsingClient(HttpClientService client, String variable) throws Exception {
			Document doc = client.contentAsDocument(new HttpGet(triplestoreURL + "/" + variable));
			for (Node node : doc.getRootNode().getChildNodes()) {
				LocalizedText labels = getLocalizedText(node);
				return labels;
			}
			return null;
		}
	}


	private class AreaLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Map<String, Area>> {
		@Override
		public Map<String, Area> load() {
			HttpClientService client = null;
			try {
				client = getClient();
				Map<String, Area> areas = loadUsingClient(client);
				return areas;
			} catch (Exception e) {
				errorReporter.report("Area loader");
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
		private Map<String, Area> loadUsingClient(HttpClientService client) throws Exception {
			String uri = triplestoreURL + "/search?type=ML.area&format=rdfxml&limit=10000";
			Map<String, Area> areas = new LinkedHashMap<>();
			Document doc = client.contentAsDocument(new HttpGet(uri));
			for (Node node : doc.getRootNode().getChildNodes("rdf:Description")) {
				if (!node.hasChildNodes("ML.areaType")) continue;
				Qname type = getObjectResourceQname(node.getNode("ML.areaType"));
				if (!given(type)) continue;
				LocalizedText name = new LocalizedText();
				for (Node label : node.getChildNodes("ML.name")) {
					if (label.hasAttribute("xml:lang")) {
						String langCode = label.getAttribute("xml:lang");
						name.set(langCode, label.getContents());
					}
				}
				String abbreviation = null;
				for (Node abbrNode : node.getChildNodes("ML.provinceCodeAlpha")) {
					if (abbrNode.hasAttribute("xml:lang") && abbrNode.getAttribute("xml:lang").equals("fi")) {
						abbreviation = abbrNode.getContents();
						break;
					}
				}
				Area area = new Area(getObjectResourceQname(node), name, type).setAbbreviation(abbreviation);
				if (node.hasChildNodes("ML.isPartOf")) {
					Qname isPartOf = getObjectResourceQname(node.getNode("ML.isPartOf"));
					area.setPartOf(isPartOf);
				}
				areas.put(area.getQname().toString(), area);
			}
			return areas;
		}
	}

	private static final Map<String, String> PINKKA_PREFIX = initPinkkaPrefixes();
	private static Map<String, String> initPinkkaPrefixes() {
		Map<String, String> map = new LinkedHashMap<>();
		map.put(null, "Pinkka");
		map.put("fi", "Pinkka oppimisympäristö");
		map.put("sv", "Pinkka inlärningsmiljö");
		map.put("en", "Pinkka e-learning");
		return map;
	}

	private class ContentContextDescriptionsLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Map<String, ContentContextDescription>> {
		@Override
		public Map<String, ContentContextDescription> load() {
			HttpClientService client = null;
			try {
				client = getClient();
				List<ContentContextDescription> descriptions = loadUsingClient(client);
				Collections.sort(descriptions);
				Map<String, ContentContextDescription> map = new LinkedHashMap<>();
				for (ContentContextDescription desc : descriptions) {
					map.put(desc.getQname() == null ? null : desc.getQname().toString(), desc);
				}
				return map;
			} catch (Exception e) {
				errorReporter.report("Content context loader", e);
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
		private List<ContentContextDescription> loadUsingClient(HttpClientService client) throws Exception {
			List<ContentContextDescription> descriptions = new ArrayList<>();

			URIBuilder uri = new URIBuilder(triplestoreURL + "/search")
					.addParameter("predicate", "rdf:type")
					.addParameter("objectresource", "LA.Pinkka")
					.addParameter("objectresource", "MX.contentContextDescription");

			Document doc = client.contentAsDocument(new HttpGet(uri.getURI()));
			for (Node node : doc.getRootNode().getChildNodes()) {
				Qname qname = getObjectResourceQname(node);
				if (qname.toString().equals("MX.FinBIFdefaultContextDescription")) {
					qname = null;
				}
				LocalizedText name = new LocalizedText();
				for (Node nameNode : node.getChildNodes("rdfs:label")) {
					String locale = null;
					if (nameNode.hasAttribute("xml:lang")) {
						locale = nameNode.getAttribute("xml:lang");
					}
					String contents = nameNode.getContents();
					if (qname != null && qname.toString().startsWith("LA.")) {
						contents = PINKKA_PREFIX.get(locale) + ": " + contents;
					}
					name.set(locale, contents);
				}
				ContentContextDescription description = new ContentContextDescription(qname, name);
				descriptions.add(description);
			}
			return descriptions;
		}
	}

	private class PersonLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Map<String, Person>> {
		@Override
		public Map<String, Person> load() {
			HttpClientService client = null;
			try {
				client = getClient();
				return loadUsingClient(client);
			} catch (Exception e) {
				errorReporter.report("Peson loader", e);
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
		private Map<String, Person> loadUsingClient(HttpClientService client) throws Exception {
			List<Person> personsAlphabetically = new ArrayList<>();

			URIBuilder uri = new URIBuilder(triplestoreURL + "/search")
					.addParameter("type", "MA.person")
					.addParameter("predicate", "MA.role,MA.roleKotka,MA.organisation,MX.taxonEditor,MX.taxonExpert")
					.addParameter("limit", "5000");
			Document doc = client.contentAsDocument(new HttpGet(uri.getURI()));
			for (Node node : doc.getRootNode().getChildNodes()) {
				Qname qname = getObjectResourceQname(node);
				String fullname = null;
				if (node.hasChildNodes("MA.fullName")) {
					fullname = node.getNode("MA.fullName").getContents();
				}
				Person person = new Person(qname, fullname);
				personsAlphabetically.add(person);
			}

			Collections.sort(personsAlphabetically, new Comparator<Person>() {
				@Override
				public int compare(Person o1, Person o2) {
					String name1 = o1.hasFullname() ? o1.getFullname() : "" + Character.MAX_VALUE;
					String name2 = o2.hasFullname() ? o2.getFullname() : "" + Character.MAX_VALUE;
					int c = name1.compareTo(name2);
					if (c != 0) return c;
					return o1.getId().compareTo(o2.getId());
				}
			});

			Map<String, Person> personsMap = new LinkedHashMap<>();
			for (Person person : personsAlphabetically) {
				personsMap.put(person.getId().toString(), person);
			}
			return personsMap;
		}
	}

	private static class TaxonRankLoaderFromTriplestoreAPI implements SingleObjectCache.CacheLoader<Collection<Qname>> {
		private final SingleObjectCache<JSONObject> cachedAlts;
		public TaxonRankLoaderFromTriplestoreAPI(SingleObjectCache<JSONObject> cachedAlts) {
			this.cachedAlts = cachedAlts;
		}
		@Override
		public Collection<Qname> load() {
			List<Qname> list = new ArrayList<>();
			for (JSONObject json : cachedAlts.get().getArray("MX.taxonRankEnum").iterateAsObject()) {
				list.add(new Qname(json.getString("id")));
			}
			return list;
		}
	}

	private static class TaxonRankLabelLoader implements SingleObjectCache.CacheLoader<Map<String, LocalizedText>> {
		private final SingleObjectCache<JSONObject> cachedAlts;
		public TaxonRankLabelLoader(SingleObjectCache<JSONObject> cachedAlts) {
			this.cachedAlts = cachedAlts;
		}
		@Override
		public Map<String, LocalizedText> load() {
			Map<String, LocalizedText> labels = new LinkedHashMap<>();
			for (JSONObject json : cachedAlts.get().getArray("MX.taxonRankEnum").iterateAsObject()) {
				labels.put(json.getString("id"), getLocalizedText(json));
			}
			return labels;
		}
	}

	private class LicenseFullnameLoader implements SingleObjectCache.CacheLoader<Map<String, LocalizedText>> {
		@Override
		public Map<String, LocalizedText> load() {
			Map<String, LocalizedText> map = new LinkedHashMap<>();
			try (HttpClientService client = getClient()) {
				Document licencesDoc = client.contentAsDocument(new HttpGet(triplestoreURL+"/MZ.intellectualRightsEnum"));
				for (Node licenseNode : licencesDoc.getRootNode().getNode("rdf:Alt")) {
					Qname license = Qname.fromURI(licenseNode.getAttribute("rdf:resource"));
					Document licenseDoc = client.contentAsDocument(new HttpGet(triplestoreURL + "/" + license));
					LocalizedText localizedText = new LocalizedText();
					for (Node licenseLabelNode : licenseDoc.getRootNode().getNode("rdf:Description").getChildNodes("rdfs:label")) {
						String locale = licenseLabelNode.getAttribute("xml:lang");
						String label = licenseLabelNode.getContents();
						localizedText.set(locale, label);
					}
					map.put(license.toString(), localizedText);
				}
			} catch (Exception e) {
				errorReporter.report("License fullname loader", e);
				throw new RuntimeException(e);
			}
			return map;
		}
	}

	private class ContentGroupsLoaderFromTriplestoreApi implements SingleObjectCache.CacheLoader<ContentGroups> {
		@Override
		public ContentGroups load() {
			HttpClientService client = null;
			try {
				client = getClient();
				return loadUsingClient(client);
			} catch (Exception e) {
				errorReporter.report("Content groups loader", e);
				throw new RuntimeException(e);
			}
			finally {
				if (client != null) client.close();
			}
		}
		private ContentGroups loadUsingClient(HttpClientService client) throws Exception {
			String uri = triplestoreURL + "/MX.speciesDescriptionVariables?format=rdfxml";
			Document doc = client.contentAsDocument(new HttpGet(uri));
			TreeMap<Integer, ContentGroup> orderedContentGroups = new TreeMap<>();
			for (Node node : doc.getRootNode().getNode("rdf:Description").getChildNodes()) {
				if (!node.getName().startsWith("rdf:_")) continue;
				int order = Integer.valueOf(node.getName().replace("rdf:_", ""));
				Qname groupId = getObjectResourceQname(node);
				ContentGroup contentGroup = loadGroup(groupId, client);
				orderedContentGroups.put(order, contentGroup);
			}
			List<ContentGroup> groups = new ArrayList<>();
			groups.addAll(orderedContentGroups.values());
			ContentGroups contentGroups = new ContentGroups(groups);
			return contentGroups;
		}
		private ContentGroup loadGroup(Qname groupId, HttpClientService client) throws Exception {
			String uri = triplestoreURL + "/" + groupId + "?format=rdfxml";
			Document doc = client.contentAsDocument(new HttpGet(uri));
			Node rootNode = doc.getRootNode().getNode("rdf:Description");
			LocalizedText title = getLocalizedText(rootNode);
			TreeMap<Integer, ContentVariable> orderedContentVariables = new TreeMap<>();
			for (Node node : rootNode.getChildNodes()) {
				if (!node.getName().startsWith("rdf:_")) continue;
				int order = Integer.valueOf(node.getName().replace("rdf:_", ""));
				Qname variableId = getObjectResourceQname(node);
				ContentVariable variable = loadVariable(variableId, client);
				orderedContentVariables.put(order, variable);
			}
			List<ContentVariable> variables = new ArrayList<>();
			variables.addAll(orderedContentVariables.values());
			return new ContentGroup(groupId, title, variables);
		}
		private ContentVariable loadVariable(Qname variableId, HttpClientService client) throws Exception {
			String uri = triplestoreURL + "/" + variableId + "?format=rdfxml";
			Document doc = client.contentAsDocument(new HttpGet(uri));
			Node rootNode = doc.getRootNode().getNode("rdf:Description");
			LocalizedText title = getLocalizedText(rootNode);
			return new ContentVariable(variableId, title);
		}
	}

	private class AltLabelLoader implements Cached.CacheLoader<Qname, Map<String, LocalizedText>> {
		private final SingleObjectCache<JSONObject> cachedAlts;
		public AltLabelLoader(SingleObjectCache<JSONObject> cachedAlts) {
			this.cachedAlts = cachedAlts;
		}
		@Override
		public Map<String, LocalizedText> load(Qname range) {
			Map<String, LocalizedText> alt = new LinkedHashMap<>();
			for (JSONObject o : cachedAlts.get().getArray(range.toString()).iterateAsObject()) {
				alt.put(o.getString("id"), getLocalizedText(o));
			}
			return alt;
		}
	}

	@Override
	public Map<String, Publication> getPublications() {
		return cachedPublications.get();
	}

	private static boolean given(Qname qname) {
		return qname != null && qname.isSet();
	}

	@Override
	public Map<String, Publication> getPublicationsForceReload() {
		return cachedPublications.getForceReload();
	}

	@Override
	public Map<String, Checklist> getChecklists() {
		return cachedChecklists.get();
	}

	@Override
	public Map<String, Checklist> getChecklistsForceReload() {
		return cachedChecklists.getForceReload();
	}

	@Override
	public Map<String, InformalTaxonGroup> getInformalTaxonGroups() {
		return cachedInformalTaxonGroups.get();
	}

	@Override
	public Map<String, InformalTaxonGroup> getInformalTaxonGroupsForceReload() {
		return cachedInformalTaxonGroups.getForceReload();
	}

	@Override
	public Map<String, TaxonSet> getTaxonSets() {
		return cachedTaxonSets.get();
	}

	@Override
	public Map<String, TaxonSet> getTaxonSetsForceReload() {
		return cachedTaxonSets.getForceReload();
	}

	@Override
	public Map<String, RedListEvaluationGroup> getRedListEvaluationGroups() {
		return cachedIucnGroups.get();
	}

	@Override
	public Map<String, RedListEvaluationGroup> getRedListEvaluationGroupsForceReload() {
		return cachedIucnGroups.getForceReload();
	}

	@Override
	public Map<String, AdministrativeStatus> getAdministrativeStatuses() {
		return cachedAdministrativeStatuses.get();
	}

	@Override
	public Map<String, AdministrativeStatus> getAdministrativeStatusesForceReload() {
		return cachedAdministrativeStatuses.getForceReload();
	}

	@Override
	public Map<String, Person> getPersons() {
		return cachedPersons.get();
	}

	@Override
	public Collection<Qname> getTaxonRanks() {
		return cachedTaxonRanks.get();
	}

	@Override
	public Map<String, LocalizedText> getTaxonRankLabels() {
		return cachedTaxonRankLabels.get();
	}

	@Override
	public Map<String, ContentContextDescription> getContentContextDescriptions() {
		return cachedContentContextDescriptions.get();
	}

	@Override
	public Map<String, Area> getAreas() {
		return cachedAreas.get();
	}

	@Override
	public LocalizedText getLabels(Qname variable) {
		if (variable == null) return null;
		return cachedLabels.get(variable.toString());
	}

	@Override
	public ContentGroups getContentGroups() {
		return cachedContentGroups.get();
	}

	@Override
	public Map<String, OccurrenceType> getOccurrenceTypes() {
		return cachedOccurrenceTypes.get();
	}

	@Override
	public Map<String, OccurrenceType> getOccurrenceTypesForceReload() {
		return cachedOccurrenceTypes.getForceReload();
	}

	@Override
	public Map<String, LocalizedText> getLicenseFullnames() {
		return cachedLicenseFullnames.get();
	}

	@Override
	public Map<String, LocalizedText> getAlt(Qname range) {
		return cachedAltLabels.get(range);
	}

}
