package fi.luomus.commons.taxonomy;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import fi.luomus.commons.containers.Content;
import fi.luomus.commons.containers.LocalizedURL;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class TripletToTaxonHandlers {

	private final TripletToTaxonHandler NOP_HANDLER = new TripletToTaxonHandler() {
		@Override
		public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
			// do nothing
		}
	};
	private final TripletToTaxonHandler DESCRIPTIONS_HANDLER = new TripletToTaxonHandler() {
		@Override
		public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
			Content content = taxon.getDescriptions();
			setToContent(context, predicatename, objectname, resourceliteral, locale, content);
		}
	};

	private final Map<String, TripletToTaxonHandler> HANDLERS = initHandlers();

	public TripletToTaxonHandler getHandler(Qname predicatename) {
		if (!HANDLERS.containsKey(predicatename.toString())) return NOP_HANDLER;
		return HANDLERS.get(predicatename.toString());
	}

	public TripletToTaxonHandlers extend(String predicate, TripletToTaxonHandler handler) {
		HANDLERS.put(predicate, handler);
		return this;
	}

	private Map<String, TripletToTaxonHandler> initHandlers() {
		Map<String, TripletToTaxonHandler> handlers = new HashMap<>();

		handlers.put("MX.isPartOf", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setParentQname(objectname);
			}
		});

		handlers.put("MX.hasBasionym", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addBasionym(objectname);
			}
		});

		handlers.put("MX.hasSynonym", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addSynonym(objectname);
			}
		});

		handlers.put("MX.hasAlternativeName", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addAlternativeName(objectname);
			}
		});

		handlers.put("MX.hasObjectiveSynonym", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addObjectiveSynonym(objectname);
			}
		});

		handlers.put("MX.hasSubjectiveSynonym", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addSubjectiveSynonym(objectname);
			}
		});

		handlers.put("MX.hasHomotypicSynonym", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addHomotypicSynonym(objectname);
			}
		});

		handlers.put("MX.hasHeterotypicSynonym", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addHeterotypicSynonym(objectname);
			}
		});

		handlers.put("MX.hasMisappliedName", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addMisappliedName(objectname);
			}
		});

		handlers.put("MX.hasMisspelledName", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addMisspelledName(objectname);
			}
		});

		handlers.put("MX.hasOrthographicVariant", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addOrthographicVariant(objectname);
			}
		});

		handlers.put("MX.hasUncertainSynonym", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addSynonyms().addUncertainSynonym(objectname);
			}
		});

		handlers.put("MX.secureLevel", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setSecureLevel(objectname);
			}
		});

		handlers.put("MX.breedingSecureLevel", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setBreedingSecureLevel(objectname);
			}
		});

		handlers.put("MX.winteringSecureLevel", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setWinteringSecureLevel(objectname);
			}
		});

		handlers.put("MX.nestSiteSecureLevel", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setNestSiteSecureLevel(objectname);
			}
		});

		handlers.put("MX.naturaAreaSecureLevel", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setNaturaAreaSecureLevel(objectname);
			}
		});

		handlers.put("MX.alsoKnownAs", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addAlsoKnownAsName(resourceliteral);
			}
		});

		handlers.put("MX.overridingTargetName", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addOverridingTargetName(resourceliteral);
			}
		});

		handlers.put("sortOrder", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				try {
					if (resourceliteral != null) {
						taxon.setSortOrder(new Integer(resourceliteral));
					}
				} catch (NumberFormatException ignored) {}
			}
		});

		handlers.put("MX.hasAdminStatus", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addAdministrativeStatus(objectname);
			}
		});

		handlers.put("MX.isPartOfInformalTaxonGroup", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addInformalTaxonGroup(objectname);
			}
		});

		handlers.put("MX.externalLinkURL", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addExternalLink(new LocalizedURL(toUri(resourceliteral, predicatename, taxon), locale));
			}
		});

		handlers.put("MX.hiddenTaxon", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setHiddenTaxon(booleanValue(resourceliteral));
			}
		});

		handlers.put("MX.invasiveSpeciesEarlyWarning", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setInvasiveSpeciesEarlyWarning(booleanValue(resourceliteral));
			}
		});

		handlers.put("MX.stopOccurrenceInFinlandPublicationInheritance", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setStopOccurrenceInFinlandPublicationInheritance(booleanValue(resourceliteral));
			}
		});

		handlers.put("MX.stopOriginalPublicationInheritance", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setStopOriginalPublicationInheritance(booleanValue(resourceliteral));
			}
		});

		handlers.put("MX.stopInformalTaxonGroupInheritance", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setStopInformalTaxonGroupInheritance(booleanValue(resourceliteral));
			}
		});

		handlers.put("MX.nameAccordingTo", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setChecklist(objectname);
			}
		});

		handlers.put("MX.scientificName", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setScientificName(resourceliteral);
			}
		});

		handlers.put("MX.scientificNameAuthorship", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setScientificNameAuthorship(resourceliteral);
			}
		});

		handlers.put("MX.taxonRank", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setTaxonRank(objectname);
			}
		});

		handlers.put("MX.vernacularName", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addVernacularName(locale, resourceliteral);
			}
		});

		handlers.put("MX.typeOfOccurrenceInFinlandNotes", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setTypeOfOccurrenceInFinlandNotes(resourceliteral);
			}
		});

		handlers.put("MX.alternativeVernacularName", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addAlternativeVernacularName(locale, resourceliteral);
			}
		});

		handlers.put("MX.obsoleteVernacularName", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addObsoleteVernacularName(locale, resourceliteral);
			}
		});

		handlers.put("MX.colloquialVernacularName", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addColloquialVernacularName(locale, resourceliteral);
			}
		});

		handlers.put("MX.tradeName", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addTradeName(locale, resourceliteral);
			}
		});

		handlers.put("MX.taxonExpert", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addExpert(objectname);
			}
		});

		handlers.put("MX.taxonEditor", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addEditor(objectname);
			}
		});

		handlers.put("MX.finnish", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setFinnish(booleanValue(resourceliteral));
			}
		});

		handlers.put("MX.occurrenceInFinland", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setOccurrenceInFinland(objectname);
			}
		});

		handlers.put("MX.typeOfOccurrenceInFinland", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addTypeOfOccurrenceInFinland(objectname);
			}
		});

		handlers.put("MX.redListStatus2000Finland", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setRedListStatus2000Finland(objectname);
			}
		});

		handlers.put("MX.redListStatus2010Finland", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setRedListStatus2010Finland(objectname);
			}
		});

		handlers.put("MX.redListStatus2015Finland", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setRedListStatus2015Finland(objectname);
			}
		});

		handlers.put("MX.redListStatus2019Finland", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setRedListStatus2019Finland(objectname);
			}
		});

		handlers.put("MX.originalPublication", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addOriginalPublication(objectname);
			}
		});

		handlers.put("MX.originalDescription", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setOriginalDescription(objectname);
			}
		});

		handlers.put("MX.occurrenceInFinlandPublication", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addOccurrenceInFinlandPublication(objectname);
			}
		});

		handlers.put("MX.nameDecidedBy", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setNameDecidedBy(objectname);
			}
		});

		handlers.put("MX.nameDecidedDate", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setNameDecidedDate(toDate(resourceliteral));
			}
		});

		handlers.put("MX.notes", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setNotes(resourceliteral);
			}
		});

		handlers.put("MX.privateNotes", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setPrivateNotes(resourceliteral);
			}
		});

		handlers.put("MZ.createdAtTimestamp", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				try {
					taxon.setCreatedAtTimestamp(Integer.valueOf(resourceliteral));
				} catch (Exception e) {}
			}
		});

		handlers.put("MX.invasiveSpeciesCategory", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				try {
					taxon.setInvasiveSpeciesCategory(objectname);
				} catch (Exception e) {}
			}
		});

		handlers.put("MX.invasiveSpeciesEstablishment", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				try {
					taxon.setInvasiveSpeciesEstablishment(objectname);
				} catch (Exception e) {}
			}
		});

		handlers.put("MX.speciesCardAuthors", DESCRIPTIONS_HANDLER);
		handlers.put("MX.ingressText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.identificationText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.miscText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.distributionFinland", DESCRIPTIONS_HANDLER);
		handlers.put("MX.originAndDistributionText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionOrganismSize", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionStem", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionLeaf", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionFlower", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionFruitAndSeed", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionCone", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionRoot", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionThallus", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionFruitbody", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionSpore", DESCRIPTIONS_HANDLER);
		handlers.put("MX.algalPartnerOfLichen", DESCRIPTIONS_HANDLER);
		handlers.put("MX.reproduction", DESCRIPTIONS_HANDLER);
		handlers.put("MX.lifeCycle", DESCRIPTIONS_HANDLER);
		handlers.put("MX.reproductionFloweringTime", DESCRIPTIONS_HANDLER);
		handlers.put("MX.reproductionPollination", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionSporangiumAndAsexualReproduction", DESCRIPTIONS_HANDLER);
		handlers.put("MX.habitat", DESCRIPTIONS_HANDLER);
		handlers.put("MX.habitatSubstrate", DESCRIPTIONS_HANDLER);
		handlers.put("MX.ecology", DESCRIPTIONS_HANDLER);
		handlers.put("MX.behaviour", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionMicroscopicIdentification", DESCRIPTIONS_HANDLER);
		handlers.put("MX.growthFormAndGrowthHabit", DESCRIPTIONS_HANDLER);
		handlers.put("MX.management", DESCRIPTIONS_HANDLER);
		handlers.put("MX.invasiveEffectText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.invasivePreventionMethodsText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.invasiveCitizenActionsText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.invasiveSpeciesClassificationDescription", DESCRIPTIONS_HANDLER);
		handlers.put("MX.taxonomyText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.etymologyText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.cultivationText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.productionText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.economicUseText", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionToxicity", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionHostParasite", DESCRIPTIONS_HANDLER);
		handlers.put("MX.descriptionReferences", DESCRIPTIONS_HANDLER);

		handlers.put("HBE.invasiveSpeciesMainGroup", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addInvasiveSpeciesMainGroup(objectname);
			}
		});

		handlers.put("MX.isPartOfSet", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addTaxonSet(objectname);
			}
		});

		handlers.put("MX.customReportFormLink", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setCustomReportFormLink(toUri(resourceliteral, predicatename, taxon));
			}
		});

		handlers.put("MX.birdlifeCode", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setBirdlifeCode(resourceliteral);
			}
		});

		handlers.put("MX.euringCode", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setEuringCode(resourceliteral);
			}
		});

		handlers.put("MX.euringNumber", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setEuringNumber(toInteger(resourceliteral, predicatename, taxon));
			}
		});

		handlers.put("skos:exactMatch", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addTaxonConceptId(objectname);
			}
		});

		handlers.put("MX.additionalID", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.addAdditionalId(resourceliteral);
			}
		});

		handlers.put("MX.typeSpecimenURI",  new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setTypeSpecimenURI(toUri(resourceliteral, predicatename, taxon));
			}
		});

		handlers.put("MX.occurrenceInFinlandSpecimenURI",  new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setOccurrenceInFinlandSpecimenURI(toUri(resourceliteral, predicatename, taxon));
			}
		});

		handlers.put("MX.frequencyScoringPoints", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setFrequencyScoringPoints(toInteger(resourceliteral, predicatename, taxon));
			}
		});

		handlers.put("MX.autoNonWild", new TripletToTaxonHandler() {
			@Override
			public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon) {
				if (context != null) return;
				taxon.setAutoNonWild(booleanValue(resourceliteral));
			}
		});

		return handlers;
	}

	private void setToContent(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Content content) {
		if (content == null) return;
		if (given(resourceliteral)) {
			content.addText(context, predicatename, resourceliteral, locale);
		} else if (objectname != null) {
			content.addText(context, predicatename, objectname.toString(), null);
		}
	}

	private boolean given(String value) {
		return value != null && value.length() > 0;
	}

	private URI toUri(String value, Qname predicatename, Taxon taxon) {
		try {
			return new URI(value);
		} catch (Exception e) {
			new Exception("toUri failed for value " + value + " for predicate " + predicatename + " and taxon " + taxon.getQname(), e).printStackTrace();
			return null;
		}
	}

	private Integer toInteger(String value, Qname predicatename, Taxon taxon) {
		try {
			return Integer.valueOf(value);
		} catch (Exception e) {
			new Exception("toInteger failed for value " + value + " for predicate " + predicatename + " and taxon " + taxon.getQname(), e).printStackTrace();
			return null;
		}
	}


	private String toDate(String resourceliteral) {
		try {
			if (Utils.countNumberOf("-", resourceliteral) == 2) {
				return validIsoDate(resourceliteral);
			}
			if (Utils.countNumberOf(".", resourceliteral) == 2) {
				return DateUtils.format(DateUtils.convertToDate(resourceliteral), "yyyy-MM-dd");
			}
			return null;
		} catch (Exception e) {
			return null;
		}
	}

	private String validIsoDate(String resourceliteral) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setLenient(false);
		try {
			sdf.parse(resourceliteral);
			return resourceliteral;
		} catch (ParseException e) {
			return null;
		}
	}

	private boolean booleanValue(String resourceliteral) {
		return "true".equals(resourceliteral);
	}

}
