package fi.luomus.commons.taxonomy;

import fi.luomus.commons.containers.rdf.Qname;

public interface TripletToTaxonHandler {

	public void setToTaxon(Qname context, Qname predicatename, Qname objectname, String resourceliteral, String locale, Taxon taxon);
	
}
