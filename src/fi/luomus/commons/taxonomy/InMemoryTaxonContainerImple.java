package fi.luomus.commons.taxonomy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.HabitatOccurrenceCounts.HabitatOccurrenceCount;
import fi.luomus.commons.taxonomy.Occurrences.Occurrence;

public class InMemoryTaxonContainerImple implements TaxonContainer {

	private static final Qname OCCURRENCE_STATUS_NOT_EVALUATED = new Qname("MX.typeOfOccurrenceNotEvaluated");

	public static class InfiniteTaxonLoopException extends Exception {
		private static final long serialVersionUID = -8231088241367109953L;
		public InfiniteTaxonLoopException(Qname taxonId) {
			super(taxonId.toString());
		}
	}

	private final static TripletToTaxonHandlers tripletToTaxonHandlers = new TripletToTaxonHandlers();
	private final Map<Qname, Taxon> taxa = new HashMap<>(ESTIMATED_NUMBER_OF_TAXA);
	private final Map<Qname, Set<Qname>> childrenOfATaxon = new HashMap<>(ESTIMATED_NUMBER_OF_TAXA);
	private final Map<Qname, Qname> synonymParentOfASynonym = new HashMap<>();
	private Map<Qname, Set<Qname>> taxaByInformalGroup = null;
	private Map<Qname, Set<Qname>> taxaByRedListEvaluationGroup = null;
	private Map<Qname, Set<Qname>> taxaByAdminStatus = null;
	private Map<Qname, Set<Qname>> taxaByRedListStatus = null;
	private Map<Qname, Set<Qname>> taxaByTypeOfOccurrence = null;
	private Set<Qname> invasiveSpeciesFilter = null;
	private Set<Qname> invasiveSpeciesEarlyWarningFilter = null;
	private Set<Qname> hasMediaFilter = null;
	private Set<Qname> hasDescriptionsFilter = null;
	private Map<Qname, Set<Qname>> redListEvaluationGroupsOfInformalTaxonGroup = new HashMap<>(); // informal taxon group id -> evaluation group ids
	private Map<Qname, Set<Qname>> redListEvaluationGroupsOfTaxon = new HashMap<>(); // taxon id -> evaluation group ids
	private Integer latestLockedRedListEvaluationYear;

	private final InformalTaxonGroupContainer informalTaxonGroupContainer;
	private final InformalTaxonGroupContainer redListEvaluationGroupContainer;
	private final AdministrativeStatusContainer administrativeStatusContainer;

	public InMemoryTaxonContainerImple(
			InformalTaxonGroupContainer informalTaxonGroupContainer,
			InformalTaxonGroupContainer redListEvaluationGroupContainer,
			AdministrativeStatusContainer administrativeStatusContainer) {
		this.informalTaxonGroupContainer = informalTaxonGroupContainer;
		this.redListEvaluationGroupContainer = redListEvaluationGroupContainer;
		this.administrativeStatusContainer = administrativeStatusContainer;
	}

	public InMemoryTaxonContainerImple copy() {
		InMemoryTaxonContainerImple copy = new InMemoryTaxonContainerImple(informalTaxonGroupContainer, redListEvaluationGroupContainer, administrativeStatusContainer);
		copy.redListEvaluationGroupsOfInformalTaxonGroup = this.redListEvaluationGroupsOfInformalTaxonGroup;
		copy.redListEvaluationGroupsOfTaxon = this.redListEvaluationGroupsOfTaxon;
		return copy;
	}

	private final Filter informalGroupFilter = new Filter() {
		@Override
		public Set<Qname> getFilteredTaxa(Qname filterValue) {
			return getTaxaByInformalGroup(filterValue);
		}
	};

	private final Filter RedListEvaluationTaxonGroupFilter = new Filter() {
		@Override
		public Set<Qname> getFilteredTaxa(Qname filterValue) {
			return getTaxaByRedListEvaluationTaxonGroup(filterValue);
		}
	};

	private final Filter adminStatusFilter = new Filter() {
		@Override
		public Set<Qname> getFilteredTaxa(Qname filterValue) {
			return getTaxaByAdminStatus(filterValue);
		}
	};

	private final Filter redListStatusFilter = new Filter() {
		@Override
		public Set<Qname> getFilteredTaxa(Qname filterValue) {
			return getTaxaByRedListStatus(filterValue);
		}
	};

	private final Filter typeOfOccurrenceFilter = new Filter() {
		@Override
		public Set<Qname> getFilteredTaxa(Qname filterValue) {
			return getTaxaByTypeOfOccurrence(filterValue);
		}
	};

	@Override
	public Taxon getTaxon(Qname taxonId) throws NoSuchTaxonException {
		Taxon taxon = taxa.get(taxonId);
		if (taxon == null) {
			throw new NoSuchTaxonException(taxonId);
		}
		return taxon;
	}

	@Override
	public boolean hasTaxon(Qname taxonId) {
		return taxa.containsKey(taxonId);
	}

	private Set<Qname> alreadySet = new HashSet<>();
	private List<InfiniteTaxonLoopException> taxonLoopExceptions = new ArrayList<>();

	public List<InfiniteTaxonLoopException> generateTaxonomicOrders() {
		alreadySet = new HashSet<>();
		taxonLoopExceptions = new ArrayList<>();
		getRoots().forEach(t -> setOrderForRoot(t));
		return taxonLoopExceptions;
	}

	private void setOrderForRoot(Taxon root) {
		try {
			setTaxonomicOrder(root, 0);
		} catch (InfiniteTaxonLoopException e) {
			taxonLoopExceptions.add(e);
		}
	}

	private long setTaxonomicOrder(Taxon taxon, long order) throws InfiniteTaxonLoopException {
		if (alreadySet.contains(taxon.getId())) {
			throw new InfiniteTaxonLoopException(taxon.getId());
		}
		taxon.setTaxonomicOrder(order++);
		alreadySet.add(taxon.getId());
		for (Taxon child : taxon.getChildren()) {
			order = setTaxonomicOrder(child, order);
		}
		return order;
	}

	public List<InfiniteTaxonLoopException> generateInheritedOccurrencesAndHabitats() {
		alreadySet = new HashSet<>();
		taxonLoopExceptions = new ArrayList<>();
		getRoots().forEach(t -> setInheritedOccurrencesAndHabitatsCatchTaxonLoop(t));
		return taxonLoopExceptions;
	}

	private void setInheritedOccurrencesAndHabitatsCatchTaxonLoop(Taxon root) {
		try {
			setInheritedOccurrencesAndHabitats(root);
		} catch (InfiniteTaxonLoopException e) {
			taxonLoopExceptions.add(e);
		}
	}

	private void setInheritedOccurrencesAndHabitats(Taxon taxon) throws InfiniteTaxonLoopException {
		if (alreadySet.contains(taxon.getId())) {
			throw new InfiniteTaxonLoopException(taxon.getId());
		}
		alreadySet.add(taxon.getId());
		for (Taxon child : taxon.getChildren()) {
			setInheritedOccurrencesAndHabitats(child);
			setInheritedOccurrences(taxon, child);
			setInheritedHabitats(taxon, child);
		}
	}

	private void setInheritedHabitats(Taxon taxon, Taxon child) {
		if (!child.hasHabitatOccurrenceCounts()) return;
		for (HabitatOccurrenceCount childHabitatCount : child.getHabitatOccurrenceCounts()) {
			if (!taxon.getHabitatOccurrenceCounts().hasCount(childHabitatCount.getId())) {
				taxon.getHabitatOccurrenceCounts().setCount(childHabitatCount.copy());
				continue;
			}
			HabitatOccurrenceCount thisHabitatCount = taxon.getHabitatOccurrenceCounts().getCount(childHabitatCount.getId());
			thisHabitatCount.setOccurrenceCount(thisHabitatCount.getOccurrenceCount() + childHabitatCount.getOccurrenceCount());
		}
	}

	private void setInheritedOccurrences(Taxon taxon, Taxon child) {
		if (!child.hasOccurrences()) return;
		for (Occurrence childOccurrence : child.getOccurrences()) {
			if (childOccurrence.getStatus().equals(OCCURRENCE_STATUS_NOT_EVALUATED)) continue;
			Occurrence thisOccurrence = taxon.getOccurrences().getOccurrence(childOccurrence.getArea());
			if (thisOccurrence == null) {
				taxon.getOccurrences().setOccurrence(childOccurrence.copy());
				continue;
			}
			int currentCount = thisOccurrence.getOccurrenceCount() == null ? 0 : thisOccurrence.getOccurrenceCount();
			int childCount = childOccurrence.getOccurrenceCount() == null ? 0 : childOccurrence.getOccurrenceCount();
			int total = currentCount + childCount;
			if (total > 0) {
				thisOccurrence.setOccurrenceCount(total);
			}
			if (shouldTakeChildStatus(childOccurrence, thisOccurrence)) {
				thisOccurrence.setStatus(childOccurrence.getStatus());
				thisOccurrence.setYear(childOccurrence.getYear());
				thisOccurrence.setSpecimenURI(childOccurrence.getSpecimenURI());
			}
		}
	}

	private boolean shouldTakeChildStatus(Occurrence childOccurrence, Occurrence thisOccurrence) {
		if (childHasHigherStatus(childOccurrence, thisOccurrence)) return true;
		if (!haveSameStatus(childOccurrence, thisOccurrence)) return false;
		if (childHasMoreInfo(childOccurrence, thisOccurrence)) return true;
		return childHasHigherYear(childOccurrence, thisOccurrence);
	}

	private boolean childHasMoreInfo(Occurrence childOccurrence, Occurrence thisOccurrence) {
		int childC = getInfoCount(childOccurrence);
		int thisC = getInfoCount(thisOccurrence);
		return childC > thisC;
	}

	private int getInfoCount(Occurrence o) {
		int count = 0;
		if (o.getSpecimenURI() != null) count++;
		if (o.getYear() != null) count++;
		return count;
	}

	private boolean haveSameStatus(Occurrence childOccurrence, Occurrence thisOccurrence) {
		return childOccurrence.getStatus().equals(thisOccurrence.getStatus());
	}

	private boolean childHasHigherYear(Occurrence childOccurrence, Occurrence thisOccurrence) {
		int childY = childOccurrence.getYear() == null ? 0 : childOccurrence.getYear();
		int thisY = thisOccurrence.getYear() == null ? 0 : thisOccurrence.getYear();
		return childY > thisY;
	}

	private boolean childHasHigherStatus(Occurrence childOccurrence, Occurrence thisOccurrence) {
		int childRank = getStatusRank(childOccurrence);
		int thisRank = getStatusRank(thisOccurrence);
		return childRank > thisRank;
	}

	private static final Map<Qname, Integer> STATUS_RANKS;
	static {
		STATUS_RANKS = new HashMap<>();
		// >0: values that mean it strongly exists
		STATUS_RANKS.put(new Qname("MX.typeOfOccurrenceStablePopulation"), 10);
		STATUS_RANKS.put(new Qname("MX.typeOfOccurrenceOccurs"), 9);
		STATUS_RANKS.put(new Qname("MX.typeOfOccurrenceSpontaneousOldResident"), 8);
		STATUS_RANKS.put(new Qname("MX.typeOfOccurrenceSpontaneousNewResident"), 7);
		STATUS_RANKS.put(new Qname("MX.typeOfOccurrenceAlienOldResident"), 6);
		// 0: other values that mean it exists
		// -1: only marked to exist based on occurrence data
		STATUS_RANKS.put(new Qname("MX.typeOfOccurrenceOccursBasedOnOccurrences"), -1);
		// <-1: values that mean it does not exists
		STATUS_RANKS.put(new Qname("MX.typeOfOccurrenceExtirpated"), -10);
		STATUS_RANKS.put(new Qname("MX.typeOfOccurrenceAlienNewEphemeralOnlyold"), -11);
		STATUS_RANKS.put(new Qname("MX.typeOfOccurrenceSpontaneousOldFormerlyResidentExtinct"), -12);
		STATUS_RANKS.put(new Qname("MX.doesNotOccur"), -13);
		// the most useless value

		STATUS_RANKS.put(OCCURRENCE_STATUS_NOT_EVALUATED, -100);
	}

	private int getStatusRank(Occurrence childOccurrence) {
		Integer rank = STATUS_RANKS.get(childOccurrence.getStatus());
		if (rank == null) return 0;
		return rank;
	}

	private Stream<Taxon> getRoots() {
		return taxa.values().stream().filter(t -> isRoot(t));
	}

	private boolean isRoot(Taxon t) {
		return t.getChecklist() != null && t.hasChildren() && !t.hasParent();
	}

	public void handle(Qname taxonId, Qname predicate, Qname object, String resourceliteral, String locale, Qname context) {
		if (!this.hasTaxon(taxonId)) {
			this.addTaxon(taxonId);
		}
		Taxon taxon = this.getTaxon(taxonId);
		if (IS_PART_OF.equals(predicate)) {
			this.addChild(object, taxonId);
		}
		if (SYNONYM_PREDICATES.contains(predicate)) {
			this.addSynonymParent(object, taxonId);
		}
		TripletToTaxonHandler handler = tripletToTaxonHandlers.getHandler(predicate);
		handler.setToTaxon(context, predicate, object, resourceliteral, locale, taxon);
	}

	private void addTaxon(Qname taxonId) {
		taxa.put(taxonId, new Taxon(taxonId, this));
	}

	public void addTaxon(Taxon taxon) {
		if (this.hasTaxon(taxon.getQname())) throw new IllegalStateException("Already has taxon " + taxon.getQname());
		taxa.put(taxon.getQname(), taxon);
		if (taxon.getParentQname() != null) {
			this.addChild(taxon.getParentQname(), taxon.getQname());
		}
		if (taxon.getSynonymsContainer() != null) {
			for (Qname synonymId : taxon.getSynonymsContainer().getAll()) {
				this.addSynonymParent(synonymId, taxon.getQname());
			}
		}
	}

	private void addChild(Qname parentId, Qname childId) {
		if (!childrenOfATaxon.containsKey(parentId)) {
			childrenOfATaxon.put(parentId, new HashSet<Qname>(3));
		}
		childrenOfATaxon.get(parentId).add(childId);
	}

	@Override
	public Set<Qname> getChildren(Qname parentId) {
		if (!childrenOfATaxon.containsKey(parentId)) return Collections.emptySet();
		return Collections.unmodifiableSet(childrenOfATaxon.get(parentId));
	}

	private void addSynonymParent(Qname synonymId, Qname synonymParentId) {
		synonymParentOfASynonym.put(synonymId, synonymParentId);
	}

	@Override
	public Qname getSynonymParent(Qname synonymId) {
		return synonymParentOfASynonym.get(synonymId);
	}

	public Set<Qname> getTaxaByInformalGroup(Qname informalGroup) {
		if (taxaByInformalGroup == null) {
			taxaByInformalGroup = initInformalGroups();
		}
		if (!taxaByInformalGroup.containsKey(informalGroup)) return Collections.emptySet();
		return taxaByInformalGroup.get(informalGroup);
	}

	private synchronized Map<Qname, Set<Qname>> initInformalGroups() {
		if (taxaByInformalGroup != null) return taxaByInformalGroup;
		System.out.println("Initializing informal group filter...");
		Map<Qname, Set<Qname>> filters = new HashMap<>();
		for (Taxon taxon : taxa.values()) {
			for (Qname informalGroup : taxon.getInformalTaxonGroupsNoOrder()) {
				if (!filters.containsKey(informalGroup)) {
					filters.put(informalGroup, new HashSet<Qname>());
				}
				filters.get(informalGroup).add(taxon.getQname());
			}
		}
		System.out.println("Informal group filter initialized.");
		return filters;
	}

	public Set<Qname> getTaxaByRedListEvaluationTaxonGroup(Qname informalGroup) {
		if (taxaByRedListEvaluationGroup == null) {
			taxaByRedListEvaluationGroup = initEvaluationGroups();
		}
		if (!taxaByRedListEvaluationGroup.containsKey(informalGroup)) return Collections.emptySet();
		return taxaByRedListEvaluationGroup.get(informalGroup);
	}

	private synchronized Map<Qname, Set<Qname>> initEvaluationGroups() {
		if (taxaByRedListEvaluationGroup != null) return taxaByRedListEvaluationGroup;
		System.out.println("Initializing Red List Evaluation group filter...");
		Map<Qname, Set<Qname>> filters = new HashMap<>();
		for (Taxon taxon : taxa.values()) {
			for (Qname informalGroup : taxon.getRedListEvaluationGroupsNoOrder()) {
				if (!filters.containsKey(informalGroup)) {
					filters.put(informalGroup, new HashSet<Qname>());
				}
				filters.get(informalGroup).add(taxon.getQname());
			}
		}
		System.out.println("Red List Evaluation group filter initialized.");
		return filters;
	}

	public Set<Qname> getTaxaByAdminStatus(Qname adminStatus) {
		if (taxaByAdminStatus == null) {
			taxaByAdminStatus = initAdminStatuses();
		}
		if (!taxaByAdminStatus.containsKey(adminStatus)) return Collections.emptySet();
		return taxaByAdminStatus.get(adminStatus);
	}

	private synchronized Map<Qname, Set<Qname>> initAdminStatuses() {
		if (taxaByAdminStatus != null) return taxaByAdminStatus;
		System.out.println("Initializing admin status filter...");
		Map<Qname, Set<Qname>> filters = new HashMap<>();
		for (Taxon taxon : taxa.values()) {
			for (Qname adminStatus : taxon.getAdministrativeStatuses()) {
				if (!filters.containsKey(adminStatus)) {
					filters.put(adminStatus, new HashSet<Qname>());
				}
				filters.get(adminStatus).add(taxon.getQname());
			}
		}
		System.out.println("Admin status filter initialized.");
		return filters;
	}

	public Set<Qname> getTaxaByRedListStatus(Qname redListStatus) {
		if (taxaByRedListStatus == null) {
			taxaByRedListStatus = initRedListStatuses();
		}
		if (!taxaByRedListStatus.containsKey(redListStatus)) return Collections.emptySet();
		return taxaByRedListStatus.get(redListStatus);
	}

	private synchronized Map<Qname, Set<Qname>> initRedListStatuses() {
		if (taxaByRedListStatus != null) return taxaByRedListStatus;
		System.out.println("Initializing red list status filter...");
		Map<Qname, Set<Qname>> filters = new HashMap<>();
		for (Taxon taxon : taxa.values()) {
			RedListStatus status = taxon.getLatestRedListStatusFinland();
			if (status == null) continue;
			if (!filters.containsKey(status.getStatus())) {
				filters.put(status.getStatus(), new HashSet<Qname>());
			}
			filters.get(status.getStatus()).add(taxon.getQname());
		}
		System.out.println("Red list status filter intialized.");
		return filters;
	}

	public Set<Qname> getTaxaByTypeOfOccurrence(Qname typeOfOccurrence) {
		if (taxaByTypeOfOccurrence == null) {
			taxaByTypeOfOccurrence = initTypesOfOccurrence();
		}
		if (!taxaByTypeOfOccurrence.containsKey(typeOfOccurrence)) return Collections.emptySet();
		return taxaByTypeOfOccurrence.get(typeOfOccurrence);
	}

	private synchronized Map<Qname, Set<Qname>> initTypesOfOccurrence() {
		if (taxaByTypeOfOccurrence != null) return taxaByTypeOfOccurrence;
		System.out.println("Initializing type of occurrence filter...");
		Map<Qname, Set<Qname>> filters = new HashMap<>();
		for (Taxon taxon : taxa.values()) {
			for (Qname typeOfOccurrence : taxon.getTypesOfOccurrenceInFinland()) {
				if (!filters.containsKey(typeOfOccurrence)) {
					filters.put(typeOfOccurrence, new HashSet<Qname>());
				}
				filters.get(typeOfOccurrence).add(taxon.getQname());
			}
		}
		System.out.println("Type of occurrence filter intialized.");
		return filters;
	}

	@Override
	public Set<Qname> getInvasiveSpeciesFilter() {
		if (invasiveSpeciesFilter == null) {
			invasiveSpeciesFilter = initInvasiveSpeciesFilter();
		}
		return invasiveSpeciesFilter;
	}

	private synchronized Set<Qname> initInvasiveSpeciesFilter() {
		if (invasiveSpeciesFilter != null) return invasiveSpeciesFilter;
		System.out.println("Initializing invasive species filter...");
		Set<Qname> filter = new HashSet<>(300);
		for (Taxon taxon : taxa.values()) {
			if (taxon.isInvasiveSpecies()) {
				filter.add(taxon.getQname());
			}
		}
		System.out.println("Invasive species filter initialized.");
		return filter;
	}

	@Override
	public Set<Qname> getInvasiveSpeciesEarlyWarningFilter() {
		if (invasiveSpeciesEarlyWarningFilter == null) {
			invasiveSpeciesEarlyWarningFilter = initInvasiveSpeciesEarlyWarningFilter();
		}
		return invasiveSpeciesEarlyWarningFilter;
	}

	private synchronized Set<Qname> initInvasiveSpeciesEarlyWarningFilter() {
		if (invasiveSpeciesEarlyWarningFilter != null) return invasiveSpeciesEarlyWarningFilter;
		System.out.println("Initializing invasive species early warning filter...");
		Set<Qname> filter = new HashSet<>(30);
		for (Qname taxonId : getInvasiveSpeciesFilter()) {
			Taxon taxon = getTaxon(taxonId);
			if (taxon.isInvasiveSpeciesEarlyWarning()) {
				filter.add(taxon.getQname());
			}
		}
		System.out.println("Invasive species early warning filter initialized.");
		return filter;
	}

	@Override
	public Set<Qname> getHasMediaFilter() {
		if (hasMediaFilter == null) {
			throw new UnsupportedOperationException("Has media filter not implemented");
		}
		return hasMediaFilter;
	}

	public void setHasMediaFilter(Set<Qname> hasMedia) {
		this.hasMediaFilter = hasMedia;
	}

	@Override
	public Set<Qname> getHasDescriptionsFilter() {
		if (hasDescriptionsFilter == null) {
			hasDescriptionsFilter = initHasDescriptionsFilter();
		}
		return hasDescriptionsFilter;
	}

	private synchronized Set<Qname> initHasDescriptionsFilter() {
		if (hasDescriptionsFilter != null) return hasDescriptionsFilter;
		System.out.println("Initializing has descriptions filter...");
		Set<Qname> filter = new HashSet<>(10000);
		for (Taxon taxon : taxa.values()) {
			if (!taxon.getDescriptions().isEmpty()) {
				filter.add(taxon.getQname());
			}
		}
		System.out.println("Has descriptions filter initialized.");
		return filter;
	}

	@Override
	public Filter getInformalGroupFilter() {
		return informalGroupFilter;
	}

	@Override
	public Filter getRedListEvaluationGroupFilter() {
		return RedListEvaluationTaxonGroupFilter;
	}

	@Override
	public Filter getAdminStatusFilter() {
		return adminStatusFilter;
	}

	@Override
	public Filter getRedListStatusFilter() {
		return redListStatusFilter;
	}

	@Override
	public Filter getTypesOfOccurrenceFilter() {
		return typeOfOccurrenceFilter;
	}

	@Override
	public int getNumberOfTaxa() throws UnsupportedOperationException {
		return taxa.size();
	}

	public void addRedListEvaluationGroupOfInformalTaxonGroup(Qname evaluationGroupId, Qname informalTaxonGroupId) {
		if (!redListEvaluationGroupsOfInformalTaxonGroup.containsKey(informalTaxonGroupId)) {
			redListEvaluationGroupsOfInformalTaxonGroup.put(informalTaxonGroupId, new HashSet<Qname>());
		}
		redListEvaluationGroupsOfInformalTaxonGroup.get(informalTaxonGroupId).add(evaluationGroupId);
	}

	public void addRedListEvaluationGroupOfTaxon(Qname evaluationGroupId, Qname taxonId) {
		if (!redListEvaluationGroupsOfTaxon.containsKey(taxonId)) {
			redListEvaluationGroupsOfTaxon.put(taxonId, new HashSet<Qname>());
		}
		redListEvaluationGroupsOfTaxon.get(taxonId).add(evaluationGroupId);
	}

	@Override
	public Set<Qname> getRedListEvaluationGroupsOfInformalTaxonGroup(Qname informalTaxonGroupId) {
		Set<Qname> set = redListEvaluationGroupsOfInformalTaxonGroup.get(informalTaxonGroupId);
		if (set == null) return Collections.emptySet();
		return set;
	}

	@Override
	public Set<Qname> getRedListEvaluationGroupsOfTaxon(Qname taxonId) {
		Set<Qname> set = redListEvaluationGroupsOfTaxon.get(taxonId);
		if (set == null) return Collections.emptySet();
		return set;
	}

	@Override
	public Set<Qname> orderInformalTaxonGroups(Set<Qname> informalTaxonGroupIds) {
		return informalTaxonGroupContainer.orderInformalTaxonGroups(informalTaxonGroupIds);
	}

	@Override
	public Set<Qname> orderRedListEvaluationGroups(Set<Qname> groups) {
		return redListEvaluationGroupContainer.orderInformalTaxonGroups(groups);
	}

	@Override
	public Set<Qname> orderAdministrativeStatuses(Set<Qname> administrativeStatuses) {
		return administrativeStatusContainer.orderAdministrativeStatuses(administrativeStatuses);
	}

	@Override
	public Set<Qname> getParentInformalTaxonGroups(Qname groupId) {
		return informalTaxonGroupContainer.getParents(groupId);
	}

	@Override
	public Set<Qname> getParentRedListEvaluationGroups(Qname groupId) {
		return redListEvaluationGroupContainer.getParents(groupId);
	}

	@Override
	public Collection<Taxon> getAll() {
		return Collections.unmodifiableCollection(taxa.values());
	}

	@Override
	public int getLatestLockedRedListEvaluationYear() throws UnsupportedOperationException {
		if (latestLockedRedListEvaluationYear == null) throw new UnsupportedOperationException();
		return latestLockedRedListEvaluationYear.intValue();
	}

	public void setLatestLockedRedListEvaluationYear(int year) {
		this.latestLockedRedListEvaluationYear = year;
	}

	@Override
	public LocalizedText getInformalTaxonGroupNames(Set<Qname> informalGroups) {
		return informalTaxonGroupContainer.getInformalTaxonGroupNames(informalGroups);
	}

	@Override
	public LocalizedText getAdministrativeStatusNames(Set<Qname> administrativeStatuses) {
		return administrativeStatusContainer.getIAdministrativeStatusNames(administrativeStatuses);
	}

}
