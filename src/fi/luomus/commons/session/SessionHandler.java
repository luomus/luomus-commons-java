package fi.luomus.commons.session;

/**
 * Wraps a HttpSession
 */
public interface SessionHandler {
	
	public void put(String key, String value);
	
	public String get(String key);
	
	public void remove(String key);
	
	public String language();
	
	public void setLanguage(String language);
	
	public boolean isAuthenticatedFor(String system);
	
	public void authenticateFor(String system);
	
	public void removeAuthentication(String system);
	
	public String userId();
	
	public void setUserId(String userid);
	
	public String userName();
	
	public void setUserName(String username);
	
	public String userType();
	
	public void setUserType(String usertype);
	
	public void setFlash(String message);
	
	public String getFlash();
	
	public void setFlashError(String message);
	
	public String getFlashError();
	
	public void setFlashSuccess(String message);
	
	public String getFlashSuccess();
		
	public Object getObject(String key);
	
	public void setObject(String key, Object value);
	
	public boolean hasSession();

	public void setTimeout(int minutes);

	public void invalidate();
	
}
