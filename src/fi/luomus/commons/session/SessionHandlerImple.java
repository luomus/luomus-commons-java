package fi.luomus.commons.session;

import java.util.Enumeration;

import javax.servlet.http.HttpSession;

public class SessionHandlerImple implements SessionHandler {

	private static final String	LANGUAGE		= "language";
	private static final String	USER_ID			= "user_id";
	private static final String	USER_NAME		= "user_name";
	private static final String	USER_TYPE		= "user_type";
	private static final String	AUTHENTICATIONS	= "authentications_K6kcxcT8m_";
	private static final String FLASH_MESSAGE   = "flash_message";
	private static final String FLASH_MESSAGE_ERROR   = "flash_message_error";
	private static final String FLASH_MESSAGE_SUCCESS   = "flash_message_success";

	private final HttpSession	session;
	private final String		applicationID;

	public SessionHandlerImple(HttpSession session, String forApplication) {
		this.session = session;
		this.applicationID = forApplication;
	}

	@Override
	public void invalidate() {
		if (session == null) return;
		session.invalidate();
	}

	@Override
	public void put(String key, String value) throws IllegalStateException {
		if (session == null) throw new IllegalStateException("Session is null");
		session.setAttribute(applicationID + key, value);
	}

	@Override
	public String get(String key) throws IllegalStateException {
		if (session == null) throw new IllegalStateException("Session is null");
		return (String) session.getAttribute(applicationID + key);
	}

	@Override
	public void remove(String key) throws IllegalStateException {
		if (session == null) throw new IllegalStateException("Session is null");
		session.removeAttribute(applicationID + key);
	}

	@Override
	public String language() {
		if (session == null) throw new IllegalStateException("Session is null");
		return (String) session.getAttribute(applicationID + LANGUAGE);
	}

	@Override
	public void setLanguage(String language) {
		if (session == null) throw new IllegalStateException("Session is null");
		session.setAttribute(applicationID + LANGUAGE, language);
	}

	@Override
	public boolean isAuthenticatedFor(String system) {
		if (session == null) return false;
		Boolean authenticated = (Boolean) session.getAttribute(applicationID + AUTHENTICATIONS + system);
		if (authenticated == null) return false;
		return authenticated;
	}

	@Override
	public void authenticateFor(String system) {
		if (session == null) throw new IllegalStateException("Session is null");
		session.setAttribute(applicationID + AUTHENTICATIONS + system, Boolean.TRUE);
	}

	@Override
	public void removeAuthentication(String system) {
		if (session != null) {
			session.setAttribute(applicationID + AUTHENTICATIONS + system, Boolean.FALSE);
		}
	}

	@Override
	public String userId() {
		if (session == null) throw new IllegalStateException("Session is null");
		return (String) session.getAttribute(applicationID + USER_ID);
	}

	@Override
	public void setUserId(String userid) {
		if (session == null) throw new IllegalStateException("Session is null");
		session.setAttribute(applicationID + USER_ID, userid);
	}

	@Override
	public String userName() {
		if (session == null) throw new IllegalStateException("Session is null");
		return (String) session.getAttribute(applicationID + USER_NAME);
	}

	@Override
	public void setUserName(String username) {
		if (session == null) throw new IllegalStateException("Session is null");
		session.setAttribute(applicationID + USER_NAME, username);
	}

	@Override
	public String userType() {
		if (session == null) throw new IllegalStateException("Session is null");
		return (String) session.getAttribute(applicationID + USER_TYPE);
	}

	@Override
	public void setUserType(String usertype) {
		if (session == null) throw new IllegalStateException("Session is null");
		session.setAttribute(applicationID + USER_TYPE, usertype);
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		if (session != null) {
			@SuppressWarnings("rawtypes")
			Enumeration e = session.getAttributeNames();
			while (e.hasMoreElements()) {
				String s = (String) e.nextElement();
				b.append(" ").append(s).append(":").append(session.getAttribute(s)).append(" ");
			}
		}
		return "Session for " + applicationID + " contains values: " + b.toString();
	}

	@Override
	public void setFlash(String message) {
		setFlash(FLASH_MESSAGE, message);

	}

	private void setFlash(String flashType, String message) {
		session.setAttribute(applicationID + flashType, message);
	}

	@Override
	public String getFlash() {
		return getFlash(FLASH_MESSAGE);
	}

	private String getFlash(String flashType) {
		String message = (String) session.getAttribute(applicationID + flashType);
		setFlash(flashType, "");
		return message;
	}

	@Override
	public Object getObject(String key) {
		return session.getAttribute(applicationID + key);
	}

	@Override
	public void setObject(String key, Object value) {
		session.setAttribute(applicationID + key, value);
	}

	@Override
	public boolean hasSession() {
		return session != null;
	}

	@Override
	public void setFlashError(String message) {
		setFlash(FLASH_MESSAGE_ERROR, message);
	}

	@Override
	public String getFlashError() {
		return getFlash(FLASH_MESSAGE_ERROR);
	}

	@Override
	public void setFlashSuccess(String message) {
		setFlash(FLASH_MESSAGE_SUCCESS, message);
	}

	@Override
	public String getFlashSuccess() {
		return getFlash(FLASH_MESSAGE_SUCCESS);
	}

	@Override
	public void setTimeout(int minutes) {
		if (session == null) throw new IllegalStateException("Session is null");
		session.setMaxInactiveInterval(minutes*60);
	}
}
