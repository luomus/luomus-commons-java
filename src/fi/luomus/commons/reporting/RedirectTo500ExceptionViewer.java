package fi.luomus.commons.reporting;

import fi.luomus.commons.http.HttpStatus;

public class RedirectTo500ExceptionViewer implements ExceptionViewer {
	
	private final HttpStatus redirecter;
	
	public RedirectTo500ExceptionViewer(HttpStatus redirecter) {
		this.redirecter = redirecter;
	}
	
	@Override
	public void view(Throwable condition) {
		redirecter.status500();
	}

}
