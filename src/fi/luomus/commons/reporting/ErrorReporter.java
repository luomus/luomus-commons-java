package fi.luomus.commons.reporting;

public interface ErrorReporter {
	
	public void report(String message, Throwable condition);
	
	public void report(Throwable condition);
	
	public void report(String message);
	
}
