package fi.luomus.commons.reporting;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.mail.internet.AddressException;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.LogUtils;

public class ErrorReporterViaEmailAndToSystemErr extends ErrorReporterViaEmail {

	public ErrorReporterViaEmailAndToSystemErr(Config config) throws IllegalArgumentException, AddressException {
		super(config);
	}

	@Override
	public void report(String message) {
		System.err.println(LogUtils.shorten(message, 5000));
		super.report(message);
	}

	@Override
	public void report(Throwable condition) {
		StringWriter s = new StringWriter();
		condition.printStackTrace(new PrintWriter(s));
		System.err.println(LogUtils.shorten(s.toString(), 5000));
		super.report(condition);
	}

	@Override
	public void report(String message, Throwable condition) {
		StringWriter s = new StringWriter();
		s.append(message).append("\n");
		condition.printStackTrace(new PrintWriter(s));
		System.err.println(LogUtils.shorten(s.toString(), 5000));
		super.report(message, condition);
	}

}
