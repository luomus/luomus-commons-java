package fi.luomus.commons.reporting;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import fi.luomus.commons.http.HttpStatus;

public class FunnyOwlExceptionViewer implements ExceptionViewer {
	
	private final PrintWriter out;
	private final String staticURL;
	private final HttpServletResponse res;
	
	public FunnyOwlExceptionViewer(String staticURL, PrintWriter out, HttpServletResponse res) {
		this.out = out;
		this.staticURL = staticURL;
		this.res = res;
	}
	
	@Override
	public void view(Throwable condition) {
		out.print("<img src=\""+staticURL+"/FunnyOwlExceptionViewer.jpg\" alt=\"Jokin meni pieleen\">");
		out.print("<br />");
		out.print("Pahoittelemme tapahtunutta! Tästä lähti ilmoitus ylläpidolle. Tutkimme asiaa.");
		out.print("<br /><pre>");
		condition.printStackTrace(out);
		out.print("</pre>");
		out.flush();
		new HttpStatus(res).status500();
	}
	
}
