package fi.luomus.commons.reporting;


/**
 * Views the exception to the user in some 'friendly' or desired manner
 */
public interface ExceptionViewer {
	
	/**
	 * Views the exception to the user in some 'friendly' or desired manner
	 * @param condition
	 */
	public void view(Throwable condition);
	
}
