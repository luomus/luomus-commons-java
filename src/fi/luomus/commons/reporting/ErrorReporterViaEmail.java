package fi.luomus.commons.reporting;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.EmailUtil;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.Utils;

/**
 * Will send errors and messages via email using the host, username, password, to, from and subject parameters that are given
 * in the constructor.
 */
public class ErrorReporterViaEmail implements ErrorReporter {

	private final Session		mailSession;
	private final Address[]		to;
	private final String		from;
	private final String		subject;
	private final MessageRecord messageRecord = new MessageRecord();

	/**
	 * Reports errors via email using SMTP server
	 * @param host
	 * @param username
	 * @param password
	 * @param to
	 * @param from
	 * @param subject
	 * @throws AddressException
	 * @thows IllegalArgumentException if one of the parameters is not defined by the config
	 */
	public ErrorReporterViaEmail(Config config) throws IllegalArgumentException, AddressException {
		this(config.get(Config.ERROR_REPORTING_SMTP_HOST), config.get(Config.ERROR_REPORTING_SMTP_USERNAME), config.get(Config.ERROR_REPORTING_SMTP_PASSWORD), parseList(config), config.get(Config.ERROR_REPORTING_SMTP_SEND_FROM), config.get(Config.ERROR_REPORTING_SMTP_SUBJECT));
	}

	private static List<String> parseList(Config config) {
		List<String> addresses = new ArrayList<>();
		for (String address : config.get(Config.ERROR_REPORTING_SMTP_SEND_TO).split(",")) {
			address = address.trim();
			if (address.length() <0) continue;
			addresses.add(address);
		}
		return addresses;
	}

	public ErrorReporterViaEmail(String host, String username, String password, String to, String from, String subject) throws AddressException {
		this(host, username, password, Utils.list(to), from, subject);
	}

	/**
	 * Reports errors via email using SMTP server
	 * @param host
	 * @param username
	 * @param password
	 * @param to
	 * @param from
	 * @param subject
	 * @throws AddressException
	 */
	public ErrorReporterViaEmail(String host, String username, String password, List<String> to, String from, String subject) throws AddressException {
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", host);

		Authenticator authenticator = null;
		if (given(username)) {
			authenticator = new MyAuthenticator(username, password);
			props.put("mail.smtp.user", username);
			props.put("mail.smtp.auth", "true");
		}

		this.mailSession = Session.getDefaultInstance(props, authenticator);
		this.to = EmailUtil.toAddresses(to);
		this.from = from;
		this.subject = subject;
	}

	private boolean given(String string) {
		return string != null && string.length() > 0;
	}

	@Override
	public void report(Throwable condition) {
		String stackTrace = LogUtils.buildStackTrace(condition);
		this.report(stackTrace);
	}

	@Override
	public void report(String message, Throwable condition) {
		String stackTrace = LogUtils.buildStackTrace(condition);
		this.report(message + ":\n" + stackTrace);
	}

	@Override
	public void report(String message) {
		message = LogUtils.shorten(message, 20000);
		try {
			send(message);
		} catch (Exception reportingException) {
			System.err.println("Error sending message. Original message was: \n ========== ");
			System.err.println(message);
			System.err.println(" ======= ");
			reportingException.printStackTrace();
		}
	}

	private void send(String errorMessage) throws MessagingException, AddressException {
		if (messageRecord.shouldSend(errorMessage)) {
			MimeMessage message = new MimeMessage(mailSession);
			message.setRecipients(Message.RecipientType.TO, to);
			message.setFrom(new InternetAddress(from));
			message.setSubject(subject);
			message.setContent(errorMessage, "text/plain; charset=ISO-8859-1");
			Transport.send(message);
			messageRecord.record(errorMessage);
		}
	}

	private class MyAuthenticator extends Authenticator {
		private final String	username;
		private final String	password;

		public MyAuthenticator(String username, String password) {
			this.username = username;
			this.password = password;
		}

		@Override
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	}

	public static class MessageRecord {

		private static final int DEFAULT_THRESHOLD_IN_HOURS_HOW_OFTEN_SAME_MESSAGE_IS_SEND	= 7;
		private static final int DEFAULT_MAX_NUMBER_OF_EMAILS_IN_HOUR_TO_SEND = 3;
		private final Cache<String, Boolean> sentMessages;
		private final Cache<String, Integer> numberOfEmailsInHour;
		private final int maxNumberOfEmailsInHourToSend;

		public MessageRecord() {
			this(DEFAULT_THRESHOLD_IN_HOURS_HOW_OFTEN_SAME_MESSAGE_IS_SEND, DEFAULT_MAX_NUMBER_OF_EMAILS_IN_HOUR_TO_SEND);
		}

		public MessageRecord(int howOftenSameMessageIsSendInHours, int maxNumberOfEmailsInHourToSend) {
			this.maxNumberOfEmailsInHourToSend = maxNumberOfEmailsInHourToSend;
			this.sentMessages = CacheBuilder.newBuilder().expireAfterWrite(howOftenSameMessageIsSendInHours, TimeUnit.HOURS).build();
			this.numberOfEmailsInHour = CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.HOURS).build();
		}

		public void record(String message) {
			String truncated = truncateMessage(message);
			if (sentMessages.getIfPresent(truncated) == null) {
				sentMessages.put(truncated, true);
			}
			increaseNumberOfMessagesSendThisHourByOne();
		}

		private String truncateMessage(String message) {
			try {
				if (message == null) return "";
				message = Utils.removeWhitespace(message);
				if (message.contains(":")) {
					return message.substring(0, message.indexOf(":") - 1);
				}
				if (message.length() < 30) {
					return message;
				}
				return message.substring(0, 29);
			} catch (Exception e) {
				return message;
			}
		}

		private void increaseNumberOfMessagesSendThisHourByOne() {
			numberOfEmailsInHour.put(thisHour(), numberThisHour() + 1);
		}

		private String thisHour() {
			return DateUtils.getCurrentDateTime("yyyyMMddHH");
		}

		private Integer numberThisHour() {
			Integer numberThisHour = numberOfEmailsInHour.getIfPresent(thisHour());
			if (numberThisHour == null) return 0;
			return numberThisHour;
		}

		public boolean shouldSend(String message) {
			return notSentWithinThreshold(message) && numberThisHour() < maxNumberOfEmailsInHourToSend;
		}

		private boolean notSentWithinThreshold(String message) {
			return sentMessages.getIfPresent(truncateMessage(message)) == null;
		}

	}

}
