package fi.luomus.commons.reporting;

public class ErrorReportingToSystemErr implements ErrorReporter {
	
	@Override
	public void report(String message) {
		System.err.println(message);
	}
	
	@Override
	public void report(Throwable condition) {
		condition.printStackTrace();
	}

	@Override
	public void report(String message, Throwable condition) {
		System.err.println(message);
		condition.printStackTrace();
	}
	
}
