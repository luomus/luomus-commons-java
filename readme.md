luomus-commons.jar
==================

1. Checkout
2. Export | jar, include src and external folders  (and tests folder if you want to use Stubs and other test classes)
3. Add jar to your project
4. Add dependencies from this project's /lib -folder to your project
   * commons-codec-1.6.jar
   * commons-csv-1.6.jar
   * commons-io-2.3.jar
   * commons-logging-1.1.3.jar
   * coordinate-converter.jar
   * guava-17.0.jar
   * jsoup-1.10.2.jar

The following dependencies are needed only if you need the bellow specified functionality:

   * freemarker.jar - Using FreemarkerTemplateViewer
   * http*.jar + fluent-hc-4.5.14.jar - Using HttpClientService
   * iText.jar - Using PDF-tools
   * mail.jar - Using email util
   * caffeine-2.8.0.jar - Using caches
   * jena-3.13.1.jar - Using RDF functionalities
   * HikariCP-4.0.3.jar - Using Taxon Search SQL Query Imple
    
 TODO: Update this to a Maven project
 