package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import fi.luomus.commons.utils.DateParserUtil;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.Utils;

public class DateParserUtilTests extends DateParserUtil {

	public DateParserUtilTests() {
		super(false);
	}

	@Test
	public void test_remove_numbers_around_colon() {
		assertEquals(null, Utils.removeNumbersAroundChar(null, ':'));
		assertEquals("", Utils.removeNumbersAroundChar("", ':'));
		assertEquals("a", Utils.removeNumbersAroundChar("a", ':'));
		assertEquals("1", Utils.removeNumbersAroundChar("1", ':'));
		assertEquals("12", Utils.removeNumbersAroundChar("12", ':'));
		assertEquals("1 2", Utils.removeNumbersAroundChar("1 2", ':'));
		assertEquals("1.6 klo jeh 5", Utils.removeNumbersAroundChar("1.6 klo 12:23 jeh 5", ':'));
		assertEquals("ac a a 1 5", Utils.removeNumbersAroundChar("a:c 0:a a:0 1 1:234 5 0:0", ':'));
		assertEquals("15.1.2013", Utils.removeNumbersAroundChar("15.1.2013 0:00", ':'));
		assertEquals("", Utils.removeNumbersAroundChar(" ::::: ", ':'));
		assertEquals("a", Utils.removeNumbersAroundChar(" ::a::: ", ':'));
		assertEquals("2", Utils.removeNumbersAroundChar(" ::1::: 2", ':'));
		assertEquals("2", Utils.removeNumbersAroundChar(" ::0::: 2", ':'));
		assertEquals("aaa", Utils.removeNumbersAroundChar("a::2::a::3::4::a", ':'));
		
		assertEquals("M2", Utils.removeNumbersAroundChar("0:5 M2", ':'));
		assertEquals("M2", Utils.removeNumbersAroundChar("0.5 M2", '.'));
		assertEquals("^M2", Utils.removeNumbersAroundChar("0.5 ^M2", '.'));
	}

	@Test
	public void test_in_lenient_mode_that_makes_guesses() {
		DateParserUtil dateParser = new DateParserUtil(DateParserUtil.LENIENT_MODE_THAT_MAKES_QUESSES);
		assertEquals(null, dateParser.parseDate(null));
		assertEquals("", dateParser.parseDate(""));
		assertEquals("", dateParser.parseDate("   "));
		assertEquals("2009-06-17", dateParser.parseDate("17.6.2009"));
		assertEquals("2003", dateParser.parseDate("2003"));
		assertEquals("2002/2009", dateParser.parseDate("2002,2008, 2009"));
		assertEquals("2002/2008", dateParser.parseDate("2002,-03,-08"));
		assertEquals("2013-06-15/2013-06-30", dateParser.parseDate("15-30.6.2013"));   
		assertEquals("2005-07-28", dateParser.parseDate("28.7.05"));
		assertEquals(DateUtils.getCurrentYear() + "-07-28", dateParser.parseDate("28.7." +  currentYearInTwoNumberFormat())); // 28.7.14  where 14=current year
		assertEquals(DateUtils.getCurrentYear()+1-100 + "-07-28", dateParser.parseDate("28.7." + currentYearInTwoNumberFormatPlus(1))); // 28.7.15   where 15=current year + 1 
		assertEquals("1930-07-28", dateParser.parseDate("28.7.30"));
		assertEquals("1940-07-28", dateParser.parseDate("28.7.40"));
		assertEquals("1950-07-28", dateParser.parseDate("28.7.50"));
		assertEquals("1905-07-05", dateParser.parseDate("5.7.1905"));
		assertEquals("2012-06", dateParser.parseDate("6/2012"));
		assertEquals("1930", dateParser.parseDate("30"));
		assertEquals("2005", dateParser.parseDate("05"));
		assertEquals(""+DateUtils.getCurrentYear(), dateParser.parseDate(currentYearInTwoNumberFormat()));
		assertEquals(""+(DateUtils.getCurrentYear()+1-100), dateParser.parseDate(currentYearInTwoNumberFormatPlus(1)));
		assertFails("5.2222", dateParser);
		assertEquals("2014-05", dateParser.parseDate("5.2014"));
		assertEquals("2014-08", dateParser.parseDate("elokuu.2014"));
		assertEquals("2014-08", dateParser.parseDate("elo.2014"));
		assertEquals("2014-08", dateParser.parseDate("elokuu 2014"));
		assertEquals("2014-08", dateParser.parseDate("elo 2014"));
		assertEquals("2007-06-01", dateParser.parseDate("1.kesä.07"));
		assertEquals("2007-06-01", dateParser.parseDate("1 kesä 07"));
		assertEquals("2007-06-01", dateParser.parseDate("1 kesäkuuta 07"));
		assertEquals("2007-06", dateParser.parseDate("kesä.07"));
		assertEquals("2007-06", dateParser.parseDate("kesä 07")); // This could mean "the summer" generally
		assertEquals("2007", dateParser.parseDate("tammi,07"));
		assertEquals("2007", dateParser.parseDate("kesä,07"));
		assertEquals("2007", dateParser.parseDate("07,kesä"));
		assertEquals("2007", dateParser.parseDate("07 kesä"));
		assertEquals("2000-06", dateParser.parseDate("2000 kesäkuu"));
		assertEquals("2000-06", dateParser.parseDate("00 kesäkuu"));
		assertFails("2000.06", dateParser);
		assertFails("00.06", dateParser);
		assertEquals("2006-01", dateParser.parseDate("01.06"));
		assertEquals("2000-06", dateParser.parseDate("kesäkuu 2000"));
		assertEquals("2006-06", dateParser.parseDate("kesä.2006"));
		assertEquals("2006-06", dateParser.parseDate("kesäkuu.2006"));
		assertEquals("2006-06", dateParser.parseDate("kesakuu.2006"));
		assertEquals("2006-06", dateParser.parseDate("kesa.2006"));
		assertEquals("2000", dateParser.parseDate("2000 kesä"));
		assertEquals("2000", dateParser.parseDate("2000 talvi"));
		assertEquals("2000", dateParser.parseDate("talvi 2000"));
		assertEquals("2000", dateParser.parseDate("talvi,2000"));
		assertFails("talvi.2000", dateParser);
		assertEquals("2000-01", dateParser.parseDate("tammi.2000"));
		assertEquals("2006-07", dateParser.parseDate("06 heinä"));
		assertEquals("2000-07", dateParser.parseDate("2000 heinä"));
		assertEquals("2006-06", dateParser.parseDate("06 kesäkuu"));
		assertEquals("2012", dateParser.parseDate("vuoden 2012 alussa"));
		assertEquals("2012/2014", dateParser.parseDate("vuosien 2012 ja ehkä 2014 välissä"));
		assertEquals("2002/2014", dateParser.parseDate("02,06,14"));
		assertEquals("2002/2014", dateParser.parseDate("02,06, 14"));
		assertEquals("2002/2014", dateParser.parseDate("havaittu 02,06,14"));
		assertEquals("2002/2014", dateParser.parseDate("2002,2006,2014"));
		assertEquals("2002/2014", dateParser.parseDate("2002,2006, 2014"));
		assertEquals("2002/2014", dateParser.parseDate("havaittu 2002,2006,2014"));
		assertFails("20 02,2006", dateParser);
		assertEquals("2012/2014", dateParser.parseDate("vuosien 2012 tai ehkä sittenkin 2014"));
		assertFails("jossain 1.5.2012 ja 1.6 välissä", dateParser);
		assertEquals("2006-01/2012-05-01", dateParser.parseDate("jossain 01.05.2012 ja 01.06 välissä")); // this is wrongly interpeted, but one can't get everything!
		assertEquals("2012-05-01/2014-12-30", dateParser.parseDate("jossain 1.5.2012 ja 30.12.2014 välissä")); // it works, though with full years given!
		assertFails("jossain 1.5 ja 30.12.2014 välissä", dateParser); 
		assertEquals("2011-05-30/2011-07-04", dateParser.parseDate("30.5.2011; 4.7.2011"));
		assertEquals("2011-05-30/2011-07-04", dateParser.parseDate("30.5.2011, 4.7.2011"));
		assertEquals("2013-01-15", dateParser.parseDate("15.1.2013 0:00"));
		assertEquals("2008-07", dateParser.parseDate("VII.2008"));
		assertEquals("", dateParser.parseDate("x"));
		assertEquals("", dateParser.parseDate("Vanha netti-ilmoitus"));
		assertEquals("1950/1970", dateParser.parseDate("1950,1970"));
		assertEquals("2000/2009", dateParser.parseDate("2000 -luku"));
		assertEquals("2000/2009", dateParser.parseDate("2000-luku"));
		assertEquals("2000/2009", dateParser.parseDate("2000 luku"));
		assertEquals("1950/1959", dateParser.parseDate("1950 -luku"));
		assertEquals("1950/1959", dateParser.parseDate("50 -luku"));
		assertEquals("1950/1979", dateParser.parseDate("1950 -luku, 1970-luku"));
		assertEquals("1995", dateParser.parseDate("1995 -luku"));
		assertEquals("2000", dateParser.parseDate("luku 2000"));
		assertEquals("2000", dateParser.parseDate("2000 a-luku"));
		assertEquals("2000", dateParser.parseDate("2000,luku"));
		assertEquals("2000/2005", dateParser.parseDate("2000 -05")); // questionable..
		assertEquals("2000/2005", dateParser.parseDate("2000,-05"));
		assertEquals("2000/2005", dateParser.parseDate("2000-05")); // this means one can't use the util for stuff that has partial ISO formated dates already!
		assertEquals("2000/2005", dateParser.parseDate("2000-2005"));
		assertEquals("2000-05-01", dateParser.parseDate("2000-05-01")); // this ISO format it can handle, though..
		assertEquals("2013-11-01", dateParser.parseDate("2013-11-01T08:11:30+0200")); // this ISO format it can handle, though..
		assertFails("2000-05/2000-07", dateParser); // this means one can't use the util for stuff that has ISO formated dates already!
		assertEquals("2000-05-01/2000-07-25", dateParser.parseDate("2000-05-01/2000-07-25"));
		assertFails("2000-13-01", dateParser);
		assertFails("2010/ 20 vuotta vanha", dateParser);
		assertEquals("2000", dateParser.parseDate("Jun 2000")); // tukea vain suomenkielisille nyt..
		//		assertEquals("2000-08/2000-09", dateParser.parseDate("08.-09.2000"));
		//		assertEquals("2000-08/2000-09", dateParser.parseDate("08-09.2000"));
		//		assertEquals("2000-08/2000-09", dateParser.parseDate("08-09 2000"));
		//		assertEquals("2000-08/2000-09", dateParser.parseDate("elo-syyskuu 2000"));
		assertEquals("2011-05-01", dateParser.parseDate("1. 5.2011"));
		assertEquals("2011-05-01", dateParser.parseDate("1.5 .2011"));
		//		assertEquals("2011-01/2011-05", dateParser.parseDate("tammi-touko 2011"));
		//		assertEquals("2011-01/2011-05", dateParser.parseDate("touko-tammi 2011"));
		assertEquals("1982-09-10", dateParser.parseDate("10.IX.1982"));
		assertEquals("1982-09-10", dateParser.parseDate("10..IX.1982"));
		assertFails("18.23.VI.", dateParser);
		//assertEquals("asd", dateParser.parseDate("01/02/03")); TODO .. mitä pitäisi palauttaa?
	}
	
	private String currentYearInTwoNumberFormat() {
		return Integer.toString(DateUtils.getCurrentYear()).substring(2);
	}

	private String currentYearInTwoNumberFormatPlus(int years) {
		return Integer.toString(DateUtils.getCurrentYear() + years).substring(2);
	}

	@Test
	public void test_in_strict_mode() {
		// TODO strict mode
		DateParserUtil dateParser = new DateParserUtil(DateParserUtil.STRICT_MODE);
		assertEquals(null, dateParser.parseDate(null));
		assertEquals("1982-09-10", dateParser.parseDate("10.IX.1982"));
		assertEquals("1982-09-10", dateParser.parseDate("10..IX.1982"));
		assertFails("18.23.VI.", dateParser);
		
		//assertEquals("asd", dateParser.parseDate("01/02/03")); // TODO ei voida tätä tulkita oikeasti
//		assertEquals("", dateParser.parseDate(""));
//		assertEquals("", dateParser.parseDate("   "));
//		assertEquals("2009-06-17", dateParser.parseDate("17.6.2009"));
//		assertEquals("2003", dateParser.parseDate("2003"));
//		assertEquals("2002/2009", dateParser.parseDate("2002,2008, 2009"));
//		assertEquals("2002/2008", dateParser.parseDate("2002,-03,-08"));
//		assertEquals("2013-06-15/2013-06-30", dateParser.parseDate("15-30.6.2013"));   
//		assertEquals("2005-07-28", dateParser.parseDate("28.7.05"));
//		assertEquals(DateUtils.getCurrentYear() + "-07-28", dateParser.parseDate("28.7." +  currentYearInTwoNumberFormat())); // 28.7.14  where 14=current year
//		assertEquals(DateUtils.getCurrentYear()+1-100 + "-07-28", dateParser.parseDate("28.7." + currentYearInTwoNumberFormatPlus(1))); // 28.7.15   where 15=current year + 1 
//		assertEquals("1930-07-28", dateParser.parseDate("28.7.30"));
//		assertEquals("1940-07-28", dateParser.parseDate("28.7.40"));
//		assertEquals("1950-07-28", dateParser.parseDate("28.7.50"));
//		assertEquals("1905-07-05", dateParser.parseDate("5.7.1905"));
//		assertEquals("2012-06", dateParser.parseDate("6/2012"));
//		assertEquals("1930", dateParser.parseDate("30"));
//		assertEquals("2005", dateParser.parseDate("05"));
//		assertEquals(""+DateUtils.getCurrentYear(), dateParser.parseDate(currentYearInTwoNumberFormat()));
//		assertEquals(""+(DateUtils.getCurrentYear()+1-100), dateParser.parseDate(currentYearInTwoNumberFormatPlus(1)));
//		assertFails("5.2222", dateParser);
//		assertEquals("2014-05", dateParser.parseDate("5.2014"));
//		assertEquals("2014-08", dateParser.parseDate("elokuu.2014"));
//		assertEquals("2014-08", dateParser.parseDate("elo.2014"));
//		assertEquals("2014-08", dateParser.parseDate("elokuu 2014"));
//		assertEquals("2014-08", dateParser.parseDate("elo 2014"));
//		assertEquals("2007-06-01", dateParser.parseDate("1.kesä.07"));
//		assertEquals("2007-06-01", dateParser.parseDate("1 kesä 07"));
//		assertEquals("2007-06-01", dateParser.parseDate("1 kesäkuuta 07"));
//		assertEquals("2007-06", dateParser.parseDate("kesä.07"));
//		assertEquals("2007-06", dateParser.parseDate("kesä 07")); // This could mean "the summer" generally, but will pass even in strict mode because fixing would be a bit complex
//		assertFails("tammi,07", dateParser);
//		assertFails("kesä,07", dateParser);
//		assertFails("07,kesä", dateParser);
//		// assertFails("07 kesä", dateParser); TODO
//		assertFails("2000 kesäkuu", dateParser);
//		assertFails("00 kesäkuu", dateParser);
//		assertFails("2000.06", dateParser);
//		assertFails("00.06", dateParser);
//		assertEquals("2006-01", dateParser.parseDate("01.06"));
//		assertEquals("2000-06", dateParser.parseDate("kesäkuu 2000"));
//		assertEquals("2006-06", dateParser.parseDate("kesä.2006"));
//		assertEquals("2006-06", dateParser.parseDate("kesäkuu.2006"));
//		assertEquals("2006-06", dateParser.parseDate("kesakuu.2006"));
//		assertEquals("2006-06", dateParser.parseDate("kesa.2006"));
//		assertFails("2000 kesä", dateParser);
//		assertFails("2000 talvi", dateParser);
//		assertFails("talvi 2000", dateParser);
//		assertFails("talvi,2000", dateParser);
//		assertFails("talvi.2000", dateParser);
//		assertEquals("2000-01", dateParser.parseDate("tammi.2000"));
//		assertFails("06 heinä", dateParser);
//		assertFails("2000 heinä", dateParser);
//		assertFails("06 kesäkuu", dateParser);
//		assertFails("vuoden 2012 alussa", dateParser);
//		assertFails("vuosien 2012 ja ehkä 2014 välissä", dateParser);
//		assertEquals("2002/2014", dateParser.parseDate("02,06,14"));
//		assertEquals("2002/2014", dateParser.parseDate("02,06, 14"));
//		assertFails("02,06,14", dateParser);
//		assertEquals("2002/2014", dateParser.parseDate("2002,2006,2014"));
//		assertEquals("2002/2014", dateParser.parseDate("2002,2006, 2014"));
//		assertFails("havaittu 2002,2006,2014", dateParser);
//		assertFails("20 02,2006", dateParser);
//		assertFails("vuosien 2012 tai ehkä sittenkin 2014", dateParser);
//		assertFails("jossain 1.5.2012 ja 1.6 välissä", dateParser);
//		assertFails("jossain 01.05.2012 ja 01.06 välissä", dateParser);
//		assertFails("jossain 1.5.2012 ja 30.12.2014 välissä", dateParser);
//		assertFails("jossain 1.5 ja 30.12.2014 välissä", dateParser); 
//		assertEquals("2011-05-30/2011-07-04", dateParser.parseDate("30.5.2011; 4.7.2011"));
//		assertEquals("2011-05-30/2011-07-04", dateParser.parseDate("30.5.2011, 4.7.2011"));
//		assertEquals("2013-01-15", dateParser.parseDate("15.1.2013 0:00"));
//		assertEquals("2008-07", dateParser.parseDate("VII.2008"));
//		assertFails("x", dateParser);
//		assertFails("Vanha netti-ilmoitus", dateParser);
//		assertEquals("1950/1970", dateParser.parseDate("1950,1970"));
//		assertEquals("2000/2009", dateParser.parseDate("2000 -luku"));
//		assertEquals("2000/2009", dateParser.parseDate("2000-luku"));
//		assertEquals("2000/2009", dateParser.parseDate("2000 luku"));
//		assertEquals("1950/1959", dateParser.parseDate("1950 -luku"));
//		assertEquals("1950/1959", dateParser.parseDate("50 -luku"));
//		assertEquals("1950/1979", dateParser.parseDate("1950 -luku, 1970-luku"));
//		assertFails("1995 -luku", dateParser);
//		assertFails("2000", dateParser);
//		assertFails("2000 a-luku", dateParser);
//		assertFails("2000,luku", dateParser);
//		assertFails("2000 -05", dateParser);
//		assertEquals("2000/2005", dateParser.parseDate("2000,-05"));
//		assertEquals("2000/2005", dateParser.parseDate("2000-05")); // this means one can't use the util for stuff that has ISO formated dates already!
//		assertEquals("2000/2005", dateParser.parseDate("2000-2005"));
//		assertEquals("2000-05-01", dateParser.parseDate("2000-05-01")); // this ISO format it can handle, though..
//		assertFails("2000-05/2000-07", dateParser); // this means one can't use the util for stuff that has ISO formated dates already!
//		assertEquals("2000-05-01/2000-07-25", dateParser.parseDate("2000-05-01/2000-07-25"));
//		assertFails("2000-13-01", dateParser);
//		assertFails("2010/ 20 vuotta vanha", dateParser);
//		assertFails("Jun 2000", dateParser);
//		//		assertEquals("2000-08/2000-09", dateParser.parseDate("08.-09.2000"));
//		//		assertEquals("2000-08/2000-09", dateParser.parseDate("08-09.2000"));
//		//		assertEquals("2000-08/2000-09", dateParser.parseDate("08-09 2000"));
//		//		assertEquals("2000-08/2000-09", dateParser.parseDate("elo-syyskuu 2000"));
//		assertFails("1. 5.2011", dateParser);
//		assertFails("1.5 .2011", dateParser);
//		//		assertEquals("2011-01/2011-05", dateParser.parseDate("tammi-touko 2011"));
//		//		assertEquals("2011-01/2011-05", dateParser.parseDate("touko-tammi 2011"));
	}

	private void assertFails(String dateToParse, DateParserUtil dateParser) {
		try {
			String parsedDate = dateParser.parseDate(dateToParse);
			fail("Should throw " + IllegalArgumentException.class.getName() + " instead returned " + parsedDate);
		} catch (IllegalArgumentException e) {
			assertEquals(dateToParse.toLowerCase(), e.getMessage());
		}

	}

}
