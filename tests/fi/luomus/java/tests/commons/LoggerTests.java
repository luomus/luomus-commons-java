package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.commons.utils.Logger;

public class LoggerTests {

	Logger logger;
	ByteArrayOutputStream out;
	Writer writer;

	@Before
	public void setUp() {
		this.out = new ByteArrayOutputStream();
		writer = new OutputStreamWriter(out);
		this.logger = new Logger(writer);
	}

	@Test
	public void test() throws IOException {
		logger.log("Hello");
		logger.log("World!");
		logger.commit("puuppelipuu", "Esko");
		String[] lines = out.toString().split("\n");
		assertTrue(lines[0].startsWith("--- START TRANSACTION puuppelipuu @ "));
		assertTrue(lines[0].endsWith(" --- user-id: Esko --- "));
		assertEquals("puuppelipuu: Hello", lines[1]);
		assertEquals("puuppelipuu: World!", lines[2]);
		assertEquals("--- END TRANSACTION puuppelipuu ---", lines[3]);
	}

	@Test
	public void test_rollback() throws IOException {
		logger.log("Hello");
		logger.commit("trans1", "Esko");
		logger.log("Maailma!");
		logger.rollback();
		logger.log("World!");
		logger.commit("trans2", "Esko");
		String[] lines = out.toString().split("\n");
		assertTrue(lines[0].startsWith("--- START TRANSACTION trans1 @ "));
		assertEquals("trans1: Hello", lines[1]);
		assertEquals("--- END TRANSACTION trans1 ---", lines[2]);
		assertTrue(lines[3].startsWith("--- START TRANSACTION trans2 @ "));
		assertEquals("trans2: World!", lines[4]);
		assertEquals("--- END TRANSACTION trans2 ---", lines[5]);
	}

	@Test
	public void test_nothing_to_commit() throws IOException {
		logger.commit("puupaa", "Esko");
		assertEquals("", out.toString());
	}

//	@Test
//	public void test_thread_safety() throws IOException {
//		List<Thread> threads = new ArrayList<Thread>();
//		for (int i=1; i<= 100; i++) {
//			LogWriterThread thread = new LogWriterThread(writer, "Log "+i);
//			thread.start();
//			threads.add(thread);
//		}
//		for (Thread thread : threads) {
//			while (thread.isAlive()) {}
//		}
//		System.out.println("done");
//		System.out.println(out.toString());
//	}
//
//	private static class LogWriterThread extends Thread {
//		private final Logger logger;
//		private final String name;
//		public LogWriterThread(Writer writer, String name) throws FileNotFoundException {
//			//this.logger = new Logger(writer, name);
//			this.logger = new Logger(new File("c:/temp/logtext.txt"), name);
//			this.name = name;
//		}
//		@Override
//		public void run() {
//			int count = 0;
//			for (int i=1; i<=20; i++) {
//				System.out.println("Hello from a thread " + name);
//				count++;
//				try {
//					logger.log("A");
//					logger.log("B");
//					logger.log("C");
//					logger.commit(name + " " + i);
//					Thread.sleep((int)(Math.random()*100));
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//			System.out.println("Stopped " + name + " after " + count + " writes to log");
//			System.out.flush();
//		}
//		
//	}

}

