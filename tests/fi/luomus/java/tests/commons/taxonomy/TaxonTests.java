package fi.luomus.java.tests.commons.taxonomy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import fi.luomus.commons.containers.AdministrativeStatus;
import fi.luomus.commons.containers.Content;
import fi.luomus.commons.containers.Content.Context;
import fi.luomus.commons.containers.Image;
import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.AdministrativeStatusContainer;
import fi.luomus.commons.taxonomy.HabitatOccurrenceCounts;
import fi.luomus.commons.taxonomy.HabitatOccurrenceCounts.HabitatOccurrenceCount;
import fi.luomus.commons.taxonomy.InMemoryTaxonContainerImple;
import fi.luomus.commons.taxonomy.InformalTaxonGroupContainer;
import fi.luomus.commons.taxonomy.NoSuchTaxonException;
import fi.luomus.commons.taxonomy.PublicInformation;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;
import fi.luomus.commons.taxonomy.iucn.HabitatObject;
import fi.luomus.commons.utils.Utils;

public class TaxonTests {

	private static final Qname VU = new Qname("MX.iucnVU");
	private static final Qname LC = new Qname("MX.iucnLC");

	@Test
	public void test_redListStatus() {
		Taxon taxon = new Taxon(new Qname("MX.1"), null);
		taxon.setRedListStatus2010Finland(LC);
		taxon.setRedListStatus2015Finland(VU);

		assertEquals(null, taxon.getRedListStatus2000Finland());
		assertEquals(LC, taxon.getRedListStatus2010Finland());
		assertEquals(VU, taxon.getRedListStatus2015Finland());
		assertEquals(null, taxon.getRedListStatus2019Finland());

		assertEquals("RedListStatus [year=2015, status=MX.iucnVU]", taxon.getLatestRedListStatusFinland().toString());
		assertEquals("[RedListStatus [year=2015, status=MX.iucnVU], RedListStatus [year=2010, status=MX.iucnLC]]", taxon.getRedListStatusesInFinland().toString());

		assertEquals(null, taxon.getRedListStatusForYear(2000));
		assertEquals(null, taxon.getRedListStatusForYear(999));
		assertEquals(LC, taxon.getRedListStatusForYear(2010));

		assertEquals("MX.iucnGroup3", taxon.getRedListStatusGroup().toString());
	}

	@Test
	public void imageAuthors() {
		Image image = new Image(new Qname("MM.1"), new Qname("KE.3"), "");
		assertEquals(null, image.getAuthor());
		image.addAuthor("Foo");
		assertEquals("Foo", image.getAuthor());
		image.addAuthor("Foo");
		image.addAuthor("Bar");
		assertEquals("Foo, Bar", image.getAuthor());
	}

	@Test
	public void defaultContextTextsFirst() {
		Content content = new Content();
		content.addText(new Qname("LA.1"), new Qname("somedesc"), "somedesc", "fi");
		content.addText(null, new Qname("somedesc"), "somedesc", "fi");
		content.addText(new Qname("LA.2"), new Qname("somedesc"), "somedesc", "fi");
		assertEquals(3, content.getContexts().size());
		Iterator<Context> i = content.getContexts().iterator();
		assertEquals(Content.DEFAULT_DESCRIPTION_CONTEXT, i.next().getId());
		assertEquals(new Qname("LA.1"), i.next().getId());
		assertEquals(new Qname("LA.2"), i.next().getId());
	}

	private final TaxonContainer testTaxonContainer = new TaxonContainerStub() {

		@Override
		public boolean hasTaxon(Qname taxonId) {
			return true;
		}

		@Override
		public Taxon getTaxon(Qname taxonId) throws NoSuchTaxonException {
			Taxon t = new Taxon(taxonId, this);
			t.setScientificName("Sciname of " + taxonId);
			return t;
		}

	};

	@Test
	public void test_synonymNames() {
		Taxon taxon = new Taxon(new Qname("MX.1"), testTaxonContainer);
		assertEquals(null, taxon.getSynonymNames());

		taxon.addSynonyms().addBasionym(new Qname("basionym"));
		taxon.addSynonyms().addMisappliedName(new Qname("missapplied"));

		assertEquals("Sciname of basionym, Sciname of missapplied", taxon.getSynonymNames());
	}

	@Test
	public void test_names() {
		Taxon taxon = new Taxon(new Qname("MX.1"), testTaxonContainer);
		taxon.setScientificName("Parus");
		taxon.setTaxonRank(new Qname("MX.genus"));

		assertEquals("Parus sp.", taxon.getScientificNameDisplayName());

		taxon.setScientificName("Parus major");
		taxon.setTaxonRank(new Qname("MX.species"));

		assertEquals("Parus major", taxon.getScientificNameDisplayName());

		taxon.setScientificName(null);
		taxon.setTaxonRank(new Qname("MX.species"));
		taxon.addVernacularName("en", "Potato virus Y (PVY)");

		assertEquals("Potato virus Y (PVY)", taxon.getScientificNameDisplayName());

		assertEquals("Potato virus Y (PVY)", taxon.getNameEnglish());
		assertEquals(null, taxon.getNameSwedish());
	}

	@Test
	public void sortingInformalTaxonGroups() {
		Map<String, InformalTaxonGroup> groups = new HashMap<>();
		InformalTaxonGroup g1 = new InformalTaxonGroup(new Qname("MVL.1"), new LocalizedText().set("fi", "Kalat").set("en", "Fish"), 2).setExplicitlyDefinedRoot(true);
		InformalTaxonGroup g2 = new InformalTaxonGroup(new Qname("MVL.2"), new LocalizedText().set("fi", "Särkikalat"), 1);
		InformalTaxonGroup g3 = new InformalTaxonGroup(new Qname("MVL.3"), new LocalizedText().set("fi", "Pikkusärjet").set("en", "Small roach"), 1);
		InformalTaxonGroup g4 = new InformalTaxonGroup(new Qname("MVL.4"), new LocalizedText().set("fi", "Vesieläimet"), 3);
		InformalTaxonGroup g5 = new InformalTaxonGroup(new Qname("MVL.5"), new LocalizedText().set("fi", "Xällä alkava ryhmä"), 1);
		InformalTaxonGroup g6 = new InformalTaxonGroup(new Qname("MVL.6"), new LocalizedText().set("fi", "Miniminisärjet"), 1);

		// Kalat (1) order 2
		// - Särkikalat (2)
		//   - Pikkusärjet (3)
		//     - Miniminisärjet (6)
		// Vesieläimet (4) order 3
		//   - Kalat (1)
		//    - Särkikalat (2)
		//     - Pikkusärjet (3)
		//       - Miniminisärjet (6)
		// Xällä alkava ryhmä (5) order 1

		g1.addSubGroup(g2.getQname());
		g2.addParent(g1.getQname());

		g2.addSubGroup(g3.getQname());
		g3.addParent(g2.getQname());

		g3.addSubGroup(g6.getQname());
		g6.addParent(g3.getQname());

		g4.addSubGroup(g1.getQname());
		g1.addParent(g4.getQname());

		groups.put(g1.getQname().toString(), g1);
		groups.put(g2.getQname().toString(), g2);
		groups.put(g3.getQname().toString(), g3);
		groups.put(g4.getQname().toString(), g4);
		groups.put(g5.getQname().toString(), g5);
		groups.put(g6.getQname().toString(), g6);

		InMemoryTaxonContainerImple imple = new InMemoryTaxonContainerImple(new InformalTaxonGroupContainer(groups), null, null);

		Set<Qname> sorted = imple.orderInformalTaxonGroups(Utils.set(new Qname("FOOBAR")));
		assertEquals("[]", sorted.toString());

		sorted = imple.orderInformalTaxonGroups(Utils.set(new Qname("MVL.1"), new Qname("MVL.2"), new Qname("MVL.3")));
		assertEquals("[MVL.1, MVL.2, MVL.3]", sorted.toString());

		sorted = imple.orderInformalTaxonGroups(Utils.set(new Qname("MVL.1"), new Qname("MVL.2"), new Qname("MVL.3"), new Qname("MVL.4"), new Qname("MVL.5"), new Qname("MVL.6")));
		assertEquals("[MVL.5, MVL.4, MVL.1, MVL.2, MVL.3, MVL.6]", sorted.toString()); // Root starting with X(order=1) then the actual root (not expl defined), child, child+1, child+2, ...

		assertEquals("Kalat; Särkikalat; Pikkusärjet", imple.getInformalTaxonGroupNames(Utils.set(new Qname("MVL.1"), new Qname("MVL.2"), new Qname("MVL.3"))).forLocale("fi"));
		assertEquals("Fish; Small roach", imple.getInformalTaxonGroupNames(Utils.set(new Qname("MVL.1"), new Qname("MVL.2"), new Qname("MVL.3"))).forLocale("en"));
	}

	@Test
	public void sortingAdminStatuses() {
		Map<String, AdministrativeStatus> statuses = new LinkedHashMap<>();
		AdministrativeStatus s1 = new AdministrativeStatus(new Qname("MX.finlex1"), new LocalizedText().set("fi", "Laki 1"));
		AdministrativeStatus s2 = new AdministrativeStatus(new Qname("MX.finlex2"), new LocalizedText().set("fi", "Laki 2"));
		AdministrativeStatus s3 = new AdministrativeStatus(new Qname("MX.bb"), new LocalizedText().set("fi", "Bb"));
		AdministrativeStatus s4 = new AdministrativeStatus(new Qname("MX.aa"), new LocalizedText().set("fi", "Aa"));

		statuses.put(s1.getQname().toString(), s1);
		statuses.put(s2.getQname().toString(), s2);
		statuses.put(s3.getQname().toString(), s3);
		statuses.put(s4.getQname().toString(), s4);

		InMemoryTaxonContainerImple imple = new InMemoryTaxonContainerImple(null, null, new AdministrativeStatusContainer(statuses));

		Set<Qname> sorted = imple.orderAdministrativeStatuses(Utils.set(new Qname("FOOBAR")));
		assertEquals("[]", sorted.toString());

		sorted = imple.orderAdministrativeStatuses(Utils.set(new Qname("MX.aa"), new Qname("MX.finlex1")));
		assertEquals("[MX.finlex1, MX.aa]", sorted.toString());

		sorted = imple.orderAdministrativeStatuses(Utils.set(new Qname("MX.bb"), new Qname("MX.aa"), new Qname("MX.finlex2"), new Qname("MX.finlex1")));
		assertEquals("[MX.finlex1, MX.finlex2, MX.bb, MX.aa]", sorted.toString());

		assertEquals("Laki 1; Laki 2; Bb; Aa", imple.getAdministrativeStatusNames((Utils.set(new Qname("MX.bb"), new Qname("MX.aa"), new Qname("MX.finlex2"), new Qname("MX.finlex1")))).forLocale("fi"));
	}

	@Test
	public void taxonPublicFieldOrders() {
		Set<Double> usedOrders = new HashSet<>();
		for (Method method : Taxon.class.getMethods()) {
			PublicInformation annotation = method.getAnnotation(PublicInformation.class);
			if (annotation == null) continue;
			assertTrue("Order missing for " + method.getName(), annotation.order() < (Double.MAX_VALUE - 1000.0));
			if (usedOrders.contains(annotation.order())) {
				fail(method.getName() + " has same order " + annotation.order() + " than some other field");
			}
			usedOrders.add(annotation.order());
		}
	}

	@Test
	public void habitatSearchStrings() {
		HabitatObject h1 = new HabitatObject(null, new Qname("hab"), 1);
		assertEquals("hab", h1.toHabitatSearchString());

		HabitatObject h2 = new HabitatObject(null, new Qname("hab"), 1).addHabitatSpecificType(new Qname("t2"));
		assertEquals("hab[:t2:]", h2.toHabitatSearchString());

		HabitatObject h3 = new HabitatObject(null, new Qname("hab"), 1).addHabitatSpecificType(new Qname("t2")).addHabitatSpecificType(new Qname("t1"));
		assertEquals("hab[:t1:,:t2:]", h3.toHabitatSearchString());

		HabitatObject h1c = HabitatObject.fromSearchString(h1.toHabitatSearchString());
		HabitatObject h2c = HabitatObject.fromSearchString(h2.toHabitatSearchString());
		HabitatObject h3c = HabitatObject.fromSearchString(h3.toHabitatSearchString());
		HabitatObject h4 = HabitatObject.fromSearchString("hab[t1,t2]");

		assertEquals(h1, h1c);
		assertEquals(h2, h2c);
		assertEquals(h3, h3c);
		assertEquals(h3, h4);
	}

	@Test
	public void habitatOccurrenceTop() {
		HabitatOccurrenceCounts counts = new HabitatOccurrenceCounts();
		counts.setCount(new HabitatOccurrenceCount("a", new LocalizedText()).setOccurrenceCount(1000));
		counts.setCount(new HabitatOccurrenceCount("a", new LocalizedText()).setOccurrenceCount(1)); // overrides previous id "a"
		counts.setCount(new HabitatOccurrenceCount("b", new LocalizedText()).setOccurrenceCount(1));
		counts.setCount(new HabitatOccurrenceCount("c", new LocalizedText()).setOccurrenceCount(3));
		counts.setCount(new HabitatOccurrenceCount("d", new LocalizedText()).setOccurrenceCount(4));
		counts.setCount(new HabitatOccurrenceCount("e", new LocalizedText()).setOccurrenceCount(5));
		assertEquals(counts.getCounts().size(), 5);
		assertTrue(counts.hasCount("a"));
		assertFalse(counts.hasCount("foo"));
		assertEquals(1, counts.getCount("a").getOccurrenceCount());
		assertEquals("[" +
				"HabitatOccurrenceCounts [id=e, habitat={}, occurrenceCount=5], " +
				"HabitatOccurrenceCounts [id=d, habitat={}, occurrenceCount=4], " +
				"HabitatOccurrenceCounts [id=c, habitat={}, occurrenceCount=3], " +
				"HabitatOccurrenceCounts [id=a, habitat={}, occurrenceCount=1], " +
				"HabitatOccurrenceCounts [id=b, habitat={}, occurrenceCount=1]]",
				counts.toString());

		counts.retainTop(10);
		assertEquals(5, counts.getCounts().size());
		counts.retainTop(5);
		assertEquals(5, counts.getCounts().size());
		counts.retainTop(4);
		assertEquals(4, counts.getCounts().size());
		assertEquals("[" +
				"HabitatOccurrenceCounts [id=e, habitat={}, occurrenceCount=5], " +
				"HabitatOccurrenceCounts [id=d, habitat={}, occurrenceCount=4], " +
				"HabitatOccurrenceCounts [id=c, habitat={}, occurrenceCount=3], " +
				"HabitatOccurrenceCounts [id=a, habitat={}, occurrenceCount=1]]",
				counts.toString());
		counts.retainTop(2);
		assertEquals(2, counts.getCounts().size());
		assertEquals("[" +
				"HabitatOccurrenceCounts [id=e, habitat={}, occurrenceCount=5], " +
				"HabitatOccurrenceCounts [id=d, habitat={}, occurrenceCount=4]]",
				counts.toString());
	}

}
