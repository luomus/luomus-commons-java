package fi.luomus.java.tests.commons.taxonomy;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;
import fi.luomus.commons.taxonomy.TaxonSearch;
import fi.luomus.commons.taxonomy.TaxonSearch.MatchType;
import fi.luomus.commons.taxonomy.TaxonSearchResponse;
import fi.luomus.commons.taxonomy.TaxonSearchResponse.Match;

public class TaxonSearchMatchComparatorTests {

	private static final Qname MISAPPLIED = new Qname("MX.hasMisappliedName");
	private static final Qname ALTERNATIVE = new Qname("MX.alternativeVernacularName");
	private static final Qname GENUS = new Qname("MX.genus");
	private static final boolean NOT_FINNISH = false;
	private static final boolean FINNISH = true;
	private static final Qname SPECIES = new Qname("MX.species");
	private static final Qname SYNONYM = new Qname("MX.hasSynonym");
	private static final Qname SCIENTIFICNAME = new Qname("MX.scientificName");
	private static final Qname VERNACULARNAME = new Qname("MX.vernacularName");

	@Test
	public void test() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "major";
		matches.add(new Match(taxon(FINNISH, SPECIES), "parus major", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "ptiloxenus major", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals("parus major", matches.get(0).getMatchingName());
		assertEquals("ptiloxenus major", matches.get(1).getMatchingName());
	}

	@Test
	public void test_2() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "major";
		matches.add(new Match(taxon(NOT_FINNISH, SPECIES), "parus major", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "ptiloxenus major", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals("ptiloxenus major", matches.get(0).getMatchingName());
		assertEquals("parus major", matches.get(1).getMatchingName());
	}

	@Test
	public void test_3() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "parus";
		matches.add(new Match(taxon(FINNISH, GENUS), "parus", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "parus major", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals("parus major", matches.get(0).getMatchingName());
		assertEquals("parus", matches.get(1).getMatchingName());
	}

	@Test
	public void test_4() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "parus majox";
		matches.add(new Match(taxon(FINNISH, SPECIES), "parus majorx", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord).setSimilarity(0.9));
		matches.add(new Match(taxon(FINNISH, SPECIES), "parus major", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord).setSimilarity(0.8));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals("parus majorx", matches.get(0).getMatchingName());
		assertEquals("parus major", matches.get(1).getMatchingName());
	}

	@Test
	public void test_5() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "parus";
		matches.add(new Match(taxon(FINNISH, SPECIES), "parus majorx", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "parus major", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals("parus major", matches.get(0).getMatchingName());
		assertEquals("parus majorx", matches.get(1).getMatchingName());
	}

	@Test
	public void test_5_2() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "parus";
		matches.add(new Match(taxon(FINNISH, SPECIES), "parus majorx", MatchType.LIKELY, SCIENTIFICNAME, null, searchWord).setSimilarity(0.9));
		matches.add(new Match(taxon(FINNISH, SPECIES), "parus major", MatchType.LIKELY, SCIENTIFICNAME, null, searchWord).setSimilarity(0.9));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals("parus major", matches.get(0).getMatchingName());
		assertEquals("parus majorx", matches.get(1).getMatchingName());
	}

	@Test
	public void test_6() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "rupik";
		matches.add(new Match(taxon(FINNISH, SPECIES), "rupikonna", MatchType.PARTIAL, VERNACULARNAME, "fi", searchWord));  // e is before r in alphabetic order
		matches.add(new Match(taxon(FINNISH, SPECIES), "etelänrupikka", MatchType.PARTIAL, VERNACULARNAME, "fi", searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals("rupikonna", matches.get(0).getMatchingName()); // but this name starts with the search word
		assertEquals("etelänrupikka", matches.get(1).getMatchingName());
	}

	@Test
	public void test_7() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "b";
		matches.add(new Match(taxon(FINNISH, SPECIES), "axx", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "bx1", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "bx2", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "dxx", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals("bx1", matches.get(0).getMatchingName());
		assertEquals("bx2", matches.get(1).getMatchingName());
		assertEquals("axx", matches.get(2).getMatchingName());
		assertEquals("dxx", matches.get(3).getMatchingName());
	}

	@Test
	public void test_8() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "bbb";
		matches.add(new Match(taxon(FINNISH, SPECIES), "XXX aaa", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "XXX bbb", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "bbb XXX", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "bbb bbb", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "Xbbb bbb", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "bbbX bbb", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "bbb Xbbb", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "bbb bbbX", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "bbab", MatchType.LIKELY, VERNACULARNAME, null, searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals("bbb bbb", matches.get(0).getMatchingName());
		assertEquals("bbbX bbb", matches.get(1).getMatchingName());
		assertEquals("bbb bbbX", matches.get(2).getMatchingName());
		assertEquals("bbb Xbbb", matches.get(3).getMatchingName());
		assertEquals("bbb XXX", matches.get(4).getMatchingName());
		assertEquals("Xbbb bbb", matches.get(5).getMatchingName());
		assertEquals("XXX bbb", matches.get(6).getMatchingName());
		assertEquals("XXX aaa", matches.get(7).getMatchingName());
		assertEquals("bbab", matches.get(8).getMatchingName());
	}

	@Test
	public void test_9() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "P major";
		matches.add(new Match(taxon(FINNISH, SPECIES), "Parus major", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "Paragus majoranae", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals("Parus major", matches.get(0).getMatchingName());
		assertEquals("Paragus majoranae", matches.get(1).getMatchingName());
	}

	@Test
	public void test_10() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "Parus major";
		matches.add(new Match(taxon(FINNISH, SPECIES), "Parus major", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "Parus major", MatchType.PARTIAL, VERNACULARNAME, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "Parus major", MatchType.PARTIAL, SYNONYM, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "Parus major", MatchType.PARTIAL, new Qname("unknown"), null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "Parus major", MatchType.PARTIAL, ALTERNATIVE, null, searchWord));
		matches.add(new Match(taxon(FINNISH, SPECIES), "Parus major", MatchType.PARTIAL, MISAPPLIED, null, searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		assertEquals(SCIENTIFICNAME, matches.get(0).getNameType());
		assertEquals(VERNACULARNAME, matches.get(1).getNameType());
		assertEquals(ALTERNATIVE, matches.get(2).getNameType());
		assertEquals(SYNONYM, matches.get(3).getNameType());
		assertEquals(MISAPPLIED, matches.get(4).getNameType());
	}

	@Test
	public void test_prefer_fi() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "Parus major";
		matches.add(new Match(taxon("MX.1", FINNISH, SPECIES), "xxx", MatchType.PARTIAL, VERNACULARNAME, "sv", searchWord));
		matches.add(new Match(taxon("MX.2", FINNISH, SPECIES), "xxx", MatchType.PARTIAL, VERNACULARNAME, "fi", searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		matches.forEach(s->System.out.println(s));
		assertEquals("MX.2,MX.1", matches.stream().map(m->m.getTaxon().getQname().toString()).collect(Collectors.joining(",")));
	}

	@Test
	public void test_11() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "Pa";
		Taxon t1 = taxon("MX.1", FINNISH, SPECIES);
		Taxon t2 = taxon("MX.2", FINNISH, SPECIES);
		Taxon t3 = taxon("MX.3", FINNISH, SPECIES);
		Taxon t4 = taxon("MX.4", FINNISH, SPECIES);
		t1.setExplicitObservationCountFinland(400000);
		t2.setExplicitObservationCountFinland(2000);
		t3.setExplicitObservationCountFinland(1);
		t4.setExplicitObservationCountFinland(30000);
		matches.add(new Match(t1, "partiolintu", MatchType.PARTIAL, VERNACULARNAME, "fi", searchWord));
		matches.add(new Match(t2, "paistilintu", MatchType.PARTIAL, VERNACULARNAME, "fi", searchWord));
		matches.add(new Match(t3, "parulintu", MatchType.PARTIAL, VERNACULARNAME, "fi", searchWord));
		matches.add(new Match(t4, "papulintu", MatchType.PARTIAL, VERNACULARNAME, "fi", searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		matches.forEach(s->System.out.println(s));
		assertEquals("MX.1,MX.4,MX.2,MX.3", matches.stream().map(m->m.getTaxon().getQname().toString()).collect(Collectors.joining(",")));
	}

	@Test
	public void test_12() {
		List<Match> matches = new ArrayList<>();
		String searchWord = "Pa";
		Taxon t1 = taxon("MX.1", FINNISH, SPECIES);
		Taxon t2 = taxon("MX.2", FINNISH, SPECIES);
		Taxon t3 = taxon("MX.3", FINNISH, SPECIES);
		Taxon t4 = taxon("MX.4", FINNISH, SPECIES);
		t1.setExplicitObservationCountFinland(400000);
		t2.setExplicitObservationCountFinland(2000);
		t3.setExplicitObservationCountFinland(1);
		t4.setExplicitObservationCountFinland(30000);
		matches.add(new Match(t1, "Parus major", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		matches.add(new Match(t2, "Parus minor", MatchType.PARTIAL, SCIENTIFICNAME, null, searchWord));
		matches.add(new Match(t3, "parulintu", MatchType.PARTIAL, VERNACULARNAME, "fi", searchWord));
		matches.add(new Match(t4, "papulintu", MatchType.PARTIAL, VERNACULARNAME, "fi", searchWord));
		Collections.sort(matches, TaxonSearchResponse.MATCH_COMPARATOR);
		matches.forEach(s->System.out.println(s));
		assertEquals("MX.1,MX.4,MX.2,MX.3", // descending obs count
				matches.stream().map(m->m.getTaxon().getQname().toString()).collect(Collectors.joining(",")));
	}

	private Taxon taxon(boolean finnish, Qname rank) {
		return taxon("MX.1", finnish, rank);
	}

	private Taxon taxon(String id, boolean finnish, Qname rank) {
		Taxon taxon = new Taxon(new Qname(id), container);
		if (finnish) taxon.setFinnish(true);
		taxon.setTaxonRank(rank);
		return taxon;
	}

	private Taxon taxon(String id) {
		return taxon(id, true, SPECIES);
	}

	private final TaxonContainer container = new TaxonContainerStub() {
		@Override
		public boolean hasTaxon(Qname taxonId) {
			return true;
		}
	};

	@Test
	public void removingDuplicates() {
		TaxonSearchResponse response = new TaxonSearchResponse(new TaxonSearch("query", 10));
		response.getExactMatches().add(new Match(taxon("MX.1"), "Match 1", MatchType.EXACT, VERNACULARNAME, "en", "query"));
		response.getExactMatches().add(new Match(taxon("MX.2"), "Match 2", MatchType.EXACT, VERNACULARNAME, "en", "query"));

		response.getPartialMatches().add(new Match(taxon("MX.2"), "Match 2.2", MatchType.EXACT, VERNACULARNAME, "en", "query"));
		response.getPartialMatches().add(new Match(taxon("MX.3"), "Match 3", MatchType.EXACT, VERNACULARNAME, "en", "query"));
		response.getPartialMatches().add(new Match(taxon("MX.4"), "Match 4", MatchType.EXACT, VERNACULARNAME, "en", "query"));

		response.getLikelyMatches().add(new Match(taxon("MX.4"), "Match 4.1", MatchType.EXACT, VERNACULARNAME, "en", "query"));
		response.getLikelyMatches().add(new Match(taxon("MX.1"), "Match 1.2", MatchType.EXACT, VERNACULARNAME, "en", "query"));
		response.getLikelyMatches().add(new Match(taxon("MX.5"), "Match 5", MatchType.EXACT, VERNACULARNAME, "en", "query"));

		response.finalizeMatches();

		assertEquals(2, response.getExactMatches().size());
		assertEquals(2, response.getPartialMatches().size());
		assertEquals(1, response.getLikelyMatches().size());
	}

	@Test
	public void obsCounts() {
		assertEquals(0, TaxonSearchResponse.getRoundedObsCount(0));
		assertEquals(10, TaxonSearchResponse.getRoundedObsCount(1));
		assertEquals(10, TaxonSearchResponse.getRoundedObsCount(5));
		assertEquals(100, TaxonSearchResponse.getRoundedObsCount(52));
		assertEquals(100, TaxonSearchResponse.getRoundedObsCount(199));
		assertEquals(200, TaxonSearchResponse.getRoundedObsCount(200));
		assertEquals(900, TaxonSearchResponse.getRoundedObsCount(999));
		assertEquals(1000, TaxonSearchResponse.getRoundedObsCount(1000));
		assertEquals(1000, TaxonSearchResponse.getRoundedObsCount(1252));
		assertEquals(2000, TaxonSearchResponse.getRoundedObsCount(2632));
		assertEquals(7000, TaxonSearchResponse.getRoundedObsCount(7045));
		assertEquals(10000, TaxonSearchResponse.getRoundedObsCount(10045));
		assertEquals(15000, TaxonSearchResponse.getRoundedObsCount(15045));
		assertEquals(150000, TaxonSearchResponse.getRoundedObsCount(155045));
		assertEquals(120000, TaxonSearchResponse.getRoundedObsCount(123456));
		assertEquals(1230000, TaxonSearchResponse.getRoundedObsCount(1234567));
		assertEquals(12340000, TaxonSearchResponse.getRoundedObsCount(12345678));
	}

}
