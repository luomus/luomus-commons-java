package fi.luomus.java.tests.commons.taxonomy;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.commons.containers.Image;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.CategorizedTaxonImages;
import fi.luomus.commons.taxonomy.CategorizedTaxonImages.ImageCategory;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;

public class CategorizedTaxonImagesTests {

	private TaxonContainer container;

	@Before
	public void before() {
		container = new TaxonContainerStub() {
			@Override
			public Set<Qname> orderInformalTaxonGroups(Set<Qname> informalTaxonGroups) {
				List<Qname> ordered = new ArrayList<>(informalTaxonGroups);
				Collections.sort(ordered);
				return new LinkedHashSet<>(ordered);
			}
		};
	}

	@Test
	public void default_categories() {
		Taxon taxon = new Taxon(new Qname("MX.1"), container);
		CategorizedTaxonImages cat = new CategorizedTaxonImages(taxon);
		assertEquals(3, cat.getCategories().size());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=primary, title={en=Primary, fi=Pääkuva, sv=Primär}, subcategories=null, primary=true, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=null, side=null], subcategories=[], images=[]]",
				cat.getCategories().get(0).toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=general, title={en=Images, fi=Kuvia, sv=Bilder}, subcategories=null, primary=null, types=[MM.typeEnumLive, MM.typeEnumSpecimen], typeNot=null, sex=null, lifeStages=null, side=null], subcategories=[], images=[]]",
				cat.getCategories().get(1).toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=habitat, title={en=Habitat, fi=Habitaatti, sv=Habitat}, subcategories=null, primary=null, types=[MM.typeEnumHabitat], typeNot=null, sex=null, lifeStages=null, side=null], subcategories=[], images=[]]",
				cat.getCategories().get(2).toString());
	}

	@Test
	public void beetle_categories() {
		Taxon taxon = new Taxon(new Qname("MX.1"), container);
		taxon.addInformalTaxonGroup(new Qname("MVL.33")); // beetles
		taxon.addInformalTaxonGroup(new Qname("MVL.232")); // insecta
		assertEquals("[MVL.33, MVL.232]", taxon.getInformalTaxonGroups().toString());

		CategorizedTaxonImages cat = new CategorizedTaxonImages(taxon);
		assertEquals(6, cat.getCategories().size());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=primary, title={en=Primary, fi=Pääkuva, sv=Primär}, subcategories=null, primary=true, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=null, side=null], subcategories=[], images=[]]",
				cat.getCategories().get(0).toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=female, title={en=Female, fi=Naaras, sv=Hona}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumGenitalia, MM.typeEnumCarcass, MM.typeEnumSkeletal, MM.typeEnumHabitat, MM.typeEnumLabel], sex=MY.sexF, lifeStages=null, side=null], subcategories=[], images=[]]",
				cat.getCategories().get(1).toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=male, title={en=Male, fi=Koiras, sv=Hane}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumGenitalia, MM.typeEnumCarcass, MM.typeEnumSkeletal, MM.typeEnumHabitat, MM.typeEnumLabel], sex=MY.sexM, lifeStages=null, side=null], subcategories=[], images=[]]",
				cat.getCategories().get(2).toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=genitalia, title={en=Genitalia, fi=Genitaali, sv=Genitalier}, subcategories=SubCategoriesDef [defs=[SingleCategoryDef [id=genitalia_female, title={en=Female, fi=Naaras, sv=Hona}, subcategories=null, primary=null, types=[MM.typeEnumGenitalia], typeNot=null, sex=MY.sexF, lifeStages=null, side=null], SingleCategoryDef [id=genitalia_male, title={en=Male, fi=Koiras, sv=Hane}, subcategories=null, primary=null, types=[MM.typeEnumGenitalia], typeNot=null, sex=MY.sexM, lifeStages=null, side=null]]], primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=null], subcategories=[ImageCategory [def=SingleCategoryDef [id=genitalia_female, title={en=Female, fi=Naaras, sv=Hona}, subcategories=null, primary=null, types=[MM.typeEnumGenitalia], typeNot=null, sex=MY.sexF, lifeStages=null, side=null], subcategories=[], images=[]], ImageCategory [def=SingleCategoryDef [id=genitalia_male, title={en=Male, fi=Koiras, sv=Hane}, subcategories=null, primary=null, types=[MM.typeEnumGenitalia], typeNot=null, sex=MY.sexM, lifeStages=null, side=null], subcategories=[], images=[]]], images=[]]",
				cat.getCategories().get(3).toString());

		ImageCategory genitalia = cat.getCategories().get(3);
		assertEquals("{en=Genitalia, fi=Genitaali, sv=Genitalier}", genitalia.getTitle().toString());
		assertEquals(2, genitalia.getSubcategories().size());

		ImageCategory genitaliaFemale = genitalia.getSubcategories().get(0);
		ImageCategory genitaliaMale = genitalia.getSubcategories().get(1);
		assertEquals("ImageCategory [def=SingleCategoryDef [id=genitalia_female, title={en=Female, fi=Naaras, sv=Hona}, subcategories=null, primary=null, types=[MM.typeEnumGenitalia], typeNot=null, sex=MY.sexF, lifeStages=null, side=null], subcategories=[], images=[]]",
				genitaliaFemale.toString());
		assertEquals("ImageCategory [def=SingleCategoryDef [id=genitalia_male, title={en=Male, fi=Koiras, sv=Hane}, subcategories=null, primary=null, types=[MM.typeEnumGenitalia], typeNot=null, sex=MY.sexM, lifeStages=null, side=null], subcategories=[], images=[]]",
				genitaliaMale.toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=pre_adult, title={en=Life stages, fi=Elinvaiheet, sv=Livsstadier}, subcategories=SubCategoriesDef [defs=[SingleCategoryDef [id=pre_adult_egg, title={en=Egg, fi=Muna, sv=Ägg}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStageEgg], side=null], SingleCategoryDef [id=pre_adult_lava, title={en=Larva, fi=Toukka, sv=Larv}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStageLarva], side=null], SingleCategoryDef [id=pre_adult_pupa, title={en=Pupa, fi=Kotelo, sv=Puppa}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStagePupa], side=null]]], primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=null], subcategories=[ImageCategory [def=SingleCategoryDef [id=pre_adult_egg, title={en=Egg, fi=Muna, sv=Ägg}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStageEgg], side=null], subcategories=[], images=[]], ImageCategory [def=SingleCategoryDef [id=pre_adult_lava, title={en=Larva, fi=Toukka, sv=Larv}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStageLarva], side=null], subcategories=[], images=[]], ImageCategory [def=SingleCategoryDef [id=pre_adult_pupa, title={en=Pupa, fi=Kotelo, sv=Puppa}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStagePupa], side=null], subcategories=[], images=[]]], images=[]]",
				cat.getCategories().get(4).toString());

		ImageCategory lifeStages = cat.getCategories().get(4);
		assertEquals("{en=Life stages, fi=Elinvaiheet, sv=Livsstadier}", lifeStages.getTitle().toString());
		assertEquals(3, lifeStages.getSubcategories().size());

		ImageCategory lifestageEgg = lifeStages.getSubcategories().get(0);
		ImageCategory lifestageLarva = lifeStages.getSubcategories().get(1);
		ImageCategory lifestagePupa = lifeStages.getSubcategories().get(2);
		assertEquals("ImageCategory [def=SingleCategoryDef [id=pre_adult_egg, title={en=Egg, fi=Muna, sv=Ägg}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStageEgg], side=null], subcategories=[], images=[]]",
				lifestageEgg.toString());
		assertEquals("ImageCategory [def=SingleCategoryDef [id=pre_adult_lava, title={en=Larva, fi=Toukka, sv=Larv}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStageLarva], side=null], subcategories=[], images=[]]",
				lifestageLarva.toString());
		assertEquals("ImageCategory [def=SingleCategoryDef [id=pre_adult_pupa, title={en=Pupa, fi=Kotelo, sv=Puppa}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStagePupa], side=null], subcategories=[], images=[]]",
				lifestagePupa.toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=habitat_marks, title={en=Environment, fi=Ympäristössä, sv=Miljön}, subcategories=SubCategoriesDef [defs=[SingleCategoryDef [id=habitat_marks_habitat, title={en=Habitat, fi=Habitaatti, sv=Habitat}, subcategories=null, primary=null, types=[MM.typeEnumHabitat], typeNot=null, sex=null, lifeStages=null, side=null], SingleCategoryDef [id=habitat_marks_marks, title={en=Feeding marks, fi=Syömäjäljet, sv=Spår}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel, MM.typeEnumGenitalia, MM.typeEnumSkeletal, MM.typeEnumHabitat], sex=null, lifeStages=[MY.lifeStageMarks], side=null]]], primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=null], subcategories=[ImageCategory [def=SingleCategoryDef [id=habitat_marks_habitat, title={en=Habitat, fi=Habitaatti, sv=Habitat}, subcategories=null, primary=null, types=[MM.typeEnumHabitat], typeNot=null, sex=null, lifeStages=null, side=null], subcategories=[], images=[]], ImageCategory [def=SingleCategoryDef [id=habitat_marks_marks, title={en=Feeding marks, fi=Syömäjäljet, sv=Spår}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel, MM.typeEnumGenitalia, MM.typeEnumSkeletal, MM.typeEnumHabitat], sex=null, lifeStages=[MY.lifeStageMarks], side=null], subcategories=[], images=[]]], images=[]]",
				cat.getCategories().get(5).toString());
	}

	@Test
	public void butterly_categories() {
		Taxon taxon = new Taxon(new Qname("MX.1"), container);
		taxon.addInformalTaxonGroup(new Qname("MVL.31")); // butterflies
		assertEquals("[MVL.31]", taxon.getInformalTaxonGroups().toString());

		CategorizedTaxonImages cat = new CategorizedTaxonImages(taxon);
		assertEquals(6, cat.getCategories().size());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=primary, title={en=Primary, fi=Pääkuva, sv=Primär}, subcategories=null, primary=true, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=null, side=null], subcategories=[], images=[]]",
				cat.getCategories().get(0).toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=female, title={en=Female, fi=Naaras, sv=Hona}, subcategories=SubCategoriesDef [defs=[SingleCategoryDef [id=female_upside, title={en=Upside, fi=Päältä, sv=Fördel}, subcategories=null, primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=MM.sideUpside], SingleCategoryDef [id=female_downside, title={en=Downside, fi=Alta, sv=Nackdel}, subcategories=null, primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=MM.sideDownside]]], primary=null, types=null, typeNot=[MM.typeEnumGenitalia, MM.typeEnumCarcass, MM.typeEnumSkeletal, MM.typeEnumHabitat, MM.typeEnumLabel], sex=MY.sexF, lifeStages=null, side=null], subcategories=[ImageCategory [def=SingleCategoryDef [id=female_upside, title={en=Upside, fi=Päältä, sv=Fördel}, subcategories=null, primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=MM.sideUpside], subcategories=[], images=[]], ImageCategory [def=SingleCategoryDef [id=female_downside, title={en=Downside, fi=Alta, sv=Nackdel}, subcategories=null, primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=MM.sideDownside], subcategories=[], images=[]]], images=[]]",
				cat.getCategories().get(1).toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=male, title={en=Male, fi=Koiras, sv=Hane}, subcategories=SubCategoriesDef [defs=[SingleCategoryDef [id=male_upside, title={en=Upside, fi=Päältä, sv=Fördel}, subcategories=null, primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=MM.sideUpside], SingleCategoryDef [id=male_downside, title={en=Downside, fi=Alta, sv=Nackdel}, subcategories=null, primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=MM.sideDownside]]], primary=null, types=null, typeNot=[MM.typeEnumGenitalia, MM.typeEnumCarcass, MM.typeEnumSkeletal, MM.typeEnumHabitat, MM.typeEnumLabel], sex=MY.sexM, lifeStages=null, side=null], subcategories=[ImageCategory [def=SingleCategoryDef [id=male_upside, title={en=Upside, fi=Päältä, sv=Fördel}, subcategories=null, primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=MM.sideUpside], subcategories=[], images=[]], ImageCategory [def=SingleCategoryDef [id=male_downside, title={en=Downside, fi=Alta, sv=Nackdel}, subcategories=null, primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=MM.sideDownside], subcategories=[], images=[]]], images=[]]",
				cat.getCategories().get(2).toString());

		ImageCategory femaleSides = cat.getCategories().get(1);
		assertEquals(2, femaleSides.getSubcategories().size());
		assertEquals("ImageCategory [def=SingleCategoryDef [id=female_upside, title={en=Upside, fi=Päältä, sv=Fördel}, subcategories=null, primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=MM.sideUpside], subcategories=[], images=[]]",
				femaleSides.getSubcategories().get(0).toString());
		assertEquals("ImageCategory [def=SingleCategoryDef [id=female_downside, title={en=Downside, fi=Alta, sv=Nackdel}, subcategories=null, primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=MM.sideDownside], subcategories=[], images=[]]",
				femaleSides.getSubcategories().get(1).toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=genitalia, title={en=Genitalia, fi=Genitaali, sv=Genitalier}, subcategories=SubCategoriesDef [defs=[SingleCategoryDef [id=genitalia_female, title={en=Female, fi=Naaras, sv=Hona}, subcategories=null, primary=null, types=[MM.typeEnumGenitalia], typeNot=null, sex=MY.sexF, lifeStages=null, side=null], SingleCategoryDef [id=genitalia_male, title={en=Male, fi=Koiras, sv=Hane}, subcategories=null, primary=null, types=[MM.typeEnumGenitalia], typeNot=null, sex=MY.sexM, lifeStages=null, side=null]]], primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=null], subcategories=[ImageCategory [def=SingleCategoryDef [id=genitalia_female, title={en=Female, fi=Naaras, sv=Hona}, subcategories=null, primary=null, types=[MM.typeEnumGenitalia], typeNot=null, sex=MY.sexF, lifeStages=null, side=null], subcategories=[], images=[]], ImageCategory [def=SingleCategoryDef [id=genitalia_male, title={en=Male, fi=Koiras, sv=Hane}, subcategories=null, primary=null, types=[MM.typeEnumGenitalia], typeNot=null, sex=MY.sexM, lifeStages=null, side=null], subcategories=[], images=[]]], images=[]]",
				cat.getCategories().get(3).toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=pre_adult, title={en=Life stages, fi=Elinvaiheet, sv=Livsstadier}, subcategories=SubCategoriesDef [defs=[SingleCategoryDef [id=pre_adult_egg, title={en=Egg, fi=Muna, sv=Ägg}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStageEgg], side=null], SingleCategoryDef [id=pre_adult_lava, title={en=Larva, fi=Toukka, sv=Larv}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStageLarva], side=null], SingleCategoryDef [id=pre_adult_pupa, title={en=Pupa, fi=Kotelo, sv=Puppa}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStagePupa], side=null]]], primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=null], subcategories=[ImageCategory [def=SingleCategoryDef [id=pre_adult_egg, title={en=Egg, fi=Muna, sv=Ägg}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStageEgg], side=null], subcategories=[], images=[]], ImageCategory [def=SingleCategoryDef [id=pre_adult_lava, title={en=Larva, fi=Toukka, sv=Larv}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStageLarva], side=null], subcategories=[], images=[]], ImageCategory [def=SingleCategoryDef [id=pre_adult_pupa, title={en=Pupa, fi=Kotelo, sv=Puppa}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel], sex=null, lifeStages=[MY.lifeStagePupa], side=null], subcategories=[], images=[]]], images=[]]",
				cat.getCategories().get(4).toString());

		assertEquals("ImageCategory [def=SingleCategoryDef [id=habitat_marks, title={en=Environment, fi=Ympäristössä, sv=Miljön}, subcategories=SubCategoriesDef [defs=[SingleCategoryDef [id=habitat_marks_habitat, title={en=Habitat, fi=Habitaatti, sv=Habitat}, subcategories=null, primary=null, types=[MM.typeEnumHabitat], typeNot=null, sex=null, lifeStages=null, side=null], SingleCategoryDef [id=habitat_marks_marks, title={en=Feeding marks, fi=Syömäjäljet, sv=Spår}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel, MM.typeEnumGenitalia, MM.typeEnumSkeletal, MM.typeEnumHabitat], sex=null, lifeStages=[MY.lifeStageMarks], side=null]]], primary=null, types=null, typeNot=null, sex=null, lifeStages=null, side=null], subcategories=[ImageCategory [def=SingleCategoryDef [id=habitat_marks_habitat, title={en=Habitat, fi=Habitaatti, sv=Habitat}, subcategories=null, primary=null, types=[MM.typeEnumHabitat], typeNot=null, sex=null, lifeStages=null, side=null], subcategories=[], images=[]], ImageCategory [def=SingleCategoryDef [id=habitat_marks_marks, title={en=Feeding marks, fi=Syömäjäljet, sv=Spår}, subcategories=null, primary=null, types=null, typeNot=[MM.typeEnumCarcass, MM.typeEnumLabel, MM.typeEnumGenitalia, MM.typeEnumSkeletal, MM.typeEnumHabitat], sex=null, lifeStages=[MY.lifeStageMarks], side=null], subcategories=[], images=[]]], images=[]]",
				cat.getCategories().get(5).toString());

		Image femaleUpsideImage = new Image(new Qname("MM.1"), new Qname("KE.3"), "http://some.url/f_upside.jpg");
		femaleUpsideImage.addSex(new Qname("MY.sexF"));
		femaleUpsideImage.setSide(new Qname("MM.sideUpside"));

		Image femaleDownsideImage = new Image(new Qname("MM.2"), new Qname("KE.3"), "http://some.url/f_downside.jpg");
		femaleDownsideImage.addSex(new Qname("MY.sexF"));
		femaleDownsideImage.setSide(new Qname("MM.sideDownside"));
		femaleDownsideImage.setType(new Qname("MM.typeEnumLive"));

		Image femaleNoSideImage = new Image(new Qname("MM.3"), new Qname("KE.3"), "http://some.url/f_no_side.jpg");
		femaleNoSideImage.addSex(new Qname("MY.sexF"));

		Image primaryImage = new Image(new Qname("MM.4"), new Qname("KE.3"), "http://some.url/primary.jpg");
		primaryImage.addSex(new Qname("MY.sexM"));
		primaryImage.setType(new Qname("MM.typeEnumLive"));
		primaryImage.addPrimaryForTaxon(taxon.getId());

		Image genitaliaFemale = new Image(new Qname("MM.5"), new Qname("KE.3"), "http://some.url/f_genitalia.jpg");
		genitaliaFemale.addSex(new Qname("MY.sexF"));
		genitaliaFemale.setType(new Qname("MM.typeEnumGenitalia"));

		taxon.addCopyForSelf(femaleUpsideImage);
		taxon.addCopyForSelf(femaleNoSideImage);
		taxon.addCopyForSelf(femaleDownsideImage);
		taxon.addCopyForSelf(primaryImage);
		taxon.addCopyForSelf(genitaliaFemale);

		cat = new CategorizedTaxonImages(taxon);

		String images = imagesToString(cat);
		String expected = ""+
				"Primary: [Image [fullURL=http://some.url/primary.jpg]]\n" +
				"Female-Upside: [Image [fullURL=http://some.url/f_upside.jpg], Image [fullURL=http://some.url/f_no_side.jpg]]\n" +
				"Female-Downside: [Image [fullURL=http://some.url/f_downside.jpg]]\n" +
				"Genitalia-Female: [Image [fullURL=http://some.url/f_genitalia.jpg]]";
		assertEquals(expected, images);

		// test doesn't swap if repeated calls..:
		cat = new CategorizedTaxonImages(taxon);

		images = imagesToString(cat);
		assertEquals(expected, images);

		List<Image> flatted = cat.getGroupedFlatImages();
		assertEquals(""+
				"http://some.url/primary.jpg\n" +
				"http://some.url/f_upside.jpg\n" +
				"http://some.url/f_no_side.jpg\n" +
				"http://some.url/f_downside.jpg\n" +
				"http://some.url/f_genitalia.jpg",
				imagesToString(flatted));
	}

	private String imagesToString(CategorizedTaxonImages cat) {
		List<String> withImages = new ArrayList<>();
		for (ImageCategory c : cat.getCategories()) {
			if (!c.getImages().isEmpty()) {
				withImages.add(c.getTitle().forLocale("en") + ": "+c.getImages());
			}
			for (ImageCategory subc : c.getSubcategories()) {
				if (!subc.getImages().isEmpty()) {
					withImages.add(c.getTitle().forLocale("en") + "-"+subc.getTitle().forLocale("en")+": "+subc.getImages());
				}
			}
		}
		if (!cat.getUncategorizedImages().isEmpty()) {
			withImages.add("Uncategorized: "+cat.getUncategorizedImages());
		}
		String images = withImages.stream().collect(Collectors.joining("\n"));
		return images;
	}

	private String imagesToString(List<Image> flatted) {
		return flatted.stream().map(i->i.getFullURL()).collect(Collectors.joining("\n"));
	}

	@Test
	public void no_carcass_images_for_any_informal_group() {
		for (int i = 1; i<50000; i++) {
			Qname informalGroupId = new Qname("MVL." +i);
			noCarcassImagesFor(informalGroupId);
		}

	}

	@Test
	public void no_label_images_for_any_informal_group() {
		for (int i = 1; i<50000; i++) {
			Qname informalGroupId = new Qname("MVL." +i);
			noLabelImagesFor(informalGroupId);
		}

	}

	private void noLabelImagesFor(Qname informalGroupId) {
		Taxon taxon = new Taxon(new Qname("MX.1"), container);
		taxon.addInformalTaxonGroup(informalGroupId);

		Image carcassImage = new Image(new Qname("MM.1"), new Qname("KE.3"), "http://some.url/1.jpg");
		carcassImage.addKeyword("label");
		carcassImage.doLegacyConversions();
		assertEquals("MM.typeEnumLabel", carcassImage.getType().toString());

		taxon.addCopyForSelf(carcassImage);

		CategorizedTaxonImages cat = new CategorizedTaxonImages(taxon);
		for (ImageCategory c : cat.getCategories()) {
			assertEquals("For category " + c.getTitle(), 0, c.getImages().size());
		}
		assertEquals(0, cat.getUncategorizedImages().size());
	}

	private void noCarcassImagesFor(Qname informalGroupId) {
		Taxon taxon = new Taxon(new Qname("MX.1"), container);
		taxon.addInformalTaxonGroup(informalGroupId);

		assertEquals("["+informalGroupId+"]", taxon.getInformalTaxonGroups().toString());

		Image carcassImage = new Image(new Qname("MM.1"), new Qname("KE.3"), "http://some.url/1.jpg");
		carcassImage.addKeyword("carcass");
		carcassImage.doLegacyConversions();
		assertEquals("MM.typeEnumCarcass", carcassImage.getType().toString());

		taxon.addCopyForSelf(carcassImage);

		CategorizedTaxonImages cat = new CategorizedTaxonImages(taxon);

		for (ImageCategory c : cat.getCategories()) {
			assertEquals("For category " + c.getTitle(), 0, c.getImages().size());
		}
		assertEquals(0, cat.getUncategorizedImages().size());
	}

}
