package fi.luomus.java.tests.commons.taxonomy;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;
import fi.luomus.commons.containers.AdministrativeStatus;
import fi.luomus.commons.containers.Area;
import fi.luomus.commons.containers.Checklist;
import fi.luomus.commons.containers.ContentContextDescription;
import fi.luomus.commons.containers.ContentGroups.ContentGroup;
import fi.luomus.commons.containers.InformalTaxonGroup;
import fi.luomus.commons.containers.OccurrenceType;
import fi.luomus.commons.containers.Person;
import fi.luomus.commons.containers.Publication;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;
import fi.luomus.commons.taxonomy.TaxonSearch;
import fi.luomus.commons.taxonomy.TaxonSearchResponse;
import fi.luomus.commons.taxonomy.TaxonomyDAOBaseImple;

public class TaxonomyDaoTests {

	private static class TestDao extends TaxonomyDAOBaseImple {
		public TestDao(Config config, int longCacheTime, int shortCacheTime) {
			super(config, longCacheTime, shortCacheTime);
		}
		@Override
		public Taxon getTaxon(Qname qname) { throw new UnsupportedOperationException(); }
		@Override
		public TaxonSearchResponse search(TaxonSearch taxonSearch) throws Exception  { throw new UnsupportedOperationException(); }
		@Override
		public TaxonContainer getTaxonContainer() throws Exception  { throw new UnsupportedOperationException(); }
	}

	private final Config config = getConfig();
	private final TestDao dao = new TestDao(config, 5, 1);

	private Config getConfig() {
		try {
			return new ConfigReader(this.getClass().getResourceAsStream("secrets.properties"));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Test
	public void getAdministrativeStatuses() {
		AdministrativeStatus s = dao.getAdministrativeStatuses().get("MX.finlex160_1997_appendix4_2021");
		assertEquals("MX.finlex160_1997_appendix4_2021", s.getQname().toString());
		assertEquals("Uhanalaiset lajit (LSA 2023/1066, liite 6)", s.getName().forLocale("fi"));
		assertEquals("Hotade arter (NVF 2023/1066, bilaga 6)", s.getName("sv"));
	}

	@Test
	public void getAreas() {
		Area area = dao.getAreas().get("ML.206");
		assertEquals("ML.206", area.getQname().toString());
		assertEquals("Suomi", area.getName().forLocale("fi"));
		assertEquals("Finland", area.getName().forLocale("en"));
		assertEquals("ML.country", area.getType().toString());
		assertEquals(null, area.getAbbreviation());

		area = dao.getAreas().get("ML.271");
		assertEquals("Inarin Lappi", area.getName().forLocale("fi"));
		assertEquals("ML.206", area.getPartOf().toString());
		assertEquals("ML.biogeographicalProvince", area.getType().toString());
		assertEquals("InL", area.getAbbreviation());
	}

	@Test
	public void getChecklists() {
		Checklist c = dao.getChecklists().get("MR.1");
		assertEquals("FinBIF master checklist", c.getFullname("en"));
		assertEquals("MX.37600", c.getRootTaxon().toString());
	}

	@Test
	public void getContentContextDescriptions() {
		ContentContextDescription desc = dao.getContentContextDescriptions().get(null);
		assertEquals(null, desc.getQname());
		assertEquals("Laji.fi -lajikuvaukset", desc.getName("fi"));

		desc = dao.getContentContextDescriptions().get("LA.100");
		assertEquals("LA.100", desc.getQname().toString());
		assertEquals("Pinkka oppimisympäristö: BIO-503 Suomen kasvisto ja kasvillisuus - Sisävedet: rantaniityt", desc.getName("fi"));
	}

	@Test
	public void getContentGroups() {
		ContentGroup g = dao.getContentGroups().iterator().next();
		assertEquals("Yleistä", g.getTitle().forLocale("fi"));
		assertEquals("Ingressi", g.iterator().next().getTitle().forLocale("fi"));
	}

	@Test
	public void getInformalTaxonGroups() {
		InformalTaxonGroup g = dao.getInformalTaxonGroups().get("MVL.1");
		assertEquals("Linnut", g.getName("fi"));
	}

	@Test
	public void getLabels() {
		assertEquals("superdomain", dao.getLabels(new Qname("MX.superdomain")).forLocale("en"));
		assertEquals("Koko nimi", dao.getLabels(new Qname("MA.fullName")).forLocale("fi"));
	}

	@Test
	public void getOccurrenceTypes() {
		OccurrenceType type = dao.getOccurrenceTypes().get("MX.typeOfOccurrenceOccurs");
		assertEquals("Occurs", type.getName("en"));
	}

	@Test
	public void getPersons() {
		Person p = dao.getPersons().get("MA.5");
		assertEquals("Esko Piirainen", p.getFullname());
	}

	@Test
	public void getPublications() {
		Publication p = dao.getPublications().get("MP.1041");
		assertEquals("BirdLife Suomi ry., Suomessa tavatut lintulajit, http://www.birdlife.fi/havainnot/rk/suomessa_tavatut_lintulajit.shtml, 8.3.2015", p.getCitation());
		assertEquals("https://www.birdlife.fi/lintutieto/suomessa-havaitut-lintulajit/", p.getURI());
	}

	@Test
	public void getTaxonRanks() {
		Qname first = dao.getTaxonRanks().iterator().next();
		assertEquals("MX.superdomain", first.toString());
	}

	@Test
	public void getLicenseFullNames() {
		assertEquals(""+
				"[MZ.intellectualRightsCC-BY-SA-4.0, MZ.intellectualRightsCC-BY-NC-4.0, MZ.intellectualRightsCC-BY-NC-SA-4.0, MZ.intellectualRightsCC-BY-4.0, MZ.intellectualRightsCC0-4.0, MZ.intellectualRightsODBL-1.0, MZ.intellectualRightsPD, MZ.intellectualRightsARR, MZ.intellectualRightsCC-BY-2.0, MZ.intellectualRightsCC-BY-SA-2.0, MZ.intellectualRightsCC-BY-SA-2.0-DE, MZ.intellectualRightsCC-BY-NC-2.0, MZ.intellectualRightsCC-BY-NC-SA-2.0, MZ.intellectualRightsCC-BY-NC-ND-2.0, MZ.intellectualRightsCC-BY-SA-2.5, MZ.intellectualRightsCC-BY-SA-2.5-SE, MZ.intellectualRightsCC-BY-3.0, MZ.intellectualRightsCC-BY-SA-3.0, MZ.intellectualRightsCC-BY-NC-SA-3.0, MZ.intellectualRightsCC-BY-ND-4.0, MZ.intellectualRightsCC-BY-NC-ND-4.0, MY.intellectualRightsCC-BY, MY.intellectualRightsCC0]",
				dao.getLicenseFullnames().keySet().toString());

		assertEquals("{en=Creative Commons Attribution Share-Alike 4.0, fi=Creative Commons Nimeä Jaa-Samoin 4.0, sv=Creative Commons erkännande, dela lika 4.0}",
				dao.getLicenseFullnames().get("MZ.intellectualRightsCC-BY-SA-4.0").toString());
	}

	@Test
	public void getAtlLabels() {
		assertEquals("" +
				"{MM.sideUpside={en=Upside, fi=Päältä, sv=Fördel}, MM.sideDownside={en=Downside, fi=Alta, sv=Nackdel}}",
				dao.getAlt(new Qname("MM.sideEnum")).toString());
	}

}
