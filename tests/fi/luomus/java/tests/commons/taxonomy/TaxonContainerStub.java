package fi.luomus.java.tests.commons.taxonomy;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import fi.luomus.commons.containers.LocalizedText;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.taxonomy.Filter;
import fi.luomus.commons.taxonomy.NoSuchTaxonException;
import fi.luomus.commons.taxonomy.Taxon;
import fi.luomus.commons.taxonomy.TaxonContainer;

public class TaxonContainerStub implements TaxonContainer {

	@Override
	public Taxon getTaxon(Qname taxonId) throws NoSuchTaxonException {
		// Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasTaxon(Qname taxonId) {
		// Auto-generated method stub
		return false;
	}

	@Override
	public Set<Qname> getChildren(Qname parentId) {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public Qname getSynonymParent(Qname synonymId) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Filter getInformalGroupFilter() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Filter getRedListEvaluationGroupFilter() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Filter getAdminStatusFilter() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Filter getRedListStatusFilter() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Filter getTypesOfOccurrenceFilter() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public Set<Qname> getInvasiveSpeciesFilter() {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public Set<Qname> getInvasiveSpeciesEarlyWarningFilter() {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public Set<Qname> getHasDescriptionsFilter() {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public Set<Qname> getHasMediaFilter() throws UnsupportedOperationException {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public int getNumberOfTaxa() throws UnsupportedOperationException {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public Set<Qname> getRedListEvaluationGroupsOfInformalTaxonGroup(Qname informalTaxonGroupId) {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public Set<Qname> getRedListEvaluationGroupsOfTaxon(Qname taxonId) {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public Set<Qname> orderInformalTaxonGroups(Set<Qname> informalTaxonGroups) {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public Set<Qname> orderRedListEvaluationGroups(Set<Qname> groups) {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public Set<Qname> orderAdministrativeStatuses(Set<Qname> administrativeStatuses) {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public Set<Qname> getParentInformalTaxonGroups(Qname groupId) {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public Collection<Taxon> getAll() {
		// Auto-generated method stub
		return Collections.emptyList();
	}

	@Override
	public int getLatestLockedRedListEvaluationYear() throws UnsupportedOperationException {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public Set<Qname> getParentRedListEvaluationGroups(Qname groupId) {
		// Auto-generated method stub
		return Collections.emptySet();
	}

	@Override
	public LocalizedText getInformalTaxonGroupNames(Set<Qname> informalGroups) {
		// Auto-generated method stub
		return null;
	}

	@Override
	public LocalizedText getAdministrativeStatusNames(Set<Qname> administrativeStatuses) {
		// Auto-generated method stub
		return null;
	}

}
