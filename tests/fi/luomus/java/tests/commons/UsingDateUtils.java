package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

import fi.luomus.commons.containers.DateValue;
import fi.luomus.commons.utils.DateUtils;

public class UsingDateUtils {

	@Test
	public void test__validate_iso_date() {
		assertTrue(DateUtils.validIsoDateTime("2015"));
		assertTrue(DateUtils.validIsoDateTime("2014-03"));
		assertTrue(DateUtils.validIsoDateTime("2014-03-15"));
		assertTrue(DateUtils.validIsoDateTime("2012-02-01T09:23:35"));
		assertTrue(DateUtils.validIsoDateTime("20"));
		assertTrue(DateUtils.validIsoDateTime("201"));
		
		assertFalse(DateUtils.validIsoDateTime("a"));
		assertFalse(DateUtils.validIsoDateTime("20111215213007"));
		assertFalse(DateUtils.validIsoDateTime("12345678"));
		assertFalse(DateUtils.validIsoDateTime("11-11-2011"));
		assertFalse(DateUtils.validIsoDateTime("2012-02-01T09:23:35a"));
		assertFalse(DateUtils.validIsoDateTime("2012-02-01T09:23:35:322"));
		assertFalse(DateUtils.validIsoDateTime("2012-02-01T09:23:35.322"));
		assertFalse(DateUtils.validIsoDateTime("2012-02-01T09"));
		assertFalse(DateUtils.validIsoDateTime("2012-02-01T09:25"));
		assertFalse(DateUtils.validIsoDateTime("2012-02-01T"));
		assertFalse(DateUtils.validIsoDateTime("2012-02-01a"));
		assertFalse(DateUtils.validIsoDateTime("2012-02-01Ta"));
		assertFalse(DateUtils.validIsoDateTime("2012-01T09:23:35"));
	}
	
	@Test
	public void test___to_years_months_days() {
		DateValue actual = DateUtils.getYearsMonthsDays(new DateValue(1, 1, 2000), new DateValue(1, 1, 2000));
		assertEquals("0", actual.getYear());
		assertEquals("0", actual.getMonth());
		assertEquals("0", actual.getDay());
	}

	@Test
	public void test___to_years_months_days_1() {
		DateValue actual = DateUtils.getYearsMonthsDays(new DateValue(1, 1, 2000), new DateValue(2, 1, 2000));
		assertEquals("0", actual.getYear());
		assertEquals("0", actual.getMonth());
		assertEquals("1", actual.getDay());
	}
	
	@Test
	public void test___to_years_months_days_2() {
		DateValue actual = DateUtils.getYearsMonthsDays(new DateValue(1, 1, 2000), new DateValue(6, 2, 2000));
		assertEquals("0", actual.getYear());
		assertEquals("1", actual.getMonth());
		assertEquals("5", actual.getDay());
	}

	@Test
	public void test___to_years_months_days_3() {
		DateValue actual = DateUtils.getYearsMonthsDays(new DateValue(1, 1, 2000), new DateValue(31, 12, 2000));
		assertEquals("0", actual.getYear());
		assertEquals("11", actual.getMonth());
		assertEquals("30", actual.getDay());
	}

	@Test
	public void test___to_years_months_days_3_2() {
		DateValue actual = DateUtils.getYearsMonthsDays(new DateValue(1, 1, 2000), new DateValue(1, 1, 2001));
		assertEquals("1", actual.getYear());
		assertEquals("0", actual.getMonth());
		assertEquals("0", actual.getDay());
	}
	
	@Test
	public void test___to_years_months_days_4() {
		DateValue actual = DateUtils.getYearsMonthsDays(new DateValue(1, 1, 2000), new DateValue(2, 1, 2001));
		assertEquals("1", actual.getYear());
		assertEquals("0", actual.getMonth());
		assertEquals("1", actual.getDay());
	}

	@Test
	public void test___to_years_months_days_5() {
		DateValue actual = DateUtils.getYearsMonthsDays(new DateValue(22, 10, 1982), new DateValue(22, 10, 1983));
		assertEquals("1", actual.getYear());
		assertEquals("0", actual.getMonth());
		assertEquals("0", actual.getDay());
	}
	
	@Test
	public void test___to_years_months_days_6() {
		DateValue actual = DateUtils.getYearsMonthsDays(new DateValue(22, 10, 1982), new DateValue(23, 10, 1982));
		assertEquals("0", actual.getYear());
		assertEquals("0", actual.getMonth());
		assertEquals("1", actual.getDay());
	}
	
	@Test
	public void test___to_years_months_days_7() {
		DateValue actual = DateUtils.getYearsMonthsDays(new DateValue(22, 10, 1982), new DateValue(1, 11, 1982));
		assertEquals("0", actual.getYear());
		assertEquals("0", actual.getMonth());
		assertEquals("10", actual.getDay());
	}
	
	@Test
	public void test___humanize() {
		String actual = DateUtils.humanize(new DateValue(19, 9, 1), DateUtils.FI);
		assertEquals("1 vuosi 9 kuukautta 19 päivää", actual);
	}

	@Test
	public void test___humanize_2() {
		String actual = DateUtils.humanize(new DateValue(2, 1, 2), DateUtils.FI);
		assertEquals("2 vuotta 1 kuukausi 2 päivää", actual);
	}

	@Test
	public void test___humanize_3() {
		String actual = DateUtils.humanize(new DateValue(1, null, null), DateUtils.FI);
		assertEquals("1 päivä", actual);
	}

	@Test
	public void test___humanize_4() {
		String actual = DateUtils.humanize(new DateValue("1", "", "0"), DateUtils.FI);
		assertEquals("1 päivä", actual);
	}

	@Test
	public void test___humanize_5() {
		String actual = DateUtils.humanize(new DateValue(1, 1, 0), DateUtils.FI);
		assertEquals("1 kuukausi 1 päivä", actual);
	}

	@Test
	public void test___humanize_6() {
		String actual = DateUtils.humanize(new DateValue(0, 1, 3), DateUtils.FI);
		assertEquals("3 vuotta 1 kuukausi", actual);
	}

	@Test
	public void test___humanize_en() {
		String actual = DateUtils.humanize(new DateValue(19, 9, 1), DateUtils.EN);
		assertEquals("1 year 9 months 19 days", actual);
	}

	@Test
	public void test___humanize_2_en() {
		String actual = DateUtils.humanize(new DateValue(2, 1, 2), DateUtils.EN);
		assertEquals("2 years 1 month 2 days", actual);
	}

	@Test
	public void test___humanize_3_en() {
		String actual = DateUtils.humanize(new DateValue(1, null, null), DateUtils.EN);
		assertEquals("1 day", actual);
	}

	@Test
	public void test___humanize_4_en() {
		String actual = DateUtils.humanize(new DateValue("1", "", "0"), DateUtils.EN);
		assertEquals("1 day", actual);
	}

	@Test
	public void test___humanize_5_en() {
		String actual = DateUtils.humanize(new DateValue(1, 1, 0), DateUtils.EN);
		assertEquals("1 month 1 day", actual);
	}

	@Test
	public void test___humanize_6_en() {
		String actual = DateUtils.humanize(new DateValue(0, 1, 3), DateUtils.EN);
		assertEquals("3 years 1 month", actual);
	}

	@Test
	public void test___humanize_sv() {
		String actual = DateUtils.humanize(new DateValue(19, 9, 1), DateUtils.SV);
		assertEquals("1 år 9 månader 19 dagar", actual);
	}

	@Test
	public void test___humanize_2_sv() {
		String actual = DateUtils.humanize(new DateValue(2, 1, 2), DateUtils.SV);
		assertEquals("2 år 1 månad 2 dagar", actual);
	}

	@Test
	public void test___humanize_3_sv() {
		String actual = DateUtils.humanize(new DateValue(1, null, null), DateUtils.SV);
		assertEquals("1 dagen", actual);
	}

	@Test
	public void test___humanize_4_sv() {
		String actual = DateUtils.humanize(new DateValue("1", "", "0"), DateUtils.SV);
		assertEquals("1 dagen", actual);
	}

	@Test
	public void test___humanize_5_sv() {
		String actual = DateUtils.humanize(new DateValue(1, 1, 0), DateUtils.SV);
		assertEquals("1 månad 1 dagen", actual);
	}

	@Test
	public void test___humanize_6_sv() {
		String actual = DateUtils.humanize(new DateValue(0, 1, 3), DateUtils.SV);
		assertEquals("3 år 1 månad", actual);
	}

	@Test
	public void test___generating_epoch() {
		long epoch = DateUtils.getEpoch(1, 1, 1970);
		assertEquals(0, epoch/1000/60/60/24);

		epoch = DateUtils.getEpoch(1, 1, 2011);
		assertEquals(1293832800L, epoch); 
	}

	@Test
	public void test_convert_to_date_value_finnish_format() {
		DateValue date = DateUtils.convertToDateValue("5.10.2009");
		assertEquals("05", date.getDay());
		assertEquals("10", date.getMonth());
		assertEquals("2009", date.getYear());

		date = DateUtils.convertToDateValue((String)null);
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue("");
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue(".");
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue("..");
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue("...");
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue("5..2009");
		assertEquals("05", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("2009", date.getYear());

		date = DateUtils.convertToDateValue("asdada");
		assertEquals("asdada", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue("1.2.3.4.5.6.7.8");
		assertEquals("01", date.getDay());
		assertEquals("02", date.getMonth());
		assertEquals("3", date.getYear());
	}

	@Test
	public void test_convert_to_date_value_us_format() {
		DateValue date = DateUtils.convertToDateValue("2009-10-05", "yyyy-MM-dd");
		assertEquals("05", date.getDay());
		assertEquals("10", date.getMonth());
		assertEquals("2009", date.getYear());

		date = DateUtils.convertToDateValue("2009-10-5", "yyyy-MM-dd");
		assertEquals("05", date.getDay());
		assertEquals("10", date.getMonth());
		assertEquals("2009", date.getYear());

		date = DateUtils.convertToDateValue(null, "yyyy-MM-dd");
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue("", "yyyy-MM-dd");
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue("-", "yyyy-MM-dd");
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue("--", "yyyy-MM-dd");
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue("---", "yyyy-MM-dd");
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("", date.getYear());

		date = DateUtils.convertToDateValue("2009--5", "yyyy-MM-dd");
		assertEquals("05", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("2009", date.getYear());

		date = DateUtils.convertToDateValue("asdada", "yyyy-MM-dd");
		assertEquals("", date.getDay());
		assertEquals("", date.getMonth());
		assertEquals("asdada", date.getYear());

		date = DateUtils.convertToDateValue("1-2-3-4-5-6-7-8", "yyyy-MM-dd");
		assertEquals("03", date.getDay());
		assertEquals("02", date.getMonth());
		assertEquals("1", date.getYear());
	}

	@Test
	public void test_convert_to_date_value_format_2() {
		DateValue date = DateUtils.convertToDateValue("2009-10", "yyyy-MM");
		assertEquals("", date.getDay());
		assertEquals("10", date.getMonth());
		assertEquals("2009", date.getYear());

		date = DateUtils.convertToDateValue("2009.10", "yyyy.MM");
		assertEquals("", date.getDay());
		assertEquals("10", date.getMonth());
		assertEquals("2009", date.getYear());

		date = DateUtils.convertToDateValue("10/2009", "MM/yyyy");
		assertEquals("", date.getDay());
		assertEquals("10", date.getMonth());
		assertEquals("2009", date.getYear());

		date = DateUtils.convertToDateValue("1.10/2009", "MM/yyyy");
		assertEquals("", date.getDay());
		assertEquals("1.10", date.getMonth());
		assertEquals("2009", date.getYear());

		try {
			date = DateUtils.convertToDateValue("1.10/2009", "dd.MM/yyyy");
		} catch (UnsupportedOperationException e) {
			assertEquals("Not implemented for date format dd.MM/yyyy", e.getMessage());
		}
	}

	@Test
	public void test_dateString() {
		assertEquals("05.02.2042", DateUtils.catenateDateString("5", "2", "2042"));
	}

	@Test
	public void test_format_datevalue() {
		assertEquals("2013-01-15", DateUtils.format(new DateValue("15", "1", "2013"), "yyyy-MM-dd"));
	}
	
	
	@Test
	public void test___day_of_year() {
		assertEquals(1, DateUtils.getDayOfYear(new DateValue("1", "1", "2011")));
		assertEquals(2, DateUtils.getDayOfYear(new DateValue("2", "1", "2011")));
		assertEquals(31, DateUtils.getDayOfYear(new DateValue("31", "1", "2011")));
		assertEquals(32, DateUtils.getDayOfYear(new DateValue("1", "2", "2011")));
		assertEquals(365, DateUtils.getDayOfYear(new DateValue("31", "12", "2011")));
		assertEquals(366, DateUtils.getDayOfYear(new DateValue("31", "12", "2012"))); // 2012 is leap year
		try {
			DateUtils.getDayOfYear(new DateValue("32", "1", "2011"));
			fail();
		} catch (IllegalArgumentException e) {}
		try {
			DateUtils.getDayOfYear(new DateValue("29", "2", "2011")); // feb has 28 days in 2011
			fail();
		} catch (IllegalArgumentException e) {}
	}
	
	@Test
	public void test_convert_date_to_datevaluse() {
		DateValue dateValue = DateUtils.convertToDateValue(new Date(1446454677L*1000));
		assertEquals(2015, dateValue.getYearAsInt());
		assertEquals(11, dateValue.getMonthAsInt());
		assertEquals(2, dateValue.getDayAsInt());
		
	}
	
	@Test
	public void datevalue_1() {
		Date date = new Date(1447854679*1000L);
		DateValue dateValue = DateUtils.convertToDateValue(date);
		assertEquals("2015", dateValue.getYear());
		assertEquals("11", dateValue.getMonth());
		assertEquals("18", dateValue.getDay());
		assertEquals(18, dateValue.getDayAsInt());
		assertEquals(11, dateValue.getMonthAsInt());
		assertEquals(2015, dateValue.getYearAsInt());
		assertEquals(1447797600, DateUtils.getEpoch(dateValue));
	}
	
	@Test
	public void datevalue_2() {
		DateValue d1 = new DateValue(1, 2, 1900);
		DateValue d2 =  new DateValue("1", "2", "1900");
		DateValue d3 = new DateValue("01", "02", "1900");
		
		assertEquals("01.02.1900", d1.toString());
		assertEquals(d1.toString(), d2.toString());
		assertEquals(d2.toString(), d3.toString());
		
		assertEquals("1", d1.getDay());
		assertEquals("1", d2.getDay());
		assertEquals("01", d3.getDay());
		
		assertEquals(1, d1.getDayAsInt());
		assertEquals(1, d2.getDayAsInt());
		assertEquals(1, d3.getDayAsInt());
	}
	
	
	@Test
	public void dayofweek() throws ParseException {
		Date monday = DateUtils.convertToDate("23.11.2015"); 
		Date friday = DateUtils.convertToDate("27.11.2015");
		Date sunday = DateUtils.convertToDate("29.11.2015");
		assertEquals(1, DateUtils.getDayOfWeek(monday));
		assertEquals(5, DateUtils.getDayOfWeek(friday));
		assertEquals(7, DateUtils.getDayOfWeek(sunday));
	}
	
}