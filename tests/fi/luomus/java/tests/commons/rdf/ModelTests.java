package fi.luomus.java.tests.commons.rdf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.List;

import org.junit.Test;

import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.ObjectLiteral;
import fi.luomus.commons.containers.rdf.ObjectResource;
import fi.luomus.commons.containers.rdf.Predicate;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.Statement;
import fi.luomus.commons.containers.rdf.Subject;
import fi.luomus.commons.utils.Utils;

public class ModelTests {

	@Test
	public void qname_with_so_called_default_prefix() {
		assertEquals("MX.1", new Qname("tun:MX.1").toString());
		assertEquals("MX.1", new ObjectResource("tun:MX.1").getQname());
	}

	@Test
	public void qname_behaviour_1() {
		assertEquals(null, new Qname(null).toURI());
	}

	@Test
	public void qname_behaviour_2() {
		Qname qname = Qname.fromURI("http://tun.fi/KE.423/DABUH:SEMF:130677");
		assertEquals("KE.423/DABUH:SEMF:130677", qname.toString());
		assertEquals("http://tun.fi/KE.423/DABUH:SEMF:130677", qname.toURI());
		assertEquals("gbif-dataset:5cc9ff48-37b9-409b-9dfa-9b8b73014f46", Qname.fromURI("https://www.gbif.org/dataset/5cc9ff48-37b9-409b-9dfa-9b8b73014f46").toString());
	}

	@Test
	public void qname_behaviour_3() {
		try {
			Qname.fromURI("http://UNKNOWN.COM/1");
			fail("Should throw exception");
		} catch (Exception e) {
			assertEquals("Unresolveable uri: http://UNKNOWN.COM/1", e.getMessage());
		}
	}

	@Test
	public void qname_behaviour_4() {
		Qname qname = new Qname("DABUH:SEMF:130677");
		assertEquals("DABUH:SEMF:130677", qname.toString());
		assertEquals("http://tun.fi/DABUH:SEMF:130677", qname.toURI());
	}

	@Test
	public void qname_behaviour_5() {
		Qname qname = new Qname(":::DABUH:SEMF:130677::");
		assertEquals(":::DABUH:SEMF:130677::", qname.toString());
		assertEquals("http://tun.fi/:::DABUH:SEMF:130677::", qname.toURI());
	}

	@Test
	public void qname_behaviour_6() {
		Qname qname = new Qname("rdf:type");
		assertEquals("rdf:type", qname.toString());
		assertEquals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type", qname.toURI());
	}

	@Test
	public void qname_behaviour_7() {
		Qname qname = new Qname("luomus:GV.1");
		assertEquals("luomus:GV.1", qname.toString());
		assertEquals("http://id.luomus.fi/GV.1", qname.toURI());
	}

	@Test
	public void qnameFromUri() {
		assertEquals("MA.1", Qname.fromURI("http://tun.fi/MA.1").toString());
		assertEquals("naturforskaren:species", Qname.fromURI("http://naturforskaren.se/species").toString());
		assertEquals("rdf:type", Qname.fromURI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type").toString());
		assertNull(Qname.fromURI(null).toString());
		try {
			Qname.fromURI("http://foobar.com/foobar");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Unresolveable uri: http://foobar.com/foobar", e.getMessage());
		}
		try {
			Qname.fromURI("52d25fa6e4b0ac939083bdbd");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Unresolveable uri: 52d25fa6e4b0ac939083bdbd", e.getMessage());
		}
		assertEquals("dcmitype:Dataset", Qname.fromURI("http://purl.org/dc/dcmitype/Dataset").toString());
		assertEquals("rdfschema:comment", Qname.fromURI("http://www.w3.org/TR/2014/REC-rdf-schema-20140225/comment").toString());
	}

	@Test
	public void qname_sorting() {
		List<Qname> l = Utils.list(
				new Qname("MA.9"),
				new Qname("MA.123"),
				new Qname("MX.7"),
				new Qname("MX.6")
				);
		Collections.sort(l);
		assertEquals("[MA.9, MA.123, MX.6, MX.7]", l.toString());
	}

	@Test
	public void removeAllPredicates() {
		Model m = new Model(new Qname("MA.1"));
		m.addStatement(new Statement(new Predicate("MA.fullName"), new ObjectLiteral("Dare Talvitie")));
		m.addStatement(new Statement(new Predicate("MA.fullName"), new ObjectLiteral("Dare Winterroad", "en")));
		m.addStatement(new Statement(new Predicate("MA.blaa"), new ObjectLiteral("faa")));
		assertEquals(3, m.getStatements().size());
		m.removeAll(new Predicate("MA.Fullname"));
		assertEquals(3, m.getStatements().size());
		m.removeAll(new Predicate("MA.fullName"));
		assertEquals(1, m.getStatements().size());
	}

	@Test
	public void setType() {
		Model m = new Model(new Qname("MA.1"));
		assertEquals(0, m.getStatements().size());
		m.setType("MA.person");
		assertEquals(1, m.getStatements().size());
		assertEquals("rdf:type", m.getStatements().get(0).getPredicate().toString());
		assertEquals("MA.person", m.getStatements().get(0).getObjectResource().getQname());
	}

	@Test
	public void longText() {
		String längText = "läng text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng. Lång text is läng.";
		assertEquals(4255, längText.length());
		assertEquals(4703, Utils.countOfUTF8Bytes(längText));
		String result = new ObjectLiteral(längText).getContent();
		assertEquals(3614, result.length());
		assertEquals(3995, Utils.countOfUTF8Bytes(result));
	}

	@Test
	public void styles() { // Allow p and styles
		String content = "<p style=\"color: red;\">Jee</p>";
		assertEquals(content, new ObjectLiteral(content).getContent());
	}

	@Test
	public void doubleSanitation_causes_problems() {
		String originalContent = "Foo <iframe src=\"http://...\"></a>";
		String content = new ObjectLiteral(originalContent).getContent();
		assertEquals("Foo </a>", content);
		content = new ObjectLiteral(content).getContent();
		assertEquals("Foo", content); // this is bad; see doubleSanitation_prevention()
	}

	@Test
	public void doubleSanitation_prevention() {
		String originalContent = "Foo <iframe src=\"http://...\"></a>";
		String content = new ObjectLiteral(originalContent).getContent();
		assertEquals("Foo </a>", content);
		content = ObjectLiteral.unsanitazedObjectLiteral(content, null).getContent();
		assertEquals("Foo </a>", content);
	}

	@Test
	public void tags() {
		assertEquals("Foo bar", new ObjectLiteral("Foo <iframe href=\"...\"></iframe> bar").getContent());
		assertEquals("Foo", new ObjectLiteral("Foo <iframe src=\"http://...\"></iframe>").getContent());
		assertEquals("Foo <p>bar</p>", new ObjectLiteral("Foo <p>bar").getContent());
		assertEquals("Foo < p >bar", new ObjectLiteral("Foo < p >bar").getContent());
		assertEquals("Foo bar", new ObjectLiteral("Foo bar</p>").getContent());
		assertEquals("Foo bar< /p>", new ObjectLiteral("Foo bar< /p>").getContent());
		assertEquals("Foo <a href=\"http://..\">bar</a>", new ObjectLiteral("Foo <a href=\"http://..\">bar</a>").getContent());
		assertEquals("Foo <a href=\"http://..\">bar</a>", new ObjectLiteral("Foo <a href=\"http://..\">bar").getContent());
		assertEquals("Foo <a href=\"http://..\"></a>", new ObjectLiteral("Foo <a href=\"http://..\"bar</a>").getContent());
		assertEquals("Foo", new ObjectLiteral("Foo <a href=\"http://..\"bar").getContent());
		assertEquals("Foo <a href=\"/relative/MX.1\">bar</a>", new ObjectLiteral("Foo <a href=\"/relative/MX.1\">bar</a>").getContent());
	}

	@Test
	public void escapedTags() {
		assertEquals("<script>alert('Hello');</script>", new ObjectLiteral("&lt;script&gt;alert('Hello');&lt;/script&gt;").getContent()); // We have to allow this
	}

	@Test
	public void noEntities() {
		assertEquals("\"Hornet's nest\"", new ObjectLiteral("\"Hornet's nest\"").getContent());
		assertEquals("<p>Letter ó</p>", new ObjectLiteral("<p>Letter ó</p>").getContent());
		assertEquals("\"Linnea\"", new ObjectLiteral("\"Linnea\"").getContent());
		assertEquals("<p><5</p>", new ObjectLiteral("<p><5</p>").getContent());
		assertEquals("Linnea & Goris", new ObjectLiteral("Linnea & Goris").getContent());
	}

	@Test
	public void namespaces() {
		Subject subject = new Subject(new Qname("luomus:Q.1"));
		assertEquals("luomus", subject.getNamespacePrefix());
		assertEquals("http://id.luomus.fi/", subject.getNamespaceURI());
		assertEquals("Q", subject.getSubNamespacePrefix());

		subject = new Subject(new Qname("Q.1"));
		assertEquals("", subject.getNamespacePrefix());
		assertEquals("http://tun.fi/", subject.getNamespaceURI());
		assertEquals("Q", subject.getSubNamespacePrefix());

		subject = new Subject(new Qname("luomus:123"));
		assertEquals("luomus", subject.getNamespacePrefix());
		assertEquals("http://id.luomus.fi/", subject.getNamespaceURI());
		assertEquals("123", subject.getSubNamespacePrefix());

		subject = new Subject(new Qname("123"));
		assertEquals("", subject.getNamespacePrefix());
		assertEquals("http://tun.fi/", subject.getNamespaceURI());
		assertEquals("123", subject.getSubNamespacePrefix());
	}

}
