package fi.luomus.java.tests.commons.rdf;

import static org.junit.Assert.assertEquals;

import java.util.regex.Pattern;

import org.apache.jena.riot.RDFFormat;
import org.junit.Assert;
import org.junit.Test;

import fi.luomus.commons.containers.rdf.Context;
import fi.luomus.commons.containers.rdf.InternalModelToJenaModelConverter;
import fi.luomus.commons.containers.rdf.JenaUtils;
import fi.luomus.commons.containers.rdf.Model;
import fi.luomus.commons.containers.rdf.ObjectLiteral;
import fi.luomus.commons.containers.rdf.ObjectResource;
import fi.luomus.commons.containers.rdf.Predicate;
import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.Statement;
import fi.luomus.commons.utils.Utils;

public class InternalModelToRDFTests {

	private void assertRdfEquals(String expected, String actual) {
		Assert.assertEquals(cleanForCompare(expected), cleanForCompare(actual));
	}

	private static String cleanForCompare(String rdf) {
		rdf = rdf.replace("<?xml version='1.0' encoding='utf-8'?>", "");
		String nameSpaces = rdf.split(Pattern.quote(">"))[0];
		rdf = rdf.replace(nameSpaces, "rdf:RDF");
		return Utils.removeWhitespace(rdf);
	}

	@Test
	public void generateSimpleRDF() {
		Model model = new Model(new Qname("JA.123"));
		model.addStatement(new Statement(new Predicate("foo"), new ObjectLiteral("bar")));

		String expected = "" +
				"<rdf:RDF															\n" +
				"    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"		\n" +
				"    xmlns=\"http://tun.fi/\"							    > 	\n" + 
				"  <rdf:Description rdf:about=\"http://tun.fi/JA.123\">		\n" +
				"    <foo>bar</foo>				        							\n" +
				"  </rdf:Description>												\n" +
				"</rdf:RDF>															\n";
		assertRdfEquals(expected, model.getRDF());
	}

	@Test
	public void generateSimpleJsonLD() {
		Model model = new Model(new Qname("JA.123"));
		model.addStatement(new Statement(new Predicate("foo"), new ObjectLiteral("bar")));
		model.setType("mytype");
		String expected = "" +
				"{ \n" + 
				"  \"@id\" : \"http://tun.fi/JA.123\",	\n" + 
				"  \"@type\" : \"mytype\",	\n" + 
				"  \"foo\" : \"bar\",	\n" + 
				"  \"@context\" : {	\n" + 
				"    \"foo\" : {	\n" + 
				"      \"@id\" : \"http://tun.fi/foo\"	\n" + 
				"    },	\n" + 
				"    \"@vocab\" : \"http://tun.fi/\",	\n" + 
				"    \"rdf\" : \"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"	\n" + 
				"  }	\n" + 
				"}";
		assertEquals(Utils.removeWhitespace(expected), Utils.removeWhitespace(model.getSerialized(RDFFormat.JSONLD)));
	}

	@Test
	public void generateSimpleRDF_with_lancode() {
		Model model = new Model(new Qname("JA.123"));
		model.addStatement(new Statement(new Predicate("foo"), new ObjectLiteral("bar", "en")));

		String expected = "" +
				"<rdf:RDF															\n" +
				"    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"		\n" +
				"    xmlns=\"http://tun.fi/\"							    > 	\n" + 
				"  <rdf:Description rdf:about=\"http://tun.fi/JA.123\">		\n" +
				"    <foo xml:lang=\"en\">bar</foo>				 					\n" +
				"  </rdf:Description>												\n" +
				"</rdf:RDF>															\n";
		assertRdfEquals(expected, model.getRDF());
	}

	@Test
	public void generateSimpleRDF_with_external_resource_as_predicate() {
		Model model = new Model(new Qname("JA.123"));
		model.addStatement(new Statement(new Predicate("abcd:Person"), new ObjectResource("MA.1")));

		String expected = "" +
				"<rdf:RDF															\n" +
				"    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"		\n" +
				"    xmlns=\"http://tun.fi/\"							  		\n" +
				"    xmlns:abcd=\"http://www.tdwg.org/schemas/abcd/2.06#\" 		 > 	\n" + 
				"  <rdf:Description rdf:about=\"http://tun.fi/JA.123\">		\n" +
				"    <abcd:Person rdf:resource=\"http://tun.fi/MA.1\" />		\n" +
				"  </rdf:Description>												\n" +
				"</rdf:RDF>															\n";
		assertRdfEquals(expected, model.getRDF());
	}

	@Test
	public void generateSimpleRDF_with_default_namespace_resource_as_object() {
		Model model = new Model(new Qname("JA.123"));
		model.addStatement(new Statement(new Predicate("foo"), new ObjectResource("MA.1")));

		String expected = "" +
				"<rdf:RDF															\n" +
				"    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"		\n" +
				"    xmlns=\"http://tun.fi/\"							  	> 	\n" + 
				"  <rdf:Description rdf:about=\"http://tun.fi/JA.123\">		\n" +
				"    <foo rdf:resource=\"http://tun.fi/MA.1\" />				\n" +
				"  </rdf:Description>												\n" +
				"</rdf:RDF>															\n";
		assertRdfEquals(expected, model.getRDF());
	}

	@Test
	public void generateSimpleRDF_from_external_namespace() {
		Model model = new Model(new Qname("rdf:Something"));
		model.addStatement(new Statement(new Predicate("rdf:Is"), new ObjectResource("rdf:Pretty")));

		String expected = "" +
				"<rdf:RDF																				\n" +
				"    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"	> 						\n" + 
				"  <rdf:Description rdf:about=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Something\">	\n" +
				"    <rdf:Is rdf:resource=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#Pretty\" />		\n" +
				"  </rdf:Description>																	\n" +
				"</rdf:RDF>																				\n";
		assertRdfEquals(expected, model.getRDF());
	}

	@Test
	public void generateSimpleRDF_with_context() {
		Model model = new Model(new Qname("JA.123"));

		model.addStatement(new Statement(new Predicate("foo"), new ObjectLiteral("bar"), new Context("LA.1")));

		String expected = "" +
				"<rdf:RDF															\n" +
				"    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"		\n" +
				"    xmlns=\"http://tun.fi/\"							    > 	\n" + 
				"  <rdf:Description rdf:about=\"http://tun.fi/JA.123\">		\n" +
				"    <foo_CONTEXT_LA.1>bar</foo_CONTEXT_LA.1>				        \n" +
				"  </rdf:Description>												\n" +
				"</rdf:RDF>															\n";
		assertRdfEquals(expected, model.getRDF());
	}

	@Test
	public void sorting() {
		Model model1 = new Model(new Qname("MX.2"));
		Model model2 = new Model(new Qname("MX.1"));
		Model model3 = new Model(new Qname("MX.3"));
		Model model4 = new Model(new Qname("MX.5"));
		Model model5 = new Model(new Qname("MX.A"));

		model1.setType("MX.taxon");
		model2.setType("MX.taxon");
		model3.setType("MX.taxon");
		model4.setType("MX.taxon");
		model5.setType("rdf:Property");

		org.apache.jena.rdf.model.Model jenaModel = new InternalModelToJenaModelConverter(Utils.list(model1, model2, model3, model4, model5)).getJenaModel();

		String rdf = JenaUtils.getSerialized(jenaModel, RDFFormat.RDFXML_ABBREV);

		String expected = "" +
				"<?xml version='1.0' encoding='utf-8'?>"+
				"<rdf:RDF xmlns=\"http://tun.fi/\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"> "+
				"	<MX.taxon rdf:about=\"http://tun.fi/MX.1\" /> "+
				"	<MX.taxon rdf:about=\"http://tun.fi/MX.2\" /> "+
				"	<MX.taxon rdf:about=\"http://tun.fi/MX.3\" /> "+
				"	<MX.taxon rdf:about=\"http://tun.fi/MX.5\" /> "+
				"	<rdf:Property rdf:about=\"http://tun.fi/MX.A\" /> "+
				"</rdf:RDF>";
		assertRdfEquals(expected, rdf);
	}

}
