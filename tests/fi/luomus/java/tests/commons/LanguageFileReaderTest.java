package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import fi.luomus.commons.languagesupport.LanguageFileReader;
import fi.luomus.commons.languagesupport.LocalizedTextsContainer;

public class LanguageFileReaderTest {
	

	public static class OpeningANewReader {
		
		@Test
		public void test_it_only_accepts_one_or_more_languages() {
			try {
				new LanguageFileReader("a", "a");
				fail("Should not accept no languages");
			} catch (IllegalArgumentException e) {
			} catch (Exception e) {
			}
			
			try {
				new LanguageFileReader("a", "a", "");
				fail("Should not accept empty_strings a language");
			} catch (IllegalArgumentException e) {
			} catch (Exception e) {
			}
			
		}
		
		@Test
		@Ignore
		public void test_it_only_accepts_parameters_that_can_find_a_language_file() throws Exception {
			
			String folder = TestConfig.getConfig().languagefileFolder();
			
			try {
				new LanguageFileReader("doesnotexist", "doesnotexist", "FI");
				fail("Should fail");
			} catch (FileNotFoundException e) {
			}
			
			try {
				new LanguageFileReader(folder, "doesnotexist", "FI");
				fail("Should fail");
			} catch (FileNotFoundException e) {
			}
			
			try {
				new LanguageFileReader(folder, "langfile_", "FI");
			} catch (FileNotFoundException e) {
				fail("Should not fail: " + e);
			}
			
			try {
				new LanguageFileReader(folder, "langfile_", "FI", "EN");
			} catch (FileNotFoundException e) {
				fail("Should not fail: " + e);
			}
			
		}
		
	}
	
	public static class ReadingContents {
		
		private String	folder;
		
		@Before
		public void setUp() throws Exception {
			folder = TestConfig.getConfig().languagefileFolder();
		}
		
		@Test
		@Ignore
		public void test_it_reads_a_text() throws Exception {
			LanguageFileReader r = new LanguageFileReader(folder, "langfile_", "FI");
			LocalizedTextsContainer uiTexts = r.readUITexts();
			
			assertEquals("Testi", uiTexts.getText("text.test", "FI"));
		}
		
		@Ignore
		@Test
		public void test_it_reads_a_text_in_all_languages() throws Exception {
			LanguageFileReader r = new LanguageFileReader(folder, "langfile_", "FI", "EN");
			LocalizedTextsContainer uiTexts = r.readUITexts();
			
			assertEquals("Testi", uiTexts.getText("text.test", "FI"));
			assertEquals("Test", uiTexts.getText("text.test", "EN"));
		}
		
		@Test
		@Ignore
		public void test_it_throws_exception_if_a_text_is_not_found_from_all_lang_files() throws Exception {
			LanguageFileReader r = new LanguageFileReader(folder, "langfile_", "FI", "EN", "SE");
			try {
				r.readUITexts();
				fail("Should fail because 'error.general_required' not found from  se-langfile");
			} catch (NoSuchFieldException e) {
				assertTrue("Should contain the name of the field missing", e.toString().contains("error.general_required"));
			}
		}
		
	}
	
}
