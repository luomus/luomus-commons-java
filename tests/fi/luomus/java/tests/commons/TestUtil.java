package fi.luomus.java.tests.commons;

import java.io.File;
import java.net.URL;

import fi.luomus.commons.utils.FileUtils;

public class TestUtil {

	public static String getTestData(String filename) {
		URL url = TestUtil.class.getResource(filename);
		File file = new File(url.getFile());
		try {
			String data = FileUtils.readContents(file);
			return data;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
