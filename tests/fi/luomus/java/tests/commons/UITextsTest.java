package fi.luomus.java.tests.commons;

import java.util.Map;

import fi.luomus.commons.languagesupport.LocalizedTextsContainerImple;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class UITextsTest {

	public static Test suite() {
		return new TestSuite(UITextsTest.class.getDeclaredClasses());
	}

	public static class SettingTextsToUITexts extends TestCase {

		public void test_setting_and_getting_a_texts() {
			LocalizedTextsContainerImple uit = new LocalizedTextsContainerImple("FI", "SE");
			uit.setText("test", "testiarvo", "FI");
			uit.setText("test", "testvarde", "SE");
			assertEquals("testiarvo", uit.getText("test", "FI"));
			assertEquals("testvarde", uit.getText("test", "SE"));

			assertEquals(true, uit.hasText("test"));
			assertEquals(false, uit.hasText("foobar"));
		}

		public void test_it_accepts_two_same_langugaes() {
			new LocalizedTextsContainerImple("FI", "FI");
		}

		public void test_it_doesnt_accept_empty_language() {
			try {
				new LocalizedTextsContainerImple("FI", "");
				fail("Should have thrown an exception");
			} catch (IllegalArgumentException e) {
			}
		}

		public void test_it__accepts_null_nowadays() {
			new LocalizedTextsContainerImple((String[]) null);
		}

		public void test_it_fails_when_asking_for_language_or_text_that_doesnt_exist() {
			LocalizedTextsContainerImple uit = new LocalizedTextsContainerImple("EN");
			uit.setText("test", "test", "EN");
			try {
				uit.getText("foo", "EN");
				fail("Should have thrown an exception");
			} catch (IllegalArgumentException e) {
			}
			try {
				uit.getText("test", "FO");
				fail("Should have thrown an exception");
			} catch (IllegalArgumentException e) {
			}
		}

		public void test_gettingAll() {
			LocalizedTextsContainerImple uit = new LocalizedTextsContainerImple("FI", "EN");
			uit.setText("foo", "bar", "EN");
			uit.setText("dog", "cat", "EN");
			uit.setText("foo", "tanko", "FI");
			uit.setText("dog", "kissa", "FI");

			Map<String, String> fi = uit.getAllTexts("FI");
			Map<String, String> en = uit.getAllTexts("EN");

			Map<String, String> unknown = uit.getAllTexts("UN");
			assertTrue(unknown.isEmpty());
			assertEquals("bar", en.get("foo"));
			assertEquals("cat", en.get("dog"));
			assertEquals("tanko", fi.get("foo"));
			assertEquals("kissa", fi.get("dog"));
		}

	}

}
