package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import fi.luomus.commons.json.JSONArray;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.utils.LogUtils;
import fi.luomus.commons.utils.StringToColorUtil;
import fi.luomus.commons.utils.URIBuilder;
import fi.luomus.commons.utils.Utils;


public class UtilsTest {

	@Test
	public void uribuilder() throws URISyntaxException {
		URIBuilder b = new URIBuilder("http://base.com");
		b.addParameter("foo", "bar<>\"&! --.");
		b.addParameter("foo2", 2);
		b.addParameter("foo2", 20);
		b.addParameter("foo3", 3);
		b.replaceParameter("foo3", "rep");

		assertEquals("http://base.com?foo=bar%3C%3E%22%26%21+--.&foo2=2&foo2=20&foo3=rep", b.toString());
		b.getURI();
	}

	@Test
	public void uribuilder_2() throws URISyntaxException {
		URIBuilder b = new URIBuilder("http://base.com?foo=bar&drinks=free");
		assertEquals("http://base.com?foo=bar&drinks=free", b.toString());
		b.addParameter("food", "extra");
		assertEquals("http://base.com?foo=bar&drinks=free&food=extra", b.toString());
		b.getURI();
	}

	@Test
	public void uribuilder_3() throws URISyntaxException {
		URIBuilder b = new URIBuilder("http://base.com?foo=&=bar");
		assertEquals("http://base.com", b.toString());
		b.addParameter("foo", "bar");
		assertEquals("http://base.com?foo=bar", b.toString());
		b.getURI();
	}

	@Test
	public void uribuilder_4() throws URISyntaxException {
		URIBuilder b = new URIBuilder("http://base.com?x&x");
		assertEquals("http://base.com", b.toString());
		b.getURI();
	}

	@Test
	public void uppercaseFirst() {
		assertEquals(null, Utils.upperCaseFirst(null));
		assertEquals("", Utils.upperCaseFirst(""));
		assertEquals("S", Utils.upperCaseFirst("s"));
		assertEquals("Ss", Utils.upperCaseFirst("ss"));
		assertEquals("SS", Utils.upperCaseFirst("SS"));
		assertEquals(" ss", Utils.upperCaseFirst(" ss"));
		assertEquals("SsssssS", Utils.upperCaseFirst("ssssssS"));
	}

	@Test
	public void test_countOfNumberSequences() {
		assertEquals(0, Utils.countOfNumberSequences("a"));
		assertEquals(0, Utils.countOfNumberSequences(""));
		assertEquals(0, Utils.countOfNumberSequences(null));
		assertEquals(1, Utils.countOfNumberSequences("1"));
		assertEquals(1, Utils.countOfNumberSequences("12"));
		assertEquals(2, Utils.countOfNumberSequences("12.3"));
		assertEquals(2, Utils.countOfNumberSequences("12,3"));
		assertEquals(2, Utils.countOfNumberSequences(" 12 3 "));
		assertEquals(3, Utils.countOfNumberSequences("asd5asd5asd7"));
	}

	@Test
	public void test_string_to_rgb_color() {
		String rgb = StringToColorUtil.toRGBColor("Hei hoi hui");
		assertEquals("232,226,191", rgb);
		assertEquals("E8E2BF", StringToColorUtil.toHex("Hei hoi hui"));

	}

	@Test
	public void test_trimMultipleSpaces() {
		assertEquals(null, Utils.trimMultipleSpaces(null));
		assertEquals("", Utils.trimMultipleSpaces(""));
		assertEquals(" ", Utils.trimMultipleSpaces(" "));
		assertEquals(" ", Utils.trimMultipleSpaces("  "));
		assertEquals(" aavan meren tuollapuolen", Utils.trimMultipleSpaces(" aavan meren  tuollapuolen"));
		assertEquals("KUUKKELI (505)", Utils.trimMultipleSpaces("KUUKKELI                                  (505)"));
	}

	@Test
	public void test_countNumberOf() {
		assertEquals(2, Utils.countNumberOf("a", "baba"));
		assertEquals(3, Utils.countNumberOf(".", "/././//."));
		assertEquals(5, Utils.countNumberOf("/", "/././//."));
		assertEquals(1, Utils.countNumberOf("huu", "aahuuh"));
		assertEquals(0, Utils.countNumberOf("huu", "huh"));
	}

	@Test
	public void test_splitDecimal() {
		String[] s = Utils.splitDecimal("1");
		assertEquals("1", s[0]);

		s = Utils.splitDecimal("1,5");
		assertEquals("1", s[0]);
		assertEquals("5", s[1]);

		s = Utils.splitDecimal("1.5");
		assertEquals("1", s[0]);
		assertEquals("5", s[1]);

		s = Utils.splitDecimal("1.5.1");
		assertEquals("1", s[0]);
		assertEquals("5,1", s[1]);

		s = Utils.splitDecimal(".");
		assertEquals("", s[0]);
		assertEquals("", s[1]);

		s = Utils.splitDecimal(",.");
		assertEquals("", s[0]);
		assertEquals(",", s[1]);
	}

	@Test
	public void test__max() {
		assertEquals("5", Utils.maxOf("1", "5", "2"));
		assertEquals("5", Utils.maxOf("1", "5", "-7"));
		assertEquals("5", Utils.maxOf("1", "5", "a"));
		assertEquals("", Utils.maxOf(""));
		assertEquals("", Utils.maxOf("", "a"));
		assertEquals("1", Utils.maxOf("", "a", "1"));
		assertEquals("", Utils.maxOf((String[]) null));
	}

	@Test
	public void test__min() {
		assertEquals("1", Utils.minOf("1", "5", "2"));
		assertEquals("-7", Utils.minOf("1", "5", "-7"));
		assertEquals("1", Utils.minOf("1", "5", "a"));
		assertEquals("", Utils.minOf(""));
		assertEquals("", Utils.minOf("", "a"));
		assertEquals("1", Utils.minOf("", "a", "1"));
		assertEquals("", Utils.minOf((String[]) null));
	}

	@Test
	public void test__fitsinrangecheck() {
		assertEquals(true, Utils.fitsInRange(5, 5, 0));
		assertEquals(true, Utils.fitsInRange(5, 5, 200));
		assertEquals(true, Utils.fitsInRange(8, 10, 2));
		assertEquals(true, Utils.fitsInRange(8, 10, 3));
		assertEquals(false, Utils.fitsInRange(8, 10, 1));
		assertEquals(true, Utils.fitsInRange(12, 10, 2));
		assertEquals(false, Utils.fitsInRange(5, 6, 0));
		assertEquals(false, Utils.fitsInRange(7, 10, 2));
		assertEquals(false, Utils.fitsInRange(13, 10, 2));
	}

	@Test
	public void test_url_encoding() {
		String original = Utils.urlEncode("Pesä 1");
		assertEquals("Pes%C3%A4+1", original);

		original = Utils.urlEncode("Uhkia: Valtatie lähellä<br /> Uusi liittymä suunnitteilla");
		assertEquals("Uhkia%3A+Valtatie+l%C3%A4hell%C3%A4%3Cbr+%2F%3E+Uusi+liittym%C3%A4+suunnitteilla", original);

		original = Utils.urlEncode("pääsky");
		assertEquals("p%C3%A4%C3%A4sky", original);
	}

	@Test
	public void test_entity_utils() {
		String rawData = "<p>Hämet-Ahti, L., Palmén, A., Alanko, P. &amp; Tigerstedt, P.M.A. 1992: Suomen puu- ja pensaskasvio. 2. uudistettu painos. 373 s. Dendrologian Seura, Helsinki.</p><p>Lindholm, T. &amp; Tiainen, J. 1982:Dispersal and establishment of an introduced conifer, the Siberian fir Abies sibirica, in a nemorial forest in Southern Finland. &ndash; Ann. Bot. Fennici 19:235-245.</p><p>Ryttäri, T<u>.</u>, Väre, H.&nbsp; 2014.&nbsp; Puuvartiset kasvit vieraslajeina = Abstract: The invasive woody species in Finland. <em>Sorbifolia</em> 45 (4): 161-174.</p><p><a href=\"http://www.metla.fi/metinfo/puulajit/lajikuvaukset/lajisivu-abies-sibirica.htm\">http://www.metla.fi/metinfo/puulajit/lajikuvaukset/lajisivu-abies-sibirica.htm</a></p><p>&nbsp;</p>";
		String expected = "&lt;p&gt;Hämet-Ahti, L., Palmén, A., Alanko, P. &amp;amp; Tigerstedt, P.M.A. 1992: Suomen puu- ja pensaskasvio. 2. uudistettu painos. 373 s. Dendrologian Seura, Helsinki.&lt;/p&gt;&lt;p&gt;Lindholm, T. &amp;amp; Tiainen, J. 1982:Dispersal and establishment of an introduced conifer, the Siberian fir Abies sibirica, in a nemorial forest in Southern Finland. &amp;ndash; Ann. Bot. Fennici 19:235-245.&lt;/p&gt;&lt;p&gt;Ryttäri, T&lt;u&gt;.&lt;/u&gt;, Väre, H.&amp;nbsp; 2014.&amp;nbsp; Puuvartiset kasvit vieraslajeina = Abstract: The invasive woody species in Finland. &lt;em&gt;Sorbifolia&lt;/em&gt; 45 (4): 161-174.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.metla.fi/metinfo/puulajit/lajikuvaukset/lajisivu-abies-sibirica.htm&quot;&gt;http://www.metla.fi/metinfo/puulajit/lajikuvaukset/lajisivu-abies-sibirica.htm&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;";

		assertEquals(expected, Utils.toHTMLEntities(rawData));
	}

	@Test
	public void test__trim_to_length() {
		assertEquals("AAA", Utils.trimToLength("AAA", 20));
		assertEquals("AAA", Utils.trimToLength("AAA", 3));
		assertEquals("AA", Utils.trimToLength("AAA", 2));
		assertEquals("A", Utils.trimToLength("AAA", 1));
		assertEquals("", Utils.trimToLength("AAA", 0));
		try {
			Utils.trimToLength("AAA", -1);
			fail();
		} catch (IllegalArgumentException e) {
			// ok
		}
	}

	@Test
	public void test__return_accuracy() {
		assertEquals(1, Utils.accuracyOf(0));
		assertEquals(1, Utils.accuracyOf(12345));
		assertEquals(10, Utils.accuracyOf(123450));
		assertEquals(100, Utils.accuracyOf(1234500));
		assertEquals(1000, Utils.accuracyOf(1234000));
		assertEquals(10000, Utils.accuracyOf(123450000));

		assertEquals(10000, Utils.accuracyOf(120450000));
		assertEquals(10000, Utils.accuracyOf(10000));

		assertEquals(1000, Utils.accuracyOf(6726000));
		assertEquals(1000, Utils.accuracyOf(3338000));

		assertEquals(10, Utils.accuracyOf(10));
		assertEquals(10, Utils.accuracyOf(210));
		assertEquals(10, Utils.accuracyOf(-210));
		assertEquals(10, Utils.accuracyOf(20010));
		assertEquals(1, Utils.accuracyOf(2001));
		assertEquals(1, Utils.accuracyOf(002001));
	}

	private static class SomeClass {
		@Override
		public String toString() {
			return "SomeClass";
		}
	}

	@Test
	public void test__debugS() {
		String s = Utils.debugS(null, "1", new Integer(1), new SomeClass());
		assertEquals("null : 1 : 1 : SomeClass", s);
		assertEquals("", Utils.debugS());
	}

	@Test
	public void test__median() {
		assertEquals(2.0, Utils.medianOf(Utils.list(3.0, 2.0, 1.0)), 0);
	}

	@Test
	public void test__median_2() {
		assertEquals(1.5, Utils.medianOf(Utils.list(2.0, 1.0)), 0);
	}

	@Test
	public void test__median_3() {
		assertEquals(0.0, Utils.medianOf(new ArrayList<Double>()), 0);
	}

	private static class NoExceptionThrower {
		public void doit(int timeToThrowExceptionYet) {
			if (timeToThrowExceptionYet++ > 5) {
				new ExceptionThrower().throwit();
			} else {
				try {
					new NoExceptionThrower().doit(timeToThrowExceptionYet);
				} catch (RuntimeException e) {
					throw new RuntimeException(timeToThrowExceptionYet + " " + e.getMessage(), e);
				}
			}
		}
	}
	private static class ExceptionThrower {
		public void throwit() {
			throw new RuntimeException("Foobar!");
		}
	}

	@Test
	public void test__logutils_buildStacktrace() {
		try {
			new NoExceptionThrower().doit(0);
		} catch (Exception e) {
			String stackTrace = LogUtils.buildStackTrace(e);
			assertTrue(stackTrace.contains("java.lang.RuntimeException: Foobar!"));
			assertTrue(stackTrace.contains("java.lang.RuntimeException: 6 Foobar!"));
			assertTrue(stackTrace.contains("java.lang.RuntimeException: 5 6 Foobar!"));
			assertTrue(stackTrace.contains("java.lang.RuntimeException: 1 2 3 4 5 6 Foobar!"));
		}
	}

	@Test
	public void test__logutils_shorten_message() {
		assertEquals("123456", LogUtils.shorten("123456", 10000));
		assertEquals("123456", LogUtils.shorten("123456", 6));
		assertEquals("1", LogUtils.shorten("1", 6));
		assertEquals("1", LogUtils.shorten("1", 1));

		assertEquals("" +
				"1\n" +
				"[--- LONG MESSAGE SHORTENED ---]\n" +
				"2",
				LogUtils.shorten("12", 1));

		assertEquals("" +
				"123\n" +
				"[--- LONG MESSAGE SHORTENED ---]\n" +
				"456",
				LogUtils.shorten("123456", 5));

		assertEquals("" +
				"123\n" +
				"[--- LONG MESSAGE SHORTENED ---]\n" +
				"456",
				LogUtils.shorten("123456", 4));

		assertEquals("" +
				"12\n" +
				"[--- LONG MESSAGE SHORTENED ---]\n" +
				"56",
				LogUtils.shorten("123456", 3));

		assertEquals("" +
				"LONG TEXT IS LONG LON\n" +
				"[--- LONG MESSAGE SHORTENED ---]\n" +
				"ONG LONG TEXT IS LONG",
				LogUtils.shorten(
						"LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG LONG TEXT IS LONG",
						40));
	}

	@Test
	public void test__beautify_json() {
		JSONObject json = new JSONObject("{\"test\":\"Format: \\\"PROFESSIONAL: NEUTRAL,UNCERTAIN\\\".\", \"collectionId\":\"http://tun.fi/HR.407\",\"processSuccess\":true,\"editors\":[\"http://tun.fi/MA.5\"],\"publicDocument\":{\"gatherings\":[{\"country\":\"FI\",\"agents\":[\"Seppä P.\"],\"units\":[{\"sex\":[\"FEMALE\"],\"lifeStage\":[\"ADULT\"],\"det\":\"von Bonsdorff\",\"taxonName\":\"Apamea crenata\",\"individualCount\":1,\"recordBasis\":[\"PRESERVED_SPECIMEN\"],\"facts\":[{\"fact\":\"http://tun.fi/MY.lifeStage\",\"value\":\"http://tun.fi/MY.lifeStageAdult\"},{\"fact\":\"http://tun.fi/MY.sex\",\"value\":\"http://tun.fi/MY.sexF\"},{\"fact\":\"http://tun.fi/MY.count\",\"value\":\"1\"},{\"fact\":\"http://tun.fi/MY.recordBasis\",\"value\":\"http://tun.fi/MY.recordBasisPreservedSpecimen\"}]}],\"higherGeography\":\"Europe\",\"municipality\":\"Turku\",\"biogeographicalProvince\":\"Varsinais-Suomi\",\"facts\":[{\"fact\":\"http://tun.fi/MY.country\",\"value\":\"FI\"},{\"fact\":\"http://tun.fi/MY.biologicalProvince\",\"value\":\"Varsinais-Suomi\"},{\"fact\":\"http://tun.fi/MY.leg\",\"value\":\"Seppälä, J.\"},{\"fact\":\"http://tun.fi/MY.higherGeography\",\"value\":\"Europe\"},{\"fact\":\"http://tun.fi/MY.county\",\"value\":\"Turku\"}]}],\"keywords\":[\"http://tun.fi/GX.270\",\"http://tun.fi/GX.643\"],\"notes\":\"previous collection owner; historiallinen museo\",\"facts\":[{\"fact\":\"http://tun.fi/MZ.editor\",\"value\":\"http://tun.fi/MA.88\"},{\"fact\":\"http://tun.fi/MY.documentLocation\",\"value\":\"In collection; Cabinet: Noctuidae; Box: 10\"},{\"fact\":\"http://tun.fi/MY.datatype\",\"value\":\"zoospecimen\"},{\"fact\":\"http://tun.fi/MZ.dateEdited\",\"value\":\"2013-11-01T08:11:30+0200\"},{\"fact\":\"http://tun.fi/MY.datasetID\",\"value\":\"http://tun.fi/GX.270\"},{\"fact\":\"http://tun.fi/MY.datasetID\",\"value\":\"http://tun.fi/GX.643\"},{\"fact\":\"http://tun.fi/MZ.owner\",\"value\":\"http://tun.fi/MOS.1007\"},{\"fact\":\"http://tun.fi/MZ.dateCreated\",\"value\":\"2013-11-01T08:11:30+0200\"},{\"fact\":\"http://tun.fi/MY.language\",\"value\":\"finnish\"},{\"fact\":\"http://tun.fi/MY.notes\",\"value\":\"previous collection owner; historiallinen museo\"},{\"fact\":\"http://tun.fi/MY.inMustikka\",\"value\":\"true\"},{\"fact\":\"http://tun.fi/MY.collectionID\",\"value\":\"http://tun.fi/HR.407\"},{\"fact\":\"http://tun.fi/MY.preservation\",\"value\":\"http://tun.fi/MY.preservationPinned\"}]},\"documentId\":\"http://tun.fi/JA.1\",\"entryDate\":\"2013-11-01T08:11:30+0200\",\"protectedDocument\":{},\"processTime\":1446121746}");
		String beautified = json.beautify();
		System.out.println(beautified);
		assertEquals(TestUtil.getTestData("expected_beautified.json"), beautified);
		// Note: beautify() is not to be used with anything serious; use toString().
		// It does not handle all cases right, like escaping escape char etc.
	}

	@Test
	public void jsonDataTypes() {
		JSONObject json = new JSONObject();
		assertTrue(json.isEmpty());
		json.setArray("arr", new JSONArray().appendString("strval").appendInteger(4));
		json.setBoolean("bool", true);
		json.setDouble("double", 53.3);
		json.setInteger("int", 2);
		json.setObject("obj", new JSONObject().setString("str", "strval"));
		json.setString("str", "strval");
		json.setString("istr", "1");

		String toStr = "{\"arr\":[\"strval\",4],\"bool\":true,\"double\":53.3,\"int\":2,\"obj\":{\"str\":\"strval\"},\"str\":\"strval\",\"istr\":\"1\"}";
		assertEquals(toStr, json.toString());
		assertEquals("arr,bool,double,int,obj,str,istr", keys(json));

		assertFalse(json.isEmpty());
		assertTrue(json.isArray("arr"));
		assertFalse(json.isArray("k"));
		assertFalse(json.isArray("obj"));
		assertTrue(json.isNull("k"));
		assertFalse(json.isNull("arr"));
		assertTrue(json.isObject("obj"));
		assertFalse(json.isObject("arr"));
		assertFalse(json.isObject("k"));
		assertEquals("[\"strval\",4]", json.getArray("arr").toString());
		try { json.getArray("bool"); } catch (Exception e) { assertEquals("Get array for key bool\n"+toStr, e.getMessage()); }
		assertEquals(true, json.getBoolean("bool"));
		try { json.getBoolean("str"); } catch (Exception e) { assertEquals("Get boolean for key str\n"+toStr, e.getMessage()); }
		assertEquals(53.3, json.getDouble("double"), 0);
		try { json.getDouble("str"); } catch (Exception e) { assertEquals("Get double for key str\n"+toStr, e.getMessage()); }
		assertEquals(1.0, json.getDouble("istr"), 0);
		assertEquals(2.0, json.getDouble("int"), 0);
		assertEquals(2, json.getInteger("int"));
		try { json.getInteger("bool"); } catch (Exception e) { assertEquals("Get int for key bool\n"+toStr, e.getMessage()); }
		try { json.getInteger("str"); } catch (Exception e) { assertEquals("Get int for key str\n"+toStr, e.getMessage()); }
		assertEquals(1, json.getInteger("istr"));
		assertEquals(53, json.getInteger("double"));
		assertEquals("{\"str\":\"strval\"}", json.getObject("obj").toString());
		try { json.getObject("arr"); } catch (Exception e) { assertEquals("Get object for key arr\n"+toStr, e.getMessage()); }
		assertEquals("strval", json.getString("str"));
		assertEquals("1", json.getString("istr"));
		assertEquals("", json.getString("obj"));
		assertEquals("true", json.getString("bool"));
		assertEquals("2", json.getString("int"));
		assertEquals("53.3", json.getString("double"));

		try { json.getBoolean("k"); } catch (Exception e) { assertEquals("No key k\n"+toStr, e.getMessage()); }
		try { json.getDouble("k"); } catch (Exception e) { assertEquals("No key k\n"+toStr, e.getMessage()); }
		try { json.getInteger("k"); } catch (Exception e) { assertEquals("No key k\n"+toStr, e.getMessage()); }

		assertEquals("[]", json.getArray("k1").toString());
		assertEquals("{}", json.getObject("k2").toString());
		assertEquals("[1.1]", json.getArray("k3").appendDouble(1.1).toString());
		assertEquals("{\"i\":1}", json.getObject("k4").setInteger("i", 1).toString());

		assertEquals("arr,bool,double,int,obj,str,istr,k1,k2,k3,k4", keys(json));
	}

	private String keys(JSONObject json) {
		return Arrays.asList(json.getKeys()).stream().collect(Collectors.joining(","));
	}

	@Test
	public void json_copy() {
		JSONObject json =  new JSONObject();
		json.setString("a", "a");
		json.setBoolean("b", true);
		json.setInteger("c", 2);
		json.setDouble("d", 3.4);
		json.setObject("e", new JSONObject().setString("e1", "ae"));
		json.setArray("f", new JSONArray().appendObject(new JSONObject().setString("a1", "a1a")));

		JSONObject copy = json.copy();
		assertEquals(json.toString(), copy.toString());

		json.setString("a", "c");
		assertNotEquals(json.toString(), copy.toString());

		copy.setString("a", "c");
		assertEquals(json.toString(), copy.toString());

		System.out.println(copy.beautify());
	}

	@Test
	public void json_array_copy() {
		JSONArray json =  new JSONArray();
		json.appendObject(new JSONObject().setString("a", "b"));

		JSONArray copy = json.copy();
		assertEquals(json.toString(), copy.toString());

		json.iterateAsObject().get(0).setString("c", "d");
		assertNotEquals(json.toString(), copy.toString());

		copy.iterateAsObject().get(0).setString("c", "d");
		assertEquals(json.toString(), copy.toString());

		System.out.println(copy.toString());
	}

	@Test
	public void json_is_array_object() {
		JSONObject json = new JSONObject();
		json.getArray("a1").appendInteger(1);
		json.getArray("a2").appendObject(new JSONObject().setString("s", "s"));
		json.getObject("o").setString("s", "s");
		json.setString("s", "s");
		json.setString("s2", "");

		assertEquals(false, json.isNull("a1"));
		assertEquals(false, json.isNull("a2"));
		assertEquals(false, json.isNull("o"));
		assertEquals(false, json.isNull("s"));
		assertEquals(false, json.isNull("s2"));
		assertEquals(true, json.isNull("xxx"));

		assertEquals(true, json.isArray("a1"));
		assertEquals(true, json.isArray("a2"));
		assertEquals(false, json.isArray("o"));
		assertEquals(false, json.isArray("s"));
		assertEquals(false, json.isArray("s2"));
		assertEquals(false, json.isArray("xxx"));

		assertEquals(false, json.isObject("a1"));
		assertEquals(false, json.isObject("a2"));
		assertEquals(true, json.isObject("o"));
		assertEquals(false, json.isObject("s"));
		assertEquals(false, json.isObject("s2"));
		assertEquals(false, json.isObject("xxx"));

		try {
			json.getArray("a1").iterateAsArray();
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("[1]", e.getMessage());
		}
		assertEquals("[1.0]", json.getArray("a1").iterateAsDouble().toString());
		assertEquals("[1]", json.getArray("a1").iterateAsInteger().toString());
		try {
			assertEquals("asd", json.getArray("a1").iterateAsObject().toString());
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("[1]", e.getMessage());
		}
		try {
			assertEquals("asd", json.getArray("a1").iterateAsString().toString());
		} catch (IllegalArgumentException e) {
			assertEquals("[1]", e.getMessage());
		}

		assertEquals(false, json.getArray("a1").isEmpty());
		assertEquals(true, json.getArray("foobar").isEmpty());
	}

	@Test
	public void urlEndoce() {
		assertEquals("verrucaria%2Fsphaeria+cetrariicola", Utils.urlEncode("verrucaria/sphaeria cetrariicola"));
	}

	@Test
	public void toCsv() {
		assertEquals("\"eka\",\"toka\"", Utils.toCSV("eka", "toka"));
		assertEquals("\"eka 'hups' 'jep'\",\"toka\"", Utils.toCSV(" eka \"hups\" \n\r  'jep'", "toka"));
	}

	@Test
	public void toTsv() {
		assertEquals("eka\ttoka", Utils.toTSV("eka", "toka"));
		assertEquals("eka \"hups\" 'jep'\ttoka", Utils.toTSV(" eka \"hups\" \n\r\t  'jep'", "toka"));
	}

	@Test
	public void removewhitespace() {
		String nonBreakingChar = String.valueOf(Character.toChars(Character.codePointAt("\u00A0", 0)));

		assertEquals(null, Utils.removeWhitespaceAround(null));
		assertEquals("", Utils.removeWhitespaceAround(""));
		assertEquals("", Utils.removeWhitespaceAround(nonBreakingChar));

		assertEquals("a", Utils.removeWhitespaceAround(" a "));
		assertEquals("a", Utils.removeWhitespaceAround(nonBreakingChar+"a"+nonBreakingChar));
		assertEquals("a", Utils.removeWhitespaceAround(nonBreakingChar+" a "+nonBreakingChar));
		assertEquals("a", Utils.removeWhitespaceAround(" " + nonBreakingChar+"a"+nonBreakingChar+" "));

		assertEquals("a", Utils.removeWhitespaceAround("\n"+"a"+"\n"));
	}

	@Test
	public void roundDecimal() {
		assertEquals(1.111, Utils.round(1.1111111111111111111, 3), 0);
		assertEquals(2, Utils.round(1.9999999999999999999, 3), 0);
		assertEquals(1.189, Utils.round(1.18888888888888, 3), 0);
		assertEquals(1.1, Utils.round(1.1111111111111111111, 1), 0);
		assertEquals(1.0, Utils.round(1.1111111111111111111, 0), 0);
	}

	@Test
	public void roundInteger() {
		assertEquals(1234600, Utils.round(1234567, 100));
		assertEquals(3238000, Utils.round(3238001, 100));
		assertEquals(6763700, Utils.round(6763699, 100));

		assertEquals(0, Utils.round(0, 10));
		assertEquals(0, Utils.round(1, 10));
		assertEquals(0, Utils.round(4, 10));
		assertEquals(10, Utils.round(5, 10));
		assertEquals(10, Utils.round(6, 10));
		assertEquals(10, Utils.round(9, 10));
		assertEquals(10, Utils.round(10, 10));
		assertEquals(10, Utils.round(11, 10));
		assertEquals(10, Utils.round(14, 10));
		assertEquals(20, Utils.round(15, 10));
		assertEquals(20, Utils.round(16, 10));
		assertEquals(20, Utils.round(19, 10));

		assertEquals(-0, Utils.round(-0, 10));
		assertEquals(-0, Utils.round(-1, 10));
		assertEquals(-0, Utils.round(-4, 10));
		assertEquals(-0, Utils.round(-5, 10));
		assertEquals(-10, Utils.round(-6, 10));
		assertEquals(-10, Utils.round(-9, 10));
		assertEquals(-10, Utils.round(-10, 10));
		assertEquals(-10, Utils.round(-11, 10));
		assertEquals(-10, Utils.round(-14, 10));
		assertEquals(-10, Utils.round(-15, 10));
		assertEquals(-20, Utils.round(-16, 10));
		assertEquals(-20, Utils.round(-19, 10));
	}

	@Test
	public void catenate() {
		assertEquals("", Utils.catenate(Utils.list("")));
		assertEquals("a", Utils.catenate(Utils.list("a")));
		assertEquals("a a", Utils.catenate(Utils.list("a", "a")));
		assertEquals("a.a", Utils.catenate(Utils.list("a", "a"), "."));
	}

	@Test
	public void capitalizeName() {
		assertEquals(null, Utils.capitalizeName(null));
		assertEquals("", Utils.capitalizeName(""));
		assertEquals("", Utils.capitalizeName(" "));
		assertEquals("Esko", Utils.capitalizeName("ESKO"));
		assertEquals("Esko-Matti", Utils.capitalizeName("ESKO-MATTI"));
		assertEquals("von Bondstroff", Utils.capitalizeName("von Bondstroff"));
		assertEquals("Von", Utils.capitalizeName("von"));
		assertEquals("Von", Utils.capitalizeName("von "));
		assertEquals("von B", Utils.capitalizeName("von B"));
		assertEquals("Antti J.", Utils.capitalizeName("ANTTI J."));
		assertEquals("A.H.", Utils.capitalizeName("A.H."));
	}

	@Test
	public void singleElementList() {
		Utils.singleEntryList(null);

		List<String> list = Utils.singleEntryList("a");
		assertEquals(false, list.isEmpty());
		assertEquals("a", list.get(0));

		try {
			list.get(-1);
		} catch (IndexOutOfBoundsException e) {}

		try {
			list.get(1);
		} catch (IndexOutOfBoundsException e) {}

		assertEquals(true, list.contains("a"));
		assertEquals(false, list.contains(null));
		assertEquals(false, list.contains("b"));
		assertEquals(true, Utils.list("a").equals(list));
		assertEquals(false, Utils.list("a", "b").equals(list));
		assertEquals(false, Utils.list().equals(list));
		assertEquals(true, list.equals(Utils.list("a")));
		assertEquals(false, list.equals(Utils.list("a", "b")));
		assertEquals(false, list.equals(Utils.list()));
		for  (String s : list) {
			assertEquals("a", s);
		}
		assertEquals(1, list.size());
		assertEquals("a", list.stream().collect(Collectors.joining()));
		try {
			list.remove(0);
			fail();
		} catch (UnsupportedOperationException e) {}
	}

}
