package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.List;

import org.junit.Test;

import fi.luomus.commons.utils.LegacyRingCodeHandlerUtil;
import fi.luomus.commons.utils.Utils;

public class RengasKoodiHandlingTests {

	@Test
	public void db_to_actual_with_all_existing_cases() {
		assertEquals("CT 123456", dbToActual("CT 123456")); // CT = 6
		assertEquals("CT 123456", dbToActual("CT0123456"));
		assertEquals("CT 003456", dbToActual("CT   3456"));
		assertEquals("V 123456",  dbToActual("V 0123456")); // V  = 6
		assertEquals("V 123456",  dbToActual("V00123456"));
		assertEquals("V 023456",  dbToActual("V  023456"));
		assertEquals("V 003456",  dbToActual("V 0003456"));
		assertEquals("V 003456",  dbToActual("V    3456"));
		assertEquals("V 003456",  dbToActual("V   03456"));
		assertEquals("ML 123456", dbToActual("ML 123456")); // ML = 5, L-loppuisista ainut jossa "M" ei mene loppuun
		assertEquals("ML 00009",  dbToActual("ML 000009"));
		assertEquals("ML 00009",  dbToActual("ML0000009"));
		assertEquals("ML 00009",  dbToActual("ML     09"));
		assertEquals("123556X",   dbToActual("XL 123556")); // XL = 6, L-loppuinen: X loppuun
		assertEquals("123556X",   dbToActual("XL0123556"));
		assertEquals("023556X",   dbToActual("XL0023556"));
		assertEquals("03556L",    dbToActual("LL0003556")); // LL = 5, L-loppuinen: L loppuun
		assertEquals("00006L",    dbToActual("LL      6"));
		assertEquals("UX 4000",   dbToActual("UX0004000")); // UX = 4
		assertEquals("UX 0400",   dbToActual("UX0000400"));
		assertEquals("T 00009",   dbToActual("T 0000009")); // T  = 5
	}

	@Test
	public void actual_to_possible_db_values_with_all_existing_cases() {
		assertEquals(list("CT0123456", "CT 123456"),							actualToDb("CT 123456")); // CT = 6 mutta emme luota siihen että olisi kuusi padattu nollilla
		assertEquals(list("CT0003456", "CT 003456", "CT  03456", "CT   3456"), 	actualToDb("CT 003456"));
		assertEquals(list("V 0123456", "V  123456"),			 				actualToDb("V 123456")); // V = 6
		assertEquals(list("ML0123456", "ML 123456"), 							actualToDb("ML 123456")); // ML = 5, L-loppuisista ainut jossa "M" ei mene loppuun
		assertEquals(list("ML0023456", "ML 023456", "ML  23456"), 				actualToDb("ML 23456"));
		assertEquals(list("XL0123556", "XL 123556"), 							actualToDb("123556X")); // XL = 6, L-loppuinen: X loppuun
		assertEquals(list("LL0003556", "LL 003556", "LL  03556", "LL   3556"), 	actualToDb("03556L")); // LL = 5, L-loppuinen: L loppuun
		assertEquals(list("UX0004000", "UX 004000", "UX  04000", "UX   4000"),	actualToDb("UX 4000")); // UX = 4
		assertEquals(list("ML0000009", "ML 000009", "ML  00009", "ML   0009", "ML    009", "ML     09", "ML      9"), actualToDb("ML 00009"));
		assertEquals(list("LL0000006", "LL 000006", "LL  00006", "LL   0006", "LL    006", "LL     06", "LL      6"), actualToDb("00006L"));
		assertEquals(list("T 0000009", "T  000009", "T   00009", "T    0009", "T     009", "T      09", "T       9"), actualToDb("T 00009")); // T  = 5


		assertEquals(list("VL0023456", "VL 023456", "VL  23456"), 				actualToDb("23456 V"));
		assertEquals(list("VL0023456", "VL 023456", "VL  23456"), 				actualToDb("23456  V"));
		assertEquals(list("VL0023456", "VL 023456", "VL  23456"), 				actualToDb("VL 23456"));
		assertEquals(list("VL0023456", "VL 023456", "VL  23456"), 				actualToDb("VL23456"));
		assertEquals(list("X 0023456", "X  023456", "X   23456"), 				actualToDb("X 23456"));
		assertEquals(list("X 0023456", "X  023456", "X   23456"), 				actualToDb("X23456"));
		assertEquals(list("LL0003556", "LL 003556", "LL  03556", "LL   3556"), 	actualToDb("03556 L"));
	}

	private Object list(String ... strings) {
		for (String s : strings) {
			if (s.length() != 9) {
				throw new IllegalArgumentException("Bad db ring: " + s + ". Length: " + s.length() + " expected 9.");
			}
		}
		List<String> list = Utils.list(strings);
		Collections.reverse(list);
		return list;
	}

	@Test
	public void db_to_actual__some_random_cases_from_actual_data() {
		assertEquals("EH 01804",  dbToActual("EH0001804"));
		assertEquals("399085J",   dbToActual("JL0399085"));
		assertEquals("DT 42720", dbToActual("DT0042720"));
		assertEquals("MM 30313",  dbToActual("MM0030313"));
		assertEquals("S 327609",  dbToActual("S 0327609"));
		assertEquals("D+ 017757",  dbToActual("D+0017757"));
	}

	@Test
	public void actual__to_db_some_random_cases_from_actual_data() {
		assertEquals(list("EH0001804", "EH 001804", "EH  01804", "EH   1804"), actualToDb("EH 01804"));
		assertEquals(list("JL0399085", "JL 399085"), actualToDb("399085J"));
		assertEquals(list("DT0042720", "DT 042720", "DT  42720"), actualToDb("DT 042720"));
		assertEquals(list("MM0030313", "MM 030313", "MM  30313"), actualToDb("MM 30313"));
		assertEquals(list("S 0327609", "S  327609"), actualToDb("S 327609"));
	}

	@Test
	public void incorrect_db_values_to_actual___these_cant_actually_be_in_the_db__but_just_in_case() {
		assertEquals("invalid:000000000",  dbToActual("000000000"));
		assertEquals("invalid:00000000X", dbToActual("00000000X"));
		assertEquals("invalid:T",  dbToActual("T"));
		assertEquals("T 00022",  dbToActual("T22"));
		assertEquals("T 00222",  dbToActual("T222"));
		assertEquals("T 00222",  dbToActual("T 222"));
		assertEquals("X 000222",  dbToActual("X 222"));
		assertEquals("invalid:null",  dbToActual(""));
		assertEquals("invalid:1",  dbToActual("1"));
		assertEquals("X 000222",  dbToActual("x 222"));
	}

	@Test
	public void incorrect_actual_outputs_to_db_values() {
		try {
			actualToDb("0000");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid ring: 0000", e.getMessage());
		}
		try {
			actualToDb("0001");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid ring: 0001", e.getMessage());
		}
		try {
			actualToDb("invalid:T");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid ring: T", e.getMessage());
		}
		assertEquals(list("T 0000222", "T  000222", "T   00222", "T    0222", "T     222"), actualToDb("T 00222"));
	}

	private String dbToActual(String ring) {
		return LegacyRingCodeHandlerUtil.dbToActual(ring);

	}

	private Object actualToDb(String ring) {
		return LegacyRingCodeHandlerUtil.actualToDb(ring);
	}

}
