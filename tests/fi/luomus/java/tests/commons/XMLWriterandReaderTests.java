package fi.luomus.java.tests.commons;

import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.Document.Node;
import fi.luomus.commons.xml.XMLReader;
import fi.luomus.commons.xml.XMLWriter;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class XMLWriterandReaderTests {

	public static Test suite() {
		return new TestSuite(XMLWriterandReaderTests.class.getDeclaredClasses());
	}

	public static class UsingXMLWriterAndReader extends TestCase {

		public void test_() {
			Document document = new Document("root");
			Node root = document.getRootNode();
			root.addAttribute("attribute", "value with \"quotes\" & 'apostrophes' ");
			root.addAttribute("second", "other <value> ");
			root.addAttribute("third");
			String xml = new XMLWriter(document).generateXML();
			String expected = "" +
					"<?xml version='1.0' encoding='utf-8'?>\n" +
					"<root attribute=\"value with &quot;quotes&quot; &amp; 'apostrophes'\" second=\"other &lt;value&gt;\" third=\"third\" />";
			assertEquals(expected.trim(), xml.trim());

			XMLReader reader = new XMLReader();
			document = reader.parse(xml);
			root = document.getRootNode();
			assertEquals("root", root.getName());
			assertEquals("", root.getContents());
			assertEquals(false, root.hasChildNodes());
			assertEquals(false, root.hasContents());
			assertEquals(0, root.getChildNodes().size());
			assertEquals(true, root.hasAttributes());
			assertEquals(3, root.getAttributes().size());
			assertEquals("attribute", root.getAttributes().get(0).getName());
			assertEquals("second", root.getAttributes().get(1).getName());
			assertEquals("value with \"quotes\" & 'apostrophes'", root.getAttributes().get(0).getValue());
			assertEquals("other <value>", root.getAttributes().get(1).getValue());
			assertEquals("third", root.getAttributes().get(2).getValue());
		}

		public void test__2() {
			Document document = new Document("root");
			Node root = document.getRootNode();
			root.addChildNode("node").setContents("value's öäÖÄ jes").addAttribute("a1", "v1");
			root.addChildNode("other-node").addChildNode("sub-node").addAttribute("a2", "v2");
			root.addChildNode("data").setCDATA("1234567890 &ent; &amp; & // &quot;    ");
			String xml = new XMLWriter(document).generateXML();
			String expected = "" +
					"<?xml version='1.0' encoding='utf-8'?>\n" +
					"<root>\n" +
					"\n" +
					"	<node a1=\"v1\">value's öäÖÄ jes</node>\n" +
					"\n" +
					"	<other-node>\n" +
					"		<sub-node a2=\"v2\" />\n" +
					"	</other-node>\n" +
					"\n" +
					"	<data>\n" +
					"<![CDATA[1234567890 &ent; &amp; & // &quot;    ]]>\n" +
					"	</data>\n" +
					"\n" +
					"</root>";
			assertEquals(expected.trim(), xml.trim());

			XMLReader reader = new XMLReader();
			document = reader.parse(xml);
			root = document.getRootNode();
			assertEquals(true, root.hasChildNodes());
			assertEquals(false, root.hasContents());
			assertEquals(3, root.getChildNodes().size());
			assertEquals(false, root.hasAttributes());
			assertEquals(0, root.getAttributes().size());

			Node node = root.getChildNodes().get(0);
			assertEquals("node", node.getName());
			assertEquals(1, node.getAttributes().size());
			assertEquals(0, node.getChildNodes().size());
			assertEquals("value's öäÖÄ jes", node.getContents());

			Node otherNode = root.getChildNodes().get(1);
			assertEquals("other-node", otherNode.getName());
			assertEquals(false, otherNode.hasAttributes());
			assertEquals(1, otherNode.getChildNodes().size());
			assertEquals("sub-node", otherNode.getChildNodes().get(0).getName());
			assertEquals("v2", otherNode.getChildNodes().get(0).getAttributes().get(0).getValue());

			assertEquals("1234567890 &ent; &amp; & // &quot;    ", root.getChildNodes().get(2).getContents());
		}

		public void test__3() {
			Document document = new Document("root");
			Node root = document.getRootNode();
			root.addChildNode("node").setContents("something with a \n newline");
			String xml = new XMLWriter(document).generateXML();
			String expected = "" +
					"<?xml version='1.0' encoding='utf-8'?>\n" +
					"<root>\n" +
					"\n" +
					"	<node>something with a \n" +
					" newline</node>\n" +
					"\n" +
					"</root>";
			assertEquals(expected.trim(), xml.trim());

			XMLReader reader = new XMLReader();
			document = reader.parse(xml);
			Node node = document.getRootNode().getNode("node");

			expected = "" +
					"something with a \n" +
					" newline";
			assertEquals(expected, node.getContents());
		}

		public void test__nodeID() {
			Node n = new Node("n");
			assertEquals(36, n.getID().length());
			assertEquals(n.getID(), n.getID());
			assertNotSame(n.getID(), new Node("n").getID());
		}

		public void test__remove_attribute() {
			Node n = new Node("n");
			n.addAttribute("foo1", "bar");
			n.addAttribute("foo2", "bar");
			assertEquals(2, n.getAttributes().size());
			n.removeAttribute("foo1");
			assertEquals(1, n.getAttributes().size());
		}

		public void test__remove_node() {
			Node n = new Node("n");
			n.addChildNode("1");
			n.addChildNode("1");
			n.addChildNode("2");
			assertEquals(3, n.getChildNodes().size());
			n.removeNodes("1");
			assertEquals(1, n.getChildNodes().size());
		}

		public void test__writing_document_with_empty_root_with_noemptyelements_set_to_false___should_write_root_element() {
			Document document = new Document("root");
			String xml = new XMLWriter(document).generateXML(XMLWriter.INCLUDE_EMPTY_ELEMENTS);
			String expected = "" +
					"<?xml version='1.0' encoding='utf-8'?>\n" +
					"<root />";
			assertEquals(expected.trim(), xml.trim());
		}

		public void test__writing_document_with_empty_root_with_noemptyelements_set_to_true___should_write_root_element() {
			Document document = new Document("root");
			String xml = new XMLWriter(document).generateXML(XMLWriter.NO_EMPTY_ELEMENTS);
			String expected = "" +
					"<?xml version='1.0' encoding='utf-8'?>\n" +
					"<root />";
			assertEquals(expected.trim(), xml.trim());
		}

		public void test__node_parent() {
			Document document = new Document("root");
			assertEquals(null, document.getRootNode().getParent());
			Node root = document.getRootNode();
			root.addChildNode("child");
			assertEquals(root, root.getNode("child").getParent());
		}

		public void test__apos() {
			Document doc = new Document("root");
			doc.getRootNode().setContents("talazac's shrew tenrec");
			doc.getRootNode().addAttribute("attr", "v'lue");
			assertEquals("talazac's shrew tenrec", doc.getRootNode().getContents());
			assertEquals("v'lue", doc.getRootNode().getAttribute("attr"));

			String xml = new XMLWriter(doc).generateXML();
			String expected = "" +
					"<?xml version='1.0' encoding='utf-8'?>\n" +
					"<root attr=\"v'lue\">talazac's shrew tenrec</root>\n";
			assertEquals(expected, xml);

			doc = new XMLReader().parse(xml);
			assertEquals("talazac's shrew tenrec", doc.getRootNode().getContents());
			assertEquals("v'lue", doc.getRootNode().getAttribute("attr"));
		}
	}

}
