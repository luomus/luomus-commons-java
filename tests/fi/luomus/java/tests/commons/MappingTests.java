package fi.luomus.java.tests.commons;

import java.util.Iterator;

import fi.luomus.commons.containers.Mapping;
import fi.luomus.commons.containers.Mapping.AlreadyMappedException;
import fi.luomus.commons.containers.Mapping.Pair;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MappingTests {

	public static Test suite() {
		return new TestSuite(MappingTests.class.getDeclaredClasses());
	}

	public static class WhenCreatingAndUsingMappings extends TestCase {

		public void test___1() {
			Mapping<String> mapping = new Mapping<>();
			mapping.map("A", "B");
			assertEquals("B", mapping.get("A"));
			assertEquals(null, mapping.get("B"));
			assertEquals("A", mapping.getInverse("B"));
			assertEquals(null, mapping.get("C"));
			assertEquals(null, mapping.getInverse("C"));
		}
		
		public void test___2() {
			Mapping<String> mapping = new Mapping<>();
			
			mapping.map("1", "A");
			
			try {
				mapping.map("2", "A");
				fail();
			} catch (AlreadyMappedException e) {
				assertEquals("A is already mapped to 1", e.getMessage());
			}
			
			try {
				mapping.map("1", "B");
				fail();
			} catch (AlreadyMappedException e) {
				assertEquals("1 is already mapped to A", e.getMessage());
			}
			
			mapping.map("3", "3");

			try {
				mapping.map("4", "3");
				fail();
			} catch (AlreadyMappedException e) {
				assertEquals("3 is already mapped to 3", e.getMessage());
			}
			
			assertEquals("A", mapping.get("1"));
			assertEquals("1", mapping.getInverse("A"));
			assertEquals("3", mapping.get("3"));
			assertEquals("3", mapping.getInverse("3"));
		}
		
		public void test__iterator() {
			Mapping<String> sijoitukset = new Mapping<>();
			sijoitukset.map("eka", "1");
			sijoitukset.map("toka", "2");
			sijoitukset.map("kolmas", "3");
			int i = 1;
			for (Pair<String> pair : sijoitukset) {
				if (i == 1) {
					assertEquals("eka", pair.getValue1());
					assertEquals("1", pair.getValue2());
				}
				if (i == 2) {
					assertEquals("toka", pair.getValue1());
					assertEquals("2", pair.getValue2());
				}
				if (i == 3) {
					assertEquals("kolmas", pair.getValue1());
					assertEquals("3", pair.getValue2());
				}
				i++;
			}
		}
		
		public void test__iterator_inverse() {
			Mapping<String> sijoitukset = new Mapping<>();
			sijoitukset.map("eka", "1");
			sijoitukset.map("toka", "2");
			sijoitukset.map("kolmas", "3");
			int i = 1;
			Iterator<Pair<String>> iter = sijoitukset.inverseIterator();
			while (iter.hasNext()) {
				Pair<String> pair = iter.next();
				if (i == 1) {
					assertEquals("1", pair.getValue1());
					assertEquals("eka", pair.getValue2());
				}
				if (i == 2) {
					assertEquals("2", pair.getValue1());
					assertEquals("toka", pair.getValue2());
				}
				if (i == 3) {
					assertEquals("3", pair.getValue1());
					assertEquals("kolmas", pair.getValue2());
				}
				i++;
			}
		}
	}

}


