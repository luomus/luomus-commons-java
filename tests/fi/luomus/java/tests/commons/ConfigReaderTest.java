package fi.luomus.java.tests.commons;

import fi.luomus.commons.config.ConfigReader;
import fi.luomus.commons.db.connectivity.ConnectionDescription;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ConfigReaderTest {
	
	public static Test suite() {
		return new TestSuite(ConfigReaderTest.class.getDeclaredClasses());
	}
	
	public static class ReadingPropertiesFromConfigFile extends TestCase {
		
		private static final String	TEMP	= "tempfileforConfigReaderTest";
		private ConfigReader		r		= null;
		
		@Override
		protected void setUp() throws Exception {
			File f = new File(TEMP);
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f)));
			writer.write("DBdriver = driver");
			writer.newLine();
			writer.write("DBurl = url");
			writer.newLine();
			writer.write("DBusername = username");
			writer.newLine();
			writer.write("DBpassword = password");
			writer.newLine();
			writer.close();
			r = new ConfigReader(TEMP);
			if (r == null) fail("didn't open the file");
		}
		
		@Override
		protected void tearDown() throws Exception {
			File f = new File(TEMP);
			if (!f.delete()) throw new Exception("delete failed: ConfigReader blocks the file");
		}
		
		public void test_itFailsToOpenAFileThatDoesntExists() {
			try {
				new ConfigReader("thisfiledoesntexists");
			} catch (Exception e) {
				return;
			}
			fail("Didn't fail");
		}
		
		public void test_reading_a_property() throws Exception {
			assertEquals("url", r.get("DBurl"));
			try {
				r.get("asdasdas");
				fail("didn't fail");
			} catch (IllegalArgumentException e) {
			} catch (Exception e) {
				fail("throws some other exceptions than IllegalArgumentException");
			}
		}
		
		public void test_reading_connection_description() throws Exception {
			ConnectionDescription d = r.connectionDescription();
			assertEquals("url", d.url());
		}
		
	}
	
}
