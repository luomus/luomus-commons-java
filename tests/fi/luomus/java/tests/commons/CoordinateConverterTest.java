package fi.luomus.java.tests.commons;

import fi.luomus.commons.utils.CoordinateConverter;
import fi.luomus.commons.utils.Utils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class CoordinateConverterTest {
	
	public static Test suite() {
		return new TestSuite(CoordinateConverterTest.class.getDeclaredClasses());
	}
	
	public static class WhenConvertingCoordinates extends TestCase {
		
		private CoordinateConverter	converter;
		
		@Override
		protected void setUp() {
			converter = new CoordinateConverter();
		}
		
		public void test__no_source_given() throws Exception {
			converter.setAst_leveys("25");
			try {
				converter.convert();
				fail("Should have thrown exception");
			} catch (IllegalStateException e) {
			}
		}
		
		public void test__converting_from_yht() throws Exception {
			converter.setYht_leveys("6899295");
			converter.setYht_pituus("3210757");
			
			converter.convert();
			
			assertEquals(Long.valueOf(6896401), converter.getEur_leveys());
			assertEquals(Long.valueOf(210703), converter.getEur_pituus());
			
			assertEquals(round(62.0881693), round(converter.getDes_leveys()));
			assertEquals(round(21.4593275), round(converter.getDes_pituus()));
			
			assertEquals(Long.valueOf(620517), converter.getAst_leveys());
			assertEquals(Long.valueOf(212733), converter.getAst_pituus());
		}
		
		private String round(double value) {
			String valueString = Double.toString(value); 
			return valueString.substring(0, length(valueString));
		}
		private int length(String value) {
			if (value.length() > 10) {
				return 10;
			}
			return value.length();
		}
		
		public void test__converting_from_yht_2() throws Exception {
			converter.setYht_leveys("6979294");
			converter.setYht_pituus("3716756");
			
			converter.convert();
			
			assertEquals(Long.valueOf(6976371), converter.getEur_leveys());
			assertEquals(Long.valueOf(716497), converter.getEur_pituus());
			
			assertEquals(round(62.8525758), round(converter.getDes_leveys()));
			assertEquals(round(31.2581838), round(converter.getDes_pituus()));
			
			assertEquals(Long.valueOf(625109), converter.getAst_leveys());
			assertEquals(Long.valueOf(311529), converter.getAst_pituus());
		}
		
		public void test__converting_from_yht_3() throws Exception {
			converter.setYht_leveys("7676979");
			converter.setYht_pituus("3244527");
			
			converter.convert();
			
			assertEquals(Long.valueOf(7673776), converter.getEur_leveys());
			assertEquals(Long.valueOf(244457), converter.getEur_pituus());
			
			assertEquals(round(69.0536776), round(converter.getDes_leveys()));
			assertEquals(round(20.5895646), round(converter.getDes_pituus()));
			
			assertEquals(Long.valueOf(690313), converter.getAst_leveys());
			assertEquals(Long.valueOf(203522), converter.getAst_pituus());
		}
		
		public void test__converting_from_yht_4() throws Exception {
			converter.setYht_leveys("6698979");
			converter.setYht_pituus("3088527");
			
			converter.convert();
			
			assertEquals(Long.valueOf(6696166), converter.getEur_leveys());
			assertEquals(Long.valueOf(88525), converter.getEur_pituus());
			
			assertEquals(round(60.1935008), round(converter.getDes_leveys()));
			assertEquals(round(19.5723869), round(converter.getDes_pituus()));
			
			assertEquals(Long.valueOf(601136), converter.getAst_leveys());
			assertEquals(Long.valueOf(193420), converter.getAst_pituus());
		}
		
		public void test__converting_from_eur() throws Exception {
			converter.setEur_leveys("6668179");
			converter.setEur_pituus("382405");
			
			converter.convert();
			
			assertEquals(Long.valueOf(6670979), converter.getYht_leveys());
			assertEquals(Long.valueOf(3382527), converter.getYht_pituus());
			
			assertEquals(round(60.1334734), round(converter.getDes_leveys()));
			assertEquals(round(24.8860862), round(converter.getDes_pituus()));
			
			assertEquals(Long.valueOf(600800), converter.getAst_leveys());
			assertEquals(Long.valueOf(245309), converter.getAst_pituus());
		}
		
		public void test__converting_from_eur2() throws Exception {
			converter.setEur_leveys("7501844");
			converter.setEur_pituus("626304");
			
			converter.convert();
			
			assertEquals(Long.valueOf(7504979), converter.getYht_leveys());
			assertEquals(Long.valueOf(3626527), converter.getYht_pituus());
			
			assertEquals(round(67.6042827), round(converter.getDes_leveys()));
			assertEquals(round(29.9754721), round(converter.getDes_pituus()));
			
			assertEquals(Long.valueOf(673615), converter.getAst_leveys());
			assertEquals(Long.valueOf(295831), converter.getAst_pituus());
		}
		
		public void test__converting_from_ast() throws Exception {
			converter.setAst_leveys("625744");
			converter.setAst_pituus("254226");
			
			converter.convert();
			
			assertEquals(round(62.9622222), round(converter.getDes_leveys()));
			assertEquals(round(25.7072222), round(converter.getDes_pituus()));
			
			assertTrue(Utils.fitsInRange(converter.getYht_leveys(), 6984997, 10));
			assertTrue(Utils.fitsInRange(converter.getYht_pituus(), 3434407, 10));
			
			assertTrue(Utils.fitsInRange(converter.getEur_leveys(), 6982070, 10));
			assertTrue(Utils.fitsInRange(converter.getEur_pituus(), 434262, 10));
		}
		
		public void test__converting_from_ast2() throws Exception {
			converter.setAst_leveys("660806");
			converter.setAst_pituus("235751");
			
			converter.convert();
			
			assertEquals(round(66.135), round(converter.getDes_leveys()));
			assertEquals(round(23.9641666), round(converter.getDes_pituus()));
			
			assertTrue(Utils.fitsInRange(converter.getYht_leveys(), 7341396, 10));
			assertTrue(Utils.fitsInRange(converter.getYht_pituus(), 3362921, 10));
			
			assertTrue(Utils.fitsInRange(converter.getEur_leveys(), 7338326, 10));
			assertTrue(Utils.fitsInRange(converter.getEur_pituus(), 362804, 10));
		}
		
		public void test__converting_from_yht_5() throws Exception {
			converter.setYht_leveys("6762000");
			converter.setYht_pituus("3353800");
			
			converter.convert();
			
			assertEquals(Long.valueOf(6759163), converter.getEur_leveys());
			assertEquals(Long.valueOf(353690), converter.getEur_pituus());
			
			assertEquals(round(60.9402073), round(converter.getDes_leveys()));
			assertEquals(round(24.30264782), round(converter.getDes_pituus()));
			
			assertEquals(Long.valueOf(605624), converter.getAst_leveys());
			assertEquals(Long.valueOf(241809), converter.getAst_pituus());
		}
		
		public void test__converting_foregn_coordinates() throws Exception {
			converter.setDes_leveys("0.7");
			converter.setDes_pituus("-57.0");
			
			converter.convertToDegreesOnly();
			
			assertEquals(Long.valueOf(4200), converter.getAst_leveys());
			assertEquals(Long.valueOf(-570000), converter.getAst_pituus());
		}
		
		public void test__converting_foregn_coordinates_2() throws Exception {
			converter.setDes_leveys("0.703");
			converter.setDes_pituus("-57.011");
			
			converter.convertToDegreesOnly();
			
			assertEquals(Long.valueOf(4210), converter.getAst_leveys());
			assertEquals(Long.valueOf(-570039), converter.getAst_pituus());
		}
		
		public void test__converting_foregn_coordinates_3() throws Exception {
			converter.setAst_leveys("4211");
			converter.setAst_pituus("-570040");
			
			converter.convertToDecimalsOnly();
			
			assertEquals(round(0.70305555555), round(converter.getDes_leveys()));
			assertEquals(round(-57.011111111), round(converter.getDes_pituus()));
		}
		
		public void test__degrees_rounding() throws Exception {
			converter.setDes_leveys("61.449999999999");
			converter.setDes_pituus("28.99999999999"); 
			
			converter.convertToDegreesOnly();
			
			assertEquals(Long.valueOf(612659), converter.getAst_leveys());
			assertEquals(Long.valueOf(285959), converter.getAst_pituus());
		}
		
		public void test__degrees_leading_zeros() throws Exception {
			converter.setAst_leveys("005800");
			converter.setAst_pituus("333333"); 
			
			converter.convertToDecimalsOnly();
			
			assertEquals(round(0.96666666), round(converter.getDes_leveys()));
			assertEquals(round(33.5591666), round(converter.getDes_pituus()));
		}
		
		public void test__degrees_leading_zeros_2() throws Exception {
			converter.setAst_leveys("00439");
			converter.setAst_pituus("240020"); 
			
			converter.convertToDecimalsOnly();
			
			assertEquals(round(0.0775), round(converter.getDes_leveys()));
			assertEquals(round(24.0055555), round(converter.getDes_pituus()));
		}
		
		public void test__degrees_from_far_far_away_with_three_number_lat_or_lon() throws Exception {
			converter.setAst_leveys("632700");
			converter.setAst_pituus("1202000"); // 120° 20' 00"
			
			converter.convertToDecimalsOnly();
			
			assertEquals(round(63.45), round(converter.getDes_leveys()));
			assertEquals(round(120.33333333333333), round(converter.getDes_pituus()));
		}	
		
		public void test__decimals_to_degrees_from_far_far_away_with_three_number_lat_or_lon() throws Exception {
			converter.setDes_leveys("63.45");
			converter.setDes_pituus("120.33333333333333");
			
			converter.convertToDegreesOnly();
			
			assertEquals(Long.valueOf(632700), converter.getAst_leveys());
			assertEquals(Long.valueOf(1202000), converter.getAst_pituus());
		}
				
	}
	
}
