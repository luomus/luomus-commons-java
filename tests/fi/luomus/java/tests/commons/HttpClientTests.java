package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.net.ssl.SSLPeerUnverifiedException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.commons.http.HttpClientService;
import fi.luomus.commons.http.HttpClientService.NotFoundException;
import fi.luomus.commons.json.JSONObject;
import fi.luomus.commons.xml.Document;

public class HttpClientTests {

	private HttpClientService client = null;

	@Before
	public void initClient() {
		client = new HttpClientService();
	}

	@After
	public void closeClient() {
		if (client != null) client.close();
	}

	@Test
	public void nonSSLConnection() throws Exception {
		String content = client.contentAsString(new HttpGet("http://google.fi"));
		assertTrue(content.startsWith("<!doctype html"));
	}

	@Test
	public void SSLConnectionWithValidSertificate() throws Exception {
		String content = client.contentAsString(new HttpGet("https://google.fi"));
		System.out.println(content);
		assertTrue(content.startsWith("<!doctype html>"));
	}

	@Test
	public void SSLConnectionWithSelfSigned() throws Exception {
		try (CloseableHttpResponse res = client.execute(new HttpGet("https://intra.digitarium.fi/"))) {

		} catch (SSLPeerUnverifiedException e) {
			fail("should accept invalid cert");
		}
	}

	@Test
	public void contentAsJson() throws Exception {
		HttpGet request = new HttpGet("http://laji.fi/api/taxa/MX.1");
		JSONObject json = client.contentAsJson(request);
		assertEquals("MX.1", json.getString("id"));
	}

	@Test
	public void contentAsJson_404() throws Exception {
		HttpGet request = new HttpGet("http://laji.fi/api/taxa/MX.1a");
		try {
			client.contentAsJson(request);
			fail("Should throw exception");
		} catch (NotFoundException e) {
			assertEquals("{\"error\":{\"statusCode\":404,\"name\":\"Error\",\"message\":\"Not found\",\"details\":\"Could not find taxon MX.1a\"}}",
					e.getMessage());
		}
	}

	@Test
	public void contentAsJson_invalidjson() {
		HttpGet request = new HttpGet("http://tun.fi/KE.3?format=RDFXML");
		try {
			client.contentAsJson(request);
			fail("Should throw exception");
		} catch (Exception e) {
			assertTrue(e.getMessage().startsWith("A JSONObject text must begin with '{' at 1 [character 2 line 1]. The JSON:"));
		}
	}

	@Test
	public void contentAsXml() throws Exception {
		HttpGet request = new HttpGet("http://tun.fi/KE.3?format=RDFXML");
		Document d = client.contentAsDocument(request);
		assertEquals("http://tun.fi/KE.3", d.getRootNode().getChildNodes().get(0).getAttribute("rdf:about"));
	}

	@Test
	public void contentAsXml_invalidxml() {
		HttpGet request = new HttpGet("http://laji.fi/api/taxa/MX.1");
		try {
			client.contentAsDocument(request);
			fail("Should throw exception");
		} catch (Exception e) {
			assertTrue(e.getMessage().startsWith("Error parsing xml. Xml content was: {\""));
		}
	}

	@Test
	public void contentAsXml_404() throws Exception {
		HttpGet request = new HttpGet("http://laji.fi/api/taxa/MX.1a");
		try {
			client.contentAsDocument(request);
			fail("Should throw exception");
		} catch (NotFoundException e) {
			assertEquals("{\"error\":{\"statusCode\":404,\"name\":\"Error\",\"message\":\"Not found\",\"details\":\"Could not find taxon MX.1a\"}}", e.getMessage());
		}
	}

	@Test
	public void contentAsList() throws Exception {
		List<String> response = client.contentAsList(new HttpGet("https://bitbucket.org/luomus/luomus-commons-java/raw/HEAD/readme.md"));
		assertEquals("luomus-commons.jar", response.get(0));
	}

	@Test
	public void basicAuth() throws NotFoundException, ClientProtocolException, IOException, URISyntaxException {
		// http://httpbin.org/basic-auth/  expects credentials that are given as parameters  (  /user/passwd )
		try (
				HttpClientService client = new HttpClientService();
				CloseableHttpResponse res = client.execute(new HttpGet("http://httpbin.org/basic-auth/user/passwd"))) {
			assertEquals(401, res.getStatusLine().getStatusCode());
		}

		try (HttpClientService client = new HttpClientService("http://httpbin.org", "user", "passwd")) {
			JSONObject res = client.contentAsJson(new HttpGet("http://httpbin.org/basic-auth/user/passwd"));
			assertEquals(true, res.getBoolean("authenticated"));
		}
	}

	@Test
	public void basicAuth_isThreadSafe() throws URISyntaxException, InterruptedException {
		try (HttpClientService client = new HttpClientService("http://httpbin.org", "user", "passwd")) {
			Runnable r = new Runnable() {
				@Override
				public void run() {
					for (int i = 0; i<5; i++) {
						try {
							JSONObject res = client.contentAsJson(new HttpGet("http://httpbin.org/basic-auth/user/passwd"));
							assertEquals(true, res.getBoolean("authenticated"));
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
					}
				}
			};

			Thread t1 = new Thread(r);
			Thread t2 = new Thread(r);
			Thread t3 = new Thread(r);
			t1.start();
			t2.start();
			t3.start();
			t1.join();
			t2.join();
			t3.join();
		}
	}

}
