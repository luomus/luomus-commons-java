package fi.luomus.java.tests.commons;

import fi.luomus.commons.containers.Selection;
import fi.luomus.commons.containers.Selection.SelectionOption;
import fi.luomus.commons.containers.SelectionImple;
import fi.luomus.commons.containers.SelectionOptionImple;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class SelectionsTest {

	public static Test suite() {
		return new TestSuite(SelectionsTest.class.getDeclaredClasses());
	}

	public static class WhenCreatingAndUsingSelections extends TestCase {

		public void test___options() {
			SelectionOption o = new SelectionOptionImple("value", "description");
			assertEquals("VALUE", o.getValue());
			assertEquals("description", o.getText());
		}

		public void test___selections() {
			Selection s = new SelectionImple("PESA.puulaji"); 
			//assertEquals("pesa.puulaji", s.getName());
			try {
				s.get("a");
				fail("Should throw exception: does not contain value");
			} catch (IllegalArgumentException e) {
				assertEquals("Selection pesa.puulaji does not define value 'a'.", e.getMessage());
			}
		}

		public void test__adding_option() {
			SelectionImple s = new SelectionImple("name");
			s.addOption(new SelectionOptionImple("value", "descripTION"));
			assertEquals("VALUE", s.getOptions().iterator().next().getValue());
			assertEquals("descripTION", s.getOptions().iterator().next().getText());
		}

		public void test__adding_option_with_existing_value() {
			SelectionImple s = new SelectionImple("my-selection");
			s.addOption(new SelectionOptionImple("myvalue", "descripTION"));
			try {
				s.addOption(new SelectionOptionImple("myvalue", "whatever"));
				fail("Should throw exception");
			} catch (IllegalStateException e) {
				assertEquals("Selection my-selection already contains value MYVALUE" , e.getMessage());
			}
		}
		
		public void test__it_maintais_correct_order() {
			SelectionImple s = new SelectionImple("name");
			s.addOption(new SelectionOptionImple("a", "A"));
			s.addOption(new SelectionOptionImple("b", "B"));
			s.addOption(new SelectionOptionImple("c", "C"));
			s.addOption(new SelectionOptionImple("d", "D"));
			s.addOption(new SelectionOptionImple("e", "E"));
			s.addOption(new SelectionOptionImple("f", "F"));
			s.addOption(new SelectionOptionImple("g", "G"));
			s.addOption(new SelectionOptionImple("h", "H"));
			String descriptions = "";
			for (SelectionOption o : s) {
				descriptions += o.getText();
			}
			assertEquals("ABCDEFGH", descriptions);
		}

		public void test___containsValue() {
			SelectionImple s = new SelectionImple("name");
			s.addOption(new SelectionOptionImple("a", "A"));
			Selection selection = s;
			assertTrue("Should contain this option", selection.containsOption("A"));
			assertTrue("Should contain this option", selection.containsOption("a"));
			assertFalse("Should NOT contain this option", selection.containsOption("b"));
		}

//		public void test___ei_html_enkoodausta() {
//			SelectionImple s = new SelectionImple("name");
//			s.addOption(new SelectionOptionImple("a", "A <> \" ' "));
//			assertEquals("A <> \" ' ", s.get("A"));
//		}
		
		public void test___size() {
			SelectionImple s = new SelectionImple("name");
			s.addOption(new SelectionOptionImple("A", "A")); // 1
			s.addOption(new SelectionOptionImple("B", "B")); // 2
			assertEquals(2, s.getSize());
		}
		
		public void test___size_2() {
			SelectionImple s = new SelectionImple("name");
			s.addOption(new SelectionOptionImple("A", "A", "Group 1")); // 2
			s.addOption(new SelectionOptionImple("B", "B", "Group 1")); // 3
			s.addOption(new SelectionOptionImple("C", "C", "Group 2")); // 5
			assertEquals(5, s.getSize());
		}
		
		public void test___size_3() {
			SelectionImple s = new SelectionImple("name");
			s.addOption(new SelectionOptionImple("A", "A", "")); // 1
			s.addOption(new SelectionOptionImple("B", "B", "")); // 2
			s.addOption(new SelectionOptionImple("C", "C", "Group 1")); // 4
			s.addOption(new SelectionOptionImple("D", "D", "Group 1")); // 5
			s.addOption(new SelectionOptionImple("E", "E", "")); // 6
			s.addOption(new SelectionOptionImple("F", "F", "")); // 7
			s.addOption(new SelectionOptionImple("G", "G", "Group 2")); // 9
			assertEquals(9, s.getSize());
		}
		
		public void test___values_are_caseinsensitive() {
			SelectionImple s = new SelectionImple("My Selection");
			s.addOption(new SelectionOptionImple("value", "Desc"));
			assertEquals("Desc", s.get("value"));
			assertEquals("Desc", s.get("valUe"));
		}
		
		public void test___localization_of_description_texts() {
			SelectionImple s = new SelectionImple("My Selection");
			s.addOption(new SelectionOptionImple("value", "Default desc").setText("fi", "Suomi desc"));
			assertEquals("Default desc", s.get("value"));
			assertEquals("Default desc", s.get("value", "en"));
			assertEquals("Default desc", s.get("value", "EN"));
			assertEquals("Default desc", s.get("value", null));
			assertEquals("Suomi desc", s.get("VALUE", "fi"));
			assertEquals("Suomi desc", s.get("VALUE", "FI"));
		}
		
		public void test___localization_of_optgroups() {
			SelectionImple s = new SelectionImple("My Selection");
			s.addOption(new SelectionOptionImple("value", "Default desc", "My optgroup").setText("fi", "Suomi desc").setGroup("fi", "Minun optgrouppi"));
			
			SelectionOption o = s.getOption("value");
			
			assertEquals("My optgroup", o.getGroup());
			assertEquals("My optgroup", o.getGroup("en"));
			assertEquals("My optgroup", o.getGroup("EN"));
			assertEquals("My optgroup", o.getGroup(null));
			assertEquals("Minun optgrouppi", o.getGroup("fi"));
			assertEquals("Minun optgrouppi", o.getGroup("FI"));
		}
		
		public void test___localization_of_optgroups_2() {
			SelectionImple s = new SelectionImple("My Selection");
			s.addOption(new SelectionOptionImple("value", "Default desc", "My optgroup").setText("fi", "Suomi desc").setGroup("fi", "Minun optgrouppi"));
			s.addOption(new SelectionOptionImple("othervalue", "Other desc"));
			
			SelectionOption o = s.getOption("othervalue");
			
			assertEquals("", o.getGroup());
			assertEquals("", o.getGroup("en"));
			assertEquals("", o.getGroup("EN"));
			assertEquals("", o.getGroup(null));
			assertEquals("", o.getGroup("fi"));
			assertEquals("", o.getGroup("FI"));
		}
		
	}
	
}
