package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;

import fi.luomus.commons.utils.FileUtils;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.Test;

public class UsingFileUtils {

	File testfile = new File("Luomus_commons_UtilsTest_testfile");

	@After
	public void tearDown() {
		if (testfile.exists() && testfile.isFile()) {
			try {
				testfile.delete();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void test___getting_file_extension() {
		File file = new File("x:CD/jj.abf/myfile.name.has.dots.txt");
		assertEquals("txt", FileUtils.getFileExtension(file));

		file = new File("x:CD/folder.has.dots/myfile");
		assertEquals("", FileUtils.getFileExtension(file));

		file = new File("x:CD/folder.has.dots/myfile.txt");
		assertEquals("txt", FileUtils.getFileExtension(file));

		file = new File("x:CD/folder.has.dots.and.different.file.separator\\myfile");
		assertEquals("", FileUtils.getFileExtension(file));

		file = new File("myfile");
		assertEquals("", FileUtils.getFileExtension(file));

		file = new File(".myfile");
		assertEquals("", FileUtils.getFileExtension(file));

		file = new File("../myfile");
		assertEquals("", FileUtils.getFileExtension(file));
	}

	@Test
	public void test___writing_utf_8() throws IOException {
		FileUtils.writeToFile(testfile, "κόσμε русский алфавит Ą Ć Ę Ł Ń Ó Ś Ź Ż ą ń ł");
		assertEquals("κόσμε русский алфавит Ą Ć Ę Ł Ń Ó Ś Ź Ż ą ń ł", FileUtils.readContents(testfile));
	}

	@Test
	public void test___writing_utf_8_with_append() throws IOException {
		FileUtils.writeToFile(testfile, "firstline?\n", true);
		FileUtils.writeToFile(testfile, "secondline\n", true);
		FileUtils.writeToFile(testfile, "third", true);
		FileUtils.writeToFile(testfile, "andfourthline\n", true);
		FileUtils.writeToFile(testfile, "κόσμε русский алфавит Ą Ć Ę Ł Ń Ó Ś Ź Ż ą ń ł", true);
		assertEquals("" +
				"firstline?\n"+
				"secondline\n"+
				"thirdandfourthline\n"+
				"κόσμε русский алфавит Ą Ć Ę Ł Ń Ó Ś Ź Ż ą ń ł", 
				FileUtils.readContents(testfile));
	}
}