package fi.luomus.java.tests.commons;

import fi.luomus.commons.db.connectivity.ConnectionDescription;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ConnectionDescriptionTest {
	
	public static Test suite() {
		return new TestSuite(ConnectionDescriptionTest.class.getDeclaredClasses());
	}
	
	public static class Initializing_Connection_description extends TestCase {
		
		private ConnectionDescription	descr;
		
		@Override
		protected void setUp() {}
		
		public void test_good_parameters() {
			descr = new ConnectionDescription("driver", "url", "username", "password");
			assertEquals("driver", descr.driver());
			assertEquals("url", descr.url());
			assertEquals("username", descr.username());
			assertEquals("password", descr.password());
		}
		
		public void test_illegal_url() {
			try {
				descr = new ConnectionDescription("d", null, "u", "p");
				fail("Should fail to null url");
			} catch (IllegalArgumentException e) {
			}
			try {
				descr = new ConnectionDescription("d", "", "u", "p");
				fail("Should fail to empty url");
			} catch (IllegalArgumentException e) {
			}
		}
		
		public void test_illegal_username() {
			try {
				descr = new ConnectionDescription("d", "u", null, "p");
				fail("Should fail to null username");
			} catch (IllegalArgumentException e) {
			}
			try {
				descr = new ConnectionDescription("d", "u", "", "p");
				fail("Should fail to empty username");
			} catch (IllegalArgumentException e) {
			}
		}
		
		public void test_illegal_password() {
			try {
				descr = new ConnectionDescription("d", "u", "u", null);
				fail("Should fail to null password");
			} catch (IllegalArgumentException e) {
			}
			try {
				descr = new ConnectionDescription("d", "u", "u", "");
				fail("Should fail to empty password");
			} catch (IllegalArgumentException e) {
			}
		}
		
	}
	
}
