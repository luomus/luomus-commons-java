package fi.luomus.java.tests.commons;

import java.awt.Point;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import fi.luomus.commons.utils.SerializationCache;
import fi.luomus.commons.utils.SerializationCache.Loader;
import fi.luomus.commons.utils.SerializationCache.LoaderNotSetException;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class SerializationCacheTests {

	public static Test suite() {
		return new TestSuite(SerializationCacheTests.class.getDeclaredClasses());
	}

	public static class WhenUsingPermanentCache extends TestCase {

		private static final String NAME_FOR_THE_CACHE = "NameForTheCache";
		
		@Override
		public void tearDown() {
			File file = SerializationCache.getSerializetionFileFor(NAME_FOR_THE_CACHE);
			if (file.exists()) {
				file.delete();
			}
		}
		
		public void test___getting_string_object() {
			SerializationCache<String, String> cache = new SerializationCache<>(NAME_FOR_THE_CACHE);
			cache.setLoader(new Loader<String, String>() {
				@Override
				public String load(String key) {
					return "bar";
				} 
			});
			String value = cache.get("foo");
			assertEquals("bar", value);
		}

		public void test___getting_different_string_object() {
			SerializationCache<String, String> cache = new SerializationCache<>(NAME_FOR_THE_CACHE);
			cache.setLoader(new Loader<String, String>() {
				@Override
				public String load(String key) {
					if (key.equals("foo")) return "bar";
					return "bar2";
				} 
			});
			assertEquals("bar", cache.get("foo"));
			assertEquals("bar2", cache.get("foo2"));
		}

		public void test___getting_custom_object_type_with_custom_object_key() {
			SerializationCache<Integer, Point> cache = new SerializationCache<>(NAME_FOR_THE_CACHE);
			cache.setLoader(new Loader<Integer, Point>() {
				@Override
				public Point load(Integer key) {
					return new Point();
				} 
			});
			Object point = cache.get(new Integer(1));
			assertTrue(point instanceof Point);
		}

		public static class SomeObject implements Serializable {
			private static final long serialVersionUID = 1L;
		}
		
		public void test___will_support_any_objects_that_impelments_seriaziable() {
			SerializationCache<String, SomeObject> cache = new SerializationCache<>(NAME_FOR_THE_CACHE);
			cache.setLoader(new Loader<String, SomeObject>() {
				@Override
				public SomeObject load(String key) {
					return new SomeObject();
				}
			});
			Object o = cache.get("key");
			assertTrue(o instanceof SomeObject);
		}

		public void test__will_throw_exception_if_loader_not_set() {
			SerializationCache<String, String> cache = new SerializationCache<>(NAME_FOR_THE_CACHE);
			try {
				cache.get("key");
				fail("Should throw exception");
			} catch (LoaderNotSetException e) {

			}
		}

		public void test___will_load_from_cache_if_already_loaded() {
			SerializationCache<String, String> cache = new SerializationCache<>(NAME_FOR_THE_CACHE);
			class TestLoader implements Loader<String, String> {
				Set<String> calledFor = new HashSet<>();
				@Override
				public String load(String key) {
					if (calledFor.contains(key)) {
						throw new IllegalStateException("Should not call loader twice!");
					}
					calledFor.add(key);
					return "cached" + key;
				}
			}
			cache.setLoader(new TestLoader());
			String value = cache.get("foo");
			assertEquals("cachedfoo", value);
			cache.get("foo");
		}
		
		public void test___will_remember_values() throws InterruptedException {
			SerializationCache<String, String> cache = new SerializationCache<>(NAME_FOR_THE_CACHE);
			class TestLoader implements Loader<String, String> {
				@Override
				public String load(String key) {
					return "cached" + key + "_" + new Date().getTime();
				}
			}
			cache.setLoader(new TestLoader());
			String value = cache.get("foo");
			
			Thread.sleep(20);
			
			cache = new SerializationCache<>(NAME_FOR_THE_CACHE);
			cache.setLoader(new TestLoader());
			
			assertEquals(value, cache.get("foo"));
		}
		
		
	}
	
}
