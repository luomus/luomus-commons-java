package fi.luomus.java.tests.commons;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

@SuppressWarnings("deprecation")
public class HttpSessionStub implements HttpSession {
	
	private final HashMap<String, Object>	values;
	
	public HttpSessionStub() {
		values = new HashMap<>();
	}
	
	@Override
	public Object getAttribute(String arg0) {
		// Auto-generated method stub
		return values.get(arg0);
	}
	
	
	@Override
	public Enumeration<String> getAttributeNames() {
		// Auto-generated method stub
		return null;
	}
	
	@Override
	public long getCreationTime() {
		// Auto-generated method stub
		return 0;
	}
	
	@Override
	public String getId() {
		// Auto-generated method stub
		return null;
	}
	
	@Override
	public long getLastAccessedTime() {
		// Auto-generated method stub
		return 0;
	}
	
	@Override
	public int getMaxInactiveInterval() {
		// Auto-generated method stub
		return 0;
	}
	
	@Override
	public ServletContext getServletContext() {
		// Auto-generated method stub
		return null;
	}
	
	@Override
	public Object getValue(String arg0) {
		// Auto-generated method stub
		return null;
	}
	
	@Override
	public String[] getValueNames() {
		// Auto-generated method stub
		return null;
	}
	
	@Override
	public void invalidate() {
	// Auto-generated method stub
	
	}
	
	@Override
	public boolean isNew() {
		// Auto-generated method stub
		return false;
	}
	
	@Override
	public void putValue(String arg0, Object arg1) {
	// Auto-generated method stub
	
	}
	
	@Override
	public void removeAttribute(String arg0) {
		// Auto-generated method stub
		values.remove(arg0);
	}
	
	@Override
	public void removeValue(String arg0) {
	// Auto-generated method stub
	
	}
	
	@Override
	public void setAttribute(String arg0, Object arg1) {
		// Auto-generated method stub
		values.put(arg0, arg1);
	}
	
	@Override
	public void setMaxInactiveInterval(int arg0) {
	// Auto-generated method stub
	
	}

	@Override
	public HttpSessionContext getSessionContext() {
		// Auto-generated method stub
		return null;
	}
	
}
