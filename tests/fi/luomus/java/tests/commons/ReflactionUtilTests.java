package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import fi.luomus.commons.utils.ReflectionUtil;

public class ReflactionUtilTests {

	@SuppressWarnings("unused")
	private class SomeClass {

		private String weather;
		private boolean fine;

		public String getWeather() {
			return weather;
		}
		public void setWeather(String Weather) {
			this.weather = Weather;
		}
		public boolean isFine() {
			return fine;
		}
		public void setFine(boolean fine) {
			this.fine = fine;
		}
	}

	@Test
	public void test_getters() throws NoSuchMethodException {
		Method m = ReflectionUtil.getGetter("weather", SomeClass.class);
		assertEquals("getWeather", m.getName());

		m = ReflectionUtil.getGetter("fine", SomeClass.class);
		assertEquals("isFine", m.getName());
	}

	@Test
	public void test_missing_getter() {
		try {
			ReflectionUtil.getGetter("foo", SomeClass.class);
			fail("Should throw exception");
		} catch (NoSuchMethodException e) {
			assertEquals("foo in class fi.luomus.java.tests.commons.ReflactionUtilTests$SomeClass", e.getMessage());
		}
	}

	@Test
	public void test_setters() throws NoSuchMethodException {
		Method m = ReflectionUtil.getSetter("weather", SomeClass.class);
		assertEquals("setWeather", m.getName());
		m = ReflectionUtil.getSetter("fine", SomeClass.class);
		assertEquals("setFine", m.getName());
	}

	@Test
	public void test_missing_setter() {
		try {
			ReflectionUtil.getSetter("foo", SomeClass.class);
			fail("Should throw exception");
		} catch (NoSuchMethodException e) {
			assertEquals("foo in class fi.luomus.java.tests.commons.ReflactionUtilTests$SomeClass", e.getMessage());
		}
	}

	@Test
	public void test_all_getters() {
		Collection<Method> methods = ReflectionUtil.getGetters(SomeClass.class);
		assertEquals(2, methods.size());
		Set<String> names = new HashSet<>();
		for (Method m : methods) {
			names.add(m.getName());
		}
		assertTrue(names.contains("getWeather"));
		assertTrue(names.contains("isFine"));
	}

	@Test
	public void test_all_setters() {
		Collection<Method> methods = ReflectionUtil.getSetters(SomeClass.class);
		assertEquals(2, methods.size());
		Set<String> names = new HashSet<>();
		for (Method m : methods) {
			names.add(m.getName());
		}
		assertTrue(names.contains("setWeather"));
		assertTrue(names.contains("setFine"));
	}

	@Test
	public void is_unmodifiable() {
		try { ReflectionUtil.getGetters(SomeClass.class).clear(); fail("should not be modifiable"); } catch (UnsupportedOperationException e) {}
		try { ReflectionUtil.getSetters(SomeClass.class).clear(); fail("should not be modifiable"); } catch (UnsupportedOperationException e) {}
		try { ReflectionUtil.getSettersMap(SomeClass.class).clear(); fail("should not be modifiable"); } catch (UnsupportedOperationException e) {}
		try { ReflectionUtil.getGettersMap(SomeClass.class).clear(); fail("should not be modifiable"); } catch (UnsupportedOperationException e) {}
	}

}
