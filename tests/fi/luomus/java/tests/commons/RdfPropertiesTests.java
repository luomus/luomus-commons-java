package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import fi.luomus.commons.containers.rdf.Qname;
import fi.luomus.commons.containers.rdf.RdfProperties;
import fi.luomus.commons.containers.rdf.RdfProperty;

import org.junit.Test;

public class RdfPropertiesTests {

	@Test
	public void test_accessing_nonexisting_property() {
		RdfProperties properties = new RdfProperties();
		try {
			properties.getProperty("foo");
			fail("Shoudl throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Property foo is not defined!", e.getMessage());
		}
		
	}

	@Test
	public void test_comparing() {
		RdfProperty p1 = new RdfProperty(new Qname("rdf:_1"));
		RdfProperty p2 = new RdfProperty(new Qname("rdf:_2"));
		assertEquals(-1, p1.compareTo(p2));
		assertEquals(1, p2.compareTo(p1));
		assertEquals(0, p1.compareTo(p1));
		
		RdfProperty pa = new RdfProperty(new Qname("rdf:a"));
		RdfProperty pb = new RdfProperty(new Qname("rdf:b"));
		assertEquals(-1, pa.compareTo(pb));
		pa.setOrder(500);
		pb.setOrder(1);
		assertEquals(1, pa.compareTo(pb));
	}
	
}
