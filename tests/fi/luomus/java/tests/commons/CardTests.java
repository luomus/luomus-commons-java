package fi.luomus.java.tests.commons;

import fi.luomus.commons.containers.Card;

import org.junit.Assert;
import org.junit.Test;

public class CardTests {

	@Test
	public void test() {
		Card card = new Card("1234567");
		Assert.assertEquals("1", card.v(1));
		Assert.assertEquals("12", card.v(1, 2));
		Assert.assertEquals("1234567", card.v(1, 7));
		Assert.assertEquals("1234567", card.v(1, 8));
		Assert.assertEquals("1234567", card.v(1, 200));
		Assert.assertEquals("234567", card.v(2, 200));
		Assert.assertEquals("7", card.v(7));
		Assert.assertEquals("7", card.v(7, 1));
		Assert.assertEquals("7", card.v(7, 2));
		Assert.assertEquals("7", card.v(7, 3));
		Assert.assertEquals("", card.v(8));
		Assert.assertEquals("", card.v(8, 2));
		Assert.assertEquals("", card.v(9, 200));
		Assert.assertEquals("", card.v(200, 300));
	}

	@Test(expected=IllegalArgumentException.class)
	public void test_invalid_args() {
		Card card = new Card("1234567");
		card.v(7, 0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void test_invalid_args_2() {
		Card card = new Card("1234567");
		card.v(7, -1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void test_invalid_args_3() {
		Card card = new Card("1234567");
		card.v(0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void test_invalid_args_4() {
		Card card = new Card("1234567");
		card.v(-1);
	}
	
}
