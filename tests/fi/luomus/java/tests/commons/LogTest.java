package fi.luomus.java.tests.commons;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.utils.DateUtils;
import fi.luomus.commons.utils.LogUtils;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class LogTest {
	
	public static Test suite() {
		return new TestSuite(LogTest.class.getDeclaredClasses());
	}
	
	public static class LogWriting extends TestCase {
		
		private static final String	LOG_TEST_FILE	= "LogTestFile";
		private Config				config;
		
		@Override
		protected void setUp() {
			config = TestConfig.getConfig();
		}
		
		@Override
		protected void tearDown() throws Exception {
			File logfile = logFile();
			if (!logfile.delete()) throw new Exception("Could not delete " + logfile.getAbsolutePath());
		}
		
		private File logFile() {
			return new File(config.baseFolder() + File.separator + LOG_TEST_FILE + DateUtils.getCurrentDateTime("yyyy-MM") + ".txt");
		}
		
		public void test__writing_to_log() throws Exception {
			LogUtils.write("test", config.baseFolder(), LOG_TEST_FILE);
			assertTrue(readContents().contains("] test"));
		}
		
		private String readContents() throws FileNotFoundException, IOException {
			File logfile = logFile();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(logfile)));
			StringBuilder contents = new StringBuilder();
			String row;
			while ((row = reader.readLine()) != null) {
				contents.append(row);
			}
			reader.close();
			return contents.toString();
		}
	}
	
}
