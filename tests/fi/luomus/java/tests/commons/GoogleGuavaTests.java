package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class GoogleGuavaTests {
	
	private static class MyKey {
		private final String key;
		public MyKey(String value) {
			this.key = value;
		}
		@Override
		public String toString() {
			return key;
		}
	}
	
	private static class MyValue {
		private final String value;
		public MyValue(String value) {
			this.value = value;
		}
		@Override
		public String toString() {
			return value;
		}
	}
	
	@Test
	public void usingGoogleCache() {
		LoadingCache<MyKey, MyValue> cache = CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.SECONDS).build(new CacheLoader<MyKey, MyValue>() {

			@Override
			public MyValue load(MyKey key) {
				return new MyValue ("value of " + key);
			}

		});
		MyKey key = new MyKey("foo");
		assertEquals(null, cache.getIfPresent(key));
		assertEquals("value of foo", cache.getUnchecked(key).toString());
	}

}
