package fi.luomus.java.tests.commons;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.config.ConfigReader;

import java.io.FileNotFoundException;

public class TestConfig {
	
	public static Config getConfig()  {
		try {
			return new ConfigReader("C:/apache-tomcat/app-conf/pese_frameworktests.config"); // TODO testit menemään läpi ilman PeSe-systemiä
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
