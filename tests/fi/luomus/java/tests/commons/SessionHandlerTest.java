package fi.luomus.java.tests.commons;

import javax.servlet.http.HttpSession;

import fi.luomus.commons.session.SessionHandlerImple;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class SessionHandlerTest {
	
	public static Test suite() {
		return new TestSuite(SessionHandlerTest.class.getDeclaredClasses());
	}
	
	public static class WhenCreatingAndUsingTheSessionHandler extends TestCase {
		
		private static final String	THIS_SYSTEMID	= "thisSystemid";
		private SessionHandlerImple		session;
		
		@Override
		protected void setUp() {
			HttpSession mockSession = new HttpSessionStub();
			session = new SessionHandlerImple(mockSession, THIS_SYSTEMID);
		}
		
		public void test_that_it_sets_returns_and_removes_values() {
			session.put("testi", "value");
			assertEquals("value", session.get("testi"));
			
			assertEquals(null, session.get("notset"));
			
			session.remove("testi");
			assertEquals(null, session.get("testi"));
		}
		
		public void test_at_creating_session_is_not_authenticated() {
			assertFalse(session.isAuthenticatedFor(THIS_SYSTEMID));
		}
		
		public void test_authenticating() {
			session.authenticateFor(THIS_SYSTEMID);
			assertTrue(session.isAuthenticatedFor(THIS_SYSTEMID));
			assertFalse(session.isAuthenticatedFor("somethingelse"));
		}
		
		public void test_authenticating__and__removing_authentication() {
			session.authenticateFor(THIS_SYSTEMID);
			session.removeAuthentication("somethingelse");
			assertTrue(session.isAuthenticatedFor(THIS_SYSTEMID));
			
			session.removeAuthentication(THIS_SYSTEMID);
			assertFalse(session.isAuthenticatedFor(THIS_SYSTEMID));
			
			session.authenticateFor(THIS_SYSTEMID);
			assertTrue(session.isAuthenticatedFor(THIS_SYSTEMID));
		}
		
	}
	
	/*
	 * public static class Tests extends TestCase {
	 * protected void setUp() throws Exception {
	 * }
	 * protected void tearDown() throws Exception {
	 * }
	 * public void test_() {
	 * }
	 * }
	 */

}
