package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import fi.luomus.commons.containers.rdf.GenericRdfResource;
import fi.luomus.commons.containers.rdf.Predicate;
import fi.luomus.commons.containers.rdf.Subject;

public class RdfResourceTests {

	@Test
	public void whenCreatingResourceToDefaultNamespaceFromQname() {
		GenericRdfResource resource = new Subject("MA.1");
		assertEquals("MA.1", resource.getQname());
		assertEquals("http://tun.fi/MA.1", resource.getURI());
		
	}
	
	@Test
	public void whenCreatingResourceToOtherNamespaceFromQname() {
		GenericRdfResource resource = new Subject("abcd:Person");
		assertEquals("abcd:Person", resource.getQname());
		assertEquals("http://www.tdwg.org/schemas/abcd/2.06#Person", resource.getURI());
	}
	
	@Test
	public void whenCreatingResourceToUnknownNamespaceFromQname() {
		Subject subject = new Subject("foofarbar:Person");
		assertEquals("http://tun.fi/foofarbar:Person", subject.getURI());
		
	}
	
	@Test
	public void whenCreatingResourceWithUri_expects_qname() {
		try {
			new Subject("http://tun.fi/MA.1");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Expecting QNAME, was given <http://tun.fi/MA.1>", e.getMessage());
		}
	}
	
	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void equals() {
		assertEquals(false, new Predicate("abc").equals(new Predicate("123")));
		assertEquals(true, new Predicate("abc").equals(new Predicate("abc")));
		assertEquals(false, new Predicate("abc").equals(new Predicate("Abc")));
		assertEquals(false, new Predicate("dwc:abc").equals(new Predicate("abc")));
		assertEquals(true, new Predicate("dwc:abc").equals(new Predicate("dwc:abc")));
		try {
			new Predicate("abc").equals("123");
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Can't compare java.lang.String to " +Predicate.class.getName(), e.getMessage());
		}
	}
	
}
