package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import fi.luomus.commons.config.Config;
import fi.luomus.commons.pdf.ITextPDFWriter;
import fi.luomus.commons.pdf.PDFWriter;

public class ITextPDFWriterTest {
	
		private final Config		config = TestConfig.getConfig();
		private PDFWriter			pdfWriter;
		private Map<String, String>	data;
		
		@Before
		public void setUp() throws Exception {
			pdfWriter = new ITextPDFWriter(config.templateFolder(), config.pdfFolder());
			data = new HashMap<>();
		}
		
		@After
		public void clean() throws Exception {
			File file = pdfFile("null");
			if (file.exists()) {
				if (!file.delete()) throw new Exception("Could not delete " + file.getAbsolutePath());
			}
			file = new File(config.pdfFolder(), "Kokoelma.zip");
			if (file.exists()) {
				if (!file.delete()) throw new Exception("Could not delete " + file.getAbsolutePath());
			}
			file = new File(config.pdfFolder(), "test.pdf");
			if (file.exists()) {
				if (!file.delete()) throw new Exception("Could not delete " + file.getAbsolutePath());
			}
		}
		
		@Test
		@Ignore
		public void test__writing_a_pdf() {
			data.put("tark_vuosi_1", "2010");
			data.put("tark_vuosi_2", "2010");
			data.put("pesa_pesanimi", "Pesänimi");
			data.put("prev_paras_tarkastus_pesimistulos", "R - Huuhkajalta perittyjä (3)");
			data.put("foo", "bar");
			data.put("pesa_koord_tyyppi_eur", "Yes");
			data.put("pesa_koord_mittaus", "G");
			data.put("tarkastus_uhat", "Monirivinen \n" + "teksti");
			
			assertTrue("Writing pdf should succeed", null != pdfWriter.writePdf("tarkastus", data, "test"));
			File file = pdfFile("test");
			assertTrue("File should exist", file.exists());
			assertTrue("There should be no notice texts", pdfWriter.noticeTexts().isEmpty());
			assertEquals("test.pdf", pdfWriter.producedFiles().get(0));
		}
		
		private File pdfFile(String file) {
			return new File(config.pdfFolder() + File.separator + file + ".pdf");
		}
		
		@Test
		public void test__writing_a_pdf___not_found_template_folder() throws Exception {
			pdfWriter = new ITextPDFWriter("not_found_template_folder", config.pdfFolder());
			assertTrue("Writing pdf should fail", null == pdfWriter.writePdf("tarkastus", data, "test"));
			assertTrue(pdfWriter.noticeTexts().get(0).startsWith("ERROR: test.pdf: java.io.FileNotFoundException: not_found_template_folder" + File.separator + "tarkastus.pdf"));
		}
		
		@Test
		public void test__writing_a_pdf___not_found_template_file() {
			assertTrue("Writing pdf should fail", null == pdfWriter.writePdf("foo", data, "test"));
			assertTrue("But starts with " + pdfWriter.noticeTexts().get(0), pdfWriter.noticeTexts().get(0).toLowerCase().startsWith("ERROR: test.pdf: java.io.FileNotFoundException: C:\\apache-tomcat\\webapps\\PeSe\\FrameworkTests\\template\\foo.pdf".toLowerCase()));
		}
		
		@Test
		@Ignore
		public void test__writing_a_pdf___null_as_filename() {
			assertTrue("Writing pdf should succeed", null != pdfWriter.writePdf("tarkastus", data, null));
			File file = pdfFile("null");
			assertTrue("File should exist", file.exists());
			assertTrue("There should be no notice texts", pdfWriter.noticeTexts().isEmpty());
		}
		
//		public void test__unicode_fonts() throws Exception {
//			pdfWriter = new ITextPDFWriter(config.templateFolder(), config.pdfFolder(), "C:/apache-tomcat/webapps/PeSe/common/fonts");
//			data.put("pesa_pesanimi", "Pesänimi русский алфавит Ą Ć Ę Ł Ń Ó Ś Ź Ż ą ń ł");
//			pdfWriter.writePdf("tarkastus", data, "unicodeTest");
//		}
		
	
}
