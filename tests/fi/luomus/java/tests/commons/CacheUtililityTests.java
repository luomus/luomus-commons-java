package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.commons.utils.Cached;
import fi.luomus.commons.utils.Cached.CacheLoader;
import fi.luomus.commons.utils.SingleObjectCache;

public class CacheUtililityTests {


	private static final String THROW_EXCEPTION_EVERY_SECOND_TIME = "throwExceptionEverySecondTime";
	private static final String HEAVY = "heavy";
	private static final String THROW_ERROR = "throwError";
	private static final String THROW_EXCEPTION = "throwException";
	private static final int CACHE_SIZE = 100;
	private static final String	VALUEFOR	= "valuefor_";
	private Cached<String, String> cache;
	private Map<String, Integer> loadcount;
	private CacheLoader<String, String> loader;

	@Before
	public void setUp() {
		loadcount = new HashMap<>();
		loader = new CacheLoader<String, String>() {
			private int i = 1;
			@Override
			public String load(String key) {
				if (HEAVY.equals(key)) {
					System.out.println("Doing something very heavy...");
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						fail("Got interupted");
					}
					System.out.println(" ... heavy completed!");
				}

				if (loadcount.containsKey(key)) {
					loadcount.put(key, loadcount.get(key) + 1);
				} else {
					loadcount.put(key, 1);
				}

				if (THROW_EXCEPTION.equals(key)) throw new RuntimeException("I was asked to throw this exception");
				if (THROW_ERROR.equals(key)) throw new Error("I was asked to throw this error");
				if (THROW_EXCEPTION_EVERY_SECOND_TIME.equals(key)) {
					if (i++ % 2 == 0) throw new RuntimeException("I was asked to throw this exception");
				}

				return VALUEFOR + key;
			}
		};
	}

	@Test
	public void test___with_long_timer() {
		cache = new Cached<>(loader, 1000000, TimeUnit.SECONDS, CACHE_SIZE);
		assertTrue("Nothing loaded yet", loadcount.isEmpty());
		String s = cache.get("my string");
		assertEquals(VALUEFOR + "my string", s);
		assertEquals(Integer.valueOf(1), loadcount.get("my string")); 
		assertTrue("nothing else loaded", loadcount.size() == 1);

		s = cache.get("my string");
		s = cache.get("my string");
		s = cache.get("my string");
		s = cache.get("my string");
		assertEquals(Integer.valueOf(1), loadcount.get("my string"));

		s = cache.get("some other string");
		assertEquals(VALUEFOR + "some other string", s);
		assertEquals(Integer.valueOf(1), loadcount.get("some other string"));

		cache.getForceReload("my string");
		assertEquals(Integer.valueOf(2), loadcount.get("my string"));
	}

	@Test
	public void test___with_very_short_timer() throws InterruptedException  {
		cache = new Cached<>(loader, 50, TimeUnit.MILLISECONDS, CACHE_SIZE);
		cache.get("my string");
		assertEquals(Integer.valueOf(1), loadcount.get("my string"));

		Thread.sleep(10);
		cache.get("my string");
		cache.get("my string");
		assertEquals(Integer.valueOf(1), loadcount.get("my string"));

		Thread.sleep(55);
		cache.get("my string");
		cache.get("my string");
		assertEquals(Integer.valueOf(2), loadcount.get("my string"));
	}

	@Test
	public void test___single_object_cache() {
		SingleObjectCache.CacheLoader<String> socLoader = new SingleObjectCache.CacheLoader<String>() {
			@Override
			public String load() {
				if (loadcount.containsKey(null)) {
					loadcount.put(null, loadcount.get(null) + 1);
				} else {
					loadcount.put(null, 1);
				}
				return VALUEFOR + null;
			}
		};
		SingleObjectCache<String> cache = new SingleObjectCache<>(socLoader, 1, TimeUnit.HOURS);
		assertTrue("Nothing loaded yet", loadcount.isEmpty());
		String s = cache.get();
		assertEquals(VALUEFOR + null, s);
		assertEquals(Integer.valueOf(1), loadcount.get(null)); 
		assertTrue("nothing else loaded", loadcount.size() == 1);

		s = cache.get();
		s = cache.get();
		assertEquals(Integer.valueOf(1), loadcount.get(null));

		s = cache.get();
		assertEquals(VALUEFOR + null, s);
		assertEquals(Integer.valueOf(1), loadcount.get(null));

		cache.getForceReload();
		assertEquals(Integer.valueOf(2), loadcount.get(null));
	}

	@Test
	public void loaderThrowsException() throws InterruptedException {
		cache = new Cached<>(loader, 50, TimeUnit.MILLISECONDS, CACHE_SIZE);
		try {
			cache.get(THROW_EXCEPTION);
		} catch (Exception e) {
			assertEquals("I was asked to throw this exception", e.getMessage());
		}
		try {
			cache.get(THROW_ERROR);
		} catch (Error e) {
			assertEquals("I was asked to throw this error", e.getMessage());
		}

		String s = cache.get(THROW_EXCEPTION_EVERY_SECOND_TIME);

		assertEquals(VALUEFOR+THROW_EXCEPTION_EVERY_SECOND_TIME, s);
		assertEquals(Integer.valueOf(1), loadcount.get(THROW_EXCEPTION_EVERY_SECOND_TIME)); 

		s = cache.get(THROW_EXCEPTION_EVERY_SECOND_TIME);
		assertEquals(VALUEFOR+THROW_EXCEPTION_EVERY_SECOND_TIME, s);

		Thread.sleep(55);

		s = cache.get(THROW_EXCEPTION_EVERY_SECOND_TIME);
		assertEquals(VALUEFOR+THROW_EXCEPTION_EVERY_SECOND_TIME, s);


	}

	@Test 
	public void isAsync() throws InterruptedException {
		cache = new Cached<>(loader, 200, TimeUnit.MILLISECONDS, CACHE_SIZE, true);

		long start = System.currentTimeMillis();
		cache.get(HEAVY);
		long after = System.currentTimeMillis();
		assertTrue(after-start >= 50L);
		assertTrue(after-start < 100L);
		assertEquals(Integer.valueOf(1), loadcount.get(HEAVY));

		start = System.currentTimeMillis();
		cache.get(HEAVY);
		assertEquals(Integer.valueOf(1), loadcount.get(HEAVY));
		after = System.currentTimeMillis();
		assertTrue(after-start < 10L);
		assertEquals(Integer.valueOf(1), loadcount.get(HEAVY));

		Thread.sleep(250);

		start = System.currentTimeMillis();
		cache.get(HEAVY);
		assertEquals(Integer.valueOf(1), loadcount.get(HEAVY));
		after = System.currentTimeMillis();
		assertTrue(after-start < 10L);
		assertEquals(Integer.valueOf(1), loadcount.get(HEAVY));

		Thread.sleep(100);
		assertEquals(Integer.valueOf(2), loadcount.get(HEAVY));
	}

	@Test 
	public void isNotAsync() throws InterruptedException {
		cache = new Cached<>(loader, 200, TimeUnit.MILLISECONDS, CACHE_SIZE, false);

		long start = System.currentTimeMillis();
		cache.get(HEAVY);
		long after = System.currentTimeMillis();
		assertTrue(after-start >= 50L);
		assertTrue(after-start < 100L);
		assertEquals(Integer.valueOf(1), loadcount.get(HEAVY));

		start = System.currentTimeMillis();
		cache.get(HEAVY);
		assertEquals(Integer.valueOf(1), loadcount.get(HEAVY));
		after = System.currentTimeMillis();
		assertTrue(after-start < 10L);
		assertEquals(Integer.valueOf(1), loadcount.get(HEAVY));

		Thread.sleep(250);

		start = System.currentTimeMillis();
		cache.get(HEAVY);
		assertEquals(Integer.valueOf(2), loadcount.get(HEAVY));
		after = System.currentTimeMillis();
		assertTrue(after-start > 50L);
		assertTrue(after-start < 100L);

		Thread.sleep(100);
		assertEquals(Integer.valueOf(2), loadcount.get(HEAVY));
	}

}
