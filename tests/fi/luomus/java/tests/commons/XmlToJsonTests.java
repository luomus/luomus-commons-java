package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.junit.Test;

import fi.luomus.commons.xml.Document;
import fi.luomus.commons.xml.XMLWriter;

public class XmlToJsonTests {

	@Test
	public void test_it_doesnt_convert_numbers_to_numbers_and_drop_front_zeros() throws JSONException {
		Document doc = new Document("root");
		doc.getRootNode().addChildNode("int").setContents("0100");
		doc.getRootNode().addChildNode("int").setContents("00100");
		doc.getRootNode().addChildNode("int").setContents("000100");
		doc.getRootNode().addChildNode("int").setContents("000000");
		doc.getRootNode().addChildNode("int").setContents("1");
		String xml = new XMLWriter(doc).generateXML();

		JSONObject json = XML.toJSONObject(xml);
		String expected = "{\"root\":{\"int\":[\"0100\",\"00100\",\"000100\",\"000000\",\"1\"]}}";
		assertEquals(expected, json.toString());
	}
	
	@Test
	public void test_attribute_node_same_name() throws JSONException {
		Document doc = new Document("root");
		doc.getRootNode().addChildNode("name").setContents("nodevalue");
		doc.getRootNode().addAttribute("name", "attributevalue");
		String xml = new XMLWriter(doc).generateXML();

		JSONObject json = XML.toJSONObject(xml);
		String expected = "{\"root\":{\"name\":[\"attributevalue\",\"nodevalue\"]}}";
		assertEquals(expected, json.toString());
	}
	
	@Test
	public void test_apos_in_content() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("ke'y", "v'v");
		assertEquals("v'v", json.get("ke'y"));
		String expected = "{\"ke'y\":\"v'v\"}";
		assertEquals(expected, json.toString());
		
		String xml = "<root>val'ue</root>";
		json = XML.toJSONObject(xml);
		assertEquals("val'ue", json.get("root"));
	}

}
