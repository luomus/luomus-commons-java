package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;

import fi.luomus.commons.containers.Ring;

import org.junit.Test;

public class RingFormatValidationTesting {

	@Test
	public void formatting() {
		assertEquals("", new Ring("").getFormatString());
		assertEquals("A", new Ring("S").getFormatString());
		assertEquals("AN", new Ring("S1").getFormatString());
		assertEquals("A_N", new Ring("S 1").getFormatString());
		assertEquals("A__N", new Ring("S  1").getFormatString());
		assertEquals("_", new Ring(" ").getFormatString());
		assertEquals("X", new Ring("_").getFormatString());
		assertEquals("N_A", new Ring("1234 N").getFormatString());
		assertEquals("N_N_A", new Ring("1234 5 N").getFormatString());
		assertEquals("_N_A", new Ring(" 12345 N").getFormatString());
	}

	@Test
	public void validating() {
		// valid: A[A][_]NN* and NN*[_]A
		assertEquals(true, new Ring("").validFormat());
		assertEquals(true, new Ring("AA 123").validFormat());
		assertEquals(true, new Ring("A 111").validFormat());
		assertEquals(true, new Ring("A111").validFormat());
		assertEquals(true, new Ring("111 A").validFormat());
		assertEquals(true, new Ring("AA111").validFormat());
		assertEquals(true, new Ring("111A").validFormat());
		assertEquals(true, new Ring("AA1").validFormat());
		assertEquals(true, new Ring("1A").validFormat());

		assertEquals(false, new Ring(" ").validFormat());
		assertEquals(false, new Ring("A").validFormat());
		assertEquals(false, new Ring("AA").validFormat());
		assertEquals(false, new Ring("1").validFormat());
		assertEquals(false, new Ring("11A11").validFormat());
		assertEquals(false, new Ring("AAA11").validFormat());
		assertEquals(false, new Ring("AA  11").validFormat());
		assertEquals(false, new Ring(" AA 11").validFormat());
		assertEquals(false, new Ring("AA 11 ").validFormat());
		assertEquals(false, new Ring("111 AA").validFormat());
		assertEquals(false, new Ring("111 A ").validFormat());
		assertEquals(false, new Ring("111  A").validFormat());
		assertEquals(false, new Ring(" ").validFormat());
		assertEquals(false, new Ring("*").validFormat());
		assertEquals(false, new Ring("111*").validFormat());
		assertEquals(false, new Ring("*111").validFormat());
		assertEquals(false, new Ring("A* 111").validFormat());
		assertEquals(false, new Ring("111 *").validFormat());

	}
}
