package fi.luomus.java.tests.commons;

import fi.luomus.commons.reporting.ErrorReporterViaEmail.MessageRecord;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class MessageRecordTests {
	
	public static Test suite() {
		return new TestSuite(MessageRecordTests.class.getDeclaredClasses());
	}
	
	public static class WhenUsingMessageRecord extends TestCase {
		
		private MessageRecord messageRecord;
		
		public void test___it_tells_us_to_send_message__if_not_send_before() {
			messageRecord = new MessageRecord();
			assertTrue(messageRecord.shouldSend("foo"));
		}
		
		public void test___it_tells_us_NOT_to_send_message_if_send_before() {
			messageRecord = new MessageRecord();
			assertTrue(messageRecord.shouldSend("foo"));
			messageRecord.record("foo");
			assertFalse(messageRecord.shouldSend("foo"));
		}
		
		public void test___it_truncates_message_to_first_colon() {
			messageRecord = new MessageRecord();
			String message = "Hello I'm a message with colon: everything after it, should not count.";
			assertTrue(messageRecord.shouldSend(message));
			assertTrue(messageRecord.shouldSend(message));
			messageRecord.record(message);
			assertFalse(messageRecord.shouldSend(message));
			message = "Hello I'm a message with colon: but a different content after the colon";
			assertFalse(messageRecord.shouldSend(message));
			assertTrue(messageRecord.shouldSend("Something entirely different: foo"));
		}
		
		public void test__it_stops_sending_if_messages_per_hour_threshold_is_exteeted() {
			messageRecord = new MessageRecord(300, 5); // will send will send max 5 messages in one hour (no matter if they are the same or different messages)
			
			for(int i = 1; i<10; i++) {
				String message = Double.toString(Math.random());
				if (i <= 5) {
					assertTrue("attempt count " + i, messageRecord.shouldSend(message));
					messageRecord.record(message);
				} else {
					assertFalse("attempt count " + i, messageRecord.shouldSend(message));
				}
			}
		}
		
		
	}
	
}
