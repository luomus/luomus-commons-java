package fi.luomus.java.tests.commons;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fi.luomus.commons.containers.Content;
import fi.luomus.commons.containers.rdf.Qname;

public class ContentTests {

	@Test
	public void testAddText() {
		Content content = new Content();
		content.addText(null, new Qname("property"), "value", "fi");
		assertEquals("{default={property={fi=value}}}", content.toString());
		
		content.addText(null, new Qname("property"), "othervalue", "fi");
		assertEquals("{default={property={fi=othervalue}}}", content.toString());
		
		content.addText(null, new Qname("property"), "othervalue", "sv");
		assertEquals("{default={property={fi=othervalue, sv=othervalue}}}", content.toString());
		
		content.addText(null, new Qname("otherproperty"), "value", "en");
		assertEquals("{default={property={fi=othervalue, sv=othervalue}, otherproperty={en=value}}}", content.toString());
		
		content.addText(new Qname("LA.1"), new Qname("property"), "value", "fi");
		assertEquals("{default={property={fi=othervalue, sv=othervalue}, otherproperty={en=value}}, LA.1={property={fi=value}}}", content.toString());
	}

	@Test
	public void testGetText() {
		Content content = initTetsData();
		assertEquals("", content.getDefaultContext().getText("huuhaa", "fi"));
		assertEquals("valuefi", content.getDefaultContext().getText("property", "fi"));
		assertEquals("valuefi", content.getContext(Content.DEFAULT_DESCRIPTION_CONTEXT).getText("property", "fi"));		
		assertEquals("valuefornulllocale", content.getDefaultContext().getText("property", null));
		assertEquals("valuefornulllocale", content.getDefaultContext().getText("property", ""));
		assertEquals("valueforemptystringlocale", content.getDefaultContext().getText("property2", null));
		assertEquals("valueforemptystringlocale", content.getDefaultContext().getText("property2", ""));
		
		assertEquals("othervaluefi", content.getDefaultContext().getText("otherproperty", "fi"));
		assertEquals("othervaluesv", content.getDefaultContext().getText("otherproperty", "sv"));
		assertEquals("othervaluesv", content.getDefaultContext().getText("otherproperty", "SV"));
		
		assertEquals("valueforcontext", content.getContext("LA.1").getText("property", "fi"));
		assertEquals(null, content.getContext("FOO"));
		assertEquals("[property]", content.getContext("LA.1").getProperties().toString());
		assertEquals("[property, property2, otherproperty]", content.getDefaultContext().getProperties().toString());
	}

	private Content initTetsData() {
		Content content = new Content();
		content.addText(null, new Qname("property"), "valuefi", "fi");
		content.addText(null, new Qname("property"), "valuefornulllocale", null);
		content.addText(null, new Qname("property2"), "valueforemptystringlocale", "");
				
		content.addText(null, new Qname("otherproperty"), "othervaluefi", "fi");
		content.addText(null, new Qname("otherproperty"), "othervaluesv", "sv");

		content.addText(new Qname("LA.1"), new Qname("property"), "valueforcontext", "fi");
		return content;
	}

}
