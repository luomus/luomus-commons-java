package fi.nls.common.grt.projections.euref;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.io.DataInputStream;
import java.io.IOException;

import fi.nls.vs.GaussKrueger;

/**
 * Coordinate transformation between ykj and ETRS-TM35FIN.
 * <p>
 * This class provides only static methods, it can't be instantiated. The transformation depends on the binary file "Ykjetrsdata.bin". The file contains the following data twice, first for transformation from ykj to ETRS-TM35FIN and then for the reverse direction:
 * <ul>
 * <li>Two shorts containing the number of rows and columns in the index grid.
 * <li>Two doubles containing the N- and E-coordinates of the southwest corner of the index.
 * <li>Two doubles containing the size of one index slot in N- and E-axis.
 * <li>A short containing the number of points.
 * <li>Two doubles for each point containing N- and E-coordinates of that point.
 * <li>A short containing the number of triangles.
 * <li>Three shorts for each triangle containing indices of the points at the corners of that triangle.
 * <li>Six doubles for each triangle containing the transformation matrix for the corresponding triangle.
 * <li>A variable number of shorts for each index slot where the first short is the number of the following shorts and the rest are indices to triangles. The southwestmost slot appears first, then the slots to the east, then the rows to the north.
 * <li>12 doubles containing the transformation matrix for 3D transformtaion used outside the triangle network.
 * </ul>
 * @author Ville Saari
 * @version 2003-03-27
 */

public final class Ykjetrs {
	private Ykjetrs() {}

	private static class Kolmio {
		double			ax, ay, bx, by, cx, cy;
		AffineTransform	t;

		boolean pistesisalla(double x, double y) {
			double fab, fbc, fca;

			fab = (y - ay) * (bx - ax) - (x - ax) * (by - ay);
			fbc = (y - by) * (cx - bx) - (x - bx) * (cy - by);

			if ((fab < 0.0 && fbc > 0.0) || (fab > 0.0 && fbc < 0.0)) return false;

			fca = (y - cy) * (ax - cx) - (x - cx) * (ay - cy);

			if ((fbc < 0.0 && fca > 0.0) || (fbc > 0.0 && fca < 0.0)) return false;

			return true;
		}

		@SuppressWarnings("unused")
		boolean pistesisalla(Point2D piste) {
			return pistesisalla(piste.getX(), piste.getY());
		}

		Point2D transform(Point2D vanhapiste, Point2D uusipiste) {
			return t.transform(vanhapiste, uusipiste);
		}

		void transform(double[] v, int voff, double[] u, int uoff, int n) {
			t.transform(v, voff, u, uoff, n);
		}
	}

	private static class Muunnos {
		private final static double	Hayford_a	= 6378388.0;
		private final static double	Hayford_f	= 1.0 / 297.0;
		private final static double	GRS80_a		= 6378137.0;
		private final static double	GRS80_f		= 1.0 / 298.257222101;

		private final int			numcellsN, numcellsE;
		private final double		originN, originE, cellsizeN, cellsizeE;
		private final Kolmio		index[][][];
		private boolean				reverse;
		private final double		transf3D[];
		private GaussKrueger		inproj, outproj;
		private double				in_a, in_f, out_a, out_f;

		/*
		 * Luetaan binääridata ja tehdään muita initialisointijippoja.
		 */

		Muunnos(DataInputStream s, Muunnos reverse) throws IOException {
			int n, f, i, sz;
			double t00, t01, t02, t10, t11, t12;
			double[] x, y;
			Kolmio[] kolmio;

			if (reverse == null) {
				this.reverse = false;
				in_a = Hayford_a;
				in_f = Hayford_f;
				out_a = GRS80_a;
				out_f = GRS80_f;
				this.inproj = new GaussKrueger(in_a, in_f, 1.0, 3500000.0, Math.toRadians(27.0));
				this.outproj = new GaussKrueger(out_a, out_f, 0.9996, 500000.0, Math.toRadians(27.0));
			} else {
				this.reverse = true;
				this.inproj = reverse.outproj;
				this.outproj = reverse.inproj;
				this.in_a = reverse.out_a;
				this.in_f = reverse.out_f;
				this.out_a = reverse.in_a;
				this.out_f = reverse.in_f;
			}

			numcellsN = s.readShort();
			numcellsE = s.readShort();
			originN = s.readDouble();
			originE = s.readDouble();
			cellsizeN = s.readDouble();
			cellsizeE = s.readDouble();

			sz = s.readShort();
			x = new double[sz];
			y = new double[sz];

			for (n = 0; n < sz; n++) {
				y[n] = s.readDouble();
				x[n] = s.readDouble();
			}

			sz = s.readShort();
			kolmio = new Kolmio[sz];

			for (n = 0; n < sz; n++) {
				kolmio[n] = new Kolmio();

				f = s.readShort();
				kolmio[n].ax = x[f];
				kolmio[n].ay = y[f];

				f = s.readShort();
				kolmio[n].bx = x[f];
				kolmio[n].by = y[f];

				f = s.readShort();
				kolmio[n].cx = x[f];
				kolmio[n].cy = y[f];
			}

			for (n = 0; n < sz; n++) {
				t00 = s.readDouble();
				t01 = s.readDouble();
				t02 = s.readDouble();
				t10 = s.readDouble();
				t11 = s.readDouble();
				t12 = s.readDouble();
				kolmio[n].t = new AffineTransform(t11, t01, t10, t00, t12, t02);
			}

			index = new Kolmio[numcellsN][][];

			for (f = 0; f < numcellsN; f++) {
				index[f] = new Kolmio[numcellsE][];

				for (n = 0; n < numcellsE; n++) {
					sz = s.readShort();
					index[f][n] = new Kolmio[sz];
					for (i = 0; i < sz; i++)
						index[f][n][i] = kolmio[s.readShort()];
				}
			}

			transf3D = new double[12];

			for (f = 0; f < 12; f++)
				transf3D[f] = s.readDouble();
		}

		/*
		 * 3D-muunnoksen tarvitsemia datatyyppejä
		 */

		private static class PointEllipsoid {
			double	phi, lambda, h;
		}

		private static class Point3D {
			double	X, Y, Z;
		}

		/*
		 * Pallokoordinaattien muunnnos XYZ:ksi
		 */

		private static void ellipsoidtoXYZ(double a, double f, PointEllipsoid in, Point3D out) {
			double sinphi, cosphi, e2, N;

			sinphi = Math.sin(in.phi);
			cosphi = Math.cos(in.phi);
			e2 = 2.0 * f - f * f;
			N = a / Math.sqrt(1.0 - e2 * sinphi * sinphi);
			out.X = (N + in.h) * cosphi * Math.cos(in.lambda);
			out.Y = (N + in.h) * cosphi * Math.sin(in.lambda);
			out.Z = (N * (1.0 - e2) + in.h) * sinphi;
		}

		/*
		 * Sama toisin päin
		 */

		private static void XYZtoEllipsoid(double a, double f, Point3D in, PointEllipsoid out) {
			double e2, r, sinphi, N;
			int n;
			boolean t;

			e2 = 2.0 * f - f * f;
			r = Math.sqrt(in.X * in.X + in.Y * in.Y);

			out.lambda = Math.atan2(in.Y, in.X);
			out.phi = Math.atan(in.Z / ((1.0 - e2) * r));
			t = Math.abs(out.phi) < Math.PI / 4.0;

			for (n = 0; n < 6; n++) {
				sinphi = Math.sin(out.phi);
				N = a / Math.sqrt(1.0 - e2 * sinphi * sinphi);
				out.h = t ? r / Math.cos(out.phi) - N : in.Z / sinphi - (1.0 - e2) * N;
				out.phi = Math.atan(in.Z / (r * (1.0 - (e2 * N / (N + out.h)))));
			}
		}

		/*
		 * 3D muunnos käytettäväksi kolmioverkon ulkopuolella
		 */

		private Point2D transform3D(Point2D point) {
			@SuppressWarnings("unused")
			double X, Y, Z, x, y, z;
			Point2D.Double t = new Point2D.Double();
			Point3D p = new Point3D();
			PointEllipsoid e = new PointEllipsoid();

			y = point.getY();
			if (reverse)
				y -= 500000.0;
			else
				y -= 3500000.0;
			if (Math.abs(y) > 14500000.0) throw new IllegalArgumentException("point outside supported area");

			inproj.inverseTransform(point, t);
			e.phi = t.x;
			e.lambda = t.y;
			e.h = 0.0;
			ellipsoidtoXYZ(in_a, in_f, e, p);

			x = transf3D[0] * p.X + transf3D[1] * p.Y + transf3D[2] * p.Z + transf3D[3];
			y = transf3D[4] * p.X + transf3D[5] * p.Y + transf3D[6] * p.Z + transf3D[7];
			p.Z = transf3D[8] * p.X + transf3D[9] * p.Y + transf3D[10] * p.Z + transf3D[11];
			p.X = x;
			p.Y = y;

			if (!reverse) /* säädetään korkeutta ja lasketaan uudestaan */
			{
				XYZtoEllipsoid(out_a, out_f, p, e);
				e.phi = t.x;
				e.lambda = t.y;
				e.h = -e.h;
				ellipsoidtoXYZ(in_a, in_f, e, p);

				x = transf3D[0] * p.X + transf3D[1] * p.Y + transf3D[2] * p.Z + transf3D[3];
				y = transf3D[4] * p.X + transf3D[5] * p.Y + transf3D[6] * p.Z + transf3D[7];
				p.Z = transf3D[8] * p.X + transf3D[9] * p.Y + transf3D[10] * p.Z + transf3D[11];
				p.X = x;
				p.Y = y;
			}

			XYZtoEllipsoid(out_a, out_f, p, e);
			t.x = e.phi;
			t.y = e.lambda;
			outproj.transform(t, point);

			return point;
		}

		Point2D transform(Point2D vanhapiste, Point2D uusipiste) {
			int n, f, i;
			double x, y;

			n = (int) Math.floor(((x = vanhapiste.getX()) - originE) / cellsizeE);
			f = (int) Math.floor(((y = vanhapiste.getY()) - originN) / cellsizeN);

			if (f >= 0 && n >= 0 && f < numcellsN && n < numcellsE) for (i = 0; i < index[f][n].length; i++)
				if (index[f][n][i].pistesisalla(x, y)) return index[f][n][i].transform(vanhapiste, uusipiste);

			if (uusipiste == null) uusipiste = new Point2D.Double();
			uusipiste.setLocation(y, x);
			transform3D(uusipiste);
			uusipiste.setLocation(uusipiste.getY(), uusipiste.getX());

			return uusipiste;
		}

		void transform(double[] v, int voff, double[] u, int uoff, int num) {
			int n, f, i;
			int p, e, s;
			Point2D.Double P = null;

			/*
			 * valitaan iterointisuunta niin, ettei pisteitä ylikirjoiteta
			 * ennen kuin ne on käsitelty
			 */
			if ((uoff -= voff) > 0) {
				p = voff + num * 2 - 2;
				e = voff - 2;
				s = -2;
			} else {
				p = voff;
				e = voff + num * 2;
				s = 2;
			}

			for (; p != e; p += s) {
				n = (int) Math.floor((v[p] - originE) / cellsizeE);
				f = (int) Math.floor((v[p + 1] - originN) / cellsizeN);

				if (f >= 0 && n >= 0 && f < numcellsN && n < numcellsE) {
					for (i = 0; i < index[f][n].length; i++)
						if (index[f][n][i].pistesisalla(v[p], v[p + 1])) {
							index[f][n][i].transform(v, p, u, p + uoff, 1);
							break;
						}

					if (i < index[f][n].length) continue;
				}

				if (P == null) P = new Point2D.Double();
				P.x = v[p + 1];
				P.y = v[p];

				transform3D(P);

				u[p + uoff] = P.y;
				u[p + uoff + 1] = P.x;
			}
		}
	}

	private static Muunnos	kkjetrs	= null, etrskkj = null;

	static {
		try {
			DataInputStream s = new DataInputStream(Ykjetrs.class.getResourceAsStream("Ykjetrsdata.bin"));

			kkjetrs = new Muunnos(s, null);
			etrskkj = new Muunnos(s, kkjetrs);
		} catch (IOException e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	/**
	 * Transforms the source point from ykj to ETRS-TM35FIN and stores the
	 * result in destination. If destination is null, a new Point2D
	 * object is allocated for the result. In either case the result point
	 * is returned for convenience. The source and destination may refer
	 * to the same object.
	 * @param source
	 *            a Point2D object containing the ykj point to transform
	 * @param destination
	 *            a Point2D object to receive the transformed ETRS-TM35FIN point
	 * @throws IllegalArgumentException
	 *             the source point is outside supported area
	 */

	public static Point2D transform(Point2D source, Point2D destination) {
		return kkjetrs.transform(source, destination);
	}

	/**
	 * Transforms an array of double precision coordinates from ykj to
	 * ETRS-TM35FIN. The two coordinate array sections can be exactly the
	 * same or can be overlapping sections of the same array without affecting
	 * the validity of the results. This method ensures that no source
	 * coordinates are overwritten by a previous operation before they can
	 * be transformed. The coordinates are stored in the arrays starting at
	 * the indicated offset in the order [E0, N0, E1, N1, ..., En, Nn].
	 * <p>
	 * If a point is outside the supported area, then the transformed coordinates for that point will be NANs.
	 * @param source
	 * @param source srcPts
	 *            the array containing the source point coordinates. Each point is
	 *            stored as a pair of E, N coordinates.
	 * @param dstPts
	 *            the array into which the transformed point coordinates are returned.
	 *            Each point is stored as a pair of E, N coordinates.
	 * @param srcOff
	 *            the offset to the first point to be transformed in the source array
	 * @param dstOff
	 *            the offset to the location of the first transformed point that is
	 *            stored in the destination array
	 * @param numPts
	 *            the number of points to be transformed
	 */

	public static void transform(double[] srcPts, int srcOff, double[] dstPts, int dstOff, int numPts) {
		kkjetrs.transform(srcPts, srcOff, dstPts, dstOff, numPts);
	}

	/**
	 * Transforms the source point from ETRS-TM35FIN to ykj and stores the
	 * result in destination. If destination is null, a new Point2D
	 * object is allocated for the result. In either case the result point
	 * is returned for convenience. The source and destination may refer
	 * to the same object.
	 * @param source
	 *            a Point2D object containing the ETRS-TM35FIN point to transform
	 * @param destination
	 *            a Point2D object to receive the transformed ykj point
	 * @throws IllegalArgumentException
	 *             the source point is outside supported area
	 */

	public static Point2D inverseTransform(Point2D source, Point2D destination) {
		return etrskkj.transform(source, destination);
	}

	/**
	 * Transforms an array of double precision coordinates from ETRS-TM35FIN
	 * to ykj. The two coordinate array sections can be exactly the same
	 * or can be overlapping sections of the same array without affecting
	 * the validity of the results. This method ensures that no source
	 * coordinates are overwritten by a previous operation before they can
	 * be transformed. The coordinates are stored in the arrays starting at
	 * the indicated offset in the order [E0, N0, E1, N1, ..., En, Nn].
	 * <p>
	 * If a point is outside the supported area, then the transformed coordinates for that point will be NANs.
	 * @param source
	 * @param source srcPts
	 *            the array containing the source point coordinates. Each point is
	 *            stored as a pair of E, N coordinates.
	 * @param dstPts
	 *            the array into which the transformed point coordinates are returned.
	 *            Each point is stored as a pair of E, N coordinates.
	 * @param srcOff
	 *            the offset to the first point to be transformed in the source array
	 * @param dstOff
	 *            the offset to the location of the first transformed point that is
	 *            stored in the destination array
	 * @param numPts
	 *            the number of points to be transformed
	 */

	public static void inverseTransform(double[] srcPts, int srcOff, double[] dstPts, int dstOff, int numPts) {
		etrskkj.transform(srcPts, srcOff, dstPts, dstOff, numPts);
	}
}
