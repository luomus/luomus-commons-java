package fi.nls.vs;

import java.awt.geom.Point2D;

/**
 * Coordinate transformation between latitude and longitude on a reference
 * ellipsoid and northing and easting on a plane using Gauss-Kr�ger
 * projection.
 * @author Ville Saari
 * @version 2003-03-04
 */

public final class GaussKrueger {
	@SuppressWarnings("unused")
	private final double	E0, lambda0, a, f, b, e, A1;
	private final double	h1p, h2p, h3p, h4p, h1, h2, h3, h4;
	
	private static double asinh(double x) {
		return Math.log(x + Math.sqrt(x * x + 1.0));
	}
	
	private static double atanh(double x) {
		return 0.5 * Math.log((1.0 + x) / (1.0 - x));
	}
	
	private static double sinh(double x) {
		return 0.5 * (Math.exp(x) - Math.exp(-x));
	}
	
	private static double cosh(double x) {
		return 0.5 * (Math.exp(x) + Math.exp(-x));
	}
	
	/**
	 * Constructs a projection transformation object for Gauss-Kr�ger
	 * projection using specified parameters.
	 * @param a semimajor axis of the reference ellipsoid
	 * @param f flattening of the reference ellipsoid
	 * @param k0 scale factor at prime meridian
	 * @param E0 false easting
	 * @param lambda0 prime meridian
	 */
	
	public GaussKrueger(double a, double f, double k0, double E0, double lambda0) {
		double Dn, rn2, rn3, rn4;
		
		this.a = a;
		this.f = f;
		this.E0 = E0;
		this.lambda0 = lambda0;
		
		b = a * (1.0 - f);
		e = Math.sqrt(a * a - b * b) / a;
		
		Dn = (a - b) / (a + b);
		rn2 = Dn * Dn;
		rn3 = Dn * rn2;
		rn4 = rn2 * rn2;
		
		A1 = k0 * (a / (1.0 + Dn)) * (1.0 + rn2 / 4.0 + rn4 / 64.0);
		h1p = Dn / 2.0 - 2.0 * rn2 / 3.0 + 5.0 * rn3 / 16.0 + 41.0 * rn4 / 180.0;
		h2p = 13.0 * rn2 / 48.0 - 3.0 * rn3 / 5.0 + 557.0 * rn4 / 1440.0;
		h3p = 61.0 * rn3 / 240.0 - 103.0 * rn4 / 140.0;
		h4p = 49561.0 * rn4 / 161280.0;
		h1 = Dn / 2.0 - 2.0 * rn2 / 3.0 + 37.0 * rn3 / 96.0 - rn4 / 360.0;
		h2 = rn2 / 48.0 + rn3 / 15.0 - 437.0 * rn4 / 1440.0;
		h3 = 17.0 * rn3 / 480.0 - 37.0 * rn4 / 840.0;
		h4 = 4397.0 * rn4 / 161280.0;
	}
	
	/**
	 * Transforms the source point from latitude and longitude on the
	 * reference ellipsoid to northing and easting on the projection plane.
	 * The result is stored in destination point or if it is null, a new
	 * Point2D object is allocated for the result. In either case the result
	 * point is returned for convenience. The source and destination may refer
	 * to the same object.
	 * @param source
	 *            a Point2D object containing the latitude and longitude to transform
	 * @param destination
	 *            a Point2D object to receive the transformed northing and easting
	 */
	
	public Point2D transform(Point2D source, Point2D destination) {
		double phi, lambda, t, ksi1, eta1;
		
		phi = source.getX();
		lambda = source.getY() - lambda0;
		
		t = sinh(asinh(Math.tan(phi)) - e * atanh(e * Math.sin(phi)));
		ksi1 = Math.atan(t / Math.cos(lambda));
		eta1 = atanh(Math.sin(lambda) / Math.sqrt(1.0 + t * t));
		
		if (destination == null) destination = new Point2D.Double();
		
		destination.setLocation(A1 *
								(ksi1 + h1p * Math.sin(2.0 * ksi1) * cosh(2.0 * eta1) + h2p * Math.sin(4.0 * ksi1) * cosh(4.0 * eta1) + h3p * Math.sin(6.0 * ksi1) * cosh(6.0 * eta1) + h4p *
																																														Math.sin(8.0 * ksi1) *
																																														cosh(8.0 * eta1)), A1 *
																																																			(eta1 +
																																																				h1p *
																																																				Math.cos(2.0 * ksi1) *
																																																				sinh(2.0 * eta1) +
																																																				h2p *
																																																				Math.cos(4.0 * ksi1) *
																																																				sinh(4.0 * eta1) +
																																																				h3p *
																																																				Math.cos(6.0 * ksi1) *
																																																				sinh(6.0 * eta1) + h4p *
																																																									Math.cos(8.0 * ksi1) *
																																																									sinh(8.0 * eta1)) +
																																																			E0);
		
		return destination;
	}
	
	/**
	 * Transforms the source point from northing and easting on the
	 * projection plane to latitude and longitude on the reference ellipsoid.
	 * The result is stored in destination point or if it is null, a new
	 * Point2D object is allocated for the result. In either case the result
	 * point is returned for convenience. The source and destination may refer
	 * to the same object.
	 * @param source
	 *            a Point2D object containing the norting and easting to transform
	 * @param destination
	 *            a Point2D object to receive the transformed latitude and longitude
	 */
	
	public Point2D inverseTransform(Point2D source, Point2D destination) {
		double ksi, eta, sksi, seta, q, t, lambda;
		int i;
		
		ksi = source.getX() / A1;
		eta = (source.getY() - E0) / A1;
		
		sksi = ksi - h1 * Math.sin(2.0 * ksi) * cosh(2.0 * eta) - h2 * Math.sin(4.0 * ksi) * cosh(4.0 * eta) - h3 * Math.sin(6.0 * ksi) * cosh(6.0 * eta) - h4 * Math.sin(8.0 * ksi) *
				cosh(8.0 * eta);
		
		seta = eta - h1 * Math.cos(2.0 * ksi) * sinh(2.0 * eta) - h2 * Math.cos(4.0 * ksi) * sinh(4.0 * eta) - h3 * Math.cos(6.0 * ksi) * sinh(6.0 * eta) - h4 * Math.cos(8.0 * ksi) *
				sinh(8.0 * eta);
		
		lambda = Math.atan(sinh(seta) / Math.cos(sksi));
		q = asinh(Math.cos(lambda) * Math.tan(sksi));
		t = Math.sin(Math.atan(sinh(1.0040685 * q)));
		
		i = 6;
		do {
			t = sinh(q + e * atanh(e * t));
			t /= Math.sqrt(1.0 + t * t);
		} while ((--i) > 0);
		
		if (destination == null) destination = new Point2D.Double();
		
		destination.setLocation(Math.asin(t), lambda + lambda0);
		return destination;
	}
}
