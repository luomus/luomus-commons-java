package fi.nls.vs;

import java.awt.geom.*;
import java.io.*;
import fi.nls.common.grt.projections.euref.Ykjetrs;

public final class Ykjetrsmain {
	private static String[] splitString(String s) {
		String[] t, l = new String[4];
		int i, j, k;
		
		s = s.replace('\t', ' ');
		
		for (i = k = 0;; i = j, k++) {
			while (i < s.length() && s.charAt(i) == ' ')
				i++;
			if (i == s.length()) break;
			
			if (k == 3 || (j = s.indexOf(' ', i)) == -1) j = s.length();
			l[k] = s.substring(i, j);
		}
		
		if (k == 4) return l;
		
		for (t = new String[k], i = 0; i < k; i++)
			t[i] = l[i];
		
		return t;
	}
	
	private static void muunnaTiedosto(BufferedReader r) throws IOException {
		Point2D.Double p = new Point2D.Double();
		String l;
		String[] t;
		
		while ((l = r.readLine()) != null) {
			t = splitString(l);
			
			try {
				if (t.length == 2) {
					p.y = Double.parseDouble(t[0]);
					p.x = Double.parseDouble(t[1]);
				} else {
					p.y = Double.parseDouble(t[1]);
					p.x = Double.parseDouble(t[2]);
				}
				
				if (p.x >= 2000000.0)
					Ykjetrs.transform(p, p);
				else
					Ykjetrs.inverseTransform(p, p);
				
				if (t.length == 2)
					System.out.println(p.y + "\t" + p.x);
				else if (t.length == 3)
					System.out.println(t[0] + "\t" + p.y + "\t" + p.x);
				else
					System.out.println(t[0] + "\t" + p.y + "\t" + p.x + "\t" + t[3]);
			} catch (ArrayIndexOutOfBoundsException e) {
			} catch (NumberFormatException e) {
			} catch (IllegalArgumentException e) {
			}
		}
	}
	
	public static void mainDISABLED(String[] args) throws IOException {
		Point2D.Double piste;
		
		try {
			piste = new Point2D.Double(Double.parseDouble(args[1]), Double.parseDouble(args[0]));
			
			if (piste.x >= 2000000.0)
				Ykjetrs.transform(piste, piste);
			else
				Ykjetrs.inverseTransform(piste, piste);
			
			System.out.println(piste.y + " " + piste.x);
			System.exit(0);
		} catch (NumberFormatException e) {
		} catch (ArrayIndexOutOfBoundsException e) {
		} catch (IllegalArgumentException e) {
			System.out.println("piste ulkona tuetulta alueelta");
			System.exit(1);
		}
		
		if (args.length == 0)
			muunnaTiedosto(new BufferedReader(new InputStreamReader(System.in)));
		else
			for (int i = 0; i < args.length; i++)
				muunnaTiedosto(new BufferedReader(new FileReader(args[i])));
	}
}
